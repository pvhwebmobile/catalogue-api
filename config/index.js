const nconf = require('nconf')
const path = require('path')

const ENV_CONFIG = process.env.NODE_ENV
  ? 'config.' + process.env.NODE_ENV + '.json'
  : 'config.development.json'

nconf.argv()
  .env()
  .file('general', { file: path.join(__dirname, 'config.json') })
  .file('env', { file: path.join(__dirname, ENV_CONFIG) })

module.exports = nconf.get()
