# Setting up AWS

This document describes setting up the Catalogue API on AWS. For this example the dev version is used. See also migrate-to-aws.md

## Setup a AWS CodeStar project

Go to Services/CodeStar

Choose a Node.js with Elastic Beanstalk project
- Name: catalogue-dev
- ProjectId: catalogue-dev
- Repository name: catalogue-dev

**Note 1**: Setting up a project with an existing repository fails; we will setup the correct repo later.

Select a key-value pair and create the project. Wait untill that is finished.

## Pipeline

Now goto the newly created Pipeline (there is a button in the bar on the left of your dashboard sceen)

Select Edit. In the form change the repository to 'catalogue' and the branch to 'develop'. Save.

Goto CodeCommit (from the services menu). Delete the newly created catalogue-dev repository.

If you want the code to be autodeployed continue to **Environment**

If not, you need to take some extra steps:
- goto the CodePipeline and select 'edit'
- select the 'source' tab to edit
- find the event rule used (under Change detection options)
- goto CloudWatch (from the services menu)
- find the rule and disable it

This works perfectly, but only if you have a 1-to-1 relation between branch and pipeline. For dev and tst, both using the develop branch, I did this:
- find the rule
- select 'edit'
- find the pipeline you do not want to be updated and delete it from the list of targets

This also works, but I expect that it may/will be overwritten when you make changes to the pipeline.

**Note**: it is possible to use BitBucket if we would want that: <https://aws.amazon.com/about-aws/whats-new/2017/08/aws-codebuild-now-supports-atlassian-bitbucket-cloud-as-a-source-type/>

## Environment

Go the environment page by using the 'deploy' button at the left of the dashboard.

#### Server setup

The catalogue-api requires two environment vars to run:
```
NODE_ENV=development
NODE_PATH=.
```
These can be setup in Services/CodeStar/Deploy/catalogue-devapp - Configuration - Software configuration

The catalogue-api requires Node v8; this is setup at the same page.

You now have to push release changes in the CodePipeline (that is a button on that page). After this the whole setup is restarted.

#### Per server configuration

To setup the Elastic Beanstalk environment you can create files in the repository, in a dir called .ebextensions. More info can be found at <http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/customize-containers-ec2.html>

To add the WCS ip numbers to /etc/hosts there is now a file .ebextensions/hostsfile.config that contains this YAML code:
```
commands:
  add_wcs_to_etc_hosts_0: 
    command: echo 127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain4\n > /etc/hosts
  add_wcs_to_etc_hosts_1: 
    command: echo ::1 localhost6 localhost6.localdomain6\n >> /etc/hosts
  add_wcs_to_etc_hosts_2: 
    command: echo 52.18.130.121 nl.systestp.tommy.com\n >> /etc/hosts
    ignoreErrors: true
  add_wcs_to_etc_hosts_3: 
    command: echo 52.18.130.121 uk.systestp.tommy.com\n >> /etc/hosts
    ignoreErrors: true
  add_wcs_to_etc_hosts_4: 
    command: echo 52.18.130.121 nl.b2ceuup.tommy.com\n >> /etc/hosts
    ignoreErrors: true
  add_wcs_to_etc_hosts_5: 
    command: echo 52.18.130.121 uk.b2ceuup.tommy.com\n >> /etc/hosts
    ignoreErrors: true
```

## Setup fixed ip

The beanstalk instances are running in a Virtual Private Clud. To setup a fixed IP that can be used for whitelisting a NAT router needs to be setup. There are many descriptions of this process available; a clear and simple explanation can be found at <https://medium.com/@obezuk/how-to-use-elastic-beanstalk-with-an-ip-whitelisted-api-69a6f8b5f844>.

The process comes down to:
- Find or create a new subnet and give it a suitable CIDR block
- Create a new NAT Gateway in the new subnet. Amazon will create an Elastic IP.
- Create a Route Table for the new NAT Gateway. This should be configured to send internet traffic out via the Internet Gateway.
- Modify the NAT subnet to use your new route table.
- Modify the Main Route Table to direct traffic to your target server via the NAT Gateway and direct other internet traffic via the Internet Gateway.

This has been setup so that all traffic to 52.18.130.121 will be routed through the NAT. It will then use 35.163.204.117 as outgoing IP address.

## Storage

Storage at AWS is S3. Connecting to S3 from NodeJS is done with the aws-sdk package provided by Amazon. See <http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/_index.html>.

Create a S3 bucked named **catalogue-dev-upload**

Goto IAM (from the services menu).

Create a policy. The policy should enable S3 access on that new bucket only.

Create a user for accessing s3 from the catalogue app. Name the user **catalogue-api-dev**. Remove all default added groups from the user. Create a new group 'ManageS3catalogue-dev' that uses the 

The application config is extended with an s3 block. This is in the environment specific config files:
```
  "AWS": {
    "S3": {
      "accessKeyId": "NEW_USERS_ACCESS_KEY", 
      "secretAccessKey": "NEW_USERS_SECRET_ACCESS_KEY",
      "region": "eu-west-2",
      "bucketName": "catalogue-dev-upload"
    }
  }
```

## TODO

- sessions & multiple instances: how will that work

- It is possible to integrate Jira into CodeStar. This should be done by someone with Jira administrator permissions.
