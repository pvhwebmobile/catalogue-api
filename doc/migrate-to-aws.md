# Migrating to AWS

This document describes the migration process from IBM Bluemix to AWS based on an example project: the catalogue-api.

## Setup a AWS CodeStar project

Go to Services/CodeStar

Choose a Node.js with Elastic Beanstalk project
- Name: catalogue-api-dev
- ProjectId: cat-api-dev (can only be 15 chars)
- Repository name: catalogue-api-dev

**Note 1**: I use my own key-value pair (niels-laptop); this should be replaced later.

**Note 2**: I tried setting up a project using an existing repo but that failed

I created a new branch migrate-to-aws.

To bring the code to the AWS repo I added the CodeCommit repo as a new origin with
```
git remote add origin-aws ssh://git-codecommit.us-west-2.amazonaws.com/v1/repos/catalogue-api-dev
```
and pused the new branch with
```
git push -u origin-aws migrate-to-aws
```

In cat-api-dev-Pipeline I could now add this new branch as the one to be used.

If you want to use another repo than the default one you must changes the permissions of the pipeline role. This is done in IAM/Roles. Search for the role that is aasociated with the CodeStar project. It will have a name like CodeStarWorker-catalogue-dev-CodePipeline. Edit the policy (a JSON object) to change the name of the repository.

It also was changed to be the default branch in Services/CodeCommit - catalogue-api-dev - Settings. This should be temporary.

git push to the migrate-to-aws branch will now automatically be deployed.

**Note**: it is possible to use BitBucket if we would want that: <https://aws.amazon.com/about-aws/whats-new/2017/08/aws-codebuild-now-supports-atlassian-bitbucket-cloud-as-a-source-type/>

## Environment

The catalog-api requires two environment vars to run:
```
NODE_ENV=staging
NODE_PATH=.
```
These can be setup in Services/CodeStar/Deploy/cat-api-devapp - Configuration - Software configuration

The catalogue-api requires Node v8; this is setup at the same page.

You now have to push release changes in the CodePipeline (that is a button on that page). After this the whole setup is restarted.

To setup the Elastic Beanstalk environment you can create files in the repository, in a dir called .ebextensions. More info can be found at <http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/customize-containers-ec2.html>

To add the WCS ip numbers to /etc/hosts there is now a file .ebextensions/hostsfile.config that contains this YAML code:
```
commands:
  add_wcs_to_etc_hosts_0: 
    command: echo 127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain4\n > /etc/hosts
  add_wcs_to_etc_hosts_1: 
    command: echo ::1 localhost6 localhost6.localdomain6\n >> /etc/hosts
  add_wcs_to_etc_hosts_2: 
    command: echo 52.18.130.121 nl.systestp.tommy.com\n >> /etc/hosts
    ignoreErrors: true
  add_wcs_to_etc_hosts_3: 
    command: echo 52.18.130.121 uk.systestp.tommy.com\n >> /etc/hosts
    ignoreErrors: true
  add_wcs_to_etc_hosts_4: 
    command: echo 52.18.130.121 nl.b2ceuup.tommy.com\n >> /etc/hosts
    ignoreErrors: true
  add_wcs_to_etc_hosts_5: 
    command: echo 52.18.130.121 uk.b2ceuup.tommy.com\n >> /etc/hosts
    ignoreErrors: true
```

## Setup fixed ip

The beanstalk instances are running in a Virtual Private Clud. To setup a fixed IP that can be used for whitelisting a NAT router needs to be setup. There are many descriptions of this process available; a clear and simple explanation can be found at <https://medium.com/@obezuk/how-to-use-elastic-beanstalk-with-an-ip-whitelisted-api-69a6f8b5f844>.

The process comes down to:
- Find or create a new subnet and give it a suitable CIDR block
- Create a new NAT Gateway in the new subnet. Amazon will create an Elastic IP.
- Create a Route Table for the new NAT Gateway. This should be configured to send internet traffic out via the Internet Gateway.
- Modify the NAT subnet to use your new route table.
- Modify the Main Route Table to direct traffic to your target server via the NAT Gateway and direct other internet traffic via the Internet Gateway.

This has been setup so that all traffic to 52.18.130.121 will be routed through the NAT. It will then use 35.163.204.117 as outgoing IP address.

## Removing VCAP settings

VCAP settings are IBM Bluemix specific and should be removed. For the catalog-api there are two: storage and statica.

#### VCAP storage
Used for images. See below.

#### VCAP statica
No longer relevant: the aws instance should have a fixed IP (see above). CustomProxy is then no longer needed and therefore deleted.

## Storage

Storage at AWS is S3. Connecting to S3 from NodeJS is done with the aws-sdk package provided by Amazon. See <http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/_index.html>.

A special user for accessing s3 from the catalogue app was created. The name of this user is **catalogue-api-dev**.

The application config is extended with an s3 block. This is in the environment specific config files:
```
  "AWS": {
    "S3": {
      "accessKeyId": "SPECIAL_USERS_ACCESS_KEY", 
      "secretAccessKey": "SPECIAL_USERS_SECRET_ACCESS_KEY",
      "region": "us-west-2",
      "bucketName": "catalogue-app-dev-upload"
    }
  }
```

The image controller is updated to use S3 instead of Bluemix as the storage. Filenames have been migrated, so the code should not need any changes.

To migrate from Bluemix a script was added. It downloads all data from the Bluemix storage and then uploads it to S3. This can be found in the /migration directory.

## TODO

- sessions & multiple instances: how will that work

- multiple ssh keys seem not possible. niels-laptop should be replaced by something else.

- for cli access i use **eb**, which is an aws package. Write something about that.

- The catalogue app has no database, but a migration should be setup for other apps.

- Research the use of multiple storages in different regions and the synchronisation thatre of.

- It is possible to integrate Jira into CodeStar. This should be done by someone with Jira administrator permissions.
