const Koa = require('koa')
const body = require('koa-body')
const koaLogger = require('koa-logger-winston')

const locale = require('koa-locale')

const i18n = require('koa-i18n')

const config = require('config')

const db = require('./libs/db')
const {
  languageMiddleware,
} = require('./libs/lang')
const logger = require('./libs/logger')
const {
  clientVersionRequestMiddleware,
  serverVersionRequestMiddleware,
} = require('./libs/versioning')
const {
  userAgentMiddleware,
} = require('./libs/userAgent')

let app = module.exports = new Koa()
let api = require('./api')
let port = process.env.PORT || config.PORT

// #region versioning middleware
app.use(serverVersionRequestMiddleware)
app.use(clientVersionRequestMiddleware)
// #endregion

// #region user-agent middleware
app.use(userAgentMiddleware)
// #endregion

// #region locales
locale(app, 'lang')

app.use(i18n(app, {
  directory: './config/locales',
  extension: '.json',
  locales: [
    'en_GB',
    'de_DE',
    'nl_NL',
    'it_IT',
    'ru_RU',
    'fr_FR',
  ],
  modes: [
    'query',
    'url',
  ],
  devMode: false,
}))

app.use(languageMiddleware)
// #endregion locales

// logger
if (!process.env.JEST) app.use(koaLogger(logger))

// multipart parser
app.use(body({ multipart: true }))

app.use(async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    ctx.status = err.status || 500
    ctx.type = 'JSON'
    ctx.body = { success: false, error: err.message, code: err.code }
  }
})

app.use(api.routes())

app.on('error', err =>
  logger.error('server error', err)
);

(async () => {
  try {
    const info = await db.connect()
    logger.log(`Connected to ${info.host}:${info.port}/${info.name}`)
  } catch (error) {
    logger.error('Unable to connect to database: ' + error.message)
  }

  try {
    if (!module.parent) {
      app.listen(port)
      logger.log('listening on port: ' + port)
    }
  } catch (error) {
    logger.error(error)
  }
})()
