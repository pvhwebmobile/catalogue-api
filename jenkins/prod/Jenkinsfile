pipeline {
    agent any

    tools {
        nodejs 'node'
    }

    stages {
        stage('Checkout & Build') {
            steps {
                sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Pipeline initiated.\"}' https://hooks.slack.com/services/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"
                checkout([
                    $class: 'GitSCM',
                    branches: [[name: 'origin/master']],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    submoduleCfg: [],
                    userRemoteConfigs: [[
                        credentialsId: 'eis-web-01',
                        name: 'origin',
                        url: 'git@bitbucket.org:pvhwebmobile/catalogue-api.git'
                    ]]
                ])
                sh "npm run build"
            }
            post {
                unsuccessful {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Checkout & Build stage was unsuccessful.\"}' https://hooks.slack.com/services/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"
                }
            }
        }
        stage('Test') {
            steps {
                sh "npm run test"

                step([
                    $class: 'CloverPublisher',
                    cloverReportDir: 'coverage/',
                    cloverReportFileName: 'clover.xml',
                    healthyTarget: [methodCoverage: 70, conditionalCoverage: 80, statementCoverage: 80],
                    unhealthyTarget: [methodCoverage: 50, conditionalCoverage: 50, statementCoverage: 50],
                    failingTarget: [methodCoverage: 0, conditionalCoverage: 0, statementCoverage: 0]
                ])
            }
            post {
                success {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Test stage was successful. View testing coverage <$BUILD_URL/clover-report/|here>.\"}' https://hooks.slack.com/services/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"
                }
                unsuccessful {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Test stage was unsuccessful.\"}' https://hooks.slack.com/services/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"
                }
            }
        }
        stage('Docker') {
            steps {
                sh "cp docker/prod/Dockerfile ."
                sh "docker build -t eis-api-prod ."
                sh "docker image tag eis-api-prod localhost:5000/eis-api-prod"
                sh "docker push localhost:5000/eis-api-prod"
            }
            post {
                success {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Waiting to deploy to servers... Please visit <$BUILD_URL/input/|here> to deploy.\"}' https://hooks.slack.com/services/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"
                }
                unsuccessful {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Docker stage was unsuccessful.\"}' https://hooks.slack.com/services/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"
                }
            }
        }
        stage('Deploy') {
            input {
                message 'Deployment type'
                ok 'Submit'
                parameters {
                    booleanParam defaultValue: false, description: 'Will immediately deploy code; otherwise will deploy next day at 06.00 UTC.', name: 'immediate'
                }
            }
            stages {
                stage('Immediate Deployment') {
                    when {
                        environment name: 'immediate', value: 'true'
                    }
                    steps {
                        sshPublisher(publishers: [
                            sshPublisherDesc(
                                configName: 'eis-web-02',
                                transfers: [sshTransfer(
                                    cleanRemote: false,
                                    excludes: '',
                                    execCommand: 'cd downloads/scripts/prod && bash deploy.sh',
                                    execTimeout: 120000,
                                    flatten: false,
                                    makeEmptyDirs: false,
                                    noDefaultExcludes: false,
                                    patternSeparator: '[, ]+',
                                    remoteDirectory: 'downloads',
                                    remoteDirectorySDF: false,
                                    removePrefix: '',
                                    sourceFiles: 'scripts/prod/deploy.sh'
                                )],
                                usePromotionTimestamp: false,
                                useWorkspaceInPromotion: false,
                                verbose: false
                            )
                        ])
                        sh "bash scripts/prod/deploy.sh"
                    }
                    post {
                        success {
                            sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Immediate deployment was successful.\"}' https://hooks.slack.com/services/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"
                        }
                        unsuccessful {
                            sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Immediate deployment was unsuccessful.\"}' https://hooks.slack.com/services/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"
                        }
                    }
                }
                stage('Scheduled Deployment') {
                    environment {
                        SLEEP_TIME = get_sleep_time()
                    }
                    when {
                        environment name: 'immediate', value: 'false'
                    }
                    steps {
                        sleep env.SLEEP_TIME
                        sshPublisher(publishers: [
                            sshPublisherDesc(
                                configName: 'eis-web-02',
                                transfers: [sshTransfer(
                                    cleanRemote: false,
                                    excludes: '',
                                    execCommand: 'cd downloads/scripts/prod && bash deploy.sh',
                                    execTimeout: 120000,
                                    flatten: false,
                                    makeEmptyDirs: false,
                                    noDefaultExcludes: false,
                                    patternSeparator: '[, ]+',
                                    remoteDirectory: 'downloads',
                                    remoteDirectorySDF: false,
                                    removePrefix: '',
                                    sourceFiles: 'scripts/prod/deploy.sh'
                                )],
                                usePromotionTimestamp: false,
                                useWorkspaceInPromotion: false,
                                verbose: false
                            )
                        ])
                        sh "bash scripts/prod/deploy.sh"
                    }
                    post {
                        success {
                            sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Scheduled deployment was successful.\"}' https://hooks.slack.com/services/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"
                        }
                        unsuccessful {
                            sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Scheduled deployment was unsuccessful.\"}' https://hooks.slack.com/services/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"
                        }
                    }
                }
            }
        }
    }
}

import java.util.Calendar;

def get_sleep_time() {
    def now = new Date()
    def sched = now.copyWith(date: now[Calendar.DATE] + 1, hourOfDay: 6, minute: 0, second: 0)
    def duration = (sched.getTime() - now.getTime()) / 1000
    return duration.toString()
}
