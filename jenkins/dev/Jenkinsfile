pipeline {
    agent any

    tools {
        nodejs 'node'
    }

    stages {
        stage('Checkout & Build') {
            steps {
                checkout([
                    $class: 'GitSCM',
                    branches: [[name: 'origin/develop']],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    submoduleCfg: [],
                    userRemoteConfigs: [[
                        credentialsId: 'eis-web-01',
                        name: 'origin',
                        url: 'git@bitbucket.org:pvhwebmobile/catalogue-api.git'
                    ]]
                ])
                sh "npm run build"
            }
            post {
                unsuccessful {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Checkout & Build stage was unsuccessful.\"}' https://hooks.slack.com/services/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"
                }
            }
        }
        stage('Test') {
            steps {
                sh "npm run test"

                step([
                    $class: 'CloverPublisher',
                    cloverReportDir: 'coverage/',
                    cloverReportFileName: 'clover.xml',
                    healthyTarget: [methodCoverage: 70, conditionalCoverage: 80, statementCoverage: 80],
                    unhealthyTarget: [methodCoverage: 50, conditionalCoverage: 50, statementCoverage: 50],
                    failingTarget: [methodCoverage: 0, conditionalCoverage: 0, statementCoverage: 0]
                ])
            }
            post {
                success {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Test stage was successful. View testing coverage <$BUILD_URL/clover-report/|here>.\"}' https://hooks.slack.com/services/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"
                }
                unsuccessful {
                    sh "curl -X POST --data-urlencode 'payload={\"channel\": \"#ci-catalog\", \"username\": \"app-eis-ci\", \"text\": \"[<$JOB_URL|$JOB_NAME> <$BUILD_URL|$BUILD_DISPLAY_NAME>] Test stage was unsuccessful.\"}' https://hooks.slack.com/services/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"
                }
            }
        }
    }
}