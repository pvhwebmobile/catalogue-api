const KoaRoute = require('koa-router')
const joi = require('@hapi/joi')

const EmailsController = require('../controllers/emailsController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { validateBody } = require('../libs/bodyValidator')
const { validateURL } = require('../libs/url')

let emailsApi = new KoaRoute({
  prefix: '',
})

let emailsController = new EmailsController()

emailsApi.post('/:tenant/:countryCode/:storeId/emails/wishlist',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    const urlParameters = [
      /^(?:th|ck)$/i,
      /^[A-Za-z]{2}$/,
      /^\d{5}$/,
      /^emails$/,
      /^wishlist$/,
    ]
    const queryParameters = {
      lang: {
        pattern: /^[A-Za-z]{2}$/,
        required: false,
      },
    }
    validateURL(ctx.url, urlParameters, queryParameters)

    const itemSchema = joi.object().keys({
      id: joi.string().required(),
      name: joi.string().required(),
      size: joi.string().required(),
      price: joi.string().required(),
      image: joi.string().required(),
      url: joi.string().optional(),
    })
    const schema = joi.object().keys({
      email: joi.string().email().required(),
      items: joi.array().items(itemSchema).required(),
      total: joi.string().required(),
    })
    validateBody(ctx.request.body, schema)
    // TODO: remove ctx from controller
    return emailsController.sendWishlistEmail(ctx, next)
  })

module.exports = emailsApi
