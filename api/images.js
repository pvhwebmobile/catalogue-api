const KoaRoute = require('koa-router')

const ImagesController = require('../controllers/imagesController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { validateURL } = require('../libs/url')

let imagesApi = new KoaRoute({
  prefix: '/:tenant/:countryCode/:storeId/images',
})

let imagesController = new ImagesController()

imagesApi.get('/splashScreenList', headerBearerAuthMiddleware, (ctx, next) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^images$/,
    /^splashScreenList$/,
  ]
  const queryParameters = {
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)
  // TODO: remove ctx from controller
  return imagesController.getSplashScreenImagesList(ctx, next)
})

imagesApi.get('/:folder/:imageId', headerBearerAuthMiddleware, (ctx, next) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^images$/,
    /[^/]+/,
    /[^/]+/,
  ]
  const queryParameters = {
    v: {
      pattern: /^\d+\.\d+$/,
      required: false,
    },
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)
  // TODO: remove ctx from controller
  return imagesController.getImageById(ctx, next)
})

module.exports = imagesApi
