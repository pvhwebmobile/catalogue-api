const KoaRouter = require('koa-router')

const categoriesApi = require('./categories')
const countriesApi = require('./countries')
const emailsApi = require('./emails')
const healthApi = require('./health')
const imagesApi = require('./images')
const productsApi = require('./products')
const screensApi = require('./screens')
const selectionsApi = require('./selections')
const storesApi = require('./stores')
const uploadsApi = require('./uploads')
const versionsApi = require('./versions')

// old; to delete
const noTenantCallsSupport = require('./noTenantCallsSupport')

let api = new KoaRouter()

api.use(categoriesApi.routes())
api.use(countriesApi.routes())
api.use(emailsApi.routes())
api.use(healthApi.routes())
api.use(imagesApi.routes())
api.use(productsApi.routes())
api.use(screensApi.routes())
api.use(selectionsApi.routes())
api.use(storesApi.routes())
api.use(uploadsApi.routes())
api.use(versionsApi.routes())

// old; to delete
api.use(noTenantCallsSupport.routes())

module.exports = api
