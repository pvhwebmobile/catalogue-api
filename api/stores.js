const KoaRoute = require('koa-router')
const joi = require('@hapi/joi')

const {
  getStores,
  getStore,
  createOrUpdateStore,
  patchStore,
} = require('../controllers/storesController')
const {
  headerBasicAuthMiddleware,
  headerBearerAuthMiddleware,
} = require('../libs/auth')
const { validateBody } = require('../libs/bodyValidator')
const { validateURL } = require('../libs/url')

let storesApi = new KoaRoute({
  prefix: '/:tenant/:countryCode/:wcsStoreId/stores',
})

storesApi.get('/', headerBearerAuthMiddleware, async (ctx, next) => {
  const urlParameters = [/^(?:th|ck)$/i, /^[A-Za-z]{2}$/, /^\d{5}$/, /^stores$/]
  const queryParameters = {
    merge: {
      pattern: /^(0|1)$/,
      required: false,
    },
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)

  let { tenant, countryCode } = ctx.params
  let { merge } = ctx.query
  if (merge) {
    merge = parseInt(merge)
  }

  countryCode = countryCode.toLowerCase()
  const stores = await getStores(tenant, countryCode, merge)
  if (stores) {
    ctx.status = 200
    ctx.body = { success: true, data: stores }
    next()
    return
  }

  ctx.status = 400
  ctx.body = { success: false, data: [] }
  next()
})

storesApi.get('/:storeId', headerBearerAuthMiddleware, async (ctx, next) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^stores$/,
    /^[A-Z0-9]{4}$/,
  ]
  const queryParameters = {
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)

  let { tenant, countryCode, storeId } = ctx.params
  countryCode = countryCode.toLowerCase()
  const store = await getStore(tenant, countryCode, storeId)

  if (store) {
    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: store }
    next()
    return
  }

  ctx.status = 400
  ctx.body = { success: false, data: [] }
  next()
})

storesApi.put('/:storeId', headerBasicAuthMiddleware, async (ctx, next) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^stores$/,
    /^[A-Z0-9]{4}$/,
  ]
  const queryParameters = {
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)

  const body = ctx.request.body
  const schema = joi.object().keys({
    tenant: joi.string().regex(/^(th|ck)$/).required(),
    countryCode: joi.string().regex(/^[a-zA-Z]{2}$/).required(),
    payment: joi.any().valid(['ecom', 'gk']).required(),
  })
  if (!validateBody(body, schema)) {
    ctx.status = 400
    ctx.type = 'JSON'
    ctx.body = {
      success: false,
      data: {},
      error: { message: 'Body is improperly formatted.' },
    }
    return
  }

  const { storeId } = ctx.params
  const store = await createOrUpdateStore(storeId, body)

  if (store) {
    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: store }
    next()
    return
  }

  ctx.status = 400
  ctx.body = { success: false, data: {} }
  next()
})

storesApi.patch('/:storeId', headerBasicAuthMiddleware, async (ctx, next) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^stores$/,
    /^[A-Z0-9]{4}$/,
  ]
  const queryParameters = {
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)

  const body = ctx.request.body
  const schema = joi.object().keys({
    tenant: joi.string().regex(/^(th|ck)$/),
    countryCode: joi.string().regex(/^[a-zA-Z]{2}$/),
    payment: joi.any().valid(['ecom', 'gk']),
  }).or('tenant', 'countryCode', 'payment')
  if (!validateBody(body, schema)) {
    ctx.status = 400
    ctx.type = 'JSON'
    ctx.body = {
      success: false,
      data: {},
      error: { message: 'Body is improperly formatted.' },
    }
    return
  }

  let { tenant, countryCode, storeId } = ctx.params
  countryCode = countryCode.toLowerCase()
  const store = await patchStore(tenant, countryCode, storeId, body)

  if (store) {
    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: store }
    next()
    return
  }

  ctx.status = 400
  ctx.body = { success: false, data: {} }
  next()
})

module.exports = storesApi
