const KoaRoute = require('koa-router')

const CountriesController = require('../controllers/countriesController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { validateURL } = require('../libs/url')

let countriesApi = new KoaRoute({
  prefix: '/:tenant',
})

let countriesController = new CountriesController()

countriesApi.get('/countries', headerBearerAuthMiddleware, (ctx, next) => {
  const urlParameters = [/^(?:th|ck)$/i, /^countries$/]
  const queryParameters = {
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)
  // TODO: remove ctx from controller
  return countriesController.getCountriesList(ctx, next)
})

countriesApi.get('/countries/:countryCode/languages',
  headerBearerAuthMiddleware, (ctx, next) => {
    const urlParameters = [
      /^(?:th|ck)$/i,
      /^countries$/i,
      /^[A-Z]{2}$/i,
      /^languages$/i,
    ]
    const queryParameters = {
      lang: {
        pattern: /^[A-Za-z]{2}$/,
        required: false,
      },
    }
    validateURL(ctx.url, urlParameters, queryParameters)

    return countriesController.getLanguagesByCountry(ctx, next)
  })

countriesApi.get('/:countryCode/:storeId/ping',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    const urlParameters = [/^(?:th|ck)$/i, /^[A-Za-z]{2}$/, /^\d{5}$/, /^ping$/]
    const queryParameters = {}
    validateURL(ctx.url, urlParameters, queryParameters)
    // TODO: remove ctx from controller
    return countriesController.pingWCSConnection(ctx, next)
  })

module.exports = countriesApi
