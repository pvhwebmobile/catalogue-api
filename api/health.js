const KoaRoute = require('koa-router')
const fs = require('fs')

const { headerBearerAuthMiddleware } = require('../libs/auth')

let healthApi = new KoaRoute({
  prefix: '',
})

healthApi.get('/', (ctx, next) => {
  ctx.status = 200
  next()
})

healthApi.get('/health/check', (ctx, next) => {
  ctx.status = 200
  next()
})

healthApi.get('/logs', headerBearerAuthMiddleware, (ctx, next) => {
  ctx.body = fs.createReadStream('eis-all.log')
  ctx.status = 200
  next()
})

module.exports = healthApi
