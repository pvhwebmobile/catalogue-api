const KoaRoute = require('koa-router')

const CategoriesController = require('../controllers/categoriesController')
const ProductsController = require('../controllers/productsController')
const ImagesController = require('../controllers/imagesController')
const EmailsController = require('../controllers/emailsController')
const { getStores } = require('../controllers/storesController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { versionCheck } = require('../libs/versioning')

let noTenantApi = new KoaRoute({
  prefix: '/:countryCode/:storeId',
})

let categoriesController = new CategoriesController()
let productsController = new ProductsController()
let imagesController = new ImagesController()
let emailsController = new EmailsController()

noTenantApi.get('/categories/', headerBearerAuthMiddleware, (ctx, next) => {
  if (versionCheck(ctx.state.acceptVersion, ['1'])) {
    return categoriesController.getTopCategoriesByStoreId(ctx, next)
  } else {
    ctx.status = 501
    ctx.type = 'JSON'
    ctx.body = { message: 'Version is unsupported.' }
  }
})

noTenantApi.get('/products/:productId',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['1'])) {
      return productsController.getProductById(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

noTenantApi.get('/products/suggestions/:term',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['1'])) {
      return productsController.getSuggestionsByTerm(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

noTenantApi.get('/products/search/:term',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['1'])) {
      return productsController.getProductsByTerm(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

noTenantApi.get('/products/category/:categoryId',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['1'])) {
      return productsController.getProductsByCategoryId(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

noTenantApi.get('/products/inventory/:skus',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['1'])) {
      return productsController.getInventoryBySkus(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

noTenantApi.get('/images/:imageId', headerBearerAuthMiddleware, (ctx, next) => {
  if (versionCheck(ctx.state.acceptVersion, ['1'])) {
    return imagesController.getImageById(ctx, next)
  } else {
    ctx.status = 501
    ctx.type = 'JSON'
    ctx.body = { message: 'Version is unsupported.' }
  }
})

noTenantApi.get('/images/:folder/:imageId',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['1'])) {
      return imagesController.getImageById(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

noTenantApi.get('/stores', headerBearerAuthMiddleware, async (ctx, next) => {
  if (versionCheck(ctx.state.acceptVersion, ['1'])) {
    const { countryCode } = ctx.params
    const stores = await getStores(undefined, countryCode)
    if (stores) {
      ctx.status = 200
      ctx.body = { success: true, data: stores }
      next()
      return
    }

    ctx.status = 400
    ctx.body = { success: false, data: [] }
    next()
  } else {
    ctx.status = 501
    ctx.type = 'JSON'
    ctx.body = { message: 'Version is unsupported.' }
    next()
  }
})

noTenantApi.post('/emails/wishlist',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['1'])) {
      return emailsController.sendWishlistEmail(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

module.exports = noTenantApi
