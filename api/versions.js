const KoaRoute = require('koa-router')

const {
  validateURL,
} = require('../libs/url')
const {
  headerBearerAuthMiddleware,
} = require('../libs/auth')
const {
  getVersion,
  checkForUpdate,
} = require('../controllers/versionsController')

let versionsApi = new KoaRoute({
  prefix: '/versions/:appName',
})

versionsApi.get('/', headerBearerAuthMiddleware, async (ctx) => {
  const urlParameters = [/^versions$/i, /^[a-z0-9\-_.]+$/i]
  const queryParameters = {}
  validateURL(ctx.url, urlParameters, queryParameters)

  const {
    appName,
  } = ctx.params

  const data = await getVersion(appName)
  ctx.status = 200
  ctx.body = { success: true, data }
})

versionsApi.get('/update', headerBearerAuthMiddleware, async (ctx) => {
  const urlParameters = [/^versions$/i, /^[A-Za-z0-9\-_.]+$/i, /^update$/i]
  const queryParameters = {
    clientVersion: {
      pattern: /^\d+(\.\d+(\.\d+)?)?$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)

  const {
    appName,
  } = ctx.params

  const {
    clientVersion,
  } = ctx.query

  const data = await checkForUpdate(appName, clientVersion)
  ctx.status = 200
  ctx.body = { success: true, data }
})

module.exports = versionsApi
