const KoaRoute = require('koa-router')

const ProductsController = require('../controllers/productsController')
const newProductsController = require('../controllers/newProductsController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { validateURL } = require('../libs/url')
const { versionCheck } = require('../libs/versioning')

let productsApi = new KoaRoute({
  prefix: '/:tenant/:countryCode/:storeId/products',
})

let productsController = new ProductsController()

productsApi.get('/:productId',
  headerBearerAuthMiddleware,
  async (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^\d+(?:,\d+)*$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
        byPartNumber: {
          pattern: /^(0|1)$/i,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      const { tenant, countryCode, storeId, productId } = ctx.params
      let { byPartNumber } = ctx.query
      if (byPartNumber === '1') {
        byPartNumber = true
      } else {
        byPartNumber = false
      }
      const { langId, locale, client } = ctx.state
      const globals = {
        ctx,
        tenant,
        countryCode,
        storeId,
        locale,
        client,
        langId,
      }
      const { getProductById, getProductByPartNumber } = newProductsController(globals)
      let product
      if (!byPartNumber) {
        product = await getProductById(productId)
      } else {
        product = await getProductByPartNumber(productId)
      }

      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, data: product }
    } else {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^\d+(?:,\d+)*$/,
      ]
      const queryParameters = {
        version: {
          pattern: /^\d$/,
          required: false,
        },
        byPartNumber: {
          pattern: /^\d$/,
          required: false,
        },
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)
      // TODO: remove ctx from controller
      return productsController.getProductById(ctx, next)
    }
  })

productsApi.get('/suggestions/:term',
  headerBearerAuthMiddleware,
  async (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^suggestions$/,
        /[^/^]+$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      const { tenant, countryCode, term } = ctx.params
      const { locale } = ctx.state
      const globals = {
        ctx,
        tenant,
        countryCode,
        locale,
      }
      const { getSuggestionsByTerm } = newProductsController(globals)
      const suggestions = await getSuggestionsByTerm(term)

      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, data: suggestions }
    } else {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^suggestions$/,
        /[^/^]+$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      return productsController.getSuggestionsByTerm(ctx, next)
    }
  })

productsApi.get('/search/:term',
  headerBearerAuthMiddleware,
  async (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^search$/,
        /[^/^]+$/,
      ]
      const queryParameters = {
        page: {
          pattern: /^\d{1,4}$/,
          required: false,
        },
        facet: {
          pattern: /^([^,:]+:[^,:]+(,[^,:]+:[^,:]+)*)?$/,
          // facet=maincolour:maincolour_blue,size_de:size_de_4;size_de_xl
          required: false,
        },
        categoryId: {
          pattern: /^.+(?:,.+)*$/,
          required: false,
        },
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      const { tenant, countryCode, term } = ctx.params
      const { page, facet, categoryId } = ctx.query
      const { locale, client } = ctx.state
      const globals = {
        ctx,
        tenant,
        countryCode,
        locale,
        client,
      }
      const { getProductsByTerm } = newProductsController(globals)
      const products = await getProductsByTerm(
        term, page, facet, categoryId
      )

      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, data: products }
    } else {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^search$/,
        /[^/^]+$/,
      ]
      const queryParameters = {
        page: {
          pattern: /^\d{1,4}$/,
          required: false,
        },
        facet: {
          pattern: /^([^,:]+:[^,:]+(,[^,:]+:[^,:]+)*)?$/,
          // facet=ads_f28501_ntk_cs:%2218%22,ads_f28505_ntk_cs:%22BLAU%22
          required: false,
        },
        categoryId: {
          pattern: /^\d+(?:,\d+)*$/, // should match 12345,21345,...etc
          required: false,
        },
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)
      return productsController.getProductsByTerm(ctx, next)
    }
  })

productsApi.get('/category/:categoryId',
  headerBearerAuthMiddleware,
  async (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^category$/,
        /^.+(?:,.+)*$/,
      ]
      const queryParameters = {
        page: {
          pattern: /^\d{1,4}$/,
          required: false,
        },
        facet: {
          pattern: /^([^,:]+:[^,:]+(,[^,:]+:[^,:]+)*)?$/,
          // facet=maincolour:maincolour_blue,size_de:size_de_4;size_de_xl
          required: false,
        },
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      const { tenant, countryCode, categoryId } = ctx.params
      const { page, facet } = ctx.query
      const { locale, client } = ctx.state
      const globals = {
        ctx,
        tenant,
        countryCode,
        locale,
        client,
      }
      const { getProductsByCategoryId } = newProductsController(globals)
      const products = await getProductsByCategoryId(
        categoryId, page, facet
      )

      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, data: products }
    } else {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^products$/,
        /^category$/,
        /^\d+(?:,\d+)*$/,
      ]
      const queryParameters = {
        page: {
          pattern: /^\d{1,4}$/,
          required: false,
        },
        facet: {
          pattern: /^([^,:]+:[^,:]+(,[^,:]+:[^,:]+)*)?$/,
          // facet=ads_f28501_ntk_cs:%2218%22,ads_f28505_ntk_cs:%22BLAU%22
          required: false,
        },
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)
      // TODO: remove ctx from controller
      return productsController.getProductsByCategoryId(ctx, next)
    }
  })

productsApi.get('/inventory/:skus', headerBearerAuthMiddleware, (ctx, next) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^products$/,
    /^inventory$/,
    /^\S+$/,
  ]
  const queryParameters = {
    byPartNumber: {
      pattern: /^\d$/,
      required: false,
    },
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)
  // TODO: remove ctx from controller
  return productsController.getInventoryBySkus(ctx, next)
})

module.exports = productsApi
