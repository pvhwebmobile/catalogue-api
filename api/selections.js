const KoaRoute = require('koa-router')
const cors = require('@koa/cors')
const joi = require('@hapi/joi')

const SelectionsController = require('../controllers/selectionsController')
const {
  applyPromotionCode,
  removePromotionCode,
} = require('../controllers/newSelectionsController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { validateBody } = require('../libs/bodyValidator')
const { validateURL } = require('../libs/url')
const { versionCheck } = require('../libs/versioning')

const selectionsApi = new KoaRoute({
  prefix: '',
})

const selectionController = new SelectionsController()

selectionsApi.post('/:tenant/:countryCode/:storeId/selections/',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)
      const productSchema = joi.object().keys({
        id: joi.number(),
        skuId: joi.number(),
        name: joi.string().optional(),
        promoUnitPrice: joi.number().optional(),
        size: joi.string().optional(),
        image: joi.string().optional(),
        colorPattern: joi.string().optional(),
        colorName: joi.string().optional(),
        amount: joi.number().optional(),
      }).or('id', 'skuId').unknown()

      const schema = joi.object().keys({
        name: joi.string().required(),
        siteId: joi.string().required(),
        products: joi.array().items(productSchema).min(1),
      }).unknown()

      if (!validateBody(ctx.request.body, schema)) {
        ctx.status = 400
        ctx.type = 'JSON'
        ctx.body = {
          success: false,
          data: {},
          error: { message: 'Body is formatted improperly.' },
        }
        return
      }

      return selectionController.createSelection(ctx, next, '3')
    } else if (versionCheck(ctx.state.acceptVersion, ['^2'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)
      const productSchema = joi.object().keys({
        id: joi.string().optional(),
        name: joi.string().optional(),
        promoUnitPrice: joi.number().optional(),
        size: joi.string().optional(),
        image: joi.string().optional(),
        colorPattern: joi.string().optional(),
        colorName: joi.string().optional(),
        skuId: joi.any().optional(),
        amount: joi.number().optional(),
        label: joi.string().optional(),
        fullfillment: joi.string().optional(),
      }).unknown()

      const schema = joi.object().keys({
        name: joi.string().required(),
        siteId: joi.string().required(),
        products: joi.array().items(productSchema).min(1),
      }).unknown()
      if (!validateBody(ctx.request.body, schema)) {
        ctx.status = 400
        ctx.type = 'JSON'
        ctx.body = {
          success: false,
          data: {},
          error: { message: 'Body is formatted improperly.' },
        }
        return
      }

      // TODO: remove ctx from controller
      return selectionController.createSelection(ctx, next, '2')
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.put('/:tenant/:countryCode/:storeId/selections/:selectionId',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
        /^.+$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)
      const productSchema = joi.object().keys({
        id: joi.number(),
        skuId: joi.number(),
        name: joi.string().optional(),
        promoUnitPrice: joi.number().optional(),
        size: joi.string().optional(),
        image: joi.string().optional(),
        colorPattern: joi.string().optional(),
        colorName: joi.string().optional(),
        amount: joi.number().optional(),
      }).or('id', 'skuId').unknown()

      const schema = joi.object().keys({
        name: joi.string().required(),
        siteId: joi.string().required(),
        products: joi.array().items(productSchema).min(1),
      }).unknown()

      if (!validateBody(ctx.request.body, schema)) {
        ctx.status = 400
        ctx.type = 'JSON'
        ctx.body = {
          success: false,
          data: {},
          error: { message: 'Body is formatted improperly.' },
        }
        return
      }

      return selectionController.updateSelection(ctx, next, '3')
    } else if (versionCheck(ctx.state.acceptVersion, ['^2'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
        /^.+$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      const productSchema = joi.object().keys({
        id: joi.string().optional(),
        name: joi.string().optional(),
        promoUnitPrice: joi.number().optional(),
        size: joi.string().optional(),
        image: joi.string().optional(),
        colorPattern: joi.string().optional(),
        colorName: joi.string().optional(),
        amount: joi.number().optional(),
      }).unknown()

      const schema = joi.object().keys({
        name: joi.string().required(),
        siteId: joi.string().required(),
        products: joi.array().items(productSchema).min(1),
      }).unknown()

      if (!validateBody(ctx.request.body, schema)) {
        ctx.status = 400
        ctx.type = 'JSON'
        ctx.body = {
          success: false,
          data: {},
          error: { message: 'Body is formatted improperly.' },
        }
        return
      }

      // TODO: remove ctx from controller
      return selectionController.updateSelection(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.put(
  '/:tenant/:countryCode/:storeId/selections/:selectionId/personalDetails',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
        /^.+$/,
        /^personalDetails$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      const schema = {
        shippingModeId: joi.string().required(),
        email: joi.string().email().required(),
        shippingZipCode: joi.string().optional(),
        shippingPhone: joi.string().optional(),
        shippingPersonTitle: joi.string().optional(),
        shippingLastName: joi.string().optional(),
        shippingFirstName: joi.string().optional(),
        shippingCity: joi.string().optional(),
        shippingAddress: joi.string().optional(),
        shippingAddress2: joi.string().optional(),
        billingZipCode: joi.string().optional(),
        billingPhone: joi.string().optional(),
        billingPersonTitle: joi.string().optional(),
        billingLastName: joi.string().optional(),
        billingFirstName: joi.string().optional(),
        billingCity: joi.string().optional(),
        billingAddress: joi.string().optional(),
        billingAddress2: joi.string().optional(),
        billingCountry: joi.string().optional(),
        shipStoreId: joi.string().optional(),
      }
      if (!validateBody(ctx.request.body, schema)) {
        ctx.status = 400
        ctx.type = 'JSON'
        ctx.body = {
          success: false,
          data: {},
          error: { message: 'Body is improperly formatted.' },
        }
        return
      }

      // TODO: remove ctx from controller
      return selectionController.updateSelectionPersonalDetails(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.get(
  '/:tenant/:countryCode/:storeId/selections/:selectionId/deliveryMethods',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
        /^.+$/,
        /^deliveryMethods$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      // TODO: remove ctx from controller
      return selectionController.getDeliveryMethodsBySelectionId(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.get('/:tenant/:countryCode/:storeId/selections/validationRules',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
        /^validationRules$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      // TODO: remove ctx from controller
      return selectionController.getValidationRules(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.get(
  '/:tenant/:countryCode/:storeId/selections/validateZipCode/:orderId/:zipcode/:city/:address1',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
        /^validateZipCode$/,
        /^\d+$/,
        /^.+$/,
        /^.+$/,
        /^.+$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      // TODO: remove ctx from controller
      return selectionController.getValidationZipCode(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.get(
  '/:tenant/:countryCode/:storeId/selections/:siteId([A-Z0-9]{4})',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    // workaround for conflicting urls... apparently KOA doesn't support
    // regexp for url params
    if (ctx.params.siteId === 'test') {
      return next()
    } else {
      if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
        const urlParameters = [
          /^(?:th|ck)$/i,
          /^[A-Za-z]{2}$/,
          /^\d{5}$/,
          /^selections$/,
          /^[A-Z0-9]{4}$/,
        ]
        const queryParameters = {
          status: {
            pattern: /^(?:[1-5]|created|sentToTill|addedToTransaction|paid|rejected)$/,
            required: false,
          },
          lang: {
            pattern: /^[A-Za-z]{2}$/,
            required: false,
          },
        }
        validateURL(ctx.url, urlParameters, queryParameters)

        // TODO: remove ctx from controller
        return selectionController.getSelectionsListByStatusAndSiteId(ctx, next)
      } else {
        ctx.status = 501
        ctx.type = 'JSON'
        ctx.body = { message: 'Version is unsupported.' }
      }
    }
  })

selectionsApi.options('/selections/:siteId([A-Z0-9]{4})', cors(), (ctx) => {
  ctx.status = 200
})

selectionsApi.get('/selections/:siteId([A-Z0-9]{4})', cors(),
  headerBearerAuthMiddleware,
  (ctx, next) => {
    const urlParameters = [/^selections$/, /^[A-Z0-9]{4}$/]
    const queryParameters = {
      status: {
        pattern: /^(?:[1-5]|created|sentToTill|addedToTransaction|paid|rejected)$/,
        required: false,
      },
      lang: {
        pattern: /^[A-Za-z]{2}$/,
        required: false,
      },
    }
    validateURL(ctx.url, urlParameters, queryParameters)

    // TODO: remove ctx from controller
    return selectionController.getSelectionsListByStatusAndSiteId(ctx, next)
  })

selectionsApi.get('/:tenant/:countryCode/:storeId/selections/orderid/:orderId',
  headerBearerAuthMiddleware,
  (ctx, next) => {
    if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
      const urlParameters = [
        /^(?:th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}$/,
        /^selections$/,
        /^orderid$/,
        /^\d+$/,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }
      validateURL(ctx.url, urlParameters, queryParameters)

      // TODO: remove ctx from controller
      return selectionController.getSelectionByOrderId(ctx, next)
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.options(
  '/:tenant/:countryCode/:storeId/selections/:orderId/:action(sentToTill|addedToTransaction|cancelled|paid|rejected)',
  cors(), (ctx) => { ctx.status = 200 })

selectionsApi.put(
  '/:tenant/:countryCode/:storeId/selections/:orderId/:action(sentToTill|addedToTransaction|cancelled|paid|rejected)',
  cors(),
  headerBearerAuthMiddleware,
  (ctx, next) => {
    const urlParameters = [
      /^(?:th|ck)$/i,
      /^[A-Za-z]{2}$/,
      /^\d{5}$/,
      /^selections$/,
      /^\d+$/,
      /^(sentToTill|addedToTransaction|cancelled|paid|rejected)$/,
    ]
    const queryParameters = {
      siteId: {
        pattern: /^[A-Z0-9]{4}$/,
        required: false,
      },
      lang: {
        pattern: /^[A-Za-z]{2}$/,
        required: false,
      },
    }
    validateURL(ctx.url, urlParameters, queryParameters)

    // TODO: remove ctx from controller
    return selectionController.updateSelectionStatusByOrderId(ctx, next)
  })

selectionsApi.options('/:tenant/:countryCode/:storeId/selections/test', cors(),
  (ctx) => { ctx.status = 200 })

selectionsApi.get('/:tenant/:countryCode/:storeId/selections/test', cors(),
  headerBearerAuthMiddleware,
  (ctx, next) => {
    const urlParameters = [
      /^(?:th|ck)$/i,
      /^[A-Za-z]{2}$/,
      /^\d{5}$/,
      /^selections$/,
      /^test$/,
    ]
    const queryParameters = {
      _: {
        pattern: /^\d+$/,
        required: false,
      },
      lang: {
        pattern: /^[A-Za-z]{2}$/,
        required: false,
      },
    }
    validateURL(ctx.url, urlParameters, queryParameters)

    // TODO: remove ctx from controller
    return selectionController.getConnectivityWcs(ctx, next)
  })

selectionsApi.post(
  '/:tenant/:countryCode/:storeId/selections/:selectionId/promoCode',
  headerBearerAuthMiddleware,
  async (ctx) => {
    if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
      const urlParameters = [
        /^(:?th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}/,
        /^selections$/,
        /^.+$/,
        /^promoCode$/i,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }

      const { langId } = ctx.state

      validateURL(ctx.url, urlParameters, queryParameters)

      const { tenant, countryCode, selectionId } = ctx.params
      const { promotionCode } = ctx.request.body
      try {
        const data = await applyPromotionCode(
          selectionId, promotionCode, tenant, countryCode, langId,
        )
        ctx.status = 200
        ctx.type = 'JSON'
        ctx.body = { success: true, data }
      } catch (error) {
        ctx.status = 200
        ctx.type = 'JSON'
        ctx.body = { success: false, errorkey: error.errorCode, errorMessage: error.message }
      }
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  })

selectionsApi.delete(
  '/:tenant/:countryCode/:storeId/selections/:selectionId/promoCode',
  headerBearerAuthMiddleware,
  async (ctx) => {
    if (versionCheck(ctx.state.acceptVersion, ['^2', '>=3'])) {
      const urlParameters = [
        /^(:?th|ck)$/i,
        /^[A-Za-z]{2}$/,
        /^\d{5}/,
        /^selections$/,
        /^.+$/,
        /^promoCode$/i,
      ]
      const queryParameters = {
        lang: {
          pattern: /^[A-Za-z]{2}$/,
          required: false,
        },
      }

      const { langId } = ctx.state

      validateURL(ctx.url, urlParameters, queryParameters)

      const { tenant, countryCode, selectionId } = ctx.params

      try {
        const data = await removePromotionCode(
          selectionId, tenant, countryCode, langId,
        )
        ctx.status = 200
        ctx.type = 'JSON'
        ctx.body = { success: true, data }
      } catch (error) {
        ctx.status = 200
        ctx.type = 'JSON'
        ctx.body = { success: false, errorkey: '-1', errorMessage: error.message }
      }
    } else {
      ctx.status = 501
      ctx.type = 'JSON'
      ctx.body = { message: 'Version is unsupported.' }
    }
  }
)

module.exports = selectionsApi
