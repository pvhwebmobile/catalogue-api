const KoaRoute = require('koa-router')

const { headerBasicAuthMiddleware } = require('../libs/auth')
const { validateURL } = require('../libs/url')
const { uploadStoreLocatorJSON } = require('../controllers/uploadsController')

let uploadsApi = new KoaRoute({
  prefix: '/uploads',
})

uploadsApi.post(
  '/sharepoint/storelocator',
  headerBasicAuthMiddleware,
  async (ctx) => {
    const urlParameters = [/^uploads$/i, /^sharepoint$/i, /^storelocator$/i]
    const queryParameters = {}
    validateURL(ctx.url, urlParameters, queryParameters)

    const rawData = ctx.request.body

    const result = await uploadStoreLocatorJSON(rawData)

    if (result) {
      ctx.status = 200
      ctx.body = result
      ctx.type = 'JSON'
    } else {
      ctx.status = 406
    }
  })

module.exports = uploadsApi
