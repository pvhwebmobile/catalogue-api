const KoaRoute = require('koa-router')

const { getGenderScreen } = require('../controllers/screensController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { validateURLMiddleware } = require('../libs/url')
const { getCountryProperties } = require('../libs/countryConfig')

let screensRoutes = new KoaRoute({
  prefix: '/:tenant/:countryCode/:storeId/screens',
})

screensRoutes.use(headerBearerAuthMiddleware)

screensRoutes.get('/gender',
  validateURLMiddleware([
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^screens$/,
    /^gender$/,
  ], {
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }),
  headerBearerAuthMiddleware,
  async (ctx, next) => {
    const tenant = ctx.params.tenant.toLowerCase()
    const countryCode = ctx.params.countryCode.toLowerCase()

    const countryProps = getCountryProperties(
      tenant, countryCode, ['langId', 'catalogId', 'storeId']
    )

    let { langId } = ctx.state
    if (!langId) {
      langId = countryProps.langId
    }

    const { storeId, catalogId } = countryProps
    const data = await getGenderScreen(
      tenant, countryCode, storeId, catalogId, langId
    )

    if (data) {
      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, data }
      next()
      return
    }
    ctx.status = 500
    ctx.type = 'JSON'
    ctx.body = { success: false, errormessage: 'Failed to process response.' }
    next()
  })

module.exports = screensRoutes
