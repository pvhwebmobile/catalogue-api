const KoaRoute = require('koa-router')

const CategoriesController = require('../controllers/categoriesController')
const newCategoriesController = require('../controllers/newCategoriesController')
const { headerBearerAuthMiddleware } = require('../libs/auth')
const { validateURL } = require('../libs/url')
const { getCountryProperties } = require('../libs/countryConfig')

let categoriesApi = new KoaRoute({
  prefix: '/:tenant/:countryCode/:storeId/categories',
})

let categoriesController = new CategoriesController()

categoriesApi.get('/', headerBearerAuthMiddleware, async (ctx, next) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^categories$/,
  ]
  const queryParameters = {
    showEcomCategories: {
      pattern: /^[01]$/,
      required: false,
    },
    isGrouping: {
      pattern: /^\d{1}$/,
      required: false,
    },
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)
  // TODO: remove ctx from controller
  const { showEcomCategories } = ctx.query
  if (showEcomCategories) {
    let sec = showEcomCategories === '1'
    const { tenant, countryCode, storeId } = ctx.params
    const { isGrouping } = ctx.query
    const { langId, catalogId } =
      getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const categories = await newCategoriesController.getCategories(
      tenant, countryCode, storeId, catalogId, langId, isGrouping, sec,
    )

    ctx.status = 200
    ctx.body = { success: true, data: categories }
    next()
  } else {
    return categoriesController.getTopCategoriesByStoreId(ctx, next)
  }
})

categoriesApi.get('/poc', headerBearerAuthMiddleware, (ctx) => {
  const urlParameters = [
    /^(?:th|ck)$/i,
    /^[A-Za-z]{2}$/,
    /^\d{5}$/,
    /^categories$/,
    /^poc$/,
  ]
  const queryParameters = {
    isGrouping: {
      pattern: /^\d{1}$/,
      required: false,
    },
    lang: {
      pattern: /^[A-Za-z]{2}$/,
      required: false,
    },
  }
  validateURL(ctx.url, urlParameters, queryParameters)
  // TODO: remove ctx from controller
  return categoriesController.getCategoriesForPOC(ctx)
})

module.exports = categoriesApi
