const path = require('path')
const fs = require('fs')
const { MongoMemoryServer } = require('mongodb-memory-server-core')

const globalConfigPath = path.join(__dirname, 'globalConfig.json')

let mongod

module.exports = async () => {
  mongod = new MongoMemoryServer({
    binary: {
      systemBinary: '/usr/local/bin/mongod',
    },
  })

  const mongoConfig = {
    mongoDBName: 'test_db',
    mongoUri: await mongod.getConnectionString(),
  }

  fs.writeFileSync(globalConfigPath, JSON.stringify(mongoConfig))

  global.__MONGOD__ = mongod
}
