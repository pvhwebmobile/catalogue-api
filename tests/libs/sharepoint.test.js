/* global describe it expect */

const { formatOpeningHours } = require('../../libs/sharepoint')

describe('formatOpeningHours', () => {
  it('should be defined', () => {
    expect(formatOpeningHours).toBeDefined()
  })

  it('should provide default when given empty string', () => {
    const openingHoursRaw = ''
    const openingHours = formatOpeningHours(openingHoursRaw)
    expect(openingHours).toBeDefined()
    expect(openingHours.raw).toStrictEqual(openingHoursRaw)
    expect(openingHours.monday.isOpen).toBeFalsy()
    expect(openingHours.monday.opensAt).toBeNull()
    expect(openingHours.monday.closesAt).toBeNull()

    expect(openingHours.tuesday.isOpen).toBeFalsy()
    expect(openingHours.tuesday.opensAt).toBeNull()
    expect(openingHours.tuesday.closesAt).toBeNull()

    expect(openingHours.wednesday.isOpen).toBeFalsy()
    expect(openingHours.wednesday.opensAt).toBeNull()
    expect(openingHours.wednesday.closesAt).toBeNull()

    expect(openingHours.thursday.isOpen).toBeFalsy()
    expect(openingHours.thursday.opensAt).toBeNull()
    expect(openingHours.thursday.closesAt).toBeNull()

    expect(openingHours.friday.isOpen).toBeFalsy()
    expect(openingHours.friday.opensAt).toBeNull()
    expect(openingHours.friday.closesAt).toBeNull()

    expect(openingHours.saturday.isOpen).toBeFalsy()
    expect(openingHours.saturday.opensAt).toBeNull()
    expect(openingHours.saturday.closesAt).toBeNull()

    expect(openingHours.sunday.isOpen).toBeFalsy()
    expect(openingHours.sunday.opensAt).toBeNull()
    expect(openingHours.sunday.closesAt).toBeNull()
  })

  it('should format all 7 days of week', () => {
    const openingHoursRaw = 'MON 10:00-22:00~~TUE 10:00-22:00~~WED 10:00-22:00~~THU 10:00-22:00~~FRI 10:00-22:00~~SAT 10:00-22:00~~SUN 10:00-21:00'
    const openingHours = formatOpeningHours(openingHoursRaw)
    expect(openingHours).toBeDefined()
    expect(openingHours.raw).toStrictEqual(openingHoursRaw)
    expect(openingHours.monday.isOpen).toBeTruthy()
    expect(openingHours.monday.opensAt).toEqual('10:00')
    expect(openingHours.monday.closesAt).toEqual('22:00')

    expect(openingHours.tuesday.isOpen).toBeTruthy()
    expect(openingHours.tuesday.opensAt).toEqual('10:00')
    expect(openingHours.tuesday.closesAt).toEqual('22:00')

    expect(openingHours.wednesday.isOpen).toBeTruthy()
    expect(openingHours.wednesday.opensAt).toEqual('10:00')
    expect(openingHours.wednesday.closesAt).toEqual('22:00')

    expect(openingHours.thursday.isOpen).toBeTruthy()
    expect(openingHours.thursday.opensAt).toEqual('10:00')
    expect(openingHours.thursday.closesAt).toEqual('22:00')

    expect(openingHours.friday.isOpen).toBeTruthy()
    expect(openingHours.friday.opensAt).toEqual('10:00')
    expect(openingHours.friday.closesAt).toEqual('22:00')

    expect(openingHours.saturday.isOpen).toBeTruthy()
    expect(openingHours.saturday.opensAt).toEqual('10:00')
    expect(openingHours.saturday.closesAt).toEqual('22:00')

    expect(openingHours.sunday.isOpen).toBeTruthy()
    expect(openingHours.sunday.opensAt).toEqual('10:00')
    expect(openingHours.sunday.closesAt).toEqual('21:00')
  })

  it('should format 2 days of the week and the rest are closed', () => {
    const openingHoursRaw = 'MON 10:00-22:00~~SUN 10:00-21:00'
    const openingHours = formatOpeningHours(openingHoursRaw)
    expect(openingHours).toBeDefined()
    expect(openingHours.raw).toStrictEqual(openingHoursRaw)
    expect(openingHours.monday.isOpen).toBeTruthy()
    expect(openingHours.monday.opensAt).toEqual('10:00')
    expect(openingHours.monday.closesAt).toEqual('22:00')

    expect(openingHours.tuesday.isOpen).toBeFalsy()
    expect(openingHours.tuesday.opensAt).toBeNull()
    expect(openingHours.tuesday.closesAt).toBeNull()

    expect(openingHours.wednesday.isOpen).toBeFalsy()
    expect(openingHours.wednesday.opensAt).toBeNull()
    expect(openingHours.wednesday.closesAt).toBeNull()

    expect(openingHours.thursday.isOpen).toBeFalsy()
    expect(openingHours.thursday.opensAt).toBeNull()
    expect(openingHours.thursday.closesAt).toBeNull()

    expect(openingHours.friday.isOpen).toBeFalsy()
    expect(openingHours.friday.opensAt).toBeNull()
    expect(openingHours.friday.closesAt).toBeNull()

    expect(openingHours.saturday.isOpen).toBeFalsy()
    expect(openingHours.saturday.opensAt).toBeNull()
    expect(openingHours.saturday.closesAt).toBeNull()

    expect(openingHours.sunday.isOpen).toBeTruthy()
    expect(openingHours.sunday.opensAt).toEqual('10:00')
    expect(openingHours.sunday.closesAt).toEqual('21:00')
  })

  it('should return default openingHours if raw data fails regex', () => {
    const openingHoursRaw = 'MON 10:00-22:00~~SUN 10:00-21:00~~'
    const openingHours = formatOpeningHours(openingHoursRaw)
    expect(openingHours).toBeDefined()
    expect(openingHours.raw).toStrictEqual(openingHoursRaw)
    expect(openingHours.monday.isOpen).toBeFalsy()
    expect(openingHours.monday.opensAt).toBeNull()
    expect(openingHours.monday.closesAt).toBeNull()

    expect(openingHours.tuesday.isOpen).toBeFalsy()
    expect(openingHours.tuesday.opensAt).toBeNull()
    expect(openingHours.tuesday.closesAt).toBeNull()

    expect(openingHours.wednesday.isOpen).toBeFalsy()
    expect(openingHours.wednesday.opensAt).toBeNull()
    expect(openingHours.wednesday.closesAt).toBeNull()

    expect(openingHours.thursday.isOpen).toBeFalsy()
    expect(openingHours.thursday.opensAt).toBeNull()
    expect(openingHours.thursday.closesAt).toBeNull()

    expect(openingHours.friday.isOpen).toBeFalsy()
    expect(openingHours.friday.opensAt).toBeNull()
    expect(openingHours.friday.closesAt).toBeNull()

    expect(openingHours.saturday.isOpen).toBeFalsy()
    expect(openingHours.saturday.opensAt).toBeNull()
    expect(openingHours.saturday.closesAt).toBeNull()

    expect(openingHours.sunday.isOpen).toBeFalsy()
    expect(openingHours.sunday.opensAt).toBeNull()
    expect(openingHours.sunday.closesAt).toBeNull()
  })

  it('should accept CLOSED and mark day as closed', () => {
    const openingHoursRaw = 'MON 10:00-22:00~~SUN CLOSED'
    const openingHours = formatOpeningHours(openingHoursRaw)
    expect(openingHours).toBeDefined()
    expect(openingHours.raw).toStrictEqual(openingHoursRaw)
    expect(openingHours.monday.isOpen).toBeTruthy()
    expect(openingHours.monday.opensAt).toEqual('10:00')
    expect(openingHours.monday.closesAt).toEqual('22:00')

    expect(openingHours.tuesday.isOpen).toBeFalsy()
    expect(openingHours.tuesday.opensAt).toBeNull()
    expect(openingHours.tuesday.closesAt).toBeNull()

    expect(openingHours.wednesday.isOpen).toBeFalsy()
    expect(openingHours.wednesday.opensAt).toBeNull()
    expect(openingHours.wednesday.closesAt).toBeNull()

    expect(openingHours.thursday.isOpen).toBeFalsy()
    expect(openingHours.thursday.opensAt).toBeNull()
    expect(openingHours.thursday.closesAt).toBeNull()

    expect(openingHours.friday.isOpen).toBeFalsy()
    expect(openingHours.friday.opensAt).toBeNull()
    expect(openingHours.friday.closesAt).toBeNull()

    expect(openingHours.saturday.isOpen).toBeFalsy()
    expect(openingHours.saturday.opensAt).toBeNull()
    expect(openingHours.saturday.closesAt).toBeNull()

    expect(openingHours.sunday.isOpen).toBeFalsy()
    expect(openingHours.sunday.opensAt).toBeNull()
    expect(openingHours.sunday.closesAt).toBeNull()
  })
})
