/* global describe it expect */

const {
  encodeSearchTerm,
  calculateSizeValue,
  sortSizes,
  calculateSalesPercentage,
} = require('../../libs/fredHopper')

describe('encodeSearchTerm', () => {
  it('should be defined', () => {
    expect(encodeSearchTerm).toBeDefined()
  })

  it('should not fail if no search term is present', () => {
    const result = encodeSearchTerm()
    expect(result).toBe('')
  })

  it('should encode FH delimiters', () => {
    const searchTerm = '@ !/"#'
    const result = encodeSearchTerm(searchTerm)
    expect(result).toBeDefined()
    expect(result).toBe('\\u0040%20\\u0021\\u002f\\u0022\\u0023')
  })

  it('should not encode valid characters', () => {
    const searchTerm = 'ABC123abc'
    const result = encodeSearchTerm(searchTerm)
    expect(result).toBeDefined()
    expect(result).toBe('ABC123abc')
  })

  it('should work for mixed strings', () => {
    const searchTerm = 'ABC 123'
    const result = encodeSearchTerm(searchTerm)
    expect(result).toBeDefined()
    expect(result).toBe('ABC%20123')
  })

  it('should work for weird characters with accents and stuff', () => {
    const searchTerm = 'áäâàæãåāéëêèęėēúüûùūíïìîįīöóôòõœøō'
    const result = encodeSearchTerm(searchTerm)
    expect(result).toBeDefined()
    expect(result).toBe('%C3%A1%C3%A4%C3%A2%C3%A0%C3%A6%C3%A3%C3%A5%C4%81%C3%A9%C3%AB%C3%AA%C3%A8%C4%99%C4%97%C4%93%C3%BA%C3%BC%C3%BB%C3%B9%C5%AB%C3%AD%C3%AF%C3%AC%C3%AE%C4%AF%C4%AB%C3%B6%C3%B3%C3%B4%C3%B2%C3%B5%C5%93%C3%B8%C5%8D')
  })
})

describe('calculateSizeValue', () => {
  it('should be defined', () => {
    expect(calculateSizeValue).toBeDefined()
  })

  it('should return 999999999 if no size is present', () => {
    expect(calculateSizeValue('')).toBe(999999999)
  })

  it('should return the correct shirt size index', () => {
    // const shirtSizes = ['One Size', 'XXS', 'X/S', 'S', 'M', 'S-M', 'L', 'XL', 'L-XL', 'XL+', '2XL', '3XL', '4XL', '5XL', 'XXL', 'XXXL']
    let size = 'One Size'
    expect(calculateSizeValue(size)).toBe(0)

    size = 'XXS'
    expect(calculateSizeValue(size)).toBe(1)

    size = 'XS'
    expect(calculateSizeValue(size)).toBe(2)

    size = 'XXXL'
    expect(calculateSizeValue(size)).toBe(16)
  })

  it('should return correct bebe sizes', () => {
    let size = '12months'
    expect(calculateSizeValue(size)).toBe(11200)

    size = '1yrs'
    expect(calculateSizeValue(size)).toBe(11200)

    size = '18months'
    expect(calculateSizeValue(size)).toBe(11800)

    size = '12-18months'
    expect(calculateSizeValue(size)).toBe(11218)

    size = 'abcmonths'
    expect(calculateSizeValue(size)).toBe(999999999)
  })

  it('should return country/short/reg/long sizes correctly', () => {
    let size = 'UK EU FR IT cm in CM IN 42short'
    expect(calculateSizeValue(size)).toBe(242000000)

    size = '42reg'
    expect(calculateSizeValue(size)).toBe(342000000)

    size = '42long'
    expect(calculateSizeValue(size)).toBe(442000000)

    size = '42'
    expect(calculateSizeValue(size)).toBe(542000000)

    size = '36A-48A'
    expect(calculateSizeValue(size)).toBe(536014801)
  })

  it('should return other sizes correctly', () => {
    let size = 'UK4'
    expect(calculateSizeValue(size)).toBe(504000000)

    size = 'UK4.5'
    expect(calculateSizeValue(size)).toBe(504050000)

    size = 'EU90cm'
    expect(calculateSizeValue(size)).toBe(590000000)

    size = 'EU110cm'
    expect(calculateSizeValue(size)).toBe(5110000000)
  })
})

describe('sortSizes', () => {
  it('should be defined', () => {
    expect(sortSizes).toBeDefined()
  })

  it('should sort sizes correctly', () => {
    const sizes = [
      { size: { value: 'FR56reg' } },
      { size: { value: 'One Size' } },
      { size: { value: 'M' } },
      { size: { value: '42A' } },
      { size: { value: '12months' } },
      { size: { value: 'FR46reg' } },
    ]

    const sortedSizes = [
      { size: { value: 'One Size' } },
      { size: { value: 'M' } },
      { size: { value: '12months' } },
      { size: { value: 'FR46reg' } },
      { size: { value: 'FR56reg' } },
      { size: { value: '42A' } },
    ]

    const res = sortSizes(sizes)

    expect(res).toStrictEqual(sortedSizes)
  })

  it('should sort other sizes correctly', () => {
    const sizes = [
      { size: { value: 'L-XL' } },
      { size: { value: 'S-M' } },
    ]

    const sortedSizes = [
      { size: { value: 'S-M' } },
      { size: { value: 'L-XL' } },
    ]

    const res = sortSizes(sizes)

    expect(res).toStrictEqual(sortedSizes)
  })

  it('should sort other sizes correctly', () => {
    const sizes = [
      { size: { value: '9months' } },
      { size: { value: '3yrs' } },
      { size: { value: '4yrs' } },
      { size: { value: '12months' } },
      { size: { value: '1-1.5yrs' } },
      { size: { value: '1.5-2yrs' } },
    ]

    const sortedSizes = [
      { size: { value: '9months' } },
      { size: { value: '12months' } },
      { size: { value: '1-1.5yrs' } },
      { size: { value: '1.5-2yrs' } },
      { size: { value: '3yrs' } },
      { size: { value: '4yrs' } },
    ]

    const res = sortSizes(sizes)

    expect(res).toStrictEqual(sortedSizes)
  })

  it('should sort shoe sizes correctly', () => {
    const sizes = [
      { size: { value: 'UK4' } },
      { size: { value: 'UK5' } },
      { size: { value: 'UK6' } },
      { size: { value: 'UK7' } },
      { size: { value: 'UK7.5' } },
      { size: { value: 'UK6.5' } },
      { size: { value: 'UK3.5' } },
    ]

    const sortedSizes = [
      { size: { value: 'UK3.5' } },
      { size: { value: 'UK4' } },
      { size: { value: 'UK5' } },
      { size: { value: 'UK6' } },
      { size: { value: 'UK6.5' } },
      { size: { value: 'UK7' } },
      { size: { value: 'UK7.5' } },
    ]

    const res = sortSizes(sizes)

    expect(res).toStrictEqual(sortedSizes)
  })

  it('should sort belt sizes correctly', () => {
    const sizes = [
      { size: { value: 'UK34in' } },
      { size: { value: 'UK32in' } },
      { size: { value: 'UK30in' } },
      { size: { value: 'UK38in' } },
      { size: { value: 'UK36in' } },
      { size: { value: 'UK40in' } },
    ]

    const sortedSizes = [
      { size: { value: 'UK30in' } },
      { size: { value: 'UK32in' } },
      { size: { value: 'UK34in' } },
      { size: { value: 'UK36in' } },
      { size: { value: 'UK38in' } },
      { size: { value: 'UK40in' } },
    ]

    const res = sortSizes(sizes)

    expect(res).toStrictEqual(sortedSizes)
  })

  it('should sort other belt sizes correctly', () => {
    const sizes = [
      { size: { value: 'EU80cm' } },
      { size: { value: 'EU85cm' } },
      { size: { value: 'EU90cm' } },
      { size: { value: 'EU95cm' } },
      { size: { value: 'EU100cm' } },
      { size: { value: 'EU105cm' } },
      { size: { value: 'EU110cm' } },
    ]

    const sortedSizes = [
      { size: { value: 'EU80cm' } },
      { size: { value: 'EU85cm' } },
      { size: { value: 'EU90cm' } },
      { size: { value: 'EU95cm' } },
      { size: { value: 'EU100cm' } },
      { size: { value: 'EU105cm' } },
      { size: { value: 'EU110cm' } },
    ]

    const res = sortSizes(sizes)

    expect(res).toStrictEqual(sortedSizes)
  })
})

describe('calculateSalesPercentage', () => {
  it('should be defined', () => {
    expect(calculateSalesPercentage).toBeDefined()
  })

  it('should throw error if either param is undefined', () => {
    const toThrow = () => calculateSalesPercentage(undefined, undefined)
    expect(toThrow).toThrowError()
  })

  it('should return discount rounded to 5% increments', () => {
    let wasPrice = 100
    let price = 10
    let salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-90%`)

    wasPrice = 100
    price = 10.000001
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-90%`)

    wasPrice = 100
    price = 15
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-85%`)

    wasPrice = 100
    price = 16
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-80%`)

    wasPrice = 100
    price = 17
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-80%`)

    wasPrice = 100
    price = 18
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-80%`)

    wasPrice = 100
    price = 19
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-80%`)

    wasPrice = 100
    price = 20
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-80%`)

    wasPrice = 100
    price = 25
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-75%`)

    wasPrice = 100
    price = 30
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-70%`)

    wasPrice = 100
    price = 35
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-65%`)

    wasPrice = 100
    price = 40
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-60%`)

    wasPrice = 100
    price = 45
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-55%`)

    wasPrice = 100
    price = 50
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-50%`)

    wasPrice = 100
    price = 55
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-45%`)

    wasPrice = 100
    price = 60
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-40%`)

    wasPrice = 100
    price = 65
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-35%`)

    wasPrice = 100
    price = 70
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-30%`)

    wasPrice = 100
    price = 75
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-25%`)

    wasPrice = 100
    price = 80
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-20%`)

    wasPrice = 100
    price = 85
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-15%`)

    wasPrice = 100
    price = 90
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-10%`)

    wasPrice = 100
    price = 95
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual(`-5%`)

    wasPrice = 375
    price = 187
    salesPercentage = calculateSalesPercentage(price, wasPrice)
    expect(salesPercentage).toEqual('-50%')
  })
})
