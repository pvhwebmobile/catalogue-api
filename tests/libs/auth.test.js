/* global jest describe it expect */

const config = require('config')
const {
  basicAuth,
  bearerAuth,
  headerBasicAuthMiddleware,
  headerBearerAuthMiddleware,
} = require('../../libs/auth')

describe('basicAuth', () => {
  it('should be defined', () => {
    expect(basicAuth).toBeDefined()
  })

  it('should return true if match', () => {
    const userPass = 'eisAdmin:ThePasswordIsPassword:)2019'
    const res = basicAuth(userPass, 'eisAdmin', 'ThePasswordIsPassword:)2019')
    expect(res).toBeTruthy()
  })

  it('should return false if no match', () => {
    const userPass = 'not:correct'
    const res = basicAuth(userPass, 'is', 'correct')
    expect(res).toBeFalsy()
  })
})

describe('bearerAuth', () => {
  it('should be defined', () => {
    expect(bearerAuth).toBeDefined()
  })

  it('should return true if token verified by secret', () => {
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.2Uu_keSWxpdc1FAQJzD7BzPLv2Sfmnuz_Kci0Cd_E_I'
    const secret = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

    const res = bearerAuth(token, secret)
    expect(res).toBeTruthy()
  })

  it('should return false if token not verified by secret', () => {
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.2Uu_keSWxpdc1FAQJzD7BzPLv2Sfmnuz_Kci0Cd_E_I'
    const secret = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123'

    const res = bearerAuth(token, secret)
    expect(res).toBeFalsy()
  })
})

describe('headerBasicAuthMiddleware', () => {
  it('should be defined', () => {
    expect(headerBasicAuthMiddleware).toBeDefined()
  })

  it('should throw error with invalid auth header', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const ctx = { header: { authorization: 'Fake token' }, throw: thrw }

    headerBasicAuthMiddleware(ctx, next)

    expect(next).toBeCalledTimes(0)
    expect(thrw).toBeCalledTimes(1)
    expect(thrw).toBeCalledWith(401, expect.stringMatching(/^Authorization Error.*$/))
  })

  it('should throw error with invalid auth', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const ctx = { header: { authorization: 'Basic fakeToken' }, throw: thrw }

    headerBasicAuthMiddleware(ctx, next)

    expect(next).toBeCalledTimes(0)
    expect(thrw).toBeCalledTimes(1)
    expect(thrw).toBeCalledWith(401, expect.stringMatching(/^Authorization Error.*$/))
  })

  it('should call next() with valid auth', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const ctx = { header: { authorization: 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5' }, throw: thrw }

    headerBasicAuthMiddleware(ctx, next)

    expect(next).toBeCalledTimes(1)
    expect(thrw).toBeCalledTimes(0)
  })
})

describe('headerBearerAuthMiddleware', () => {
  it('should be defined', () => {
    expect(headerBearerAuthMiddleware).toBeDefined()
  })

  it('should throw error with invalid auth header', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const ctx = { header: { authorization: 'Fake token' }, throw: thrw }

    headerBearerAuthMiddleware(ctx, next)

    expect(next).toBeCalledTimes(0)
    expect(thrw).toBeCalledTimes(1)
    expect(thrw).toBeCalledWith(401, expect.stringMatching(/^Authorization Error.*$/))
  })

  it('should throw error with invalid auth', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const ctx = { header: { authorization: 'Bearer fakeToken' }, throw: thrw }

    headerBearerAuthMiddleware(ctx, next)

    expect(next).toBeCalledTimes(0)
    expect(thrw).toBeCalledTimes(1)
    expect(thrw).toBeCalledWith(401, expect.stringMatching(/^Authorization Error.*$/))
  })

  it('should call next() with valid auth', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const ctx = { header: { authorization: `Bearer ${config.TOKEN}` }, throw: thrw }

    headerBearerAuthMiddleware(ctx, next)

    expect(next).toBeCalledTimes(1)
    expect(thrw).toBeCalledTimes(0)
  })
})
