/* global describe it expect */

const joi = require('@hapi/joi')
const {
  validateBody,
} = require('../../libs/bodyValidator')

describe('validateBody', () => {
  it('should be defined', () => {
    expect(validateBody).toBeDefined()
  })

  it('requires a body and schema', () => {
    expect(() => validateBody()).toThrow()
    expect(() => validateBody({})).toThrow()
    expect(() => validateBody(null, {})).toThrow()
  })

  it('should return value when valid', () => {
    const body = {
      price: 1234.56,
      quantity: 1,
    }

    const schema = {
      price: joi.number(),
      quantity: joi.number(),
    }

    const value = validateBody(body, schema)
    expect(value).toBeDefined()
  })

  it('should return false when invalid', () => {
    const body = {
      price: 'ABC',
      quantity: 1,
    }

    const schema = {
      price: joi.number(),
      quantity: joi.number(),
    }

    const value = validateBody(body, schema)
    expect(value).toBeFalsy()
  })
})
