/* global describe it expect */

const {
  parseAuthorizationBasicHeader,
  parseAuthorizationBearerHeader,
  parseAuthorizationHeader,
  parseUserAgentHeader,
  parseHeader,
} = require('../../libs/headers')

describe('parseAuthorizationBasicHeader', () => {
  it('should be defined', () => {
    expect(parseAuthorizationBasicHeader).toBeDefined()
  })

  it('should return undefined if no header is supplied', () => {
    let res = parseAuthorizationBasicHeader(undefined)
    expect(res).toBeUndefined()

    res = parseAuthorizationBasicHeader(null)
    expect(res).toBeUndefined()
  })

  it('should return undefined if header doesn\'t have <type> <token> format',
    () => {
      let header = 'Basicnospacehere'
      let res = parseAuthorizationBasicHeader(header)
      expect(res).toBeUndefined()

      header = 'Basic two space'
      res = parseAuthorizationBasicHeader(header)
      expect(res).toBeUndefined()
    })

  it('should return undefined if type !== Basic', () => {
    const header = 'Thicc abcd1234'
    const res = parseAuthorizationBasicHeader(header)
    expect(res).toBeUndefined()
  })

  it('should return ascii value of token', () => {
    const header = 'Basic dXNlcjpwYXNz'
    const res = parseAuthorizationBasicHeader(header)
    expect(res).toBe('user:pass')
  })
})

describe('parseAuthorizationBearerHeader', () => {
  it('should be defined', () => {
    expect(parseAuthorizationBearerHeader).toBeDefined()
  })

  it('should return undefined when header is missing', () => {
    let header
    let res = parseAuthorizationBearerHeader(header)
    expect(res).toBeUndefined()

    header = null
    res = parseAuthorizationBearerHeader(header)
    expect(res).toBeUndefined()
  })

  it('should return undefined if not in <type> <token> format', () => {
    let header = 'BearerNoSpace'
    let res = parseAuthorizationBearerHeader(header)
    expect(res).toBeUndefined()

    header = 'Bearer three space'
    res = parseAuthorizationBearerHeader(header)
    expect(res).toBeUndefined()
  })

  it('should return undefined it type is not Bearer', () => {
    const header = 'Bear token'
    const res = parseAuthorizationBearerHeader(header)
    expect(res).toBeUndefined()
  })

  it('should return token', () => {
    const header = 'Bearer 12345abcde'
    const res = parseAuthorizationBearerHeader(header)
    expect(res).toBe('12345abcde')
  })
})

describe('parseAuthorizationHeader', () => {
  it('should be defined', () => {
    expect(parseAuthorizationHeader).toBeDefined()
  })

  it('should return undefined if ctx has no headers', () => {
    const ctx = {}
    const type = 'bearer'

    const res = parseAuthorizationHeader(ctx, type)
    expect(res).toBeUndefined()
  })

  it('should return undefined if ctx has no authorization header', () => {
    const ctx = { header: { fakeheader: '213' } }
    const type = 'bearer'

    const res = parseAuthorizationHeader(ctx, type)
    expect(res).toBeUndefined()
  })

  it('should return token from basic auth', () => {
    const ctx = { header: { authorization: 'Basic dXNlcjpwYXNz' } }
    const type = 'basic'

    const res = parseAuthorizationHeader(ctx, type)
    expect(res).toBe('user:pass')
  })

  it('should return token from bearer auth', () => {
    const ctx = { header: { authorization: 'Bearer dXNlcjpwYXNz' } }
    const type = 'bearer'

    const res = parseAuthorizationHeader(ctx, type)
    expect(res).toBe('dXNlcjpwYXNz')
  })

  it('should return undefined from invalid type', () => {
    const ctx = { header: { authorization: 'Bearer dXNlcjpwYXNz' } }
    const type = 'fake'

    const res = parseAuthorizationHeader(ctx, type)
    expect(res).toBeUndefined()
  })
})

describe('parseUserAgentHeader', () => {
  it('should be defined', () => {
    expect(parseUserAgentHeader).toBeDefined()
  })

  it('should return undefined if ctx has no headers', () => {
    const ctx = {}

    const res = parseUserAgentHeader(ctx)
    expect(res).toBeUndefined()
  })

  it('should return undefined if ctx has no user-agent header', () => {
    const ctx = { header: { notReal: 'lel' } }

    const res = parseUserAgentHeader(ctx)
    expect(res).toBeUndefined()
  })

  it('should return user-agent value', () => {
    const ctx = { header: { 'user-agent': 'iTommy' } }

    const res = parseUserAgentHeader(ctx)
    expect(res).toEqual('iTommy')
  })
})

describe('parseHeader', () => {
  it('should be defined', () => {
    expect(parseHeader).toBeDefined()
  })

  it('should return undefined if ctx has no headers', () => {
    const ctx = {}

    const res = parseHeader(ctx, 'key')
    expect(res).toBeUndefined()
  })

  it('should return undefined if ctx has no `key` header', () => {
    const ctx = { header: { notReal: 'lel' } }

    const res = parseHeader(ctx, 'key')
    expect(res).toBeUndefined()
  })

  it('should return `key` value', () => {
    const ctx = { header: { key: 'value' } }

    const res = parseHeader(ctx, 'key')
    expect(res).toEqual('value')
  })
})
