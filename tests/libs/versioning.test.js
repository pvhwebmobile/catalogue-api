/* global jest describe it expect */

const config = require('config')
const {
  clientVersionRequestMiddleware,
  serverVersionRequestMiddleware,
  versionCheck,
} = require('../../libs/versioning')

describe('clientVersionRequestMiddleware', () => {
  it('should be defined', () => {
    expect(clientVersionRequestMiddleware).toBeDefined()
  })

  it('should parse headers for accept-version and add acceptVersion to ctx.state', async () => {
    const ctx = {
      request: {
        headers: {
          'accept-version': '2',
        },
      },
      state: {},
    }

    const next = jest.fn()

    await clientVersionRequestMiddleware(ctx, next)

    expect(ctx.state.acceptVersion).toBe('2.0.0')
  })

  it('should parse headers for accept-version and add minimumVersion to ctx.state when unsupported version is provided', async () => {
    const ctx = {
      request: {
        headers: {
          'accept-version': '9001',
        },
      },
      state: {},
    }

    const next = jest.fn()

    await clientVersionRequestMiddleware(ctx, next)

    expect(ctx.state.acceptVersion).toBe(config.MINIMUM_VERSION)
  })

  it('should parse headers for accept-version and add minimumVersion to ctx.state when no version is provided', async () => {
    const ctx = {
      request: {
        headers: {},
      },
      state: {},
    }

    const next = jest.fn()

    await clientVersionRequestMiddleware(ctx, next)

    expect(ctx.state.acceptVersion).toBe(config.MINIMUM_VERSION)
  })
})

describe('serverVersionRequestMiddleware', () => {
  it('should be defined', () => {
    expect(serverVersionRequestMiddleware).toBeDefined()
  })

  it('should retrieve config variable for SERVER_VERSION', async () => {
    config.SERVER_VERSION = '3'
    const setFn = jest.fn()
    const ctx = { state: {}, header: {}, set: setFn }
    const next = jest.fn()

    await serverVersionRequestMiddleware(ctx, next)

    expect(setFn).toHaveBeenCalledTimes(1)
    expect(ctx.state.serverVersion).toBe('3.0.0')
  })

  it('should retrieve config variable for SERVER_VERSION', async () => {
    config.SERVER_VERSION = '1'
    const setFn = jest.fn()
    const ctx = { state: {}, header: {}, set: setFn }
    const next = jest.fn()

    await serverVersionRequestMiddleware(ctx, next)

    expect(setFn).toHaveBeenCalledTimes(1)
    expect(ctx.state.serverVersion).toBe('1.0.0')
  })
})

describe('versionCheck', () => {
  it('should be defined', () => {
    expect(versionCheck).toBeDefined()
  })

  it('should return success status', () => {
    const requestedVersion = '1'
    const allowedVersions = ['1.0.0', '1.2.3']
    const success = versionCheck(requestedVersion, allowedVersions)

    expect(success).toBeTruthy()
  })

  it('should return unsuccess status', () => {
    const requestedVersion = '1.2.5'
    const allowedVersions = ['1.0.0', '1.2.3']
    const success = versionCheck(requestedVersion, allowedVersions)

    expect(success).toBeFalsy()
  })
})
