/* global jest describe it expect */

const { userAgentMiddleware } = require('../../libs/userAgent')

describe('userAgentMiddleware', () => {
  it('should be defined', () => {
    expect(userAgentMiddleware).toBeDefined()
  })

  it('should set ctx.state.user-agent', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const set = jest.fn()
    const ctx = { state: {}, header: { 'user-agent': 'tester' }, throw: thrw, set: set }

    userAgentMiddleware(ctx, next)

    expect(ctx.state.userAgent).toEqual('tester')

    expect(next).toBeCalledTimes(1)
    expect(thrw).toBeCalledTimes(0)
    expect(set).toBeCalledTimes(1)
    expect(set).toBeCalledWith('X-User-Agent', 'tester')
  })

  it('should set ctx.state.user-agent to anonymous if none provided', () => {
    const next = jest.fn()
    const thrw = jest.fn()
    const set = jest.fn()
    const ctx = { state: {}, header: {}, throw: thrw, set: set }

    userAgentMiddleware(ctx, next)

    expect(ctx.state.userAgent).toEqual('anonymouse <:3)~~')

    expect(next).toBeCalledTimes(1)
    expect(thrw).toBeCalledTimes(0)
    expect(set).toBeCalledTimes(1)
    expect(set).toBeCalledWith('X-User-Agent', 'anonymouse <:3)~~')
  })
})
