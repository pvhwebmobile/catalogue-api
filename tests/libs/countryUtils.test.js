/* global describe it expect */

const {
  countryCodeFromName,
  nameFromCountryCode,
  langIdFromLanguageId,
  languageFromLanguageId,
  langIdFromCountryCode,
} = require('../../libs/countryUtils')

describe('countryCodeFromName', () => {
  it('should be defined', () => {
    expect(countryCodeFromName).toBeDefined()
  })

  it('should return countryCode from country name', () => {
    let name = 'United Kingdom'
    let countryCode = countryCodeFromName(name)

    expect(countryCode).toBeDefined()
    expect(countryCode).toEqual('UK')
  })

  it('should return name from countryCode', () => {
    let countryCode = 'UK'
    let name = nameFromCountryCode(countryCode)

    expect(name).toBeDefined()
    expect(name).toEqual('United Kingdom')
  })

  it('should return undefined for missing name', () => {
    let name = 'Fake & Fake'
    let countryCode = countryCodeFromName(name)

    expect(countryCode).toBeUndefined()
  })
})

describe('nameFromCountryCode', () => {
  it('should return name from countryCode', () => {
    let countryCode = 'UK'
    let name = nameFromCountryCode(countryCode)

    expect(name).toBeDefined()
    expect(name).toEqual('United Kingdom')
  })

  it('should return undefined for missing countryCode', () => {
    let countryCode = 'Fake & Fake'
    let name = nameFromCountryCode(countryCode)

    expect(name).toBeUndefined()
  })
})

describe('langIdFromLanguageId', () => {
  it('should be defined', () => {
    expect(langIdFromLanguageId).toBeDefined()
  })

  it('should return langId for valid language', () => {
    let language = 'Nederlands'
    expect(langIdFromLanguageId(language)).toBe('31')
  })

  it('should return undefined for invalid countries', () => {
    let language = 'XX'
    expect(langIdFromLanguageId(language)).toBeUndefined()
  })
})

describe('languageFromLanguageId', () => {
  it('should be defined', () => {
    expect(languageFromLanguageId).toBeDefined()
  })

  it('should return language for valid countries', () => {
    let language = 'Nederlands'
    expect(languageFromLanguageId(language)).toBe('nl')
  })

  it('should return undefined for invalid countries', () => {
    let language = 'XX'
    expect(languageFromLanguageId(language)).toBeUndefined()
  })
})

describe('langIdFromCountryCode', () => {
  it('should be defined', () => {
    expect(langIdFromCountryCode).toBeDefined()
  })

  it('should return langId for valid countryCodes', () => {
    const countryCodes = ['nl', 'uk', 'ie', 'de', 'es']
    const result = countryCodes.map((v) => langIdFromCountryCode(v))
    expect(result).toBeDefined()
    expect(result.length).toBe(5)
    expect(result[0]).toEqual('31')
    expect(result[1]).toEqual('44')
    expect(result[2]).toEqual('44')
    expect(result[3]).toEqual('-3')
    expect(result[4]).toEqual('-5')
  })

  it('should return undefined for invalid countryCodes', () => {
    expect(langIdFromCountryCode('xx')).toBeUndefined()
  })
})
