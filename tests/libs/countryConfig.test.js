/* global describe it expect */

const { getCountry, getCountryProperties } = require('../../libs/countryConfig')

describe('getCountry', () => {
  it('should be defined', () => {
    expect(getCountry).toBeDefined()
  })

  it('should return country for valid tenant and countryCode', () => {
    const tenant = 'th'
    const countryCode = 'nl'

    const country = getCountry(tenant, countryCode)

    expect(country).toBeDefined()
  })

  it('should throw error for unsupported tenant', () => {
    const tenant = 'unsupported'
    const countryCode = 'nl'

    expect(() => getCountry(tenant, countryCode)).toThrow(/Tenant UNSUPPORTED not supported./)
  })

  it('should throw error for unsupported countryCode', () => {
    const tenant = 'th'
    const countryCode = 'unsupported'

    expect(() => getCountry(tenant, countryCode)).toThrow(/Country UNSUPPORTED not supported./)
  })
})

describe('getCountryProperties', () => {
  it('should be defined', () => {
    expect(getCountryProperties).toBeDefined()
  })

  it('should return property if present on country', () => {
    const tenant = 'th'
    const countryCode = 'nl'
    const properties = ['langId']

    const { langId } = getCountryProperties(tenant, countryCode, properties)
    expect(langId).toBeDefined()
    expect(langId).toBe('31')
  })

  it('should return properties if present on country', () => {
    const tenant = 'th'
    const countryCode = 'nl'
    const properties = ['langId', 'catalogId']

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, properties)
    expect(langId).toBeDefined()
    expect(catalogId).toBeDefined()
    expect(langId).toBe('31')
    expect(catalogId).toBe('10158')
  })

  it('should throw error if no properties provided', () => {
    const tenant = 'th'
    const countryCode = 'nl'
    const properties = null

    expect(() => getCountryProperties(tenant, countryCode, properties)).toThrow(/Properties not defined. Call 'getCountry' instead./)
  })

  it('should throw error if property is not found in country', () => {
    const tenant = 'th'
    const countryCode = 'nl'
    const properties = ['fake']

    expect(() => getCountryProperties(tenant, countryCode, properties)).toThrow(/Property fake not found./)
  })
})
