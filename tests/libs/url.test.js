/* global describe it expect */

const config = require('config')
const { buildURL, validateURL } = require('../../libs/url')

describe('buildURL', () => {
  it('should be defined', () => {
    expect(buildURL).toBeDefined()
  })

  it('should throw error for unsupported type', () => {
    const urlObject = {
      type: 'UNSUPPORTED',
      url: 'TEST',
      parameters: [],
    }
    const parameters = {}

    expect(() => buildURL(urlObject, parameters)).toThrow(/Unsupported URL type UNSUPPORTED provided./)
  })

  it('should throw error for invalid urlObject', () => {
    const urlObject = {
      type: 'UNSUPPORTED',
    }
    const parameters = {}
    expect(() => buildURL(urlObject, parameters)).toThrow(/Invalid URL object./)
  })

  it('should replace CC with lowercased countryCode', () => {
    const urlObject = {
      type: 'WCS',
      url: 'BASE_URL',
      parameters: [],
    }
    const parameters = {}
    const tenant = 'TH'
    const countryCode = 'DE'

    const url = buildURL(urlObject, parameters, tenant, countryCode)
    expect(url).toBe('https://de.systestp.tommy.com')
  })

  it('should use tenant "th" if none is provided', () => {
    const urlObject = {
      type: 'WCS',
      url: 'BASE_URL',
      parameters: [],
    }
    const parameters = {}

    const url = buildURL(urlObject, parameters)
    expect(url).toBe('https://nl.systestp.tommy.com')
  })

  it('should use countryCode "nl" if none is provided', () => {
    const urlObject = {
      type: 'WCS',
      url: 'BASE_URL',
      parameters: [],
    }
    const parameters = {}
    const tenant = 'TH'

    const url = buildURL(urlObject, parameters, tenant)
    expect(url).toBe('https://nl.systestp.tommy.com')
  })

  it('should switch to CK WCS url for tenant CK', () => {
    const urlObject = {
      type: 'WCS',
      url: 'BASE_URL',
      parameters: [],
    }
    const parameters = {}
    const tenant = 'CK'

    const url = buildURL(urlObject, parameters, tenant)
    expect(url).toBe('https://systestp.calvinklein.nl')
  })

  it('has valid config for URLS', () => {
    const urls = config.URLS
    for (let key in urls) {
      const urlObject = urls[key]
      expect(urlObject).toBeDefined()
      const { type, url, parameters } = urlObject
      expect(type).toBeDefined()
      expect(url).toBeDefined()
      expect(parameters).toBeDefined()
    }
  })

  describe('WCS', () => {
    it('should return valid url with required parameters', () => {
      const urlObject = {
        type: 'WCS',
        url: 'BASE_URL/test/PARAM_1/PARAM_2',
        parameters: [
          { name: 'PARAM_1', required: true },
          { name: 'PARAM_2', required: true },
        ],
      }
      const parameters = {
        PARAM_1: 'a',
        PARAM_2: 1,
      }

      const url = buildURL(urlObject, parameters)
      expect(url).toBe('https://nl.systestp.tommy.com/test/a/1')
    })
    it('should throw error if required parameter is not provided', () => {
      const urlObject = {
        type: 'WCS',
        url: 'BASE_URL/test/PARAM_1/PARAM_2',
        parameters: [
          { name: 'PARAM_1', required: true },
          { name: 'PARAM_2', required: true },
        ],
      }
      const parameters = {
        PARAM_1: 'a',
      }

      expect(() => buildURL(urlObject, parameters)).toThrow(/Missing required parameter PARAM_2./)
    })
    it('should not throw error if nonrequired parameter is not provided', () => {
      const urlObject = {
        type: 'WCS',
        url: 'BASE_URL/test/PARAM_1/PARAM_2',
        parameters: [
          { name: 'PARAM_1', required: true },
          { name: 'PARAM_2', required: false },
        ],
      }
      const parameters = {
        PARAM_1: 'a',
      }

      expect(() => buildURL(urlObject, parameters)).not.toThrow()
    })
  })

  describe('SOLR', () => {
    it('should return valid url with required parameters', () => {
      const urlObject = {
        type: 'SOLR',
        url: 'BASE_URL/test/PARAM_1/PARAM_2',
        parameters: [
          { name: 'PARAM_1', required: true },
          { name: 'PARAM_2', required: true },
        ],
      }
      const parameters = {
        PARAM_1: 'a',
        PARAM_2: 1,
      }

      const url = buildURL(urlObject, parameters)
      expect(url).toBe('http://52.18.130.121:9080/test/a/1')
    })
    it('should throw error if required parameter is not provided', () => {
      const urlObject = {
        type: 'SOLR',
        url: 'BASE_URL/test/PARAM_1/PARAM_2',
        parameters: [
          { name: 'PARAM_1', required: true },
          { name: 'PARAM_2', required: true },
        ],
      }
      const parameters = {
        PARAM_1: 'a',
      }

      expect(() => buildURL(urlObject, parameters)).toThrow(/Missing required parameter PARAM_2./)
    })
    it('should not throw error if nonrequired parameter is not provided', () => {
      const urlObject = {
        type: 'SOLR',
        url: 'BASE_URL/test/PARAM_1/PARAM_2',
        parameters: [
          { name: 'PARAM_1', required: true },
          { name: 'PARAM_2', required: false },
        ],
      }
      const parameters = {
        PARAM_1: 'a',
      }

      expect(() => buildURL(urlObject, parameters)).not.toThrow()
    })
  })
})

describe('validateURL', () => {
  it('should be defined', () => {
    expect(validateURL).toBeDefined()
  })

  it('should return parsed url and query parameters from url', () => {
    const url = '/th/v2/test/123?queryParam=1'
    const urlParameters = [/^(th|ck)$/, /v2/, /test/, /\d+/]
    const queryParameters = {
      'queryParam': {
        pattern: /\d{1,2}/,
        required: true,
      },
    }

    const { urlParameters: urlParams, queryParameters: queryParams } = validateURL(url, urlParameters, queryParameters)

    expect(urlParams).toBeDefined()
    expect(queryParams).toBeDefined()

    expect(urlParams.length).toBe(4)
    expect(queryParams.length).toBe(1)
  })

  it('should throw error if url is null', () => {
    const url = null
    const urlParameters = [/^(th|ck)$/, /v2/, /test/, /\d+/]
    const queryParameters = {
      'queryParam': {
        pattern: /\d{1,2}/,
        required: true,
      },
    }

    expect(() => validateURL(url, urlParameters, queryParameters)).toThrow(/URL is malformed./)
  })

  it('should throw error if number of url parameters is incorrect', () => {
    const url = '/api/v2/test?queryParam=1'
    const urlParameters = [/api/, /v2/]
    const queryParameters = {
      'queryParam': {
        pattern: /\d{1,2}/,
        required: true,
      },
    }

    expect(() => validateURL(url, urlParameters, queryParameters))
      .toThrow(/URL has unexpected URL parameters./)
  })

  it('should throw error if number of query parameters is incorrect', () => {
    const url = '/api/v2/test?queryParam=1'
    const urlParameters = [/api/, /v2/, /test/]
    const queryParameters = {}

    expect(() => validateURL(url, urlParameters, queryParameters))
      .toThrow(/URL has unexpected query parameters./)
  })

  it('should throw error if URL parameter is not matching', () => {
    const url = '/api/v2/test?queryParam=1'
    const urlParameters = [/api/, /\d{2}/, /test/]
    const queryParameters = {
      'queryParam': {
        pattern: /\d{1,2}/,
        required: true,
      },
    }

    expect(() => validateURL(url, urlParameters, queryParameters))
      .toThrow(/URL parameter v2 does not match \/\\d\{2\}\/\./)
  })

  it('should not throw error if nonrequired query parameter is not found', () => {
    const url = '/api/v2/test'
    const urlParameters = [/api/, /v2/, /test/]
    const queryParameters = {
      'queryParam': {
        pattern: /\d{1,2}/,
        required: false,
      },
    }

    expect(() => validateURL(url, urlParameters, queryParameters))
      .not.toThrow()
  })

  it('should throw error if required query parameter is not found', () => {
    const url = '/api/v2/test'
    const urlParameters = [/api/, /v2/, /test/]
    const queryParameters = {
      'queryParam': {
        pattern: /\d{1,2}/,
        required: true,
      },
    }

    expect(() => validateURL(url, urlParameters, queryParameters))
      .toThrow(/Required query parameter queryParam was not found\./)
  })

  it('should throw error if query parameter value is not matching', () => {
    const url = '/api/v2/test?queryParam=x'
    const urlParameters = [/api/, /v2/, /test/]
    const queryParameters = {
      'queryParam': {
        pattern: /\d{1,2}/,
        required: true,
      },
    }

    expect(() => validateURL(url, urlParameters, queryParameters))
      .toThrow(/Query parameter queryParam does not match \/\\d\{1,2\}\/\./)
  })
})
