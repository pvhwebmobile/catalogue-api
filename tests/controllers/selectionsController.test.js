/* global describe it expect beforeAll afterAll */

const mongoose = require('mongoose')

const selectionModel = require('../../models/selection')
const selectionsController = require('../../controllers/selectionsController')
const {
  applyPromotionCode,
} = require('../../controllers/newSelectionsController')

beforeAll(async () => {
  mongoose.Promise = require('bluebird')
  mongoose.set('useFindAndModify', false)

  let options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    autoIndex: false, // Don't build indexes
    poolSize: 1, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
  }

  await mongoose.connect(global.__MONGO_URI__, options)

  await selectionModel.create({
    name: 'test order',
    status: 1,
    orderId: '123456',
    totalPrice: 49.99,
    shippingPrice: 3.90,
    currency: 'EUR',
    WCTrustedToken: 'abc123',
    WCToken: '123abc',
    storeId: '30006',
    tenant: 'TH',
    countryCode: 'DE',
    siteId: 'A235',
    sentToTillDate: new Date(),
    terminalId: 'tid',
    pspReference: 'pspReference123',
    merchantReference: 'merchantReference123',
    cancelReason: null,
    shippingModeId: null,
    hasPromotion: true,
    promotions: [{
      code: 'testPromo1',
      description: 'This is a test promotion',
      amount: -50.00,
      currency: 'EUR',
      language: '-3',
      descriptionLanguage: '-3',
    }],
    products: [{
      id: '81818',
      skuId: '1234567890987654321',
      styleId: '45',
      name: 'Cool Shirt',
      image: 'tommy.com/cool_shirt.png',
      amount: 1,
      originalUnitPrice: 99.99,
      promoUnitPrice: 49.99,
      originalTotalPrice: 99.99,
      promoTotalPrice: 49.99,
      size: '4',
      label: 'TH',
      colorName: 'Cool blue',
      colorPattern: 'Solid',
    }],
  })
})

afterAll(async () => {
  await mongoose.disconnect()
})

describe('getPromotionsFromWCSBasket', () => {
  it('should return list of promotions and hasPromotion=true when discount adjustments are present', () => {
    const wcsBasket = {
      'recordSetTotal': '2',
      'storeUniqueID': '30006',
      'x_isPersonalAddressesAllowedForShipping': 'true',
      'recordSetCount': '2',
      'totalSalesTaxCurrency': 'EUR',
      'promotionCode': [{
        'associatedPromotion': [{
          'promotionId': '725870395',
          'description': 'Free Standard Shipping DE',
        }],
        'code': 'SALMONPVHECOMINSTOREFREESHIPPING',
      }],
      'recordSetComplete': 'true',
      'orderItem': [{
        'orderItemId': '2305022',
        'unitQuantity': '1.0',
        'totalAdjustment': {
          'value': '-26.9000',
          'currency': 'EUR',
        },
        'unitPrice': '49.90000',
        'usableShippingChargePolicy': [{
          'name': 'StandardShippingChargeBySeller',
          'uniqueID': '-7001',
          'type': 'ShippingCharge',
        }],
        'currency': 'EUR',
        'availableDate': '2018-10-25T14:49:07.162Z',
        'orderItemInventoryStatus': 'Allocated',
        'fulfillmentCenterName': 'TommyHilfiger',
        'shippingTax': '0.00000',
        'adjustment': [{
          'amount': '-24.95000',
          'displayLevel': 'OrderItem',
          'description': 'Buy A and B and get 50% of on both Products',
          'usage': 'Discount',
          'language': '44',
          'code': 'Buy A and B and Get % on Both-725885385',
          'descriptionLanguage': '44',
          'currency': 'EUR',
        },
        {
          'amount': '-1.95000',
          'displayLevel': 'Order',
          'description': 'Free Shipping',
          'usage': 'Shipping',
          'language': '44',
          'code': 'Free Shipping',
          'descriptionLanguage': '44',
          'currency': 'EUR',
        },
        ],
        'description': 'DHL Standard',
        'orderItemStatus': 'P',
        'shippingChargeCurrency': 'EUR',
        'orderItemPrice': '49.90000',
        'carrier': '77859',
        'quantity': '1.0',
        'contractId': '4000000000000000569',
        'fulfillmentCenterId': '715838484',
        'createDate': '2018-10-25T14:49:06.423Z',
        'correlationGroup': '2305022',
        'salesTaxCurrency': 'EUR',
        'unitUom': 'C62',
        'salesTax': '0.00000',
        'partNumber': '8719256922050',
        'isExpedited': 'false',
        'freeGift': 'false',
        'xitem_isPersonalAddressesAllowedForShipping': 'true',
        'shipModeCode': 'DHL Standard',
        'orderItemFulfillmentStatus': 'Unreleased',
        'shipModeDescription': 'DHL Standard',
        'shippingCharge': '0.00000',
        'shipModeLanguage': '-3',
        'lastUpdateDate': '2018-10-25T14:49:07.162Z',
        'productId': '252775',
        'shippingTaxCurrency': 'EUR',
        'offerID': '4000000000065027779',
        'UOM': 'C62',
        'expectedShipDate': '2018-10-26T14:49:07.200Z',
        'shipModeId': '715839485',
        'language': '-3',
      },
      {
        'orderItemId': '2305023',
        'unitQuantity': '1.0',
        'totalAdjustment': {
          'value': '-26.90000',
          'currency': 'EUR',
        },
        'unitPrice': '49.90000',
        'usableShippingChargePolicy': [{
          'name': 'StandardShippingChargeBySeller',
          'uniqueID': '-7001',
          'type': 'ShippingCharge',
        }],
        'currency': 'EUR',
        'availableDate': '2018-10-25T14:49:07.164Z',
        'orderItemInventoryStatus': 'Allocated',
        'fulfillmentCenterName': 'TommyHilfiger',
        'shippingTax': '0.00000',
        'adjustment': [{
          'amount': '-24.95000',
          'displayLevel': 'OrderItem',
          'description': 'Buy A and B and get 50% of on both Products',
          'usage': 'Discount',
          'language': '44',
          'code': 'Buy A and B and Get % on Both-725885385',
          'descriptionLanguage': '44',
          'currency': 'EUR',
        },
        {
          'amount': '-1.95000',
          'displayLevel': 'Order',
          'description': 'Free Shipping',
          'usage': 'Shipping',
          'language': '44',
          'code': 'Free Shipping',
          'descriptionLanguage': '44',
          'currency': 'EUR',
        },
        ],
        'description': 'DHL Standard',
        'orderItemStatus': 'P',
        'shippingChargeCurrency': 'EUR',
        'orderItemPrice': '49.90000',
        'carrier': '77859',
        'quantity': '1.0',
        'contractId': '4000000000000000569',
        'fulfillmentCenterId': '715838484',
        'createDate': '2018-10-25T14:49:06.438Z',
        'correlationGroup': '2305023',
        'salesTaxCurrency': 'EUR',
        'unitUom': 'C62',
        'salesTax': '0.00000',
        'partNumber': '8719256786744',
        'isExpedited': 'false',
        'freeGift': 'false',
        'xitem_isPersonalAddressesAllowedForShipping': 'true',
        'shipModeCode': 'DHL Standard',
        'orderItemFulfillmentStatus': 'Unreleased',
        'shipModeDescription': 'DHL Standard',
        'shippingCharge': '0.00000',
        'shipModeLanguage': '-3',
        'lastUpdateDate': '2018-10-25T14:49:07.164Z',
        'productId': '253871',
        'shippingTaxCurrency': 'EUR',
        'offerID': '4000000000067480367',
        'UOM': 'C62',
        'expectedShipDate': '2018-10-26T14:49:07.204Z',
        'shipModeId': '715839485',
        'language': '-3',
      },
      ],
      'totalAdjustment': '-53.80000',
      'totalShippingTax': '0.00000',
      'resourceId': 'https://de.b2ceuup.tommy.com:443/wcs/resources/api/pvh/eisapp/createBasketWithReservedStock',
      'adjustment': [{
        'amount': '-49.90000',
        'xadju_calUsageId': '-1',
        'displayLevel': 'OrderItem',
        'description': 'Buy A and B and get 50% of on both Products',
        'usage': 'Discount',
        'language': '44',
        'code': 'Buy A and B and Get % on Both-725885385',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-3.90000',
        'xadju_calUsageId': '-7',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      ],
      'totalProductPrice': '99.80000',
      'grandTotalCurrency': 'EUR',
      'x_isPurchaseOrderNumberRequired': 'false',
      'storeNameIdentifier': 'TommyDE',
      'orderId': '399008',
      'resourceName': 'cart',
      'shipAsComplete': 'true',
      'totalShippingTaxCurrency': 'EUR',
      'totalShippingCharge': '3.90000',
      'recordSetStartNumber': '0',
      'totalShippingChargeCurrency': 'EUR',
      'grandTotal': '49.90000',
      'orderStatus': 'P',
      'totalSalesTax': '0.00000',
      'buyerId': '511357',
      'lastUpdateDate': '2018-10-25T14:49:06.436Z',
      'x_trackingIds': '',
      'totalProductPriceCurrency': 'EUR',
      'prepareIndicator': 'false',
      'locked': 'false',
      'totalAdjustmentCurrency': 'EUR',
      'channel': {
        'description': {
          'value': 'Used when operations performed from a desktop browser.',
          'language': '-3',
        },
        'userData': null,
        'channelIdentifer': {
          'channelName': 'Web Channel',
          'uniqueID': '-1',
        },
      },
    }

    const {
      promotions,
      hasPromotion,
    } = selectionsController.getPromotionsFromWCSBasket(wcsBasket)

    expect(promotions).toBeDefined()
    expect(hasPromotion).toBeDefined()
    expect(promotions.length).toBe(1)
    expect(hasPromotion).toBeTruthy()
  })

  it('should return no promotions and hasPromotion=false when discount adjustments are not present', () => {
    const wcsBasket = {
      'recordSetTotal': '2',
      'storeUniqueID': '30006',
      'x_isPersonalAddressesAllowedForShipping': 'true',
      'recordSetCount': '2',
      'totalSalesTaxCurrency': 'EUR',
      'promotionCode': [{
        'associatedPromotion': [{
          'promotionId': '725870395',
          'description': 'Free Standard Shipping DE',
        }],
        'code': 'SALMONPVHECOMINSTOREFREESHIPPING',
      }],
      'recordSetComplete': 'true',
      'orderItem': [{
        'orderItemId': '2305022',
        'unitQuantity': '1.0',
        'totalAdjustment': {
          'value': '-1.9500',
          'currency': 'EUR',
        },
        'unitPrice': '49.90000',
        'usableShippingChargePolicy': [{
          'name': 'StandardShippingChargeBySeller',
          'uniqueID': '-7001',
          'type': 'ShippingCharge',
        }],
        'currency': 'EUR',
        'availableDate': '2018-10-25T14:49:07.162Z',
        'orderItemInventoryStatus': 'Allocated',
        'fulfillmentCenterName': 'TommyHilfiger',
        'shippingTax': '0.00000',
        'adjustment': [{
          'amount': '-1.95000',
          'displayLevel': 'Order',
          'description': 'Free Shipping',
          'usage': 'Shipping',
          'language': '44',
          'code': 'Free Shipping',
          'descriptionLanguage': '44',
          'currency': 'EUR',
        }],
        'description': 'DHL Standard',
        'orderItemStatus': 'P',
        'shippingChargeCurrency': 'EUR',
        'orderItemPrice': '49.90000',
        'carrier': '77859',
        'quantity': '1.0',
        'contractId': '4000000000000000569',
        'fulfillmentCenterId': '715838484',
        'createDate': '2018-10-25T14:49:06.423Z',
        'correlationGroup': '2305022',
        'salesTaxCurrency': 'EUR',
        'unitUom': 'C62',
        'salesTax': '0.00000',
        'partNumber': '8719256922050',
        'isExpedited': 'false',
        'freeGift': 'false',
        'xitem_isPersonalAddressesAllowedForShipping': 'true',
        'shipModeCode': 'DHL Standard',
        'orderItemFulfillmentStatus': 'Unreleased',
        'shipModeDescription': 'DHL Standard',
        'shippingCharge': '0.00000',
        'shipModeLanguage': '-3',
        'lastUpdateDate': '2018-10-25T14:49:07.162Z',
        'productId': '252775',
        'shippingTaxCurrency': 'EUR',
        'offerID': '4000000000065027779',
        'UOM': 'C62',
        'expectedShipDate': '2018-10-26T14:49:07.200Z',
        'shipModeId': '715839485',
        'language': '-3',
      },
      {
        'orderItemId': '2305023',
        'unitQuantity': '1.0',
        'totalAdjustment': {
          'value': '-1.95000',
          'currency': 'EUR',
        },
        'unitPrice': '49.90000',
        'usableShippingChargePolicy': [{
          'name': 'StandardShippingChargeBySeller',
          'uniqueID': '-7001',
          'type': 'ShippingCharge',
        }],
        'currency': 'EUR',
        'availableDate': '2018-10-25T14:49:07.164Z',
        'orderItemInventoryStatus': 'Allocated',
        'fulfillmentCenterName': 'TommyHilfiger',
        'shippingTax': '0.00000',
        'adjustment': [{
          'amount': '-1.95000',
          'displayLevel': 'Order',
          'description': 'Free Shipping',
          'usage': 'Shipping',
          'language': '44',
          'code': 'Free Shipping',
          'descriptionLanguage': '44',
          'currency': 'EUR',
        }],
        'description': 'DHL Standard',
        'orderItemStatus': 'P',
        'shippingChargeCurrency': 'EUR',
        'orderItemPrice': '49.90000',
        'carrier': '77859',
        'quantity': '1.0',
        'contractId': '4000000000000000569',
        'fulfillmentCenterId': '715838484',
        'createDate': '2018-10-25T14:49:06.438Z',
        'correlationGroup': '2305023',
        'salesTaxCurrency': 'EUR',
        'unitUom': 'C62',
        'salesTax': '0.00000',
        'partNumber': '8719256786744',
        'isExpedited': 'false',
        'freeGift': 'false',
        'xitem_isPersonalAddressesAllowedForShipping': 'true',
        'shipModeCode': 'DHL Standard',
        'orderItemFulfillmentStatus': 'Unreleased',
        'shipModeDescription': 'DHL Standard',
        'shippingCharge': '0.00000',
        'shipModeLanguage': '-3',
        'lastUpdateDate': '2018-10-25T14:49:07.164Z',
        'productId': '253871',
        'shippingTaxCurrency': 'EUR',
        'offerID': '4000000000067480367',
        'UOM': 'C62',
        'expectedShipDate': '2018-10-26T14:49:07.204Z',
        'shipModeId': '715839485',
        'language': '-3',
      },
      ],
      'totalAdjustment': '-3.90000',
      'totalShippingTax': '0.00000',
      'resourceId': 'https://de.b2ceuup.tommy.com:443/wcs/resources/api/pvh/eisapp/createBasketWithReservedStock',
      'adjustment': [{
        'amount': '-3.90000',
        'xadju_calUsageId': '-7',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      }],
      'totalProductPrice': '99.80000',
      'grandTotalCurrency': 'EUR',
      'x_isPurchaseOrderNumberRequired': 'false',
      'storeNameIdentifier': 'TommyDE',
      'orderId': '399008',
      'resourceName': 'cart',
      'shipAsComplete': 'true',
      'totalShippingTaxCurrency': 'EUR',
      'totalShippingCharge': '3.90000',
      'recordSetStartNumber': '0',
      'totalShippingChargeCurrency': 'EUR',
      'grandTotal': '99.80000',
      'orderStatus': 'P',
      'totalSalesTax': '0.00000',
      'buyerId': '511357',
      'lastUpdateDate': '2018-10-25T14:49:06.436Z',
      'x_trackingIds': '',
      'totalProductPriceCurrency': 'EUR',
      'prepareIndicator': 'false',
      'locked': 'false',
      'totalAdjustmentCurrency': 'EUR',
      'channel': {
        'description': {
          'value': 'Used when operations performed from a desktop browser.',
          'language': '-3',
        },
        'userData': null,
        'channelIdentifer': {
          'channelName': 'Web Channel',
          'uniqueID': '-1',
        },
      },
    }

    const {
      promotions,
      hasPromotion,
    } = selectionsController.getPromotionsFromWCSBasket(wcsBasket)

    expect(promotions).toBeDefined()
    expect(hasPromotion).toBeDefined()
    expect(promotions.length).toBe(0)
    expect(hasPromotion).toBeFalsy()
  })
})

describe('matchOrderPriceAndInventory', () => {
  // eslint-disable-next-line new-cap
  const sc = new selectionsController()

  it('should return array of products with promo prices when promotion is present', () => {
    const orderProducts = [{
      'orderItemId': '2305022',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-26.9000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.162Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-24.95000',
        'displayLevel': 'OrderItem',
        'description': 'Buy A and B and get 50% of on both Products',
        'usage': 'Discount',
        'language': '44',
        'code': 'Buy A and B and Get % on Both-725885385',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      ],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.423Z',
      'correlationGroup': '2305022',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256922050',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.162Z',
      'productId': '252775',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000065027779',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.200Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    {
      'orderItemId': '2305023',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-26.90000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.164Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-24.95000',
        'displayLevel': 'OrderItem',
        'description': 'Buy A and B and get 50% of on both Products',
        'usage': 'Discount',
        'language': '44',
        'code': 'Buy A and B and Get % on Both-725885385',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      ],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.438Z',
      'correlationGroup': '2305023',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256786744',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.164Z',
      'productId': '253871',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000067480367',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.204Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    ]

    const selectionProducts = [{
      id: '123',
      skuId: '8719256922050',
      name: 'Test Product 1',
      price: -1, // should not be used
      size: 'S',
      image: 'eis-fake.com/fakeImage1.png',
      colorPattern: 'polkadots',
      colorName: 'rainbow',
      amount: 1,
    },
    {
      id: '124',
      skuId: '8719256786744',
      name: 'Test Product 2',
      price: -2, // should not be used
      size: 'M',
      image: 'eis-fake.com/fakeImage2.png',
      colorPattern: 'polkadots',
      colorName: 'blue',
      amount: 1,
    },
    ]

    const products = sc.matchOrderPriceAndInventory(selectionProducts, orderProducts)

    expect(products).toBeDefined()
    expect(products.length).toBe(2)

    const [product1, product2] = products
    expect(product1).toBeDefined()
    expect(product2).toBeDefined()

    const {
      originalUnitPrice: originalUnitPrice1,
      promoUnitPrice: promoUnitPrice1,
      originalTotalPrice: originalTotalPrice1,
      promoTotalPrice: promoTotalPrice1,
    } = product1
    expect(originalUnitPrice1).toBe(49.90)
    expect(promoUnitPrice1).toBe(24.95)
    expect(originalTotalPrice1).toBe(49.90)
    expect(promoTotalPrice1).toBe(24.95)

    const {
      originalUnitPrice: originalUnitPrice2,
      promoUnitPrice: promoUnitPrice2,
      originalTotalPrice: originalTotalPrice2,
      promoTotalPrice: promoTotalPrice2,
    } = product2
    expect(originalUnitPrice2).toBe(49.90)
    expect(promoUnitPrice2).toBe(24.95)
    expect(originalTotalPrice2).toBe(49.90)
    expect(promoTotalPrice2).toBe(24.95)
  })

  it('should return array of products with promo prices when promotions are present', () => {
    const orderProducts = [{
      'orderItemId': '2305022',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-26.9000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.162Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-24.95000',
        'displayLevel': 'OrderItem',
        'description': 'Buy A and B and get 50% of on both Products',
        'usage': 'Discount',
        'language': '44',
        'code': 'Buy A and B and Get % on Both-725885385',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-49.90000',
        'display': 'OrderItem',
        'description': '50% of this product',
        'usage': 'Discount',
        'language': '44',
        'code': '50% of this product',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      ],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '99.80000',
      'carrier': '77859',
      'quantity': '2.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.423Z',
      'correlationGroup': '2305022',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256922050',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.162Z',
      'productId': '252775',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000065027779',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.200Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    {
      'orderItemId': '2305023',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-26.90000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.164Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-24.95000',
        'displayLevel': 'OrderItem',
        'description': 'Buy A and B and get 50% of on both Products',
        'usage': 'Discount',
        'language': '44',
        'code': 'Buy A and B and Get % on Both-725885385',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      ],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.438Z',
      'correlationGroup': '2305023',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256786744',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.164Z',
      'productId': '253871',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000067480367',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.204Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    ]

    const selectionProducts = [{
      id: '123',
      skuId: '8719256922050',
      name: 'Test Product 1',
      price: -1, // should not be used
      size: 'S',
      image: 'eis-fake.com/fakeImage1.png',
      colorPattern: 'polkadots',
      colorName: 'rainbow',
      amount: 1,
    },
    {
      id: '124',
      skuId: '8719256786744',
      name: 'Test Product 2',
      price: -2, // should not be used
      size: 'M',
      image: 'eis-fake.com/fakeImage2.png',
      colorPattern: 'polkadots',
      colorName: 'blue',
      amount: 1,
    },
    ]

    const products = sc.matchOrderPriceAndInventory(selectionProducts, orderProducts)

    expect(products).toBeDefined()
    expect(products.length).toBe(2)

    const [product1, product2] = products
    expect(product1).toBeDefined()
    expect(product2).toBeDefined()

    const {
      originalUnitPrice,
      promoUnitPrice,
      originalTotalPrice,
      promoTotalPrice,
    } = product1
    expect(originalUnitPrice).toBe(49.90)
    expect(Number.parseFloat(promoUnitPrice.toFixed(3))).toBe(12.475)
    expect(originalTotalPrice).toBe(99.80)
    expect(Number.parseFloat(promoTotalPrice.toFixed(3))).toBe(24.95)
  })

  it('should return array of products without promo prices when no promotion is present', () => {
    const orderProducts = [{
      'orderItemId': '2305022',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-1.95000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.162Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      }],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '99.80000',
      'carrier': '77859',
      'quantity': '2.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.423Z',
      'correlationGroup': '2305022',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256922050',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.162Z',
      'productId': '252775',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000065027779',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.200Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    {
      'orderItemId': '2305023',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-1.95000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.164Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      }],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.438Z',
      'correlationGroup': '2305023',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256786744',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.164Z',
      'productId': '253871',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000067480367',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.204Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    ]

    const selectionProducts = [{
      id: '123',
      skuId: '8719256922050',
      name: 'Test Product 1',
      price: -1, // should not be used
      size: 'S',
      image: 'eis-fake.com/fakeImage1.png',
      colorPattern: 'polkadots',
      colorName: 'rainbow',
      amount: 1,
    },
    {
      id: '124',
      skuId: '8719256786744',
      name: 'Test Product 2',
      price: -2, // should not be used
      size: 'M',
      image: 'eis-fake.com/fakeImage2.png',
      colorPattern: 'polkadots',
      colorName: 'blue',
      amount: 1,
    },
    ]

    const products = sc.matchOrderPriceAndInventory(selectionProducts, orderProducts)

    expect(products).toBeDefined()
    expect(products.length).toBe(2)

    const [product1, product2] = products
    expect(product1).toBeDefined()
    expect(product2).toBeDefined()

    const {
      originalUnitPrice,
      promoUnitPrice,
      originalTotalPrice,
      promoTotalPrice,
    } = product1
    expect(originalUnitPrice).toBe(49.90)
    expect(promoUnitPrice).toBe(49.90)
    expect(originalTotalPrice).toBe(99.80)
    expect(promoTotalPrice).toBe(99.80)
  })

  it('should return array of products without promo prices when no adjustments are present', () => {
    const orderProducts = [{
      'orderItemId': '2305022',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '0.00000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.162Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '99.80000',
      'carrier': '77859',
      'quantity': '2.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.423Z',
      'correlationGroup': '2305022',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256922050',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.162Z',
      'productId': '252775',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000065027779',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.200Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    {
      'orderItemId': '2305023',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '0.00000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.164Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.438Z',
      'correlationGroup': '2305023',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256786744',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.164Z',
      'productId': '253871',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000067480367',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.204Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    ]

    const selectionProducts = [{
      id: '123',
      skuId: '8719256922050',
      name: 'Test Product 1',
      price: -1, // should not be used
      size: 'S',
      image: 'eis-fake.com/fakeImage1.png',
      colorPattern: 'polkadots',
      colorName: 'rainbow',
      amount: 1,
    },
    {
      id: '124',
      skuId: '8719256786744',
      name: 'Test Product 2',
      price: -2, // should not be used
      size: 'M',
      image: 'eis-fake.com/fakeImage2.png',
      colorPattern: 'polkadots',
      colorName: 'blue',
      amount: 1,
    },
    ]

    const products = sc.matchOrderPriceAndInventory(selectionProducts, orderProducts)

    expect(products).toBeDefined()
    expect(products.length).toBe(2)

    const [product1, product2] = products
    expect(product1).toBeDefined()
    expect(product2).toBeDefined()

    const {
      originalUnitPrice,
      promoUnitPrice,
      originalTotalPrice,
      promoTotalPrice,
    } = product1
    expect(originalUnitPrice).toBe(49.90)
    expect(promoUnitPrice).toBe(49.90)
    expect(originalTotalPrice).toBe(99.80)
    expect(promoTotalPrice).toBe(99.80)
  })

  it('should return correct quantity of products based on availability', () => {
    const orderProducts = [{
      'orderItemId': '2305022',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '0.00000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.162Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.423Z',
      'correlationGroup': '2305022',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256922050',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.162Z',
      'productId': '252775',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000065027779',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.200Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    {
      'orderItemId': '2305023',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-26.90000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.164Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-24.95000',
        'displayLevel': 'OrderItem',
        'description': 'Buy A and B and get 50% of on both Products',
        'usage': 'Discount',
        'language': '44',
        'code': 'Buy A and B and Get % on Both-725885385',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      ],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.438Z',
      'correlationGroup': '2305023',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256786744',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.164Z',
      'productId': '253871',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000067480367',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.204Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    ]

    const selectionProducts = [{
      id: '123',
      skuId: '8719256922050',
      name: 'Test Product 1',
      price: -1, // should not be used
      size: 'S',
      image: 'eis-fake.com/fakeImage1.png',
      colorPattern: 'polkadots',
      colorName: 'rainbow',
      amount: 1,
    },
    {
      id: '123',
      skuId: '8719256922050',
      name: 'Test Product 1',
      price: -1, // should not be used
      size: 'S',
      image: 'eis-fake.com/fakeImage1.png',
      colorPattern: 'polkadots',
      colorName: 'rainbow',
      amount: 1,
    },
    {
      id: '124',
      skuId: '8719256786744',
      name: 'Test Product 2',
      price: -2, // should not be used
      size: 'M',
      image: 'eis-fake.com/fakeImage2.png',
      colorPattern: 'polkadots',
      colorName: 'blue',
      amount: 1,
    },
    {
      id: '124',
      skuId: '8719256786744',
      name: 'Test Product 2',
      price: -2, // should not be used
      size: 'M',
      image: 'eis-fake.com/fakeImage2.png',
      colorPattern: 'polkadots',
      colorName: 'blue',
      amount: 1,
    },
    ]

    const products = sc.matchOrderPriceAndInventory(selectionProducts, orderProducts)

    expect(products).toBeDefined()
    expect(products.length).toBe(2)

    const [product1, product2] = products
    expect(product1).toBeDefined()
    expect(product2).toBeDefined()

    const {
      originalUnitPrice,
      promoUnitPrice,
      originalTotalPrice,
      promoTotalPrice,
    } = product1
    expect(originalUnitPrice).toBe(49.90)
    expect(promoUnitPrice).toBe(49.90)
    expect(originalTotalPrice).toBe(49.90)
    expect(promoTotalPrice).toBe(49.90)
  })

  it('should return empty list of products when no selectionProducts', () => {
    const orderProducts = [{
      'orderItemId': '2305022',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '0.00000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.162Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.423Z',
      'correlationGroup': '2305022',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256922050',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.162Z',
      'productId': '252775',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000065027779',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.200Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    {
      'orderItemId': '2305023',
      'unitQuantity': '1.0',
      'totalAdjustment': {
        'value': '-26.90000',
        'currency': 'EUR',
      },
      'unitPrice': '49.90000',
      'usableShippingChargePolicy': [{
        'name': 'StandardShippingChargeBySeller',
        'uniqueID': '-7001',
        'type': 'ShippingCharge',
      }],
      'currency': 'EUR',
      'availableDate': '2018-10-25T14:49:07.164Z',
      'orderItemInventoryStatus': 'Allocated',
      'fulfillmentCenterName': 'TommyHilfiger',
      'shippingTax': '0.00000',
      'adjustment': [{
        'amount': '-24.95000',
        'displayLevel': 'OrderItem',
        'description': 'Buy A and B and get 50% of on both Products',
        'usage': 'Discount',
        'language': '44',
        'code': 'Buy A and B and Get % on Both-725885385',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      {
        'amount': '-1.95000',
        'displayLevel': 'Order',
        'description': 'Free Shipping',
        'usage': 'Shipping',
        'language': '44',
        'code': 'Free Shipping',
        'descriptionLanguage': '44',
        'currency': 'EUR',
      },
      ],
      'description': 'DHL Standard',
      'orderItemStatus': 'P',
      'shippingChargeCurrency': 'EUR',
      'orderItemPrice': '49.90000',
      'carrier': '77859',
      'quantity': '1.0',
      'contractId': '4000000000000000569',
      'fulfillmentCenterId': '715838484',
      'createDate': '2018-10-25T14:49:06.438Z',
      'correlationGroup': '2305023',
      'salesTaxCurrency': 'EUR',
      'unitUom': 'C62',
      'salesTax': '0.00000',
      'partNumber': '8719256786744',
      'isExpedited': 'false',
      'freeGift': 'false',
      'xitem_isPersonalAddressesAllowedForShipping': 'true',
      'shipModeCode': 'DHL Standard',
      'orderItemFulfillmentStatus': 'Unreleased',
      'shipModeDescription': 'DHL Standard',
      'shippingCharge': '0.00000',
      'shipModeLanguage': '-3',
      'lastUpdateDate': '2018-10-25T14:49:07.164Z',
      'productId': '253871',
      'shippingTaxCurrency': 'EUR',
      'offerID': '4000000000067480367',
      'UOM': 'C62',
      'expectedShipDate': '2018-10-26T14:49:07.204Z',
      'shipModeId': '715839485',
      'language': '-3',
    },
    ]
    const selectionProducts = undefined
    const products = sc.matchOrderPriceAndInventory(selectionProducts, orderProducts)

    expect(products).toBeDefined()
    expect(products.length).toBe(0)
  })

  it('should return empty list of products when no orderProducts', () => {
    const orderProducts = undefined
    const selectionProducts = [{
      id: '123',
      skuId: '8719256922050',
      name: 'Test Product 1',
      price: -1, // should not be used
      size: 'S',
      image: 'eis-fake.com/fakeImage1.png',
      colorPattern: 'polkadots',
      colorName: 'rainbow',
      amount: 1,
    },
    {
      id: '124',
      skuId: '8719256786744',
      name: 'Test Product 2',
      price: -2, // should not be used
      size: 'M',
      image: 'eis-fake.com/fakeImage2.png',
      colorPattern: 'polkadots',
      colorName: 'blue',
      amount: 1,
    },
    ]
    const products = sc.matchOrderPriceAndInventory(selectionProducts, orderProducts)

    expect(products).toBeDefined()
    expect(products.length).toBe(0)
  })
})

describe('applyPromotionCode', () => {
  it('should be defined', async () => {
    expect(applyPromotionCode).toBeDefined()
  })

  it('should successfully add code and return new selection', () => {})

  it('should throw an error if WCS response is not ok', () => {})
})

describe('deletePromotionCode', () => {
  it('should be defined', () => {})

  it('should successfully delete code and return new selection', () => {})

  it('should throw error if no code can be deleted', () => {})
})

describe('getSelection', () => {
  it('should be defined', () => {})
  it('should return selection if exists', () => {})
  it('should return undefined if no selection exists', () => {})
})

describe('updateSelection', () => {
  it('should be defined', () => {})
  it('should return updated selection if exists', () => {})
  it('should throw error if no selection exists', () => {})
})

describe('getWCSCartDetails', () => {

})

describe('wcsCartDetailsToSelection', () => {})

describe('matchOrderPriceAndInventory', () => {})

describe('matchOrderPriceAndInventoryBySKU', () => {})

describe('matchOrderPriceAndInventoryByCatEntryId', () => {})

describe('getPromotionsFromWCSBasket', () => {})