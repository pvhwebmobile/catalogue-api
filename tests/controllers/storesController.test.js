/* global describe it expect beforeAll afterAll */

const mongoose = require('mongoose')

const storeModel = require('../../models/store')
const storesController = require('../../controllers/storesController')

beforeAll(async () => {
  mongoose.Promise = require('bluebird')
  mongoose.set('useFindAndModify', false)

  let options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    autoIndex: false, // Don't build indexes
    poolSize: 1, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
  }

  await mongoose.connect(global.__MONGO_URI__, options)

  await storeModel.create({
    tenant: 'th',
    countryCode: 'DE',
    city: 'amsterdam',
    zipCode: '123abc',
    address1: 'Coolstraat 123',
    name: 'TH Cool Beans',
    storeId: 'A233',
    payment: 'gk',
  })
})

afterAll(async () => {
  await mongoose.disconnect()
})

describe('getStoreList', () => {
  it('should return store list from json file', async () => {
    const stores = await storesController.getStoreList('../config/stores', true)
    expect(stores).toBeDefined()
  })

  it('should return list from sharepoint', async () => {
    const stores = await storesController.getStoreList('http://google.com', false)
    expect(stores).toBeDefined()
  })
})

describe('getStores', () => {
  it('should return list of stores located in provided country with payment information appended', async () => {
    const tenant = 'th'
    const countryCode = 'de'
    const stores = await storesController.getStores(tenant, countryCode)

    expect(stores).toBeDefined()
    expect(stores.length).toBeGreaterThanOrEqual(1)
  })

  it('should return no stores for invalid country', async () => {
    const tenant = 'th'
    const countryCode = 'XM'
    const stores = await storesController.getStores(tenant, countryCode)

    expect(stores).toEqual([])
  })

  it('should not merge stores if merge is false', async () => {
    const tenant = 'th'
    const countryCode = 'de'
    const stores = await storesController.getStores(tenant, countryCode, false)

    expect(stores).toBeDefined()
  })

  it(
    'should return empty list if tenant or countryCode is undefined and merge is false',
    async () => {
      const tenant = undefined
      const countryCode = 'de'
      const stores = await storesController.getStores(
        tenant, countryCode, false
      )

      expect(stores).toEqual([])
    })
})

describe('getStore', () => {
  it('should return a store', async () => {
    const tenant = 'th'
    const countryCode = 'DE'
    const storeId = 'A233'
    const store = await storesController.getStore(tenant, countryCode, storeId)

    expect(store).toBeDefined()

    const {
      payment,
      storeId: _storeId,
      name,
      city,
    } = store
    expect(payment).toBeDefined()
    expect(_storeId).toBeDefined()
    expect(_storeId).toBe(storeId)
    expect(name).toBeDefined()
    expect(city).toBeDefined()
  })

  it('should return null for invalid store', async () => {
    const tenant = 'th'
    const countryCode = 'DE'
    const storeId = 'ABCD'
    const store = await storesController.getStore(tenant, countryCode, storeId)

    expect(store).toBeUndefined()
  })

  it('should merge stores', async () => {
    const tenant = 'th'
    const countryCode = 'de'
    const storeId = 'A233'
    const store = await storesController.getStore(
      tenant, countryCode, storeId, true
    )

    expect(store).toBeDefined()
  })

  it('should not merge stores', async () => {
    const tenant = 'th'
    const countryCode = 'de'
    const storeId = 'A233'
    const store = await storesController.getStore(
      tenant, countryCode, storeId, false
    )

    expect(store).toBeDefined()
  })
})

describe('createOrUpdateStore', () => {
  it('should create and return new store', async () => {
    const tenant = 'th'
    const countryCode = 'de'
    const storeId = 'A234'
    const toCreate = {
      tenant,
      countryCode,
      payment: 'ecom',
    }

    const store = await storesController.createOrUpdateStore(storeId, toCreate)
    expect(store).toBeDefined()
    expect(store.payment).toBe('ecom')
    expect(store.tenant).toBe('th')
    expect(store.countryCode).toBe('DE')
    expect(store.storeId).toBe('A234')
  })

  it('should update existing store', async () => {
    const toUpdate = {
      tenant: 'ck',
      countryCode: 'FR',
      payment: 'ecom',
    }

    const store = await storesController.createOrUpdateStore('A233', toUpdate)
    expect(store).toBeDefined()
    expect(store.payment).toBe('ecom')
    expect(store.tenant).toBe('ck')
    expect(store.countryCode).toBe('FR')
    expect(store.storeId).toBe('A233')
  })
})

describe('patchStore', () => {
  it('should patch and return new store', async () => {
    const tenant = 'th'
    const countryCode = 'DE'
    const storeId = 'A233'
    const toUpdate = {
      payment: 'ecom',
    }

    const store = await storesController.patchStore(tenant, countryCode, storeId, toUpdate)
    expect(store).toBeDefined()
    expect(store.payment).toBe('ecom')
    expect(store.tenant).toBe('th')
    expect(store.countryCode).toBe('DE')
    expect(store.storeId).toBe('A233')
  })

  it('should not patch if invalid tenant provided', async () => {
    const tenant = 'td'
    const countryCode = 'DE'
    const storeId = 'A233'
    const toUpdate = {
      payment: 'ecom',
    }

    const store = await storesController.patchStore(tenant, countryCode, storeId, toUpdate)
    expect(store).toBeNull()
  })
})
