/* global jest describe it expect beforeEach afterAll */
jest.mock('fs')

const {
  parseJSONFile,
  parseTenantFromStoreName,
  uploadStoreLocatorFile,
  uploadStoreLocatorJSON,
} = require('../../controllers/uploadsController')
const {
  mockCreateOrUpdateStore,
  mockDeleteStores,
} = require('../../controllers/__mocks__/storesControllerFunctions')

jest.mock('controllers/storesController')

const MOCK_JSON_REAL = require('./__data__/uploads/real_data.json')
const MOCK_JSON1 = require('./__data__/uploads/data1.json')
const MOCK_JSON2 = require('./__data__/uploads/data2.json')
const MOCK_JSON_BAD = require('./__data__/uploads/bad_data.json')

const MOCK_FILE_INFO = {
  'tmp/fake/read_file.json': JSON.stringify(MOCK_JSON_REAL),
  'tmp/fake/file.json': JSON.stringify(MOCK_JSON1),
  'tmp/fake/file2.json': JSON.stringify(MOCK_JSON2),
  'tmp/fake/bad_file.json': JSON.stringify(MOCK_JSON_BAD),
}

beforeEach(async () => {
  require('fs').__setMockFiles(MOCK_FILE_INFO)
  mockCreateOrUpdateStore.mockClear()
  mockDeleteStores.mockClear()
})

afterAll(async () => {
})

describe('parseJSONFile', () => {
  it('should be defined', () => {
    expect(parseJSONFile).toBeDefined()
  })

  it('should return JSON object if file exists', async () => {
    const filePath = 'tmp/fake/file.json'
    const json = await parseJSONFile(filePath)
    expect(json).toBeDefined()
    expect(json).toEqual(MOCK_JSON1)
  })
})

describe('parseTenantFromStoreName', () => {
  it('should be defined', () => {
    expect(parseTenantFromStoreName).toBeDefined()
  })

  it('should work for all tommy stores', () => {
    const possibleTommyStores = [
      'Tommy Hilfiger',
      'TOMMY HILFIGER',
      'TOMMY JEANS',
      'TOMMY HiLfIgEr',
      'Tommy Jeans',
      'Tommy Hilfiger Denim',
      'Tommy Hilfiger Jeans',
      'Tommy Hilfiger Kids',
      'Tommy Hilfiger Other',
    ]

    for (let storeName of possibleTommyStores) {
      expect(parseTenantFromStoreName(storeName)).toBe('th')
    }
  })

  it('should work for all calvin klein stores', () => {
    const possibleTommyStores = [
      'Calvin Klein',
      'Calvin Klein',
      'Calvin Klein Jeans',
      'CAlvIN KLein',
      'Calvin Klein Accessories',
      'Calvin Klein Kids',
    ]

    for (let storeName of possibleTommyStores) {
      expect(parseTenantFromStoreName(storeName)).toBe('ck')
    }
  })

  it('should return undefined in all other cases', () => {
    const possibleTommyStores = [
      'Wrong name',
      'Please try again',
    ]

    for (let storeName of possibleTommyStores) {
      expect(parseTenantFromStoreName(storeName)).toBeUndefined()
    }
  })
})

describe('uploadStoreLocatorFile', () => {
  it('should be defined', () => {
    expect(uploadStoreLocatorFile).toBeDefined()
  })

  it('should parse json file, validate json, and upload stores', async () => {
    const file = { path: 'tmp/fake/file.json' }
    const result = await uploadStoreLocatorFile(file)
    expect(mockCreateOrUpdateStore).toBeCalledTimes(4)
    expect(mockDeleteStores).toBeCalledTimes(1)
    expect(mockDeleteStores).toBeCalledWith({
      $or: [
        { uploadedAt: { $lt: new Date('2019-12-31T12:34:56-01:00') } },
        { uploadedAt: { $eq: null } },
      ],
    })
    expect(result).toEqual({
      upserted: 4,
      deleted: 1,
      invalid: 0,
    })
  })

  it('should remove stores no longer present in upload', async () => {
    const file = { path: 'tmp/fake/file2.json' }
    const result = await uploadStoreLocatorFile(file)
    expect(mockCreateOrUpdateStore).toBeCalledTimes(3)
    expect(mockDeleteStores).toBeCalledTimes(1)
    expect(mockDeleteStores).toBeCalledWith({
      $or: [
        { uploadedAt: { $lt: new Date('2020-01-01T01:23:45-01:00') } },
        { uploadedAt: { $eq: null } },
      ],
    })
    expect(result).toEqual({
      upserted: 3,
      deleted: 1,
      invalid: 0,
    })
  })

  it('should return undefined if file has no path', async () => {
    const file = {}
    const result = await uploadStoreLocatorFile(file)
    expect(result).toBeUndefined()
  })

  it('should throw error if json file is malformated', async () => {
    const file = { path: 'tmp/fake/bad_file.json' }
    let success = false
    try {
      await uploadStoreLocatorFile(file)
      success = true
    } catch (error) {
      expect(error).toBeDefined()
    }
    expect(success).toBeFalsy()
  })
})

describe('uploadStoreLocatorJSON', () => {
  it('should be defined', () => {
    expect(uploadStoreLocatorJSON).toBeDefined()
  })

  it('should parse json file, validate json, and upload stores', async () => {
    const result = await uploadStoreLocatorJSON(MOCK_JSON1)
    expect(mockCreateOrUpdateStore).toBeCalledTimes(4)
    expect(mockDeleteStores).toBeCalledTimes(1)
    expect(mockDeleteStores).toBeCalledWith({
      $or: [
        { uploadedAt: { $lt: new Date('2019-12-31T12:34:56-01:00') } },
        { uploadedAt: { $eq: null } },
      ],
    })
    expect(result).toEqual({
      upserted: 4,
      deleted: 1,
      invalid: 0,
    })
  })

  it('should remove stores no longer present in upload', async () => {
    const result = await uploadStoreLocatorJSON(MOCK_JSON2)
    expect(mockCreateOrUpdateStore).toBeCalledTimes(3)
    expect(mockDeleteStores).toBeCalledTimes(1)
    expect(mockDeleteStores).toBeCalledWith({
      $or: [
        { uploadedAt: { $lt: new Date('2020-01-01T01:23:45-01:00') } },
        { uploadedAt: { $eq: null } },
      ],
    })
    expect(result).toEqual({
      upserted: 3,
      deleted: 1,
      invalid: 0,
    })
  })

  it('should throw error if json file is malformated', async () => {
    let success = false
    try {
      await uploadStoreLocatorJSON(MOCK_JSON_BAD)
      success = true
    } catch (error) {
      expect(error).toBeDefined()
    }
    expect(success).toBeFalsy()
  })
})
