/* global jest describe it expect afterEach beforeEach */

jest.mock('node-fetch')

const fetch = require('node-fetch')
const { Response } = jest.requireActual('node-fetch')

const testData = require('./__data__/versions/test.json')
const updateAtFailData = require('./__data__/versions/update-at-fail.json')
const {
  getRemotePList,
  getiTommyPList,
  getiCalvinPList,
  getEISPlist,
  getVersion,
  checkForEISUpdate,
  checkForUpdate,
} = require('../../controllers/versionsController')

beforeEach(() => {
  fetch.mockReturnValue(
    Promise.resolve(new Response(testData.data)),
  )
})

afterEach(() => {
  fetch.mockClear()
})

describe('getRemotePList', () => {
  it('should be defined', () => {
    expect(getRemotePList).toBeDefined()
  })

  it('should fetch url and parse xml', async () => {
    fetch.mockReturnValue(
      Promise.resolve(new Response(testData.data))
    )
    const plist = await getRemotePList('apps.pvh.com/itommy')
    expect(fetch).toHaveBeenCalledTimes(1)
    expect(fetch).toHaveBeenCalledWith('apps.pvh.com/itommy')
    expect(plist).toBeDefined()
    expect(typeof plist).toBe('object')
  })
})

describe('getiTommyPList', () => {
  it('should be defined', () => {
    expect(getiTommyPList).toBeDefined()
  })

  it('should call getRemotePList with iTommyPListURL', async () => {
    const result = await getiTommyPList()
    expect(fetch).toHaveBeenCalledTimes(1)
    expect(fetch).toHaveBeenCalledWith('https://pvhmobileapps.s3.eu-central-1.amazonaws.com/RELEASE/iTommy-RELEASE/iTommy-RELEASE.ipa.plist')
    expect(result).toBeDefined()
  })
})

describe('getiCalvinPList', () => {
  it('should be defined', () => {
    expect(getiCalvinPList).toBeDefined()
  })

  it('should call getRemotePList with iCalvinPListURL', async () => {
    const result = await getiCalvinPList()
    expect(fetch).toHaveBeenCalledTimes(1)
    expect(fetch).toHaveBeenCalledWith('https://pvhmobileapps.s3.eu-central-1.amazonaws.com/RELEASE/iCalvin-RELEASE/iCalvin-RELEASE.ipa.plist')
    expect(result).toBeDefined()
  })
})

describe('getEISPlist', () => {
  it('should be defined', () => {
    expect(getEISPlist).toBeDefined()
  })

  it('should call getiTommyPList for th and return version info', async () => {
    const result = await getEISPlist('th')
    expect(result).toBeDefined()
    const { version, updateForced, updateLink } = result
    expect(version).toBe('2.4.0')
    expect(updateForced).toBeTruthy()
    expect(updateLink).toBe('https://cool-beans.com')
  })

  it('should call getiCalvinPList for ck and return version info', async () => {
    const result = await getEISPlist('ck')
    expect(result).toBeDefined()
    const { version, updateForced, updateLink } = result
    expect(version).toBe('2.4.0')
    expect(updateForced).toBeTruthy()
    expect(updateLink).toBe('https://cool-beans.com')
  })

  it('should throw error for other tenants', async () => {
    await getEISPlist('ff').catch((e) => {
      expect(e).toBeDefined()
      expect(e.status).toBe(400)
      expect(e.message).toBe('Tenant not supported')
    })
  })
})

describe('getVersion', () => {
  it('should be defined', () => {
    expect(getVersion).toBeDefined()
  })

  it('should get version for itommy', async () => {
    const result = await getVersion('ITOMMY')
    expect(result).toBeDefined()
  })

  it('should get version for icalvin', async () => {
    const result = await getVersion('ICALVIN')
    expect(result).toBeDefined()
  })

  it('should throw error for others', async () => {
    getVersion('blsh').catch(e => {
      expect(e).toBeDefined()
      expect(e.status).toBe(400)
      expect(e.message).toBe('No app found.')
    })
  })
})

describe('checkForEISUpdate', () => {
  it('should be defined', () => {
    expect(checkForEISUpdate).toBeDefined()
  })

  it('should get update data for th', async () => {
    const result = await checkForEISUpdate('th', '2.1.5')
    expect(result).toBeDefined()
    expect(result.updateAvailable).toBeTruthy()
  })

  it('should get update data for ck', async () => {
    const result = await checkForEISUpdate('ck', '2.1.5')
    expect(result).toBeDefined()
    expect(result.updateAvailable).toBeTruthy()
  })

  it('should throw error for others', async () => {
    checkForEISUpdate('ff', '2.1.5').catch((e) => {
      expect(e).toBeDefined()
      expect(e.status).toBe(400)
      expect(e.message).toBe('Tenant not supported')
    })
  })

  it('should get return updateAvailable false for 2.1.5', async () => {
    const result = await checkForEISUpdate('th', '2.1.5')
    expect(result).toBeDefined()
    expect(result.updateAvailable).toBeTruthy()
  })

  it('should get return updateAvailable true for 2.2.5', async () => {
    const result = await checkForEISUpdate('th', '2.2.5')
    expect(result).toBeDefined()
    expect(result.updateAvailable).toBeTruthy()
  })

  it('should get return updateAvailable false for 2.4.0', async () => {
    const result = await checkForEISUpdate('th', '2.4.0')
    expect(result).toBeDefined()
    expect(result.updateAvailable).toBeFalsy()
  })

  it('should get return updateAvailable false for 2.5.0', async () => {
    const result = await checkForEISUpdate('th', '2.5.0')
    expect(result).toBeDefined()
    expect(result.updateAvailable).toBeFalsy()
  })

  it('should return false for updateAt > today', async () => {
    fetch.mockClear()
    fetch.mockReturnValue(
      Promise.resolve(new Response(updateAtFailData.data)),
    )

    const result = await checkForEISUpdate('th', '2.2.5')
    expect(result).toBeDefined()
    expect(result.updateAvailable).toBeFalsy()
  })
})

describe('checkForUpdate', () => {
  it('should be defined', () => {
    expect(checkForUpdate).toBeDefined()
  })

  it('should return result for itommy', async () => {
    const result = await checkForUpdate('itommy', '2.1.5')
    expect(result).toBeDefined()
  })

  it('should return result for icalvin', async () => {
    const result = await checkForUpdate('icalvin', '2.1.5')
    expect(result).toBeDefined()
  })

  it('should throw error for others', () => {
    checkForUpdate('fake', '2.1.5').catch((e) => {
      expect(e).toBeDefined()
      expect(e.message).toBe('No app found.')
      expect(e.status).toBe(400)
    })
  })
})
