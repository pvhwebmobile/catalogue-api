/* global jest describe it expect afterEach */

jest.mock('node-fetch')

const fetch = require('node-fetch')
const { Response } = jest.requireActual('node-fetch')
const {
  mockGetCampaignLabel,
} = require('../../controllers/__mocks__/campaignsControllerFunctions')

jest.mock('controllers/campaignsController')

const globals = {
  ctx: {
    i18n: () => {},
    state: {
      acceptVersion: 4,
    },
  },
  tenant: 'th',
  countryCode: 'de',
  locale: 'de_DE',
  client: 'itommy',
}

const {
  parseFilter,
  parseFacetMap,
  parseAttributes,
  parseRelatedCombis,
  parseListFromItemsSection,
  generateImageURLsForProduct,
  generateSizesForProductColor,
  generateColorsForProduct,
  parseProductFromItemsSectionAndThemes,
  getSuggestionsByTerm,
  getProductsByTerm,
  getProductsByCategoryId,
  getProductById,
} = require('../../controllers/newProductsController')(globals)

afterEach(() => {
  fetch.mockClear()
  mockGetCampaignLabel.mockClear()
})

describe('newProductsController', () => {
  describe('parseFilter', () => {
    it('should be defined', () => {
      expect(parseFilter).toBeDefined()
    })

    it('should return undefined if filter is undefined', () => {
      expect(parseFilter()).toBeUndefined()
    })

    it('should return undefined if filter is missing title', () => {
      expect(parseFilter(undefined, {})).toBeUndefined()
    })

    it('should return undefined if filter is missing filtersection', () => {
      expect(parseFilter(undefined, { title: 'ooga booga' })).toBeUndefined()
    })

    it('should parse filter, title, and filtersection accordingly', () => {
      const filter = {
        title: 'test',
        filtersection: [
          { link: { name: 'RED' }, value: { value: 'RED' }, nr: 9001 },
          { link: { name: 'YELLOW' }, value: { value: 'YELLOW' }, nr: 9002 },
          { link: { name: 'BLUE' }, value: { value: 'BLUE' }, nr: 9003 },
          { link: { name: 'RAINBOW' }, value: { value: 'RAINBOW' }, nr: 9004 },
        ],
        on: 'test',
      }

      const result = parseFilter(undefined, filter)

      expect(result.name).toBe('test')
      expect(result.items.length).toBe(4)

      expect(result.items[0].value).toBe('test:RED')
      expect(result.items[0].label).toBe('RED')
      expect(result.items[0].count).toBe('9001')

      expect(result.items[1].value).toBe('test:YELLOW')
      expect(result.items[1].label).toBe('YELLOW')
      expect(result.items[1].count).toBe('9002')

      expect(result.items[2].value).toBe('test:BLUE')
      expect(result.items[2].label).toBe('BLUE')
      expect(result.items[2].count).toBe('9003')

      expect(result.items[3].value).toBe('test:RAINBOW')
      expect(result.items[3].label).toBe('RAINBOW')
      expect(result.items[3].count).toBe('9004')
    })
  })

  describe('parseFacetMap', () => {
    it('should be defined', () => {
      expect(parseFacetMap).toBeDefined()
    })

    it('should return default filters if facetMap is undefined', () => {
      const result = parseFacetMap(undefined, 'th')
      expect(result).toBeDefined()
    })

    it('should return default filters if facetMap is malformed', () => {
      const result = parseFacetMap([], 'th')
      expect(result).toBeDefined()
    })

    it('should return default filters if tenant is undefined', () => {
      const result = parseFacetMap([{ universe: 'cool universe' }], undefined)
      expect(result).toBeDefined()
    })

    it('should parse tenant universe and extract all filters',
      () => {
        const facetMap = [
          {
            universe: 'searchresults',
            filter: [],
          },
          {
            universe: 'th',
            filter: [
              {
                title: 'Colour',
                on: 'maincolour',
                filtersection: [
                  {
                    link: { name: 'RED' },
                    value: { value: 'RED' },
                    nr: 9001,
                  },
                  {
                    link: { name: 'YELLOW' },
                    value: { value: 'YELLOW' },
                    nr: 9002,
                  },
                  {
                    link: { name: 'BLUE' },
                    value: { value: 'BLUE' },
                    nr: 9003,
                  },
                  {
                    link: { name: 'RAINBOW' },
                    value: { value: 'RAINBOW' },
                    nr: 9004,
                  },
                ],
              },
              {
                title: 'size',
                on: 'size_de',
                filtersection: [
                  { link: { name: '2' }, value: { value: '2' }, nr: 2 },
                  { link: { name: '4' }, value: { value: '4' }, nr: 4 },
                  { link: { name: '6' }, value: { value: '6' }, nr: 6 },
                  { link: { name: '8' }, value: { value: '8' }, nr: 8 },
                ],
              },
              {
                title: 'Price',
                on: 'price_min',
                filtersection: [
                  { link: { name: 90.0 }, value: { value: 90.0 } },
                ],
              },
              {
                title: 'Price',
                on: 'price_max',
                filtersection: [
                  { link: { name: 900.0 }, value: { value: 900.0 } },
                ],
              },
            ],
          },
        ]
        const tenant = 'th'
        const countryCode = 'de'
        const result = parseFacetMap(facetMap, tenant, countryCode)
        expect(result).toBeDefined()
        expect(result.maincolour).toBeDefined()
        expect(result.size).toBeDefined()
        expect(result.price_slider).toBeDefined()
      })

    it('should parse tenant universe and extract all filters except price_slider',
      () => {
        const facetMap = [
          {
            universe: 'searchresults',
            filter: [],
          },
          {
            universe: 'th',
            filter: [
              {
                title: 'Colour',
                on: 'maincolour',
                filtersection: [
                  {
                    link: { name: 'RED' },
                    value: { value: 'RED' },
                    nr: 9001,
                  },
                  {
                    link: { name: 'YELLOW' },
                    value: { value: 'YELLOW' },
                    nr: 9002,
                  },
                  {
                    link: { name: 'BLUE' },
                    value: { value: 'BLUE' },
                    nr: 9003,
                  },
                  {
                    link: { name: 'RAINBOW' },
                    value: { value: 'RAINBOW' },
                    nr: 9004,
                  },
                ],
              },
              {
                title: 'size',
                on: 'size_de',
                filtersection: [
                  { link: { name: '2' }, value: { value: '2' }, nr: 2 },
                  { link: { name: '4' }, value: { value: '4' }, nr: 4 },
                  { link: { name: '6' }, value: { value: '6' }, nr: 6 },
                  { link: { name: '8' }, value: { value: '8' }, nr: 8 },
                ],
              },
              {
                title: 'Price',
                on: 'price_max',
                filtersection: [
                  { link: { name: 900.0 }, value: { value: 900.0 } },
                ],
              },
            ],
          },
        ]
        const tenant = 'th'
        const countryCode = 'de'
        const result = parseFacetMap(facetMap, tenant, countryCode)
        expect(result).toBeDefined()
        expect(result.maincolour).toBeDefined()
        expect(result.size).toBeDefined()
        expect(result.price_slider).toBeUndefined()
      })
  })

  describe('parseAttributes', () => {
    it('should be defined', () => {
      expect(parseAttributes).toBeDefined()
    })

    it('should return empty object if attributes is undefined', () => {
      expect(parseAttributes(null)).toEqual({})
    })

    it('should return empty object if attributes is empty', () => {
      expect(parseAttributes([])).toEqual({})
    })

    it('should parse types: asset, text, int, float, list, and set', () => {
      const attributes = [
        {
          value: [{ value: 'ASSET VALUE', 'non-ml': 'ASSET VALUE' }],
          name: 'asset',
          basetype: 'asset',
          isnull: false,
        },
        {
          value: [{ value: 'TEXT', 'non-ml': 'TEXT' }],
          name: 'text',
          basetype: 'text',
          isnull: false,
        },
        {
          value: [{ value: '1', 'non-ml': '1' }, { value: '2', 'non-ml': '2' }],
          name: 'int',
          basetype: 'int',
          isnull: false,
        },
        {
          value: [{ value: '123456789.01', 'non-ml': '123456789.01' }],
          name: 'float',
          basetype: 'float',
          isnull: false,
        },
        {
          value: [
            { value: 'Item1', 'non-ml': 'item_1' },
            { value: 'Item2', 'non-ml': 'item_2' },
          ],
          name: 'list',
          basetype: 'list',
          isnull: false,
        },
        {
          value: [
            { value: 'Item1', 'non-ml': 'item_1' },
            { value: 'Item2', 'non-ml': 'item_2' },
          ],
          name: 'set',
          basetype: 'set',
          isnull: false,
        },
      ]

      const parsedAttrs = parseAttributes(attributes)

      expect(parsedAttrs).toBeDefined()

      const { asset, text, int, float, list, set } = parsedAttrs

      expect(asset).toBeDefined()
      expect(text).toBeDefined()
      expect(int).toBeDefined()
      expect(float).toBeDefined()
      expect(list).toBeDefined()
      expect(set).toBeDefined()

      expect(asset.value[0]).toBe('ASSET VALUE')
      expect(text.value[0]).toBe('TEXT')
      expect(int.value[0]).toBe(1)
      expect(int.value[1]).toBe(2)
      expect(float.value[0]).toBe(123456789.01)
      expect(list.value[0]).toBe('Item1')
      expect(list.value[1]).toBe('Item2')
      expect(set.value[0]).toBe('Item1')
      expect(set.value[1]).toBe('Item2')

      expect(asset['non-ml'][0]).toBe('ASSET VALUE')
      expect(text['non-ml'][0]).toBe('TEXT')
      expect(int['non-ml'][0]).toBe(1)
      expect(int['non-ml'][1]).toBe(2)
      expect(float['non-ml'][0]).toBe(123456789.01)
      expect(list['non-ml'][0]).toBe('item_1')
      expect(list['non-ml'][1]).toBe('item_2')
      expect(set['non-ml'][0]).toBe('item_1')
      expect(set['non-ml'][1]).toBe('item_2')
    })

    it('should return undefined for other types', () => {
      const attributes = [
        {
          value: [{ value: '1', 'non-ml': '1' }],
          basetype: 'other',
          name: 'other',
          isnull: false,
        },
      ]

      const parsedAttrs = parseAttributes(attributes)
      expect(parsedAttrs).toBeDefined()

      const { other } = parsedAttrs
      expect(other).toBeDefined()
      expect(other.value[0]).toBeUndefined()
      expect(other['non-ml'][0]).toBeUndefined()
    })

    it('should handle isnull and return null for value', () => {
      const attributes = [
        {
          value: [{ value: '1', 'non-ml': '1' }],
          basetype: 'int',
          name: 'nullInt',
          isnull: true,
        },
      ]

      const parsedAttrs = parseAttributes(attributes)
      expect(parsedAttrs).toBeDefined()

      const { nullInt } = parsedAttrs
      expect(nullInt).toBeDefined()
      expect(nullInt.value).toBeNull()
      expect(nullInt['non-ml']).toBeNull()
    })
  })

  describe('parseRelatedCombis', () => {
    it('should be defined', () => {
      expect(parseRelatedCombis).toBeDefined()
    })

    it('should return empty array if undefined arguments', () => {
      expect(parseRelatedCombis(undefined)).toEqual([])
    })

    it('should return list of parsed relatedcombis', () => {
      const relatedcombis = 'MW0MW10800422,CHAMBRAY BLUE,' +
        't-shirt-aus-bio-baumwolle-mit-stretch-mw0mw10800422,BLUE|' +
        'MW0MW10800423,SEA GREEN,' +
        't-shirt-aus-bio-baumwolle-mit-stretch-mw0mw10800423,GREEN'
      const parsedRCs = parseRelatedCombis(relatedcombis)

      expect(parsedRCs).toBeDefined()
      expect(parsedRCs.length).toBe(2)

      const [rc1, rc2] = parsedRCs

      expect(rc1).toBeDefined()
      expect(rc2).toBeDefined()

      expect(rc1.partNumber).toBeDefined()
      expect(rc1.partNumber).toBe('MW0MW10800422')
      expect(rc1.productAttrColour).toBeDefined()
      expect(rc1.productAttrColour).toBe('CHAMBRAY BLUE')
      expect(rc1.seoUrl).toBeDefined()
      expect(rc1.seoUrl)
        .toBe('t-shirt-aus-bio-baumwolle-mit-stretch-mw0mw10800422')
      expect(rc1.mainColour).toBeDefined()
      expect(rc1.mainColour).toBe('BLUE')

      expect(rc2.partNumber).toBeDefined()
      expect(rc2.partNumber).toBe('MW0MW10800423')
      expect(rc2.productAttrColour).toBeDefined()
      expect(rc2.productAttrColour).toBe('SEA GREEN')
      expect(rc2.seoUrl).toBeDefined()
      expect(rc2.seoUrl)
        .toBe('t-shirt-aus-bio-baumwolle-mit-stretch-mw0mw10800423')
      expect(rc2.mainColour).toBeDefined()
      expect(rc2.mainColour).toBe('GREEN')
    })
  })

  describe('parseListFromItemsSection', () => {
    it('should be defined', () => {
      expect(parseListFromItemsSection).toBeDefined()
    })

    it(
      'should return [] if itemsSection is undefined or contains no items',
      async () => {
        expect(await parseListFromItemsSection(undefined, 'th', 'de'))
          .toEqual({ 'products': [], 'totalProductsAmount': 0 })
        expect(await parseListFromItemsSection({}, 'th', 'de'))
          .toEqual({ 'products': [], 'totalProductsAmount': 0 })
        expect(await parseListFromItemsSection({ items: {} }, 'th', 'de'))
          .toEqual({ 'products': [], 'totalProductsAmount': 0 })
        expect(await parseListFromItemsSection({ items: { item: [] } }, 'th', 'de'))
          .toEqual({ 'products': [], 'totalProductsAmount': 0 })
      })

    it('should parse itemsSection into products and ignore invalid products for itommy',
      async () => {
        const itemsSection = {
          items: {
            item: [
              {
                attribute: [
                  {
                    'value': [
                      {
                        'value': '3.0',
                        'non-ml': '3.0',
                      },
                    ],
                    'name': '_match_rate',
                    'basetype': 'float',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'p531093',
                        'non-ml': 'p531093',
                      },
                    ],
                    'name': 'secondid',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Herren',
                        'non-ml': 'gender_men',
                      },
                    ],
                    'name': 'gender',
                    'basetype': 'set',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'TOMMY HILFIGER',
                        'non-ml': 'label_tommyhilfiger',
                      },
                    ],
                    'name': 'label',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Essential T-Shirt aus Bio-Baumwolle',
                      },
                    ],
                    'name': 'name',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '0',
                        'non-ml': '0',
                      },
                    ],
                    'name': 'onsale_de',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW10812100',
                        'non-ml': 'MW0MW10812100',
                      },
                    ],
                    'name': 'partnumber',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '39.9',
                        'non-ml': '39.9',
                      },
                    ],
                    'name': 'price_de',
                    'basetype': 'float',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '10155:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_DE|10156:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_CH|10160:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_BE',
                      },
                    ],
                    'name': 'primarycategory',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'product_attr_new_arrival',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'NEU',
                        'non-ml': 'product_attr_promo_de_new',
                      },
                    ],
                    'name': 'product_attr_promo_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '6:S',
                        'non-ml': 'size_s_de',
                      },
                    ],
                    'name': 'size_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '0',
                        'non-ml': '0',
                      },
                    ],
                    'name': 'soldout',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$main$',
                        'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$main$',
                      },
                    ],
                    'name': '_imageurl',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '-3|essential-t-shirt-aus-bio-baumwolle-mw0mw10812100',
                      },
                    ],
                    'name': 'seo_url',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW10812526,HYACINTH VIOLET,essential-t-shirt-aus-bio-baumwolle-mw0mw10812526,PURPLE|MW0MW10812658,FIERY RED,essential-t-shirt-aus-bio-baumwolle-mw0mw10812658,RED|MW0MW10812403,SKY CAPTAIN,essential-t-shirt-aus-bio-baumwolle-mw0mw10812403,BLUE|MW0MW10812718,YELLOW CREAM,essential-t-shirt-aus-bio-baumwolle-mw0mw10812718,YELLOW|MW0MW10812100,BRIGHT WHITE,essential-t-shirt-aus-bio-baumwolle-mw0mw10812100,WHITE',
                      },
                    ],
                    'name': 'relatedcombis_de',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'BRIGHT WHITE',
                        'non-ml': 'product_attr_colour_brightwhite',
                      },
                    ],
                    'name': 'product_attr_colour',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '81',
                        'non-ml': '81',
                      },
                      {
                        'value': '115',
                        'non-ml': '115',
                      },
                      {
                        'value': '37',
                        'non-ml': '37',
                      },
                      {
                        'value': '7',
                        'non-ml': '7',
                      },
                      {
                        'value': '57',
                        'non-ml': '57',
                      },
                    ],
                    'name': 'stock',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': false,
                  },
                  {
                    'value': [
                      {
                        'value': 'Essential Organic Cotton T-Shirt',
                        'non-ml': 'Essential Organic Cotton T-Shirt',
                      },
                    ],
                    'name': 'ddproductname',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'WHITE',
                        'non-ml': 'WHITE',
                      },
                    ],
                    'name': 'ddmaincolour',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                ],
                id: 'p123456',
              },
              {
                attribute: [
                  {
                    'value': [
                      {
                        'value': '3.0',
                        'non-ml': '3.0',
                      },
                    ],
                    'name': '_match_rate',
                    'basetype': 'float',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'p531093',
                        'non-ml': 'p531093',
                      },
                    ],
                    'name': 'secondid',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Herren',
                        'non-ml': 'gender_men',
                      },
                    ],
                    'name': 'gender',
                    'basetype': 'set',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'TOMMY HILFIGER',
                        'non-ml': 'label_tommyhilfiger',
                      },
                    ],
                    'name': 'label',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Essential T-Shirt aus Bio-Baumwolle',
                      },
                    ],
                    'name': 'name',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'onsale_de',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW10812100',
                        'non-ml': 'MW0MW10812100',
                      },
                    ],
                    'name': 'partnumber',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'w50.00',
                        'non-ml': 'w50.00',
                      },
                    ],
                    'name': 'price_data_de',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '39.9',
                        'non-ml': '39.9',
                      },
                    ],
                    'name': 'price_de',
                    'basetype': 'float',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '10155:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_DE|10156:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_CH|10160:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_BE',
                      },
                    ],
                    'name': 'primarycategory',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'product_attr_new_arrival',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'NEU',
                        'non-ml': 'product_attr_promo_de_new',
                      },
                    ],
                    'name': 'product_attr_promo_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '6:S',
                        'non-ml': 'size_s_de',
                      },
                    ],
                    'name': 'size_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '0',
                        'non-ml': '0',
                      },
                    ],
                    'name': 'soldout',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$main$',
                        'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$main$',
                      },
                    ],
                    'name': '_imageurl',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '-3|essential-t-shirt-aus-bio-baumwolle-mw0mw10812100',
                      },
                    ],
                    'name': 'seo_url',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW10812526,HYACINTH VIOLET,essential-t-shirt-aus-bio-baumwolle-mw0mw10812526,PURPLE|MW0MW10812658,FIERY RED,essential-t-shirt-aus-bio-baumwolle-mw0mw10812658,RED|MW0MW10812403,SKY CAPTAIN,essential-t-shirt-aus-bio-baumwolle-mw0mw10812403,BLUE|MW0MW10812718,YELLOW CREAM,essential-t-shirt-aus-bio-baumwolle-mw0mw10812718,YELLOW|MW0MW10812100,BRIGHT WHITE,essential-t-shirt-aus-bio-baumwolle-mw0mw10812100,WHITE',
                      },
                    ],
                    'name': 'relatedcombis_de',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'BRIGHT WHITE',
                        'non-ml': 'product_attr_colour_brightwhite',
                      },
                    ],
                    'name': 'product_attr_colour',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '81',
                        'non-ml': '81',
                      },
                      {
                        'value': '115',
                        'non-ml': '115',
                      },
                      {
                        'value': '37',
                        'non-ml': '37',
                      },
                      {
                        'value': '7',
                        'non-ml': '7',
                      },
                      {
                        'value': '57',
                        'non-ml': '57',
                      },
                    ],
                    'name': 'stock',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': false,
                  },
                  {
                    'value': [
                      {
                        'value': 'Essential Organic Cotton T-Shirt',
                        'non-ml': 'Essential Organic Cotton T-Shirt',
                      },
                    ],
                    'name': 'ddproductname',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'WHITE',
                        'non-ml': 'WHITE',
                      },
                    ],
                    'name': 'ddmaincolour',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                ],
                id: 'p123457',
              }, {
                attribute: [
                  {
                    'value': [
                      {
                        'value': '3.0',
                        'non-ml': '3.0',
                      },
                    ],
                    'name': '_match_rate',
                    'basetype': 'float',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'p531093',
                        'non-ml': 'p531093',
                      },
                    ],
                    'name': 'secondid',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Herren',
                        'non-ml': 'gender_men',
                      },
                    ],
                    'name': 'gender',
                    'basetype': 'set',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'TOMMY HILFIGER',
                        'non-ml': 'label_tommyhilfiger',
                      },
                    ],
                    'name': 'label',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Essential T-Shirt aus Bio-Baumwolle',
                      },
                    ],
                    'name': 'name',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'onsale_de',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW10812100',
                        'non-ml': 'MW0MW10812100',
                      },
                    ],
                    'name': 'partnumber',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '10155:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_DE|10156:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_CH|10160:TH_MEN_FEATURES_SUSTAINABLEEVOLUTION_BE',
                      },
                    ],
                    'name': 'primarycategory',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'product_attr_new_arrival',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'NEU',
                        'non-ml': 'product_attr_promo_de_new',
                      },
                    ],
                    'name': 'product_attr_promo_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '6:S',
                        'non-ml': 'size_s_de',
                      },
                    ],
                    'name': 'size_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '0',
                        'non-ml': '0',
                      },
                    ],
                    'name': 'soldout',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$main$',
                        'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$main$',
                      },
                    ],
                    'name': '_imageurl',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '-3|essential-t-shirt-aus-bio-baumwolle-mw0mw10812100',
                      },
                    ],
                    'name': 'seo_url',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW10812526,HYACINTH VIOLET,essential-t-shirt-aus-bio-baumwolle-mw0mw10812526,PURPLE|MW0MW10812658,FIERY RED,essential-t-shirt-aus-bio-baumwolle-mw0mw10812658,RED|MW0MW10812403,SKY CAPTAIN,essential-t-shirt-aus-bio-baumwolle-mw0mw10812403,BLUE|MW0MW10812718,YELLOW CREAM,essential-t-shirt-aus-bio-baumwolle-mw0mw10812718,YELLOW|MW0MW10812100,BRIGHT WHITE,essential-t-shirt-aus-bio-baumwolle-mw0mw10812100,WHITE',
                      },
                    ],
                    'name': 'relatedcombis_de',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'BRIGHT WHITE',
                        'non-ml': 'product_attr_colour_brightwhite',
                      },
                    ],
                    'name': 'product_attr_colour',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '81',
                        'non-ml': '81',
                      },
                      {
                        'value': '115',
                        'non-ml': '115',
                      },
                      {
                        'value': '37',
                        'non-ml': '37',
                      },
                      {
                        'value': '7',
                        'non-ml': '7',
                      },
                      {
                        'value': '57',
                        'non-ml': '57',
                      },
                    ],
                    'name': 'stock',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': false,
                  },
                  {
                    'value': [
                      {
                        'value': 'Essential Organic Cotton T-Shirt',
                        'non-ml': 'Essential Organic Cotton T-Shirt',
                      },
                    ],
                    'name': 'ddproductname',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'WHITE',
                        'non-ml': 'WHITE',
                      },
                    ],
                    'name': 'ddmaincolour',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                ],
                id: 'p123458',
              },
            ],
          },
          results: {
            'total-items': 2,
          },
        }
        const tenant = 'th'
        const countryCode = 'de'

        const { products, totalProductsAmount } =
          await parseListFromItemsSection(itemsSection, tenant, countryCode)

        expect(products).toBeDefined()
        expect(products.length).toBe(2)

        expect(totalProductsAmount).toBe(2)

        const [p1, p2] = products
        expect(p1).toBeDefined()
        expect(p2).toBeDefined()

        expect(p1.id).toBe('123456')
        expect(p1.name).toBe('Essential T-Shirt aus Bio-Baumwolle')
        expect(p1.partNumber).toBe('MW0MW10812100')
        expect(p1.label).toBe('TOMMY HILFIGER')
        expect(p1.price).toBe(39.9)
        expect(p1.currency).toBe('EUR')
        expect(p1.mainImage)
          .toBe('http://tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$eisPLP$')
        expect(p1.colorId).toBe('PRODUCT_ATTR_COLOUR_BRIGHTWHITE')
        expect(p1.isStyleColor).toBe(false)
        expect(p1.styleId).toBe(undefined)
        expect(p1.colorsAmount).toBe(5)

        expect(p2.id).toBe('123457')
        expect(p2.name).toBe('Essential T-Shirt aus Bio-Baumwolle')
        expect(p2.partNumber).toBe('MW0MW10812100')
        expect(p2.label).toBe('TOMMY HILFIGER')
        expect(p2.price).toBe(39.9)
        expect(p2.wasPrice).toBe(50)
        expect(p2.currency).toBe('EUR')
        expect(p2.mainImage)
          .toBe('http://tommy-europe.scene7.com/is/image/TommyEurope/MW0MW10812_100_main?$eisPLP$')
        expect(p2.colorId).toBe('PRODUCT_ATTR_COLOUR_BRIGHTWHITE')
        expect(p2.isStyleColor).toBe(false)
        expect(p2.styleId).toBe(undefined)
        expect(p2.colorsAmount).toBe(5)
      })
  })

  describe('generateImageURLsForProduct', () => {
    it('should be defined', () => {
      expect(generateImageURLsForProduct).toBeDefined()
    })

    it('should generate 4 image urls for a given product', () => {
      let result = generateImageURLsForProduct('th', 'de', 'ABC123')

      expect(result.length).toBe(4)

      result = generateImageURLsForProduct('ck', 'de', 'ABC123')
      expect(result.length).toBe(4)
    })

    it('should autofill <SIZE> with eisPDP for iTommy app', () => {
      let result = generateImageURLsForProduct('th', 'de', 'ABC123')

      expect(result.length).toBe(4)

      const sizePlaceholderRegex = /<SIZE>/
      expect(sizePlaceholderRegex.test(result[0])).toBeFalsy()

      const eisPDPRegex = /eisPDP/
      expect(eisPDPRegex.test(result[0])).toBeTruthy()

      const swatchRegex = /minithumb/
      expect(swatchRegex.test(result[3])).toBeTruthy()
    })
  })

  describe('generateSizesForProductColor', () => {
    it('should be defined', () => {
      expect(generateSizesForProductColor).toBeDefined()
    })

    it('should generate sizes for a product style color that is on sale with different prices per size', async () => {
      const attributes = {
        price_de: { value: [99.9], 'non-ml': [99.9] },
        price_data_de: { value: ['c99.9w120.0'], 'non-ml': ['c99.9w120.0'] },
        sku_price_data_de: {
          value: ['cp_123:120.0;wp_124:120.0cp_124:99.9;wp_125:120.0cp_125:99.9'],
          'non-ml': [],
        },
        possible_variant_size_de: {
          value: ['v123:6:L|v124:4:M|v125:2:S'],
          'non-ml': ['v123:6:L|v124:4:M|v125:2:S'],
        },
        stock: {
          value: [1, 2, 3],
          'non-ml': [1, 2, 3],
        },
        product_attr_colour: {
          value: ['BLUE'],
          'non-ml': ['BLUE'],
        },
      }

      const result = await generateSizesForProductColor('th', 'de', attributes)

      expect(result).toBeDefined()
      expect(result.length).toBe(3)

      expect(result[0].price).toBe(99.9)
      expect(result[1].price).toBe(99.9)
      expect(result[2].price).toBe(120)

      expect(result[0].wasPrice).toBe(120)
      expect(result[1].wasPrice).toBe(120)
      expect(result[2].wasPrice).toBeUndefined()

      expect(result[0].id).toBe('125')
      expect(result[1].id).toBe('124')
      expect(result[2].id).toBe('123')

      expect(result[0].inventory.onlineInventory).toBe(3)
      expect(result[1].inventory.onlineInventory).toBe(2)
      expect(result[2].inventory.onlineInventory).toBe(1)

      expect(result[0].size.value).toBe('S')
      expect(result[1].size.value).toBe('M')
      expect(result[2].size.value).toBe('L')

      expect(result[0].size.length).toBeUndefined()
      expect(result[0].size.width).toBeUndefined()
      expect(result[1].size.length).toBeUndefined()
      expect(result[1].size.width).toBeUndefined()
      expect(result[2].size.length).toBeUndefined()
      expect(result[2].size.width).toBeUndefined()
    })

    it('should generate sizes for a product style color that is on sale with same prices per size', async () => {
      const attributes = {
        price_de: { value: [99.9], 'non-ml': [99.9] },
        price_data_de: { value: ['w120.0'], 'non-ml': ['w120.0'] },
        possible_variant_size_de: {
          value: ['v123:6:L|v124:4:M|v125:2:S'],
          'non-ml': ['v123:6:L|v124:4:M|v125:2:S'],
        },
        stock: {
          value: [1, 2, 3],
          'non-ml': [1, 2, 3],
        },
        product_attr_colour: {
          value: ['BLUE'],
          'non-ml': ['BLUE'],
        },
      }

      const result = await generateSizesForProductColor('th', 'de', attributes)

      expect(result).toBeDefined()
      expect(result.length).toBe(3)

      expect(result[0].price).toBe(99.9)
      expect(result[1].price).toBe(99.9)
      expect(result[2].price).toBe(99.9)

      expect(result[0].wasPrice).toBe(120)
      expect(result[1].wasPrice).toBe(120)
      expect(result[2].wasPrice).toBe(120)

      expect(result[0].id).toBe('125')
      expect(result[1].id).toBe('124')
      expect(result[2].id).toBe('123')

      expect(result[0].inventory.onlineInventory).toBe(3)
      expect(result[1].inventory.onlineInventory).toBe(2)
      expect(result[2].inventory.onlineInventory).toBe(1)

      expect(result[0].size.value).toBe('S')
      expect(result[1].size.value).toBe('M')
      expect(result[2].size.value).toBe('L')

      expect(result[0].size.length).toBeUndefined()
      expect(result[0].size.width).toBeUndefined()
      expect(result[1].size.length).toBeUndefined()
      expect(result[1].size.width).toBeUndefined()
      expect(result[2].size.length).toBeUndefined()
      expect(result[2].size.width).toBeUndefined()
    })

    it('should correctly provide prices for a product style color that is not on sale but with different prices per size', async () => {
      const attributes = {
        price_de: { value: [99.9], 'non-ml': [99.9] },
        price_data_de: { value: ['w120.0'], 'non-ml': ['w120.0'] },
        sku_price_data_de: {
          value: ['cp_123:120.0;cp_124:99.9;cp_125:99.9'],
          'non-ml': [],
        },
        possible_variant_size_de: {
          value: ['v123:6:L|v124:4:M|v125:2:S'],
          'non-ml': ['v123:6:L|v124:4:M|v125:2:S'],
        },
        stock: {
          value: [1, 2, 3],
          'non-ml': [1, 2, 3],
        },
        product_attr_colour: {
          value: ['BLUE'],
          'non-ml': ['BLUE'],
        },
      }

      const result = await generateSizesForProductColor('th', 'de', attributes)

      expect(result).toBeDefined()
      expect(result.length).toBe(3)

      expect(result[0].price).toBe(99.9)
      expect(result[1].price).toBe(99.9)
      expect(result[2].price).toBe(120)

      expect(result[0].wasPrice).toBeUndefined()
      expect(result[1].wasPrice).toBeUndefined()
      expect(result[2].wasPrice).toBeUndefined()

      expect(result[0].id).toBe('125')
      expect(result[1].id).toBe('124')
      expect(result[2].id).toBe('123')

      expect(result[0].inventory.onlineInventory).toBe(3)
      expect(result[1].inventory.onlineInventory).toBe(2)
      expect(result[2].inventory.onlineInventory).toBe(1)

      expect(result[0].size.value).toBe('S')
      expect(result[1].size.value).toBe('M')
      expect(result[2].size.value).toBe('L')

      expect(result[0].size.length).toBeUndefined()
      expect(result[0].size.width).toBeUndefined()
      expect(result[1].size.length).toBeUndefined()
      expect(result[1].size.width).toBeUndefined()
      expect(result[2].size.length).toBeUndefined()
      expect(result[2].size.width).toBeUndefined()
    })

    it('should correctly provide prices for a product style color that is not on sale', async () => {
      const attributes = {
        price_de: { value: [99.9], 'non-ml': [99.9] },
        possible_variant_size_de: {
          value: ['v123:6:L|v124:4:M|v125:2:S'],
          'non-ml': ['v123:6:L|v124:4:M|v125:2:S'],
        },
        stock: {
          value: [1, 2, 3],
          'non-ml': [1, 2, 3],
        },
        product_attr_colour: {
          value: ['BLUE'],
          'non-ml': ['BLUE'],
        },
      }

      const result = await generateSizesForProductColor('th', 'de', attributes)

      expect(result).toBeDefined()
      expect(result.length).toBe(3)

      expect(result[0].price).toBe(99.9)
      expect(result[1].price).toBe(99.9)
      expect(result[2].price).toBe(99.9)

      expect(result[0].wasPrice).toBeUndefined()
      expect(result[1].wasPrice).toBeUndefined()
      expect(result[2].wasPrice).toBeUndefined()

      expect(result[0].id).toBe('125')
      expect(result[1].id).toBe('124')
      expect(result[2].id).toBe('123')

      expect(result[0].inventory.onlineInventory).toBe(3)
      expect(result[1].inventory.onlineInventory).toBe(2)
      expect(result[2].inventory.onlineInventory).toBe(1)

      expect(result[0].size.value).toBe('S')
      expect(result[1].size.value).toBe('M')
      expect(result[2].size.value).toBe('L')

      expect(result[0].size.length).toBeUndefined()
      expect(result[0].size.width).toBeUndefined()
      expect(result[1].size.length).toBeUndefined()
      expect(result[1].size.width).toBeUndefined()
      expect(result[2].size.length).toBeUndefined()
      expect(result[2].size.width).toBeUndefined()
    })

    it(
      'should generate sizes with length and width for a product style color',
      async () => {
        const attributes = {
          price_de: { value: [99.9], 'non-ml': [99.9] },
          possible_variant_size_de: {
            value: ['v123:2:3032|v124:4:3034|v125:6:3036'],
            'non-ml': ['v123:2:3032|v124:4:3034|v125:6:3036'],
          },
          possible_variant_sizelength: {
            value: ['v123:2:32|v124:4:34|v125:6:36'],
            'non-ml': ['v123:2:32|v124:4:34|v125:6:36'],
          },
          possible_variant_sizewidth: {
            value: ['v123:2:30|v124:4:30|v125:6:30'],
            'non-ml': ['v123:2:30|v124:4:30|v125:6:30'],
          },
          stock: {
            value: [1, 2, 3],
            'non-ml': [1, 2, 3],
          },
          product_attr_colour: {
            value: ['BLUE'],
            'non-ml': ['BLUE'],
          },
        }

        const result = await generateSizesForProductColor('th', 'de', attributes)

        expect(result).toBeDefined()
        expect(result.length).toBe(3)

        expect(result[0].id).toBe('123')
        expect(result[1].id).toBe('124')
        expect(result[2].id).toBe('125')

        expect(result[0].inventory.onlineInventory).toBe(1)
        expect(result[1].inventory.onlineInventory).toBe(2)
        expect(result[2].inventory.onlineInventory).toBe(3)

        expect(result[0].size.value).toBe('30/32')
        expect(result[1].size.value).toBe('30/34')
        expect(result[2].size.value).toBe('30/36')

        expect(result[0].size.length).toBe('32')
        expect(result[0].size.width).toBe('30')
        expect(result[1].size.length).toBe('34')
        expect(result[1].size.width).toBe('30')
        expect(result[2].size.length).toBe('36')
        expect(result[2].size.width).toBe('30')
      })
  })

  describe('generateColorsForProduct', () => {
    it('should be defined', () => {
      expect(generateColorsForProduct).toBeDefined()
    })

    it('should return empty array if no themes exist', async () => {
      const theme = []
      const result = await generateColorsForProduct('th', 'de', theme)

      expect(result).toBeDefined()
      expect(result.length).toBe(0)
    })

    it('should parse theme with name "PDP - Other Style Colours"', async () => {
      const item = [
        {
          'attribute': [
            {
              'value': [
                {
                  'value': 'p429905',
                  'non-ml': 'p429905',
                },
              ],
              'name': 'secondid',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '0',
                  'non-ml': '0',
                },
              ],
              'name': 'onsale_de',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'MW0MW09499093',
                  'non-ml': 'MW0MW09499093',
                },
              ],
              'name': 'partnumber',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Logo-Jogginghose aus Baumwolle',
                },
              ],
              'name': 'name',
              'basetype': 'asset',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'TOMMY HILFIGER',
                  'non-ml': 'label_tommyhilfiger',
                },
              ],
              'name': 'label',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                  'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                },
              ],
              'name': '_imageurl',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'men',
                  'non-ml': 'gender_men',
                },
              ],
              'name': 'gender',
              'basetype': 'set',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                },
              ],
              'name': 'description',
              'basetype': 'asset',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '99.9',
                  'non-ml': '99.9',
                },
              ],
              'name': 'price_de',
              'basetype': 'float',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                },
              ],
              'name': 'primarycategory',
              'basetype': 'asset',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '1',
                  'non-ml': '1',
                },
              ],
              'name': 'product_attr_new_arrival',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Neu',
                  'non-ml': 'product_attr_promo_de_new',
                },
              ],
              'name': 'product_attr_promo_de',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '14:XXL',
                  'non-ml': 'size_xxl_de',
                },
              ],
              'name': 'size_de',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '0',
                  'non-ml': '0',
                },
              ],
              'name': 'soldout',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Sweatpants',
                  'non-ml': 'style_sweatpants',
                },
              ],
              'name': 'style',
              'basetype': 'set',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                  'non-ml': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                },
              ],
              'name': 'possible_variant_size_de',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '429906,123|429907,124|429908,125|429909,126|429910,127',
                },
              ],
              'name': 'skupartnumbers',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'MW0MW09499',
                  'non-ml': 'MW0MW09499',
                },
              ],
              'name': 'stylepartnumber',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '5',
                  'non-ml': '5',
                },
              ],
              'name': 'product_attr_alt_image_count',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'CHARCOAL HTR',
                  'non-ml': 'product_attr_colour_charcoalhtr',
                },
              ],
              'name': 'product_attr_colour',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
          ],
          'link': [
            {
              'name': 'Detail',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cdetail%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll&fh_secondid=p429905&fh_secondid2=v429910',
              'type': 'detail',
              'nr': 1,
            },
            {
              'name': 'Jogginghosen',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth137men137clothing_cth137men137clothing137trousers_cth137men137clothing137trousers137sweatpants%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'lowest_category',
              'nr': 44,
            },
            {
              'name': 'NEW IN',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth1371496236380623%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'lowest_category',
              'nr': 531,
            },
          ],
          'id': 'p429905',
        },
      ]
      const result = await generateColorsForProduct('th', 'de', item, 'http://test.com/product-123', 'itommy')

      expect(result).toBeDefined()
      expect(result.length).toBe(1)
      expect(result[0].url).toBe('http://test.com/product-123')
      expect(result[0].id).toBe('PRODUCT_ATTR_COLOUR_CHARCOALHTR')
      expect(result[0].value).toBe('CHARCOAL HTR')
    })

    it('should skip theme items with invalid data structure', async () => {
      const item = [
        {
          'attribute': [
            {
              'value': [
                {
                  'value': '0',
                  'non-ml': '0',
                },
              ],
              'name': 'onsale_de',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Logo-Jogginghose aus Baumwolle',
                },
              ],
              'name': 'name',
              'basetype': 'asset',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'TOMMY HILFIGER',
                  'non-ml': 'label_tommyhilfiger',
                },
              ],
              'name': 'label',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                  'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                },
              ],
              'name': '_imageurl',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'men',
                  'non-ml': 'gender_men',
                },
              ],
              'name': 'gender',
              'basetype': 'set',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                },
              ],
              'name': 'description',
              'basetype': 'asset',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                },
              ],
              'name': 'primarycategory',
              'basetype': 'asset',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '1',
                  'non-ml': '1',
                },
              ],
              'name': 'product_attr_new_arrival',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Neu',
                  'non-ml': 'product_attr_promo_de_new',
                },
              ],
              'name': 'product_attr_promo_de',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '14:XXL',
                  'non-ml': 'size_xxl_de',
                },
              ],
              'name': 'size_de',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '0',
                  'non-ml': '0',
                },
              ],
              'name': 'soldout',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'Sweatpants',
                  'non-ml': 'style_sweatpants',
                },
              ],
              'name': 'style',
              'basetype': 'set',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                  'non-ml': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                },
              ],
              'name': 'possible_variant_size_de',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '429906,123|429907,124|429908,125|429909,126|429910,127',
                },
              ],
              'name': 'skupartnumbers',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'MW0MW09499',
                  'non-ml': 'MW0MW09499',
                },
              ],
              'name': 'stylepartnumber',
              'basetype': 'text',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': '5',
                  'non-ml': '5',
                },
              ],
              'name': 'product_attr_alt_image_count',
              'basetype': 'int',
              'isnull': false,
              'selected': true,
            },
            {
              'value': [
                {
                  'value': 'CHARCOAL HTR',
                  'non-ml': 'product_attr_colour_charcoalhtr',
                },
              ],
              'name': 'product_attr_colour',
              'basetype': 'list',
              'isnull': false,
              'selected': true,
            },
          ],
          'link': [
            {
              'name': 'Detail',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cdetail%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll&fh_secondid=p429905&fh_secondid2=v429910',
              'type': 'detail',
              'nr': 1,
            },
            {
              'name': 'Jogginghosen',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth137men137clothing_cth137men137clothing137trousers_cth137men137clothing137trousers137sweatpants%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'lowest_category',
              'nr': 44,
            },
            {
              'name': 'NEW IN',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth1371496236380623%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'lowest_category',
              'nr': 531,
            },
          ],
          'id': 'p429905',
        },
      ]
      const result = await generateColorsForProduct(
        'th', 'de', item, 'http://test.com/product-123', 'itommy'
      )

      expect(result).toBeDefined()
      expect(result.length).toBe(0)
    })
  })

  describe('parseProductFromItemsSectionAndThemes', () => {
    it('should be defined', () => {
      expect(parseProductFromItemsSectionAndThemes).toBeDefined()
    })

    it(
      'should return empty object if items are missing from itemsSection',
      async () => {
        const result = await parseProductFromItemsSectionAndThemes('th', 'de', {}, [])
        expect(result).toBeDefined()
        expect(result).toEqual({})
      })

    it('should return product from itemsSection and themes', async () => {
      const itemsSection = {
        'items': {
          'item': [
            {
              'attribute': [
                {
                  'value': [
                    {
                      'value': 'p429811',
                      'non-ml': 'p429811',
                    },
                  ],
                  'name': 'secondid',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                    },
                  ],
                  'name': 'description',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'men',
                      'non-ml': 'gender_men',
                    },
                  ],
                  'name': 'gender',
                  'basetype': 'set',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_083_main?$main$',
                      'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_083_main?$main$',
                    },
                  ],
                  'name': '_imageurl',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'TOMMY HILFIGER',
                      'non-ml': 'label_tommyhilfiger',
                    },
                  ],
                  'name': 'label',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Logo-Jogginghose aus Baumwolle',
                    },
                  ],
                  'name': 'name',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '1',
                      'non-ml': '1',
                    },
                  ],
                  'name': 'onsale_de',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'MW0MW09499083',
                      'non-ml': 'MW0MW09499083',
                    },
                  ],
                  'name': 'partnumber',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'w199.9',
                      'non-ml': 'w199.9',
                    },
                  ],
                  'name': 'price_data_de',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '99.9',
                      'non-ml': '99.9',
                    },
                  ],
                  'name': 'price_de',
                  'basetype': 'float',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                    },
                  ],
                  'name': 'primarycategory',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '1',
                      'non-ml': '1',
                    },
                  ],
                  'name': 'product_attr_new_arrival',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Neu',
                      'non-ml': 'product_attr_promo_de_new',
                    },
                  ],
                  'name': 'product_attr_promo_de',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '6:S',
                      'non-ml': 'size_s_de',
                    },
                  ],
                  'name': 'size_de',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '0',
                      'non-ml': '0',
                    },
                  ],
                  'name': 'soldout',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Sweatpants',
                      'non-ml': 'style_sweatpants',
                    },
                  ],
                  'name': 'style',
                  'basetype': 'set',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'v429812:12:XL|v429813:14:XXL|v429814:6:S|v429815:8:M|v429816:10:L',
                      'non-ml': 'v429812:12:XL|v429813:14:XXL|v429814:6:S|v429815:8:M|v429816:10:L',
                    },
                  ],
                  'name': 'possible_variant_size_de',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'MW0MW09499',
                      'non-ml': 'MW0MW09499',
                    },
                  ],
                  'name': 'stylepartnumber',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Tommy Hilfiger',
                      'non-ml': 'th',
                    },
                    {
                      'value': 'Herren',
                      'non-ml': 'cth137men',
                    },
                    {
                      'value': 'Kleidung',
                      'non-ml': 'cth137men137clothing',
                    },
                    {
                      'value': 'Hosen',
                      'non-ml': 'cth137men137clothing137trousers',
                    },
                    {
                      'value': 'Jogginghosen',
                      'non-ml': 'cth137men137clothing137trousers137sweatpants',
                    },
                    {
                      'value': 'NEW IN',
                      'non-ml': 'cth1371496236380623',
                    },
                  ],
                  'name': 'search_categories',
                  'basetype': 'set',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'JET BLACK',
                      'non-ml': 'product_attr_colour_jetblack',
                    },
                  ],
                  'name': 'product_attr_colour',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '20',
                      'non-ml': '20',
                    },
                    {
                      'value': '39',
                      'non-ml': '39',
                    },
                    {
                      'value': '58',
                      'non-ml': '58',
                    },
                    {
                      'value': '63',
                      'non-ml': '63',
                    },
                    {
                      'value': '15',
                      'non-ml': '15',
                    },
                  ],
                  'name': 'stock',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': false,
                },
                {
                  'value': [
                    {
                      'value': '5',
                      'non-ml': '5',
                    },
                  ],
                  'name': 'product_attr_alt_image_count',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'PH02',
                      'non-ml': 'sapsizechart_ph02',
                    },
                  ],
                  'name': 'sapsizechart',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Herren für men',
                    },
                  ],
                  'name': 'productseokeyword',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '-3|logo-jogginghose-aus-baumwolle-mw0mw09499083',
                    },
                  ],
                  'name': 'seo_url',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Pure Cotton Logo Joggers',
                      'non-ml': 'Pure Cotton Logo Joggers',
                    },
                  ],
                  'name': 'ddproductname',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'D012004|TH Menswear-Men-Heavyweight Knits',
                      'non-ml': 'D012004|TH Menswear-Men-Heavyweight Knits',
                    },
                  ],
                  'name': 'ddmerchhierarchy',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'BLACK',
                      'non-ml': 'BLACK',
                    },
                  ],
                  'name': 'ddmaincolour',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'MW0MW09499083,JET BLACK,logo-jogginghose-aus-baumwolle-mw0mw09499083,BLACK,No|MW0MW09499093,CHARCOAL HTR,logo-jogginghose-aus-baumwolle-mw0mw09499093,GREY,No',
                      'non-ml': 'MW0MW09499083,JET BLACK,logo-jogginghose-aus-baumwolle-mw0mw09499083,BLACK,No|MW0MW09499093,CHARCOAL HTR,logo-jogginghose-aus-baumwolle-mw0mw09499093,GREY,No',
                    },
                  ],
                  'name': 'relatedcombis_de',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
              ],
              'link': [
                {
                  'name': 'Detail',
                  'url-params': 'fh_secondid=p429811&fh_location=%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_modification=d3d60d5c-6aae-4bcb-82e3-f3d615d54d79%2ce6a0e6f1-a5d5-4f07-a195-3720f3357f43%2c',
                  'type': 'detail',
                },
              ],
              'similar-item-fields': {
                'attribute': [
                  {
                    'value': [
                      {
                        'value': 'p429811',
                        'non-ml': 'p429811',
                      },
                    ],
                    'name': 'secondid',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                ],
                'location-param-prefix': '%2f%2fth%2fde_DE',
              },
              'id': 'p429811',
            },
          ],
        },
      }
      const themes = [{
        'theme': [{
          'title': '',
          'slogan': '',
          'custom-fields': {
            'custom-field': [
              {
                'value': 'PDP_OTHER_STYLE_COLOURS',
                'name': 'Type',
                'id': '9826e1f1-885c-40e2-b843-dbc9eae1b264',
              },
            ],
          },
          'link': [
            {
              'name': 'Tommy Hilfiger',
              'url-params': 'fh_location=%2f%2fth%2fde_DE&fh_reftheme=447ebd71440869b03710410881006b5f%2cseeall%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'see_all',
              'nr': 7723,
            },
            {
              'name': 'Filtered',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cfiltered-all%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'filtered_all',
              'nr': 1,
            },
          ],
          'items': {
            'item': [
              {
                'attribute': [
                  {
                    'value': [
                      {
                        'value': 'p429905',
                        'non-ml': 'p429905',
                      },
                    ],
                    'name': 'secondid',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'onsale_de',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW09499093',
                        'non-ml': 'MW0MW09499093',
                      },
                    ],
                    'name': 'partnumber',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Logo-Jogginghose aus Baumwolle',
                      },
                    ],
                    'name': 'name',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'TOMMY HILFIGER',
                        'non-ml': 'label_tommyhilfiger',
                      },
                    ],
                    'name': 'label',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                        'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                      },
                    ],
                    'name': '_imageurl',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'men',
                        'non-ml': 'gender_men',
                      },
                    ],
                    'name': 'gender',
                    'basetype': 'set',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                      },
                    ],
                    'name': 'description',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '99.9',
                        'non-ml': '99.9',
                      },
                    ],
                    'name': 'price_de',
                    'basetype': 'float',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                      },
                    ],
                    'name': 'primarycategory',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'product_attr_new_arrival',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Neu',
                        'non-ml': 'product_attr_promo_de_new',
                      },
                    ],
                    'name': 'product_attr_promo_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '14:XXL',
                        'non-ml': 'size_xxl_de',
                      },
                    ],
                    'name': 'size_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '0',
                        'non-ml': '0',
                      },
                    ],
                    'name': 'soldout',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Sweatpants',
                        'non-ml': 'style_sweatpants',
                      },
                    ],
                    'name': 'style',
                    'basetype': 'set',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                        'non-ml': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                      },
                    ],
                    'name': 'possible_variant_size_de',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '429906,123|429907,124|429908,125|429909,126|429910,127',
                      },
                    ],
                    'name': 'skupartnumbers',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW09499',
                        'non-ml': 'MW0MW09499',
                      },
                    ],
                    'name': 'stylepartnumber',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '5',
                        'non-ml': '5',
                      },
                    ],
                    'name': 'product_attr_alt_image_count',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'CHARCOAL HTR',
                        'non-ml': 'product_attr_colour_charcoalhtr',
                      },
                    ],
                    'name': 'product_attr_colour',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                ],
                'link': [
                  {
                    'name': 'Detail',
                    'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cdetail%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll&fh_secondid=p429905&fh_secondid2=v429910',
                    'type': 'detail',
                    'nr': 1,
                  },
                  {
                    'name': 'Jogginghosen',
                    'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth137men137clothing_cth137men137clothing137trousers_cth137men137clothing137trousers137sweatpants%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                    'type': 'lowest_category',
                    'nr': 44,
                  },
                  {
                    'name': 'NEW IN',
                    'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth1371496236380623%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                    'type': 'lowest_category',
                    'nr': 531,
                  },
                ],
                'id': 'p429905',
              },
            ],
          },
          'id': '447ebd71440869b03710410881006b5f',
          'name': 'PDP - Other Style Colours',
          'type': 'facet',
        }],
      }]

      const result = await parseProductFromItemsSectionAndThemes(
        'th', 'de', itemsSection, themes, 'itommy'
      )

      expect(result).toBeDefined()
      const {
        id, name, description, price, wasPrice, currency, label, colors, url,
      } = result
      expect(id).toBeDefined()
      expect(name).toBeDefined()
      expect(description).toBeDefined()
      expect(price).toBeDefined()
      expect(wasPrice).toBeDefined()
      expect(currency).toBeDefined()
      expect(label).toBeDefined()
      expect(colors).toBeDefined()
      expect(url).toBeDefined()
    })

    it('should not return product that doesn\'t match specified schema', async () => {
      const itemsSection = {
        'items': {
          'item': [
            {
              'attribute': [
                {
                  'value': [
                    {
                      'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                    },
                  ],
                  'name': 'description',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'men',
                      'non-ml': 'gender_men',
                    },
                  ],
                  'name': 'gender',
                  'basetype': 'set',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_083_main?$main$',
                      'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_083_main?$main$',
                    },
                  ],
                  'name': '_imageurl',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'TOMMY HILFIGER',
                      'non-ml': 'label_tommyhilfiger',
                    },
                  ],
                  'name': 'label',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Logo-Jogginghose aus Baumwolle',
                    },
                  ],
                  'name': 'name',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '0',
                      'non-ml': '0',
                    },
                  ],
                  'name': 'onsale_de',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'MW0MW09499083',
                      'non-ml': 'MW0MW09499083',
                    },
                  ],
                  'name': 'partnumber',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '99.9',
                      'non-ml': '99.9',
                    },
                  ],
                  'name': 'price_de',
                  'basetype': 'float',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                    },
                  ],
                  'name': 'primarycategory',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '1',
                      'non-ml': '1',
                    },
                  ],
                  'name': 'product_attr_new_arrival',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Neu',
                      'non-ml': 'product_attr_promo_de_new',
                    },
                  ],
                  'name': 'product_attr_promo_de',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '6:S',
                      'non-ml': 'size_s_de',
                    },
                  ],
                  'name': 'size_de',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '0',
                      'non-ml': '0',
                    },
                  ],
                  'name': 'soldout',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Sweatpants',
                      'non-ml': 'style_sweatpants',
                    },
                  ],
                  'name': 'style',
                  'basetype': 'set',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'v429812:12:XL|v429813:14:XXL|v429814:6:S|v429815:8:M|v429816:10:L',
                      'non-ml': 'v429812:12:XL|v429813:14:XXL|v429814:6:S|v429815:8:M|v429816:10:L',
                    },
                  ],
                  'name': 'possible_variant_size_de',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'MW0MW09499',
                      'non-ml': 'MW0MW09499',
                    },
                  ],
                  'name': 'stylepartnumber',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Tommy Hilfiger',
                      'non-ml': 'th',
                    },
                    {
                      'value': 'Herren',
                      'non-ml': 'cth137men',
                    },
                    {
                      'value': 'Kleidung',
                      'non-ml': 'cth137men137clothing',
                    },
                    {
                      'value': 'Hosen',
                      'non-ml': 'cth137men137clothing137trousers',
                    },
                    {
                      'value': 'Jogginghosen',
                      'non-ml': 'cth137men137clothing137trousers137sweatpants',
                    },
                    {
                      'value': 'NEW IN',
                      'non-ml': 'cth1371496236380623',
                    },
                  ],
                  'name': 'search_categories',
                  'basetype': 'set',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'JET BLACK',
                      'non-ml': 'product_attr_colour_jetblack',
                    },
                  ],
                  'name': 'product_attr_colour',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': '20',
                      'non-ml': '20',
                    },
                    {
                      'value': '39',
                      'non-ml': '39',
                    },
                    {
                      'value': '58',
                      'non-ml': '58',
                    },
                    {
                      'value': '63',
                      'non-ml': '63',
                    },
                    {
                      'value': '15',
                      'non-ml': '15',
                    },
                  ],
                  'name': 'stock',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': false,
                },
                {
                  'value': [
                    {
                      'value': '5',
                      'non-ml': '5',
                    },
                  ],
                  'name': 'product_attr_alt_image_count',
                  'basetype': 'int',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'PH02',
                      'non-ml': 'sapsizechart_ph02',
                    },
                  ],
                  'name': 'sapsizechart',
                  'basetype': 'list',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Herren für men',
                    },
                  ],
                  'name': 'productseokeyword',
                  'basetype': 'asset',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'Pure Cotton Logo Joggers',
                      'non-ml': 'Pure Cotton Logo Joggers',
                    },
                  ],
                  'name': 'ddproductname',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'D012004|TH Menswear-Men-Heavyweight Knits',
                      'non-ml': 'D012004|TH Menswear-Men-Heavyweight Knits',
                    },
                  ],
                  'name': 'ddmerchhierarchy',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'BLACK',
                      'non-ml': 'BLACK',
                    },
                  ],
                  'name': 'ddmaincolour',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
                {
                  'value': [
                    {
                      'value': 'MW0MW09499083,JET BLACK,logo-jogginghose-aus-baumwolle-mw0mw09499083,BLACK,No|MW0MW09499093,CHARCOAL HTR,logo-jogginghose-aus-baumwolle-mw0mw09499093,GREY,No',
                      'non-ml': 'MW0MW09499083,JET BLACK,logo-jogginghose-aus-baumwolle-mw0mw09499083,BLACK,No|MW0MW09499093,CHARCOAL HTR,logo-jogginghose-aus-baumwolle-mw0mw09499093,GREY,No',
                    },
                  ],
                  'name': 'relatedcombis_de',
                  'basetype': 'text',
                  'isnull': false,
                  'selected': true,
                },
              ],
              'link': [
                {
                  'name': 'Detail',
                  'url-params': 'fh_secondid=p429811&fh_location=%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_modification=d3d60d5c-6aae-4bcb-82e3-f3d615d54d79%2ce6a0e6f1-a5d5-4f07-a195-3720f3357f43%2c',
                  'type': 'detail',
                },
              ],
              'similar-item-fields': {
                'attribute': [
                  {
                    'value': [
                      {
                        'value': 'p429811',
                        'non-ml': 'p429811',
                      },
                    ],
                    'name': 'secondid',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                ],
                'location-param-prefix': '%2f%2fth%2fde_DE',
              },
              'id': 'p429811',
            },
          ],
        },
      }
      const themes = [
        {
          'title': '',
          'slogan': '',
          'custom-fields': {
            'custom-field': [
              {
                'value': 'PDP_OTHER_STYLE_COLOURS',
                'name': 'Type',
                'id': '9826e1f1-885c-40e2-b843-dbc9eae1b264',
              },
            ],
          },
          'link': [
            {
              'name': 'Tommy Hilfiger',
              'url-params': 'fh_location=%2f%2fth%2fde_DE&fh_reftheme=447ebd71440869b03710410881006b5f%2cseeall%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'see_all',
              'nr': 7723,
            },
            {
              'name': 'Filtered',
              'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cfiltered-all%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
              'type': 'filtered_all',
              'nr': 1,
            },
          ],
          'items': {
            'item': [
              {
                'attribute': [
                  {
                    'value': [
                      {
                        'value': 'p429905',
                        'non-ml': 'p429905',
                      },
                    ],
                    'name': 'secondid',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '0',
                        'non-ml': '0',
                      },
                    ],
                    'name': 'onsale_de',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW09499093',
                        'non-ml': 'MW0MW09499093',
                      },
                    ],
                    'name': 'partnumber',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Logo-Jogginghose aus Baumwolle',
                      },
                    ],
                    'name': 'name',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'TOMMY HILFIGER',
                        'non-ml': 'label_tommyhilfiger',
                      },
                    ],
                    'name': 'label',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                        'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                      },
                    ],
                    'name': '_imageurl',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'men',
                        'non-ml': 'gender_men',
                      },
                    ],
                    'name': 'gender',
                    'basetype': 'set',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                      },
                    ],
                    'name': 'description',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '99.9',
                        'non-ml': '99.9',
                      },
                    ],
                    'name': 'price_de',
                    'basetype': 'float',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                      },
                    ],
                    'name': 'primarycategory',
                    'basetype': 'asset',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '1',
                        'non-ml': '1',
                      },
                    ],
                    'name': 'product_attr_new_arrival',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Neu',
                        'non-ml': 'product_attr_promo_de_new',
                      },
                    ],
                    'name': 'product_attr_promo_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '14:XXL',
                        'non-ml': 'size_xxl_de',
                      },
                    ],
                    'name': 'size_de',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '0',
                        'non-ml': '0',
                      },
                    ],
                    'name': 'soldout',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'Sweatpants',
                        'non-ml': 'style_sweatpants',
                      },
                    ],
                    'name': 'style',
                    'basetype': 'set',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                        'non-ml': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                      },
                    ],
                    'name': 'possible_variant_size_de',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '429906,123|429907,124|429908,125|429909,126|429910,127',
                      },
                    ],
                    'name': 'skupartnumbers',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'MW0MW09499',
                        'non-ml': 'MW0MW09499',
                      },
                    ],
                    'name': 'stylepartnumber',
                    'basetype': 'text',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': '5',
                        'non-ml': '5',
                      },
                    ],
                    'name': 'product_attr_alt_image_count',
                    'basetype': 'int',
                    'isnull': false,
                    'selected': true,
                  },
                  {
                    'value': [
                      {
                        'value': 'CHARCOAL HTR',
                        'non-ml': 'product_attr_colour_charcoalhtr',
                      },
                    ],
                    'name': 'product_attr_colour',
                    'basetype': 'list',
                    'isnull': false,
                    'selected': true,
                  },
                ],
                'link': [
                  {
                    'name': 'Detail',
                    'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cdetail%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll&fh_secondid=p429905&fh_secondid2=v429910',
                    'type': 'detail',
                    'nr': 1,
                  },
                  {
                    'name': 'Jogginghosen',
                    'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth137men137clothing_cth137men137clothing137trousers_cth137men137clothing137trousers137sweatpants%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                    'type': 'lowest_category',
                    'nr': 44,
                  },
                  {
                    'name': 'NEW IN',
                    'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth1371496236380623%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                    'type': 'lowest_category',
                    'nr': 531,
                  },
                ],
                'id': 'p429905',
              },
            ],
          },
          'id': '447ebd71440869b03710410881006b5f',
          'name': 'PDP - Other Style Colours',
          'type': 'facet',
        },
      ]

      const result = await parseProductFromItemsSectionAndThemes(
        'th', 'de', itemsSection, themes, 'itommy'
      )

      expect(result).toEqual({})
      const {
        id, name, description, price, currency, label, colors, url,
      } = result
      expect(id).toBeUndefined()
      expect(name).toBeUndefined()
      expect(description).toBeUndefined()
      expect(price).toBeUndefined()
      expect(currency).toBeUndefined()
      expect(label).toBeUndefined()
      expect(colors).toBeUndefined()
      expect(url).toBeUndefined()
    })
  })

  describe('getSuggestionsByTerm', () => {
    it('should be defined', () => {
      expect(getSuggestionsByTerm).toBeDefined()
    })

    it('requires all arguments', async () => {
      const success = jest.fn()

      await getSuggestionsByTerm()
        .then(success)
        .catch((error) => {
          expect(error).toBeDefined()
        })

      await getSuggestionsByTerm(null)
        .then(success)
        .catch((error) => {
          expect(error).toBeDefined()
        })

      expect(success).toBeCalledTimes(0)
    })

    it('should fetch suggestions from FH', async () => {
      const mockResponse = {
        suggestionGroups: [
          {
            indexName: '1keywords',
            indexTitle: '1keywords',
            suggestions: [
              { searchterm: 'ABC', nrResults: 123 },
            ],
          },
        ],
      }
      fetch.mockReturnValue(
        Promise.resolve(new Response(JSON.stringify(mockResponse)))
      )

      const suggestions = await getSuggestionsByTerm('A')
      expect(suggestions).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.suggest.us1.fredhopperservices.com/pvh_th/json?scope=//th/de_DE/countries_pvh>{de}&search=a'))
    })

    it('should throw error if FH returns error', async () => {
      const success = jest.fn()
      const mockResponse = { error: 'FAILED' }
      fetch.mockReturnValue(
        Promise.resolve(
          new Response(JSON.stringify(mockResponse), { ok: false, status: 400 })
        )
      )

      await getSuggestionsByTerm('A')
        .then(success)
        .catch((error) => {
          expect(error).toBeDefined()
        })
      expect(success).toHaveBeenCalledTimes(0)
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.suggest.us1.fredhopperservices.com/pvh_th/json?scope=//th/de_DE/countries_pvh>{de}&search=a'))
    })
  })

  describe('getProductsByTerm', () => {
    const headerObject = { headers: { Accept: 'application/json', Authorization: 'Basic cHZoX3RoOjlPY2plYnJ5b3Vnew==' } }

    it('should be defined', () => {
      expect(getProductsByTerm).toBeDefined()
    })

    it('should return empty arrays if no items are found', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const result = await getProductsByTerm('A')

      expect(result).toBeDefined()
      expect(result.filters).toEqual({})
      expect(result.products.length).toBe(0)
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/$s=a/countries_pvh>{de}' +
        '&fh_view=search&fh_view_size=36&fh_start_index=0'), headerObject)
    })

    it('should throw error if response from FH has error', async () => {
      const success = jest.fn()
      const mockResponse = { error: 'FAILED' }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse), { ok: false, status: 400 })))

      await getProductsByTerm('A')
        .then(success)
        .catch((error) => {
          expect(error).toBeDefined()
        })
      expect(success).toHaveBeenCalledTimes(0)
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/$s=a/countries_pvh>{de}' +
        '&fh_view=search&fh_view_size=36&fh_start_index=0'), headerObject)
    })

    it('should fetch products from FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByTerm('A', undefined, undefined, undefined)
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/$s=a/countries_pvh>{de}' +
        '&fh_view=search&fh_view_size=36&fh_start_index=0'), headerObject)
    })

    it('should fetch products from other pages in FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByTerm('A', 2)
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/$s=a/countries_pvh>{de}' +
        '&fh_view=search&fh_view_size=36&fh_start_index=36'), headerObject)
    })

    it('should fetch products based on facets in FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByTerm(
        'A',
        undefined,
        'maincolour:maincolour_blue,maincolour:maincolour_white,size_de:size_de_4;size_de_l',
        undefined
      )
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/$s=a/countries_pvh>{de}/maincolour>{maincolour_blue;maincolour_white}/size_de>{size_de_4;size_de_l}' +
        '&fh_view=search&fh_view_size=36&fh_start_index=0'),
      headerObject)
    })

    it('should fetch products based on categoryId in FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByTerm(
        'A',
        undefined,
        undefined,
        'cth137men;cth137men137clothing;cth137men137clothing137shirts,cth137clothing137137blazers'
      )
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/$s=a/countries_pvh>{de}/categories<{cth137men}/categories<{cth137men137clothing}/categories<{cth137men137clothing137shirts,cth137clothing137137blazers}' +
        '&fh_view=search&fh_view_size=36&fh_start_index=0'),
      headerObject)
    })
  })

  describe('getProductsByCategoryId', () => {
    const headerObject = { headers: { Accept: 'application/json', Authorization: 'Basic cHZoX3RoOjlPY2plYnJ5b3Vnew==' } }

    it('should be defined', () => {
      expect(getProductsByCategoryId).toBeDefined()
    })

    it('should throw error if categoryId is undefined', async () => {
      const success = jest.fn()

      await getProductsByCategoryId(null)
        .then(success)
        .catch((error) => {
          expect(error).toBeDefined()
        })

      expect(success).toBeCalledTimes(0)
    })

    it('should return empty arrays if no items are found', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const result = await getProductsByCategoryId('cth137men;cth137men137clothing;cth137men137clothing137shirts,cth137men137clothing137shorts')

      expect(result).toBeDefined()
      expect(result.filters).toEqual({})
      expect(result.products.length).toBe(0)
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/countries_pvh>{de}/categories<{cth137men}/categories<{cth137men137clothing}/categories<{cth137men137clothing137shirts,cth137men137clothing137shorts}' +
        '&fh_view=lister&fh_view_size=36&fh_start_index=0'), headerObject)
    })

    it('should throw error if response from FH has error', async () => {
      const success = jest.fn()
      const mockResponse = { error: 'FAILED' }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse), { ok: false, status: 400 })))

      await getProductsByCategoryId('cth137men')
        .then(success)
        .catch((error) => {
          expect(error).toBeDefined()
        })
      expect(success).toHaveBeenCalledTimes(0)
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/countries_pvh>{de}/categories<{cth137men}' +
        '&fh_view=lister&fh_view_size=36&fh_start_index=0'), headerObject)
    })

    it('should fetch products from FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByCategoryId('cth137men')
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/countries_pvh>{de}/categories<{cth137men}' +
        '&fh_view=lister&fh_view_size=36&fh_start_index=0'), headerObject)
    })

    it('should fetch products from other pages in FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByCategoryId('cth137men', 2)
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/countries_pvh>{de}/categories<{cth137men}' +
        '&fh_view=lister&fh_view_size=36&fh_start_index=36'), headerObject)
    })

    it('should fetch products based on facets in FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByCategoryId(
        'cth137men',
        undefined,
        'maincolour:maincolour_blue,maincolour:maincolour_white,size_de:size_de_4;size_de_l',
      )
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/countries_pvh>{de}/categories<{cth137men}/maincolour>{maincolour_blue;maincolour_white}/size_de>{size_de_4;size_de_l}' +
        '&fh_view=lister&fh_view_size=36&fh_start_index=0'),
      headerObject)
    })

    it('should fetch products based on categoryId in FH', async () => {
      const mockResponse = {
        universes: {
          universe: [
            {
              name: 'th',
              'items-section': [],
              facetmap: [],
            },
          ],
        },
      }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

      const products = await getProductsByCategoryId(
        'cth137men;cth137men137clothing;cth137men137clothing137shirts,cth137clothing137137blazers',
        undefined,
        undefined,
      )
      expect(products).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll&fh_location=' +
        '//th/de_DE/countries_pvh>{de}/categories<{cth137men}/categories<{cth137men137clothing}/categories<{cth137men137clothing137shirts,cth137clothing137137blazers}' +
        '&fh_view=lister&fh_view_size=36&fh_start_index=0'),
      headerObject)
    })
  })

  describe('getProductsById', () => {
    const headerObject = {
      headers: {
        Accept: 'application/json',
        Authorization: 'Basic cHZoX3RoOjlPY2plYnJ5b3Vnew==',
      },
    }

    it('should be defined', () => {
      expect(getProductById).toBeDefined()
    })

    it(
      'should throw new error if no productId',
      async () => {
        const success = jest.fn()

        await getProductById('th', 'de', undefined)
          .then(() => success)
          .catch((error) => {
            expect(error).toBeDefined()
          })

        expect(success).toBeCalledTimes(0)
      })

    it('should throw error if response is not ok', async () => {
      const success = jest.fn()
      const mockResponse = { error: 'FAILED' }
      fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse), { ok: false, status: 400 })))

      await getProductById('12345')
        .then(success)
        .catch((error) => {
          expect(error).toBeDefined()
        })

      expect(success).toBeCalledTimes(0)
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll' +
        '&fh_location=//th/de_DE/countries_pvh>{de}' +
        '&fh_secondid=p12345' +
        '&fh_view=detail'), headerObject)
    })

    it(
      'should return empty object if response from FH has no items or themes',
      async () => {
        const mockResponse = {
          'universes': {
            'universe': [
              {
                'link': {
                  'name': 'Tommy Hilfiger',
                  'url-params': 'fh_location=%2f%2fth%2fde_DE',
                },
                'name': 'th',
                'type': 'selected',
              },
            ],
          },
        }
        fetch.mockReturnValue(Promise.resolve(new Response(JSON.stringify(mockResponse))))

        const result = await getProductById('12345')

        expect(result).toBeDefined()
        expect(result).toEqual({})
        expect(fetch).toHaveBeenCalledTimes(1)
        expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll' +
          '&fh_location=//th/de_DE/countries_pvh>{de}' +
          '&fh_secondid=p12345' +
          '&fh_view=detail'), headerObject)
      })

    it('should return products with required details', async () => {
      const mockResponse = {
        'universes': {
          'universe': [
            {
              'link': {
                'name': 'Tommy Hilfiger',
                'url-params': 'fh_location=%2f%2fth%2fde_DE',
              },
              'items-section': {
                'items': {
                  'item': [
                    {
                      'attribute': [
                        {
                          'value': [
                            {
                              'value': 'p429811',
                              'non-ml': 'p429811',
                            },
                          ],
                          'name': 'secondid',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                            },
                          ],
                          'name': 'description',
                          'basetype': 'asset',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'men',
                              'non-ml': 'gender_men',
                            },
                          ],
                          'name': 'gender',
                          'basetype': 'set',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_083_main?$main$',
                              'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_083_main?$main$',
                            },
                          ],
                          'name': '_imageurl',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'TOMMY HILFIGER',
                              'non-ml': 'label_tommyhilfiger',
                            },
                          ],
                          'name': 'label',
                          'basetype': 'list',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'Logo-Jogginghose aus Baumwolle',
                            },
                          ],
                          'name': 'name',
                          'basetype': 'asset',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '0',
                              'non-ml': '0',
                            },
                          ],
                          'name': 'onsale_de',
                          'basetype': 'int',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'MW0MW09499083',
                              'non-ml': 'MW0MW09499083',
                            },
                          ],
                          'name': 'partnumber',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '99.9',
                              'non-ml': '99.9',
                            },
                          ],
                          'name': 'price_de',
                          'basetype': 'float',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                            },
                          ],
                          'name': 'primarycategory',
                          'basetype': 'asset',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '1',
                              'non-ml': '1',
                            },
                          ],
                          'name': 'product_attr_new_arrival',
                          'basetype': 'int',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'Neu',
                              'non-ml': 'product_attr_promo_de_new',
                            },
                          ],
                          'name': 'product_attr_promo_de',
                          'basetype': 'list',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '6:S',
                              'non-ml': 'size_s_de',
                            },
                          ],
                          'name': 'size_de',
                          'basetype': 'list',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '0',
                              'non-ml': '0',
                            },
                          ],
                          'name': 'soldout',
                          'basetype': 'int',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'Sweatpants',
                              'non-ml': 'style_sweatpants',
                            },
                          ],
                          'name': 'style',
                          'basetype': 'set',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'v429812:12:XL|v429813:14:XXL|v429814:6:S|v429815:8:M|v429816:10:L',
                              'non-ml': 'v429812:12:XL|v429813:14:XXL|v429814:6:S|v429815:8:M|v429816:10:L',
                            },
                          ],
                          'name': 'possible_variant_size_de',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '429812,12345678901|429813,12345678902|429814,12345678903|429815,12345678904|429816,12345678905',
                            },
                          ],
                          'name': 'skupartnumbers',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'MW0MW09499',
                              'non-ml': 'MW0MW09499',
                            },
                          ],
                          'name': 'stylepartnumber',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'Tommy Hilfiger',
                              'non-ml': 'th',
                            },
                            {
                              'value': 'Herren',
                              'non-ml': 'cth137men',
                            },
                            {
                              'value': 'Kleidung',
                              'non-ml': 'cth137men137clothing',
                            },
                            {
                              'value': 'Hosen',
                              'non-ml': 'cth137men137clothing137trousers',
                            },
                            {
                              'value': 'Jogginghosen',
                              'non-ml': 'cth137men137clothing137trousers137sweatpants',
                            },
                            {
                              'value': 'NEW IN',
                              'non-ml': 'cth1371496236380623',
                            },
                          ],
                          'name': 'search_categories',
                          'basetype': 'set',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'JET BLACK',
                              'non-ml': 'product_attr_colour_jetblack',
                            },
                          ],
                          'name': 'product_attr_colour',
                          'basetype': 'list',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '20',
                              'non-ml': '20',
                            },
                            {
                              'value': '39',
                              'non-ml': '39',
                            },
                            {
                              'value': '58',
                              'non-ml': '58',
                            },
                            {
                              'value': '63',
                              'non-ml': '63',
                            },
                            {
                              'value': '15',
                              'non-ml': '15',
                            },
                          ],
                          'name': 'stock',
                          'basetype': 'int',
                          'isnull': false,
                          'selected': false,
                        },
                        {
                          'value': [
                            {
                              'value': '5',
                              'non-ml': '5',
                            },
                          ],
                          'name': 'product_attr_alt_image_count',
                          'basetype': 'int',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'PH02',
                              'non-ml': 'sapsizechart_ph02',
                            },
                          ],
                          'name': 'sapsizechart',
                          'basetype': 'list',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'Herren für men',
                            },
                          ],
                          'name': 'productseokeyword',
                          'basetype': 'asset',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': '-3|logo-jogginghose-aus-baumwolle-mw0mw09499083',
                            },
                          ],
                          'name': 'seo_url',
                          'basetype': 'asset',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'Pure Cotton Logo Joggers',
                              'non-ml': 'Pure Cotton Logo Joggers',
                            },
                          ],
                          'name': 'ddproductname',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'D012004|TH Menswear-Men-Heavyweight Knits',
                              'non-ml': 'D012004|TH Menswear-Men-Heavyweight Knits',
                            },
                          ],
                          'name': 'ddmerchhierarchy',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'BLACK',
                              'non-ml': 'BLACK',
                            },
                          ],
                          'name': 'ddmaincolour',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                        {
                          'value': [
                            {
                              'value': 'MW0MW09499083,JET BLACK,logo-jogginghose-aus-baumwolle-mw0mw09499083,BLACK,No|MW0MW09499093,CHARCOAL HTR,logo-jogginghose-aus-baumwolle-mw0mw09499093,GREY,No',
                              'non-ml': 'MW0MW09499083,JET BLACK,logo-jogginghose-aus-baumwolle-mw0mw09499083,BLACK,No|MW0MW09499093,CHARCOAL HTR,logo-jogginghose-aus-baumwolle-mw0mw09499093,GREY,No',
                            },
                          ],
                          'name': 'relatedcombis_de',
                          'basetype': 'text',
                          'isnull': false,
                          'selected': true,
                        },
                      ],
                      'link': [
                        {
                          'name': 'Detail',
                          'url-params': 'fh_secondid=p429811&fh_location=%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_modification=d3d60d5c-6aae-4bcb-82e3-f3d615d54d79%2ce6a0e6f1-a5d5-4f07-a195-3720f3357f43%2c',
                          'type': 'detail',
                        },
                      ],
                      'similar-item-fields': {
                        'attribute': [
                          {
                            'value': [
                              {
                                'value': 'p429811',
                                'non-ml': 'p429811',
                              },
                            ],
                            'name': 'secondid',
                            'basetype': 'text',
                            'isnull': false,
                            'selected': true,
                          },
                        ],
                        'location-param-prefix': '%2f%2fth%2fde_DE',
                      },
                      'id': 'p429811',
                    },
                  ],
                },
              },
              'themes': [
                {
                  'theme': [
                    {
                      'title': '',
                      'slogan': '',
                      'custom-fields': {
                        'custom-field': [
                          {
                            'value': 'PDP_OTHER_STYLE_COLOURS',
                            'name': 'Type',
                            'id': '9826e1f1-885c-40e2-b843-dbc9eae1b264',
                          },
                        ],
                      },
                      'link': [
                        {
                          'name': 'Tommy Hilfiger',
                          'url-params': 'fh_location=%2f%2fth%2fde_DE&fh_reftheme=447ebd71440869b03710410881006b5f%2cseeall%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                          'type': 'see_all',
                          'nr': 7723,
                        },
                        {
                          'name': 'Filtered',
                          'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cfiltered-all%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                          'type': 'filtered_all',
                          'nr': 1,
                        },
                      ],
                      'items': {
                        'item': [
                          {
                            'attribute': [
                              {
                                'value': [
                                  {
                                    'value': 'p429905',
                                    'non-ml': 'p429905',
                                  },
                                ],
                                'name': 'secondid',
                                'basetype': 'text',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '0',
                                    'non-ml': '0',
                                  },
                                ],
                                'name': 'onsale_de',
                                'basetype': 'int',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'MW0MW09499093',
                                    'non-ml': 'MW0MW09499093',
                                  },
                                ],
                                'name': 'partnumber',
                                'basetype': 'text',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'Logo-Jogginghose aus Baumwolle',
                                  },
                                ],
                                'name': 'name',
                                'basetype': 'asset',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'TOMMY HILFIGER',
                                    'non-ml': 'label_tommyhilfiger',
                                  },
                                ],
                                'name': 'label',
                                'basetype': 'list',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                                    'non-ml': '//tommy-europe.scene7.com/is/image/TommyEurope/MW0MW09499_093_main?$main$',
                                  },
                                ],
                                'name': '_imageurl',
                                'basetype': 'text',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'men',
                                    'non-ml': 'gender_men',
                                  },
                                ],
                                'name': 'gender',
                                'basetype': 'set',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'Mit Stretch-Taillenbund und Bündchen am Bein ist diese Jogginghose aus reiner Baumwolle gemütlich genug für das tägliche Tragen in deiner Freizeit.<BR><BR>Produktdetails<BR><BR>• reiner, softer Baumwoll-Terry<BR>• seitliches Tommy Hilfiger-Logo<BR>• Stretch-Taillenbund mit Tunnelzug<BR>• Tunnelzug-Bänder mit Kontrastfarben an den Enden<BR>• Taillenbund mit aufgesetzten Tommy-Streifen an der Innenseite und hinten<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe 32/34<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
                                  },
                                ],
                                'name': 'description',
                                'basetype': 'asset',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '99.9',
                                    'non-ml': '99.9',
                                  },
                                ],
                                'name': 'price_de',
                                'basetype': 'float',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '10155:TH_1496236380623_DE|10156:TH_1496236380623_CH|10160:TH_1496236380623_BE',
                                  },
                                ],
                                'name': 'primarycategory',
                                'basetype': 'asset',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '1',
                                    'non-ml': '1',
                                  },
                                ],
                                'name': 'product_attr_new_arrival',
                                'basetype': 'int',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'Neu',
                                    'non-ml': 'product_attr_promo_de_new',
                                  },
                                ],
                                'name': 'product_attr_promo_de',
                                'basetype': 'list',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '14:XXL',
                                    'non-ml': 'size_xxl_de',
                                  },
                                ],
                                'name': 'size_de',
                                'basetype': 'list',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '0',
                                    'non-ml': '0',
                                  },
                                ],
                                'name': 'soldout',
                                'basetype': 'int',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'Sweatpants',
                                    'non-ml': 'style_sweatpants',
                                  },
                                ],
                                'name': 'style',
                                'basetype': 'set',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                                    'non-ml': 'v429906:6:S|v429907:8:M|v429908:10:L|v429909:12:XL|v429910:14:XXL',
                                  },
                                ],
                                'name': 'possible_variant_size_de',
                                'basetype': 'text',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '429906,123|429907,124|429908,125|429909,126|429910,127',
                                  },
                                ],
                                'name': 'skupartnumbers',
                                'basetype': 'text',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'MW0MW09499',
                                    'non-ml': 'MW0MW09499',
                                  },
                                ],
                                'name': 'stylepartnumber',
                                'basetype': 'text',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': '5',
                                    'non-ml': '5',
                                  },
                                ],
                                'name': 'product_attr_alt_image_count',
                                'basetype': 'int',
                                'isnull': false,
                                'selected': true,
                              },
                              {
                                'value': [
                                  {
                                    'value': 'CHARCOAL HTR',
                                    'non-ml': 'product_attr_colour_charcoalhtr',
                                  },
                                ],
                                'name': 'product_attr_colour',
                                'basetype': 'list',
                                'isnull': false,
                                'selected': true,
                              },
                            ],
                            'link': [
                              {
                                'name': 'Detail',
                                'url-params': 'fh_location=%2f%2fth%2fde_DE%2fstylepartnumber%3dMW0MW09499%7c!secondid%3c%7bp429811%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2cdetail%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll&fh_secondid=p429905&fh_secondid2=v429910',
                                'type': 'detail',
                                'nr': 1,
                              },
                              {
                                'name': 'Jogginghosen',
                                'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth137men137clothing_cth137men137clothing137trousers_cth137men137clothing137trousers137sweatpants%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                                'type': 'lowest_category',
                                'nr': 44,
                              },
                              {
                                'name': 'NEW IN',
                                'url-params': 'fh_location=%2f%2fth%2fde_DE%2fcategories%3c%7bth_cth137men_cth1371496236380623%7d&fh_reftheme=447ebd71440869b03710410881006b5f%2clowest-category%2c%2f%2fth%2fde_DE%2fcountries_pvh%3e%7bde%7d&fh_refview=detail&fh_modification=&fh_sort=&method=getAll',
                                'type': 'lowest_category',
                                'nr': 531,
                              },
                            ],
                            'id': 'p429905',
                          },
                        ],
                      },
                      'id': '447ebd71440869b03710410881006b5f',
                      'name': 'PDP - Other Style Colours',
                      'type': 'facet',
                    },
                  ],
                  'attribute-types': {
                    'attribute-type': [
                      {
                        'value': 'secondid',
                        'name': 'secondid',
                        'type': 'text',
                      },
                      {
                        'value': 'Nur Sonderangebote',
                        'name': 'onsale_de',
                        'type': 'int',
                      },
                      {
                        'value': 'partnumber',
                        'name': 'partnumber',
                        'type': 'text',
                      },
                      {
                        'value': 'name',
                        'name': '@name',
                        'type': 'asset',
                      },
                      {
                        'value': 'markdown percentage',
                        'name': 'markdown_percentage_de',
                        'type': 'float',
                      },
                      {
                        'value': 'Label',
                        'name': 'label',
                        'type': 'list',
                      },
                      {
                        'value': 'imageurl',
                        'name': '_imageurl',
                        'type': 'text',
                      },
                      {
                        'value': 'Geschlecht',
                        'name': 'gender',
                        'type': 'set',
                      },
                      {
                        'value': 'description',
                        'name': '@description',
                        'type': 'asset',
                      },
                      {
                        'value': 'product price',
                        'name': 'price_de',
                        'type': 'float',
                      },
                      {
                        'value': 'primarycategory',
                        'name': '@primarycategory',
                        'type': 'asset',
                      },
                      {
                        'value': 'PRODUCT_ATTR_NEW_ARRIVAL',
                        'name': 'product_attr_new_arrival',
                        'type': 'int',
                      },
                      {
                        'value': 'PRODUCT_ATTR_PROMO',
                        'name': 'product_attr_promo_de',
                        'type': 'list',
                      },
                      {
                        'value': 'GRÖSSE',
                        'name': 'size_de',
                        'type': 'list',
                      },
                      {
                        'value': 'Soldout',
                        'name': 'soldout',
                        'type': 'int',
                      },
                      {
                        'value': 'STIL',
                        'name': 'style',
                        'type': 'set',
                      },
                      {
                        'value': 'possible_variant_size_de',
                        'name': 'possible_variant_size_de',
                        'type': 'text',
                      },
                      {
                        'value': 'Länge',
                        'name': 'possible_variant_sizelength',
                        'type': 'text',
                      },
                      {
                        'value': 'Breite',
                        'name': 'possible_variant_sizewidth',
                        'type': 'text',
                      },
                      {
                        'value': 'stylepartnumber',
                        'name': 'stylepartnumber',
                        'type': 'text',
                      },
                      {
                        'value': 'product price data',
                        'name': 'price_data_de',
                        'type': 'text',
                      },
                      {
                        'value': 'ImageCount',
                        'name': 'product_attr_alt_image_count',
                        'type': 'int',
                      },
                      {
                        'value': 'products sku price data',
                        'name': 'sku_price_data_de',
                        'type': 'text',
                      },
                      {
                        'value': 'Farbe',
                        'name': 'product_attr_colour',
                        'type': 'list',
                      },
                    ],
                  },
                  'itemid': 'p429811',
                },
              ],
              'name': 'th',
              'type': 'selected',
            },
          ],
        },
      }
      fetch.mockReturnValue(
        Promise.resolve(new Response(JSON.stringify(mockResponse)))
      )

      const result = await getProductById(
        '12345'
      )

      expect(result).toBeDefined()
      expect(fetch).toHaveBeenCalledTimes(1)
      expect(fetch).toHaveBeenCalledWith(encodeURI('https://query.published.test3.fas.us1.fredhopperservices.com/fredhopper/query?method=getAll' +
        '&fh_location=//th/de_DE/countries_pvh>{de}' +
        '&fh_secondid=p12345' +
        '&fh_view=detail'), headerObject)

      const { id, description, price, currency, label, colors } = result
      expect(id).toBeDefined()
      expect(description).toBeDefined()
      expect(price).toBeDefined()
      expect(currency).toBeDefined()
      expect(label).toBeDefined()
      expect(colors).toBeDefined()

      for (let color of colors) {
        const { id: colorId, value, url, images, pattern, sizes } = color
        expect(colorId).toBeDefined()
        expect(value).toBeDefined()
        expect(url).toBeDefined()
        expect(images).toBeDefined()
        expect(pattern).toBeDefined()
        expect(sizes).toBeDefined()

        for (let size of sizes) {
          const {
            id: sizeId,
            skuId,
            buyable,
            price,
            currency,
            inventory,
            colour,
            size: _size,
          } = size

          expect(sizeId).toBeDefined()
          expect(skuId).toBeDefined()
          expect(buyable).toBeDefined()
          expect(price).toBeDefined()
          expect(currency).toBeDefined()
          expect(inventory).toBeDefined()
          expect(colour).toBeDefined()
          expect(_size).toBeDefined()
        }
      }
    })
  })
})
