/* global jest describe it expect */

jest.mock('node-fetch')

const { getCategories, parseCategory, formatCategories } = require('../../controllers/newCategoriesController')
const allCategories = require('./__data__/categories/all.json')

describe('getCategories', () => {
  it('should exist', () => {
    expect(getCategories).toBeDefined()
  })
})

describe('parseCategory', () => {
  it('should parse categories', () => {
    const { catalogGroupView } = allCategories
    const result = parseCategory('TH', undefined, catalogGroupView, true)
    expect(result).toBeDefined()
    const categories = formatCategories('TH', result, true)
    expect(categories).toBeDefined()
    expect(categories.length).toBe(3)
  })
})
