/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const {
  mockGetStores,
  mockGetStore,
  mockCreateOrUpdateStore,
  mockPatchStore,
} = require('../../controllers/__mocks__/storesControllerFunctions')

jest.mock('controllers/storesController')

beforeEach(async () => {
  // clear mock counters
  mockGetStores.mockClear()
  mockGetStore.mockClear()
  mockCreateOrUpdateStore.mockClear()
  mockPatchStore.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/storesController')
})

describe('GET /:tenant/:countryCode/:countryStoreId/stores', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/stores'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetStores).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/300AB/stores'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetStores).toHaveBeenCalledTimes(0)
  })

  it('should return 400 and unsuccessful with invalid country', async () => {
    const url = '/th/dd/30006/stores'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(mockGetStores).toHaveBeenCalledTimes(1)
  })
})

describe('GET /:tenant/:countryCode/:countryStoreId/stores/:storeId', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/stores/A233'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetStore).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/stores/A-22'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetStore).toHaveBeenCalledTimes(0)
  })

  it('should return 400 and unsuccessful with invalid country', async () => {
    const url = '/th/dd/30006/stores/A999'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(mockGetStore).toHaveBeenCalledTimes(1)
  })
})

describe('PUT /:tenant/:countryCode/:countryStoreId/stores/:storeId', () => {
  it('should be successful when valid url and valid body', async () => {
    const url = '/th/de/30006/stores/A233'
    const body = {
      tenant: 'th',
      countryCode: 'de',
      payment: 'gk',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockCreateOrUpdateStore).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/stores/A-33'
    const body = {
      tenant: 'th',
      countryCode: 'de',
      payment: 'gk',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockCreateOrUpdateStore).toHaveBeenCalledTimes(0)
  })

  it('should be unsuccessful when invalid body', async () => {
    const url = '/th/de/30006/stores/A233'
    const body = {
      tenant: 'th',
      countryCode: 'de',
      storeId: 'A233',
      payment: 'pos',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockCreateOrUpdateStore).toHaveBeenCalledTimes(0)
  })

  it('should be successful when store doesnt exist', async () => {
    const url = '/th/de/30006/stores/A999'
    const body = {
      tenant: 'th',
      countryCode: 'de',
      payment: 'gk',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body.success).toBeTruthy()
    expect(response.body.data).toBeDefined()
    expect(mockCreateOrUpdateStore).toHaveBeenCalledTimes(1)
  })
})

describe('PATCH /:tenant/:countryCode/:countryStoreId/stores/:storeId', () => {
  it('should be successful when valid url and valid body', async () => {
    const url = '/th/de/30006/stores/A233'
    const body = {
      tenant: 'ck',
      countryCode: 'fr',
      payment: 'ecom',
    }

    const response = await request(server)
      .patch(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockPatchStore).toHaveBeenCalledTimes(1)
  })

  it('should be successful when valid url and only tenant', async () => {
    const url = '/th/de/30006/stores/A233'
    const body = {
      tenant: 'ck',
    }

    const response = await request(server)
      .patch(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockPatchStore).toHaveBeenCalledTimes(1)
  })

  it('should be successful when valid url and only countryCode', async () => {
    const url = '/th/de/30006/stores/A233'
    const body = {
      countryCode: 'fr',
    }

    const response = await request(server)
      .patch(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockPatchStore).toHaveBeenCalledTimes(1)
  })

  it('should be successful when valid url and only payment', async () => {
    const url = '/th/de/30006/stores/A233'
    const body = {
      payment: 'ecom',
    }

    const response = await request(server)
      .patch(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockPatchStore).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/stores/A-33'
    const body = {
      tenant: 'th',
      countryCode: 'de',
      payment: 'gk',
    }

    const response = await request(server)
      .patch(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockCreateOrUpdateStore).toHaveBeenCalledTimes(0)
  })

  it('should be unsuccessful when invalid body', async () => {
    const url = '/th/de/30006/stores/A233'
    const body = {
      monkeys: true,
    }

    const response = await request(server)
      .patch(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockCreateOrUpdateStore).toHaveBeenCalledTimes(0)
  })

  it('should be successful when store doesnt exist', async () => {
    const url = '/th/de/30006/stores/A999'
    const body = {
      tenant: 'th',
      countryCode: 'de',
      payment: 'gk',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body.success).toBeTruthy()
    expect(response.body.data).toBeDefined()
    expect(mockCreateOrUpdateStore).toHaveBeenCalledTimes(1)
  })
})
