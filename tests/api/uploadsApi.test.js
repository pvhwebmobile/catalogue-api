/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const {
  mockUploadStoreLocatorFile,
  mockUploadStoreLocatorJSON,
} = require('../../controllers/__mocks__/uploadsControllerFunctions')

jest.mock('controllers/uploadsController')

beforeEach(async () => {
  // clear mock counters
  mockUploadStoreLocatorFile.mockClear()
  mockUploadStoreLocatorJSON.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/uploadsController')
})

describe('POST /uploads/sharepoint/storelocator', () => {
  it('should be successful with raw data', async () => {
    const url = '/uploads/sharepoint/storelocator'
    const data = {
      'eCom_StoreFeed_EIS': {
        'stores': [{
          'ecomName': 'Tommy Jeans',
          'address1': 'Centro Commerciale Metropoli, Via Carlo Amoretti, 1',
          'address2': '',
          'address3': '',
          'zipCode': '20029',
          'city': 'Novate Milanese',
          'country': 'Italy',
          'sapFMS': 'AU33',
          'posCheckout': false,
        }],
        'timestamp': '2019-09-05T13:43:29',
      },
    }

    const response = await request(server)
      .post(url)
      .send(data)
      .set(
        'Authorization',
        'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5',
      )
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toEqual({ upserted: 5, deleted: 1, invalid: 5 })
  })

  it('should be unsuccessful with invalid raw data', async () => {
    const url = '/uploads/sharepoint/storelocator'

    const response = await request(server)
      .post(url)
      .send(undefined)
      .set(
        'Authorization',
        'Basic ZWlzQWRtaW46VGhlUGFzc3dvcmRJc1Bhc3N3b3JkOikyMDE5',
      )

    expect(response.status).toBe(406)
  })

  it('should return unauthorized without authorization', async () => {
    const url = '/uploads/sharepoint/storelocator'

    const response = await request(server)
      .post(url)
      .field('file', '{}')

    expect(response.status).toBe(401)
  })
})
