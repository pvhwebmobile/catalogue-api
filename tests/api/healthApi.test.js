/* global describe it expect beforeEach afterEach */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

beforeEach(async () => {
  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

describe('GET /', () => {
  const url = '/'
  it('should be successful', async () => {
    const response = await request(server)
      .get(url)

    expect(response.status).toBe(200)
  })
})

describe('GET /health/check', () => {
  const url = '/health/check'
  it('should be successful', async () => {
    const response = await request(server)
      .get(url)

    expect(response.status).toBe(200)
  })
})
