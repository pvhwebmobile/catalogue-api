/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const emailsController = require('../../controllers/emailsController')
const {
  mockSendWishlistEmail,
} = require('../../controllers/__mocks__/emailsControllerFunctions')

jest.mock('controllers/emailsController')

beforeEach(async () => {
  // clear mock counters
  emailsController.mockClear()
  mockSendWishlistEmail.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/emailsController')
})

describe('POST /:tenant/:countryCode/:storeId/emails/wishlist', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/emails/wishlist'
    const body = {
      email: 'collin@test.com',
      items: [{ id: '1', name: 'a', size: 'M', price: '100 EUR', image: 'link1.jpg' }, { id: '2', name: 'b', size: 'M', price: '400 EUR', image: 'link2.jpg' }],
      total: '500 EUR',
    }

    const response = await request(server)
      .post(url)
      .send(body)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockSendWishlistEmail).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006a/emails/wishlist'
    const body = {
      email: 'collin@test.com',
      items: [{ name: 'a' }, { name: 'b' }],
      total: 500.00,
    }

    const response = await request(server)
      .post(url)
      .send(body)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockSendWishlistEmail).toHaveBeenCalledTimes(0)
  })

  it('should be unsuccessful when invalid body', async () => {
    const url = '/th/de/30006a/emails/wishlist'
    const body = {
      email: 'collin@test.com',
    }

    const response = await request(server)
      .post(url)
      .send(body)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockSendWishlistEmail).toHaveBeenCalledTimes(0)
  })
})
