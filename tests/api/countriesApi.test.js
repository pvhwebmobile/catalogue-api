/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const countriesController = require('../../controllers/countriesController')
const {
  mockGetCountriesList,
  mockGetLanguagesByCountry,
  mockPingWCSConnection,
} = require('../../controllers/__mocks__/countriesControllerFunctions')

jest.mock('controllers/countriesController')

beforeEach(async () => {
  // clear mock counters
  countriesController.mockClear()
  mockGetCountriesList.mockClear()
  mockGetLanguagesByCountry.mockClear()
  mockPingWCSConnection.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/countriesController')
})

describe('GET /:tenant/countries', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/countries'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetCountriesList).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th2/countries'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetCountriesList).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/countries/:countryCode/languages', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/countries/de/languages'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetLanguagesByCountry).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th2/countries/de/languages'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetLanguagesByCountry).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/:countryCode/:storeId/ping', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/ping'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockPingWCSConnection).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006a/ping'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockPingWCSConnection).toHaveBeenCalledTimes(0)
  })
})
