/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const categoriesController = require('../../controllers/categoriesController')
const {
  mockGetTopCategoriesByStoreId,
  mockGetCategoriesForPOC,
} = require('../../controllers/__mocks__/categoriesControllerFunctions')

jest.mock('controllers/categoriesController')

beforeEach(async () => {
  // clear mock counters
  categoriesController.mockClear()
  mockGetTopCategoriesByStoreId.mockClear()
  mockGetCategoriesForPOC.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/categoriesController')
})

describe('GET /:tenant/:countryCode/:storeId/categories', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/categories'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetTopCategoriesByStoreId).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/3000a6/categories'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetTopCategoriesByStoreId).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/:countryCode/:storeId/categories/poc', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/categories/poc'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetCategoriesForPOC).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006a/categories/poc'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetCategoriesForPOC).toHaveBeenCalledTimes(0)
  })
})
