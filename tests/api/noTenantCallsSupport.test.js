/* global jest describe it expect beforeEach afterEach */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const categoriesController = require('../../controllers/categoriesController')
const emailsController = require('../../controllers/emailsController')
const imagesController = require('../../controllers/imagesController')
const productsController = require('../../controllers/productsController')

const {
  mockGetTopCategoriesByStoreId,
} = require('../../controllers/__mocks__/categoriesControllerFunctions')

const {
  mockGetStores,
} = require('../../controllers/__mocks__/storesControllerFunctions')

const {
  mockSendWishlistEmail,
} = require('../../controllers/__mocks__/emailsControllerFunctions')

const {
  mockGetImageById,
} = require('../../controllers/__mocks__/imagesControllerFunctions')

const {
  mockGetProductById,
  mockGetSuggestionsByTerm,
  mockGetProductsByTerm,
  mockGetProductsByCategoryId,
  mockGetInventoryBySkus,
} = require('../../controllers/__mocks__/productsControllerFunctions')

jest.mock('controllers/categoriesController')
jest.mock('controllers/emailsController')
jest.mock('controllers/imagesController')
jest.mock('controllers/productsController')
jest.mock('controllers/storesController')

beforeEach(async () => {
  // clear mock counters
  categoriesController.mockClear()
  emailsController.mockClear()
  imagesController.mockClear()
  productsController.mockClear()

  mockGetTopCategoriesByStoreId.mockClear()
  mockGetStores.mockClear()
  mockSendWishlistEmail.mockClear()
  mockGetImageById.mockClear()
  mockGetProductById.mockClear()
  mockGetSuggestionsByTerm.mockClear()
  mockGetProductsByTerm.mockClear()
  mockGetProductsByCategoryId.mockClear()
  mockGetInventoryBySkus.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

describe('GET /categories', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/categories'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetTopCategoriesByStoreId).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/categories'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetTopCategoriesByStoreId).toHaveBeenCalledTimes(0)
  })
})

describe('GET /products/:productId', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/products/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetProductById).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/products/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetProductById).toHaveBeenCalledTimes(0)
  })
})

describe('GET /products/suggestions/:term', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/products/suggestions/jeans'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetSuggestionsByTerm).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/products/suggestions/jeans'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetSuggestionsByTerm).toHaveBeenCalledTimes(0)
  })
})

describe('GET /products/search/:term', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/products/search/jeans'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetProductsByTerm).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/products/search/jeans'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetProductsByTerm).toHaveBeenCalledTimes(0)
  })
})

describe('GET /products/category/:categoryId', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/products/category/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetProductsByCategoryId).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/products/category/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetProductsByCategoryId).toHaveBeenCalledTimes(0)
  })
})

describe('GET /products/inventory/:skus', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/products/inventory/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetInventoryBySkus).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/products/inventory/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetInventoryBySkus).toHaveBeenCalledTimes(0)
  })
})

describe('GET /images/:imageId', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/images/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetImageById).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/images/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetImageById).toHaveBeenCalledTimes(0)
  })
})

describe('GET /images/:folder/:imageId', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/images/splash/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetImageById).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/images/splash/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetImageById).toHaveBeenCalledTimes(0)
  })
})

describe('GET /stores', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/stores'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockGetStores).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/stores'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockGetStores).toHaveBeenCalledTimes(0)
  })
})

describe('GET /emails/wishlist', () => {
  it('should return categories list for v1.x.x', async () => {
    const url = '/de/30006/emails/wishlist'

    const response = await request(server)
      .post(url)
      .send()
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    expect(response.body.data).toBeDefined()
    expect(mockSendWishlistEmail).toHaveBeenCalledTimes(1)
  })

  it('should be unsupported for !v1.x.x', async () => {
    const url = '/de/30006/emails/wishlist'

    const response = await request(server)
      .post(url)
      .send()
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2.0.0')

    expect(response.status).toBe(501)
    expect(mockSendWishlistEmail).toHaveBeenCalledTimes(0)
  })
})
