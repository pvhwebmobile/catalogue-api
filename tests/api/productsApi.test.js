/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const productsController = require('../../controllers/productsController')
const newProductsController = require('../../controllers/newProductsController')

const {
  mockGetInventoryBySkus,
  mockGetProductById: mockGetProductByIdDeprecated,
  mockGetProductsByTerm: mockGetProductsByTermDeprecated,
  mockGetSuggestionsByTerm: mockGetSuggestionsByTermDeprecated,
  mockGetProductsByCategoryId: mockGetProductsByCategoryIdDeprecated,
} = require('../../controllers/__mocks__/productsControllerFunctions')

const {
  mockGetSuggestionsByTerm,
  mockGetProductsByTerm,
  mockGetProductsByCategoryId,
  mockGetProductById,
} = require('../../controllers/__mocks__/newProductsControllerFunctions')

jest.mock('controllers/productsController')
jest.mock('controllers/newProductsController')

beforeEach(async () => {
  // clear mock counters
  productsController.mockClear()
  mockGetInventoryBySkus.mockClear()
  mockGetProductByIdDeprecated.mockClear()
  mockGetProductsByTermDeprecated.mockClear()
  mockGetSuggestionsByTermDeprecated.mockClear()
  mockGetProductsByCategoryIdDeprecated.mockClear()

  mockGetProductsByTerm.mockClear()
  mockGetSuggestionsByTerm.mockClear()
  mockGetProductsByCategoryId.mockClear()
  mockGetProductById.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/productsController')
})

describe('GET /:tenant/:countryCode/:storeId/products/:productId', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/products/1234?version=1&?byPartNumber=1'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetProductByIdDeprecated).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/products/1234?version=a'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetProductById).toHaveBeenCalledTimes(0)
  })

  it('should be successful when valid url with version 3.0+', async () => {
    const url = '/th/de/30006/products/12345'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '4.3.3')

    expect(response.status).toBe(200)
    expect(mockGetProductById).toHaveBeenCalledTimes(1)
  })
})

describe('GET /:tenant/:countryCode/:storeId/products/suggestions/:term', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/products/suggestions/JEANS'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetSuggestionsByTermDeprecated).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/3006/products/suggestions/JEANS?version=a'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetSuggestionsByTerm).toHaveBeenCalledTimes(0)
  })

  it('should be successful when valid url with version 3.0+', async () => {
    const url = '/th/de/30006/products/suggestions/JEANS'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '4.3.3')

    expect(response.status).toBe(200)
    expect(mockGetSuggestionsByTerm).toHaveBeenCalledTimes(1)
  })
})

describe('GET /:tenant/:countryCode/:storeId/products/search/:term', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/products/search/JEANS?page=2&facet=abs_123:"S"&categoryId=123456,123'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetProductsByTermDeprecated).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/products/search/JEANS?page=2&facet=abs_123:"S"&categoryId=123456x123'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetProductsByTerm).toHaveBeenCalledTimes(0)
  })

  it('should be successful when valid url with version 3.0+', async () => {
    const url = '/th/de/30006/products/search/JEANS?page=2&facet=maincolour:maincolour_white;maincolour_blue&categoryId=cth137men,cth137men137clothing'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '4.3.3')

    expect(response.status).toBe(200)
    expect(mockGetProductsByTerm).toHaveBeenCalledTimes(1)
  })
})

describe('GET /:tenant/:countryCode/:storeId/products/category/:categoryId', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/products/category/12345?page=123&facet=abs_1234:"S"'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetProductsByCategoryIdDeprecated).toHaveBeenCalledTimes(1)
  })

  it('\'should be successful when valid url with version 3.0+\'', async () => {
    const url = '/th/de/30006/products/category/12345?page=123&facet=maincolour:maincolour_white,maincolour:maincolour_blue'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '4.3.3')

    expect(response.status).toBe(200)
    expect(mockGetProductsByCategoryId).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/products/category/1234a5'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetProductsByCategoryId).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/:countryCode/:storeId/products/inventory/:skus', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/products/inventory/12345,123456,1267,12345869?byPartNumber=1'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetInventoryBySkus).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/products/inventory/12345,123456,1267,12345869?byPartNumber=a'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetInventoryBySkus).toHaveBeenCalledTimes(0)
  })
})
