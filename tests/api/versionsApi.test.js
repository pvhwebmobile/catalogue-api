/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')

let server

const {
  mockGetVersion,
  mockCheckForUpdate,
} = require('../../controllers/__mocks__/versionsControllerFunctions')

jest.mock('controllers/versionsController')

beforeEach(async () => {
  mockGetVersion.mockClear()
  mockCheckForUpdate.mockClear()

  server = await app.listen(config.PORT)
})

afterEach(async () => {
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/versionsController')
})

describe('GET /versions/:appName', () => {
  it('should be successful for valid appName', async () => {
    const url = '/versions/blah_blah'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetVersion).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful for invalid appName', async () => {
    const url = '/versions/!@#!@$%#'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(mockGetVersion).toHaveBeenCalledTimes(0)
  })
})

describe('GET /versions/:appName/update', () => {
  it('should be successful for valid appName & clientVersion', async () => {
    const url = '/versions/blah_blah/update?clientVersion=1.2.3'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockCheckForUpdate).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful for invalid appName & valid clientVersion', async () => {
    const url = '/versions/!@#/update?clientVersion=1.2.3'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(mockCheckForUpdate).toHaveBeenCalledTimes(0)
  })

  it('should be unsuccessful for valid appName & invalid clientVersion', async () => {
    const url = '/versions/blah_blah/update?clientVersion=monkeys'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(mockCheckForUpdate).toHaveBeenCalledTimes(0)
  })

  it('should be unsuccessful for invalid appName & invalid clientVersion', async () => {
    const url = '/versions/%$!@$/update?clientVersion=1.2.3'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(mockCheckForUpdate).toHaveBeenCalledTimes(0)
  })
})
