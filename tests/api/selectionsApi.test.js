/* global jest describe it expect beforeEach afterEach afterAll */

const request = require('supertest')
const config = require('config')
const app = require('../../app')
let server

const selectionsController = require('../../controllers/selectionsController')
const {
  mockCreateSelection,
  mockUpdateSelection,
  mockUpdateSelectionPersonalDetails,
  mockGetDeliveryMethodsBySelectionId,
  mockGetValidationRules,
  mockGetValidationZipCode,
  mockGetSelectionsListByStatusAndSiteId,
  mockGetSelectionByOrderId,
  mockUpdateSelectionStatusByOrderId,
  mockGetConnectivityWcs,
} = require('../../controllers/__mocks__/selectionsControllerFunctions')

jest.mock('controllers/selectionsController')

beforeEach(async () => {
  // clear mock counters
  selectionsController.mockClear()
  mockCreateSelection.mockClear()
  mockUpdateSelection.mockClear()
  mockUpdateSelectionPersonalDetails.mockClear()
  mockGetDeliveryMethodsBySelectionId.mockClear()
  mockGetValidationRules.mockClear()
  mockGetValidationZipCode.mockClear()
  mockGetSelectionsListByStatusAndSiteId.mockClear()
  mockGetSelectionByOrderId.mockClear()
  mockUpdateSelectionStatusByOrderId.mockClear()
  mockGetConnectivityWcs.mockClear()

  // start up new server instance
  server = await app.listen(config.PORT)
})

afterEach(async () => {
  // close server instance
  await server.close()
})

afterAll(() => {
  jest.unmock('controllers/selectionsController')
  jest.unmock('libs/versioning')
})

describe('POST /:tenant/:countryCode/:storeId/selections/', () => {
  describe('v3', () => {
    it('should be successful when valid url and body', async () => {
      const url = '/th/de/30006/selections'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '55555',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .post(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '3')

      expect(response.status).toBe(200)
      expect(mockCreateSelection).toHaveBeenCalledTimes(1)
    })

    it('should be unsuccessful when invalid url', async () => {
      const url = '/th/de/3000a6/selections'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .post(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '3')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockCreateSelection).toHaveBeenCalledTimes(0)
    })

    it('should be unsuccessful when invalid body', async () => {
      const url = '/th/de/30006/selections'
      const body = {
        name: 123,
        siteId: 'testSite',
        products: [
          {
            skuId: '1234',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .post(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '3')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockCreateSelection).toHaveBeenCalledTimes(0)
    })
  })

  describe('v2', () => {
    it('should be successful when valid url and body', async () => {
      const url = '/th/de/30006/selections'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .post(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '2')

      expect(response.status).toBe(200)
      expect(mockCreateSelection).toHaveBeenCalledTimes(1)
    })

    it('should be unsuccessful when invalid url', async () => {
      const url = '/th/de/3000a6/selections'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .post(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '2')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockCreateSelection).toHaveBeenCalledTimes(0)
    })

    it('should be unsuccessful when invalid body', async () => {
      const url = '/th/de/30006/selections'
      const body = {
        name: 123,
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .post(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '2')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockCreateSelection).toHaveBeenCalledTimes(0)
    })
  })

  describe('v1', () => {
    it('should fail for v1', async () => {
      const url = '/th/de/30006/selections'
      const body = {
        name: 123,
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .post(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '1')

      expect(response.status).toBe(501)
      expect(mockCreateSelection).toHaveBeenCalledTimes(0)
    })
  })
})

describe('PUT /:tenant/:countryCode/:storeId/selections/:selectionId', () => {
  describe('v3', () => {
    it('should be successful when valid url and body', async () => {
      const url = '/th/de/30006/selections/123abc'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .put(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '3')

      expect(response.status).toBe(200)
      expect(mockUpdateSelection).toHaveBeenCalledTimes(1)
    })

    it('should be unsuccessful when invalid url', async () => {
      const url = '/th/de/3000a6/selections/123abc'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .put(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '3')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockUpdateSelection).toHaveBeenCalledTimes(0)
    })

    it('should be unsuccessful when invalid body', async () => {
      const url = '/th/de/30006/selections/123abc'
      const body = {
        name: 123,
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .put(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '3')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockUpdateSelection).toHaveBeenCalledTimes(0)
    })
  })

  describe('v2', () => {
    it('should be successful when valid url and body', async () => {
      const url = '/th/de/30006/selections/123abc'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .put(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '2')

      expect(response.status).toBe(200)
      expect(mockUpdateSelection).toHaveBeenCalledTimes(1)
    })

    it('should be unsuccessful when invalid url', async () => {
      const url = '/th/de/3000a6/selections/123abc'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .put(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '2')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockUpdateSelection).toHaveBeenCalledTimes(0)
    })

    it('should be unsuccessful when invalid body', async () => {
      const url = '/th/de/30006/selections/123abc'
      const body = {
        name: 123,
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .put(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '2')

      expect(response.body.success).toBeFalsy()
      expect(response.body.error).toBeDefined()
      expect(response.status).toBe(400)
      expect(mockUpdateSelection).toHaveBeenCalledTimes(0)
    })
  })

  describe('v1', () => {
    it('should fail for v1', async () => {
      const url = '/th/de/30006/selections/123abc'
      const body = {
        name: 'test',
        siteId: 'testSite',
        products: [
          {
            id: '1234',
            name: 'Test',
            size: 'M',
            price: 100,
            image: 'image.jpg',
            colorPattern: 'pattern',
            colorName: 'name',
            label: 'TH',
            skuId: '1234567890',
            amount: 1,
          },
        ],
      }

      const response = await request(server)
        .put(url)
        .send(body)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Accept-Version', '1')

      expect(response.status).toBe(501)
      expect(mockUpdateSelection).toHaveBeenCalledTimes(0)
    })
  })
})

describe('PUT /:tenant/:countryCode/:storeId/selections/:selectionId/personalDetails', () => {
  it('should be successful when valid url and body', async () => {
    const url = '/th/de/30006/selections/531acb123bas-d/personalDetails'
    const body = {
      shippingModeId: '1234',
      email: 'collin@test.com',
      shippingZipCode: '1234 AB',
      shippingPhone: '+1 817 718 2346',
      shippingPersonTitle: 'Dr',
      shippingLastName: 'Duncan',
      shippingFirstName: 'Collin',
      shippingCity: 'Amsterdam',
      shippingAddress: 'Testerkade 33',
      billingZipCode: '1235 AB',
      billingPhone: '+1 817 718 2345',
      billingPersonTitle: 'Dr',
      billingLastName: 'Duncan',
      billingFirstName: 'Collin',
      billingCity: 'Amsterdam',
      billingAddress: 'Testerkade 35',
      billingCountry: 'Netherlands',
      shipStoreId: 'AQ01',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '3')

    expect(response.status).toBe(200)
    expect(mockUpdateSelectionPersonalDetails).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/300a06/selections/531acb123bas-d/personalDetails'
    const body = {
      shippingModeId: '1234',
      email: 'collin@test.com',
      shippingZipCode: '1234 AB',
      shippingPhone: '+1 817 718 2346',
      shippingPersonTitle: 'Dr',
      shippingLastName: 'Duncan',
      shippingFirstName: 'Collin',
      shippingCity: 'Amsterdam',
      shippingAddress: 'Testerkade 33',
      billingZipCode: '1235 AB',
      billingPhone: '+1 817 718 2345',
      billingPersonTitle: 'Dr',
      billingLastName: 'Duncan',
      billingFirstName: 'Collin',
      billingCity: 'Amsterdam',
      billingAddress: 'Testerkade 35',
      billingCountry: 'Netherlands',
      shipStoreId: 'AQ01',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockUpdateSelectionPersonalDetails).toHaveBeenCalledTimes(0)
  })

  it('should be unsuccessful when invalid body', async () => {
    const url = '/th/de/30006/selections/531acb123bas-d/personalDetails'
    const body = {
      shippingModeId: '1234',
      shippingZipCode: '1234 AB',
      shippingPhone: '+1 817 718 2346',
      shippingPersonTitle: 'Dr',
      shippingLastName: 'Duncan',
      shippingFirstName: 'Collin',
      shippingCity: 'Amsterdam',
      shippingAddress: 'Testerkade 33',
      billingZipCode: '1235 AB',
      billingPhone: '+1 817 718 2345',
      billingPersonTitle: 'Dr',
      billingLastName: 'Duncan',
      billingFirstName: 'Collin',
      billingCity: 'Amsterdam',
      billingAddress: 'Testerkade 35',
      billingCountry: 'Netherlands',
      shipStoreId: 'AQ01',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockUpdateSelectionPersonalDetails).toHaveBeenCalledTimes(0)
  })

  it('should fail for v1', async () => {
    const url = '/th/de/30006/selections/531acb123bas-d/personalDetails'
    const body = {
      shippingModeId: '1234',
      email: 'collin@test.com',
      shippingZipCode: '1234 AB',
      shippingPhone: '+1 817 718 2346',
      shippingPersonTitle: 'Dr',
      shippingLastName: 'Duncan',
      shippingFirstName: 'Collin',
      shippingCity: 'Amsterdam',
      shippingAddress: 'Testerkade 33',
      billingZipCode: '1235 AB',
      billingPhone: '+1 817 718 2345',
      billingPersonTitle: 'Dr',
      billingLastName: 'Duncan',
      billingFirstName: 'Collin',
      billingCity: 'Amsterdam',
      billingAddress: 'Testerkade 35',
      billingCountry: 'Netherlands',
      shipStoreId: 'AQ01',
    }

    const response = await request(server)
      .put(url)
      .send(body)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(501)
    expect(mockUpdateSelectionPersonalDetails).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/:countryCode/:storeId/selections/:selectionId/deliveryMethods', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/selections/531acb123bas-d/deliveryMethods'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(200)
    expect(mockGetDeliveryMethodsBySelectionId).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/300a06/selections/531acb123bas-d/deliveryMethods'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetDeliveryMethodsBySelectionId).toHaveBeenCalledTimes(0)
  })

  it('should fail for v1', async () => {
    const url = '/th/de/30006/selections/531acb123bas-d/deliveryMethods'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(501)
    expect(mockGetDeliveryMethodsBySelectionId).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/:countryCode/:storeId/selections/validationRules', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/selections/validationRules'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(200)
    expect(mockGetValidationRules).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/300a06/selections/validationRules'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetValidationRules).toHaveBeenCalledTimes(0)
  })

  it('should fail for v1', async () => {
    const url = '/th/de/30006/selections/validationRules'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(501)
    expect(mockGetValidationRules).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/:countryCode/:storeId/selections/validateZipCode/:orderId/:zipcode/:city/:address1', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/selections/validateZipCode/1234/1234 AB/Amsterdam/Danzigerkade 23'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(200)
    expect(mockGetValidationZipCode).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/selections/validateZipCode/1234AB/1234 AB/Amsterdam/Danzigerkade 23'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetValidationZipCode).toHaveBeenCalledTimes(0)
  })

  it('should fail for v1', async () => {
    const url = '/th/de/30006/selections/validateZipCode/1234/1234 AB/Amsterdam/Danzigerkade 23'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(501)
    expect(mockGetValidationZipCode).toHaveBeenCalledTimes(0)
  })
})

describe('GET /:tenant/:countryCode/:storeId/selections/:siteId', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/selections/AQ01?status=1'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(200)
    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/selections/aq01'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(0)
  })

  it('should not conflict with test endpoint', async () => {
    const url = '/th/de/30006/selections/test'

    await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(0)
  })

  it('should not conflict with validationRules endpoint', async () => {
    const url = '/th/de/30006/selections/validationRules'

    await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(0)
  })

  it('should fail for v1', async () => {
    const url = '/th/de/30006/selections/AQ01?status=1'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(501)
    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(0)
  })
})

describe('OPTIONS /selections/:siteId', () => {
  it('should be successful', async () => {
    const url = '/selections/AQ01?status=1'

    const response = await request(server)
      .options(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')

    expect(response.status).toBe(200)
  })
})

describe('GET /selections/:siteId', () => {
  it('should be successful when valid url', async () => {
    const url = '/selections/AQ01?status=1'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/selections/aq01'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(0)
  })

  it('should succeed for v1', async () => {
    const url = '/selections/AQ01?status=1'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(200)
    expect(mockGetSelectionsListByStatusAndSiteId).toHaveBeenCalledTimes(1)
  })
})

describe('GET /:tenant/:countryCode/:storeId/selections/orderid/:orderId', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/selections/orderid/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(200)
    expect(mockGetSelectionByOrderId).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/selections/orderid/1234abc'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetSelectionByOrderId).toHaveBeenCalledTimes(0)
  })

  it('should fail for v1', async () => {
    const url = '/th/de/30006/selections/orderid/1234'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(501)
    expect(mockGetSelectionByOrderId).toHaveBeenCalledTimes(0)
  })
})

describe('OPTIONS /:tenant/:countryCode/:storeId/selections/:orderId/:action', () => {
  it('should be successful', async () => {
    const url = '/th/de/30006/selections/531/sentToTill'

    const response = await request(server)
      .options(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')

    expect(response.status).toBe(200)
  })
})

describe('PUT /:tenant/:countryCode/:storeId/selections/:orderId/:action', () => {
  it('should be successful when valid url and body', async () => {
    const url = '/th/de/30006/selections/531/sentToTill'

    const response = await request(server)
      .put(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(200)
    expect(mockUpdateSelectionStatusByOrderId).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/30006/selections/531/invalidStatus'

    const response = await request(server)
      .put(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    expect(response.status).toBe(404)
    expect(mockUpdateSelectionStatusByOrderId).toHaveBeenCalledTimes(0)
  })

  it('should succeed for v1', async () => {
    const url = '/th/de/30006/selections/531/sentToTill'

    const response = await request(server)
      .put(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(200)
    expect(mockUpdateSelectionStatusByOrderId).toHaveBeenCalledTimes(1)
  })
})

describe('OPTIONS /:tenant/:countryCode/:storeId/selections/test', () => {
  it('should be successful', async () => {
    const url = '/th/de/30006/selections/test'

    const response = await request(server)
      .options(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')

    expect(response.status).toBe(200)
  })
})

describe('GET /:tenant/:countryCode/:storeId/selections/test', () => {
  it('should be successful when valid url', async () => {
    const url = '/th/de/30006/selections/test'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(200)
    expect(mockGetConnectivityWcs).toHaveBeenCalledTimes(1)
  })

  it('should be unsuccessful when invalid url', async () => {
    const url = '/th/de/300a06/selections/test'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '2')

    expect(response.status).toBe(400)
    expect(response.body.success).toBeFalsy()
    expect(response.body.error).toBeDefined()
    expect(mockGetConnectivityWcs).toHaveBeenCalledTimes(0)
  })

  it('should succeed for v1', async () => {
    const url = '/th/de/30006/selections/test'

    const response = await request(server)
      .get(url)
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJqd3RkYXRhIiwiaWF0IjoxNTE2MjkxNjYyfQ.65dP-NreTD01Tz-8CB_y7BONXImVQc8AN3IDrdJHSV8')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Accept-Version', '1')

    expect(response.status).toBe(200)
    expect(mockGetConnectivityWcs).toHaveBeenCalledTimes(1)
  })
})
