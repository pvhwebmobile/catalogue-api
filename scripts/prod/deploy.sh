#!/usr/bin/env bash

docker pull 10.181.97.40:5000/eis-api-prod
docker container stop eis-api-prod || true && docker container rm eis-api-prod || true
docker run --name eis-api-prod --network container_network --restart=always -p 3000:3000 -d 10.181.97.40:5000/eis-api-prod