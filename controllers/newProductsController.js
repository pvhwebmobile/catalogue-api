const newProductsController =
(globals) => (function (globals) {
  const config = require('config')
  const fetch = require('node-fetch')
  const joi = require('@hapi/joi')

  const { CustomError } = require('../libs/errors')
  const { buildURL } = require('../libs/url')
  const { getCountryProperties } = require('../libs/countryConfig')
  const { validateBody } = require('../libs/bodyValidator')
  const { versionCheck } = require('../libs/versioning')
  const { getCampaignLabel } = require('../controllers/campaignsController')
  const logger = require('../libs/logger')
  const {
    encodeSearchTerm,
    sortSizes,
  } = require('../libs/fredHopper')

  const parseFilterDEPRECATED = (filter) => {
    if (!filter || !filter.title || !filter.filtersection) {
      return
    }

    const toReturn = {
      name: filter.title,
      items: [],
    }

    for (let fs of filter.filtersection) {
      let label = fs.link.name
      if (label.indexOf(':') !== -1) {
        label = label.split(':')[1]
      }
      const nfs = {
        count: `${fs.nr}`,
        value: `${filter.on}:${fs.value.value}`,
        label: label,
      }
      toReturn.items.push(nfs)
    }

    return toReturn
  }

  const parseFacetMapDEPRECATED = (facetMap, tenant) => {
    const filters = {
      MainColour: {
        name: 'Colour',
        items: [],
      },
      Size: {
        name: 'Size',
        items: [],
      },
    }

    if (!facetMap || !facetMap.length >= 1 || !tenant) {
      return filters
    }

    for (let m of facetMap) {
      if (m.universe === tenant) {
        for (let f of m.filter) {
          if (/^maincolour$/.test(f.on)) {
            filters.MainColour = parseFilterDEPRECATED(f)
          } else if (/^size.+$/.test(f.on)) {
            filters.Size = parseFilterDEPRECATED(f)
          }
        }
      }
    }

    return filters
  }

  const parseFilter = (type = 'multiselect-or', filter) => {
    if (!filter || !filter.title || !filter.filtersection) {
      return
    }

    let toReturn = {
      type,
    }
    toReturn.name = filter.title

    if (type === 'multiselect-or') {
      toReturn.items = []

      for (let fs of filter.filtersection) {
        let label = fs.link.name
        if (label.indexOf(':') !== -1) {
          label = label.split(':')[1]
        }
        const nfs = {
          count: `${fs.nr}`,
          value: `${filter.on}:${fs.value.value}`,
          label: label,
        }
        toReturn.items.push(nfs)
      }
    } else if (type === 'slider') {
      toReturn.items = []
      for (let i = 0; i < filter.filtersection.length; i += 1) {
        const fs = filter.filtersection[i]
        const on = filter.on[i]
        const label = i === 0 ? 'Min' : 'Max'
        const nfs = {
          value: `${on}:${fs.value.value}`,
          label: label,
        }
        toReturn.items.push(nfs)
      }
    }

    return toReturn
  }

  const parseFacetMap = (facetMap, tenant, countryCode) => {
    const filters = {}

    if (!facetMap || !facetMap.length >= 1 || !tenant) {
      return filters
    }

    for (let m of facetMap) {
      if (m.universe === tenant) {
        let priceFilter = {
          filtersection: [],
          on: [],
        }
        for (let f of m.filter) {
          if (/^price_min$/.test(f.on)) {
            priceFilter.title = f.title
            priceFilter.on.unshift(f.on)
            priceFilter.filtersection.unshift(f.filtersection[0])
          } else if (/^price_max$/.test(f.on)) {
            priceFilter.title = f.title
            priceFilter.on.push(f.on)
            priceFilter.filtersection.push(f.filtersection[0])
          } else {
            const rg = new RegExp(`(.*)_${countryCode}`)
            const containsCCInKey = rg.test(f.on)
            let key = f.on
            if (containsCCInKey) {
              key = rg.exec(f.on)[1]
            }
            filters[key] = parseFilter('multiselect-or', f)
          }
        }
        if (priceFilter.on.length === 2) {
          filters['price_slider'] = parseFilter('slider', priceFilter)
        }
      }
    }

    return filters
  }

  const parseAttributes = (attributes) => {
    const newAttributes = {}

    if (!attributes || attributes.length < 1) {
      return newAttributes
    }

    for (let attribute of attributes) {
      const {
        value,
        name,
        basetype,
        isnull,
      } = attribute

      if (isnull) {
        newAttributes[name] = {
          value: null,
          'non-ml': null,
        }
      } else {
        let newValue = []
        let newNonMl = []

        value.map((v) => {
          switch (basetype) {
            case 'asset': {
              newValue.push(v.value)
              newNonMl.push(v['non-ml'])
              break
            }
            case 'text': {
              newValue.push(v.value)
              newNonMl.push(v['non-ml'])
              break
            }
            case 'int': {
              newValue.push(parseInt(v.value))
              newNonMl.push(parseInt(v['non-ml']))
              break
            }
            case 'float': {
              newValue.push(parseFloat(v.value))
              newNonMl.push(parseFloat(v['non-ml']))
              break
            }
            case 'list': {
              newValue.push(v.value)
              newNonMl.push(v['non-ml'])
              break
            }
            case 'set': {
              newValue.push(v.value)
              newNonMl.push(v['non-ml'])
              break
            }
            case 'cat': {
              newValue.push(v.value)
              newNonMl.push(v['non-ml'])
              break
            }
            default: {
              newValue.push(undefined)
              newNonMl.push(undefined)
              break
            }
          }
        })

        newAttributes[name] = {
          value: newValue,
          'non-ml': newNonMl,
        }
      }
    }

    return newAttributes
  }

  const parseRelatedCombis = (relatedcombis) => {
    const toReturn = []

    if (!relatedcombis) {
      return toReturn
    }

    const rcs = relatedcombis.split('|')
    for (let rc of rcs) {
      const [partNumber, productAttrColour, seoUrl, mainColour] = rc.split(',')
      toReturn.push({
        partNumber,
        productAttrColour,
        seoUrl,
        mainColour,
      })
    }

    return toReturn
  }

  const parseProductPromos = async (tenant, countryCode, productPromos) => {
    let campaignLabel
    if (
      !productPromos ||
      !productPromos['non-ml'] ||
      !productPromos['value']
    ) {
      return campaignLabel
    }
    for (let i = 0; i < productPromos['non-ml'].length; i += 1) {
      const nonMl = productPromos['non-ml'][i]
      const nonMlSplit = nonMl.split('_')
      const campaign = nonMlSplit[nonMlSplit.length - 1]
      const _cl = await getCampaignLabel(tenant, countryCode, campaign)
      if (_cl && (!campaignLabel || _cl.priority < campaignLabel.priority)) {
        campaignLabel = _cl
      }
    }

    if (!campaignLabel) {
      return undefined
    }

    delete campaignLabel.priority

    return campaignLabel
  }

  const parseListFromItemsSection =
    async (itemsSection, tenant, countryCode) => {
      const { client } = globals
      const products = []
      let totalProductsAmount = 0
      if (!itemsSection || !itemsSection.items || !itemsSection.items.item ||
        !itemsSection.results || !itemsSection.results['total-items']) {
        return { products, totalProductsAmount }
      }

      const {
        currency,
      } = getCountryProperties(tenant, countryCode, ['currency'])

      totalProductsAmount = itemsSection.results['total-items']

      for (let item of itemsSection.items.item) {
        const {
          attribute,
          id,
        } = item

        const convertedAttributes = parseAttributes(attribute)

        const attributeSchema = joi.object().keys({
          value: joi.array().required(),
          'non-ml': joi.array().sparse(true),
        }).unknown()

        const rcName = `relatedcombis_${countryCode}`
        const promoName = `product_attr_promo_${countryCode}`
        const attributesSchema = joi.object().keys({
          name: attributeSchema,
          partNumber: attributeSchema,
          label: attributeSchema,
          price: attributeSchema,
          _imageurl: attributeSchema,
          [rcName]: attributeSchema,
          product_attr_colour: attributeSchema,
          [promoName]: attributeSchema.optional(true),
        }).unknown()

        try {
          validateBody(convertedAttributes, attributesSchema)
          const colorId = convertedAttributes['product_attr_colour']['non-ml'][0]
          const relatedCombis = (
            convertedAttributes[rcName] &&
            parseRelatedCombis(
              convertedAttributes[rcName]['value']['0']
            )
          ) || []
          let imageSuffix = '<SIZE>'
          if (client === 'itommy') {
            imageSuffix = 'eisPLP'
          } else if (client === 'icalvin') {
            imageSuffix = 'listing'
          }
          const mainImage = 'http:' +
            convertedAttributes._imageurl.value[0]
              .replace(/\?\$\w+\$/ig, `?$${imageSuffix}$`)
          const price = (
            convertedAttributes[`price_${countryCode}`] ||
            convertedAttributes['price_uk']
          ).value[0]
          const onSale = (
            convertedAttributes[`onsale_${countryCode}`] &&
            convertedAttributes[`onsale_${countryCode}`].value
          )[0]
          let wasPrice = (
            convertedAttributes[`price_data_${countryCode}`] &&
            convertedAttributes[`price_data_${countryCode}`].value[0]
          ) || undefined
          if (wasPrice) {
            wasPrice = parseFloat(wasPrice.substring(1))
          }
          let hasLengthWidth = (
            convertedAttributes['possible_variant_sizelength'] &&
            true
          ) || false
          const categoriesContainsBlackFriday = (
            ((convertedAttributes['categories'] && convertedAttributes['categories'].value) || [])
              .some((c) => /.*black.*friday.*/ig.test(c))
          )
          const productAttrPromos = convertedAttributes[promoName] || (
            categoriesContainsBlackFriday
              ? {
                'non-ml': ['product_attr_promo_black20friday'],
                value: ['BLACK FRIDAY'],
              }
              : {}
          )
          const campaignLabel = await parseProductPromos(tenant, countryCode, productAttrPromos)
          const newProduct = {
            id: id.substring(1),
            name: convertedAttributes.name.value[0],
            partNumber: convertedAttributes.partnumber.value[0],
            label: convertedAttributes.label.value[0],
            price: price,
            wasPrice: (onSale && wasPrice) || undefined,
            currency: currency,
            mainImage: mainImage,
            colorId: colorId.toUpperCase(),
            isStyleColor: false,
            styleId: undefined,
            colorsAmount: relatedCombis.length,
            hasLengthWidth,
            campaignLabel,
          }
          products.push(newProduct)
        } catch (error) {
          logger.error(error.message)
        }
      }

      return { products, totalProductsAmount }
    }

  const generateImageURLsForProduct =
    (tenant, countryCode, partNumber) => {
      const { client } = globals
      const imageURLs = []
      const imageBaseUrl = tenant === 'th'
        ? config.URLS.TH_S7_IMAGE_URL : config.URLS.CK_S7_IMAGE_URL
      let imageSuffix = '<SIZE>'
      if (client === 'itommy') {
        imageSuffix = 'eisPDP'
      }

      if (client === 'icalvin') {
        imageSuffix = 'main@2x'
      }

      const swatchSuffix = (client === 'itommy' || client === 'icalvin')
        ? 'minithumb'
        : 'minithumb'
      const imageTypes = [
        `_main?$${imageSuffix}$`,
        `_alternate1?$${imageSuffix}$`,
        `_alternate2?$${imageSuffix}$`,
        `_main?$${swatchSuffix}$`,
      ]

      for (let type of imageTypes) {
        imageURLs.push(
          buildURL(
            imageBaseUrl,
            { IMAGE_ID: `${partNumber}${type}` },
            tenant,
            countryCode,
          )
        )
      }

      return imageURLs
    }

  const generateSizesForProductColor =
    async (tenant, countryCode, productAttributes) => {
      const sizes = []

      const {
        currency,
      } = getCountryProperties(tenant, countryCode, ['currency'])
      const sizeVariants = (
        (
          productAttributes[`possible_variant_size_${countryCode}`] ||
          productAttributes['possible_variant_size_uk']
        )
          .value[0]
          .split('|')
      ) || undefined
      const lengthVariants = (
        productAttributes['possible_variant_sizelength'] &&
        productAttributes['possible_variant_sizelength'].value &&
        productAttributes['possible_variant_sizelength'].value[0].split('|')
      ) || undefined
      const widthVariants = (
        productAttributes['possible_variant_sizewidth'] &&
        productAttributes['possible_variant_sizewidth'].value &&
        productAttributes['possible_variant_sizewidth'].value[0].split('|')
      ) || undefined
      const sizeStocks = (
        productAttributes['stock'] &&
        productAttributes['stock'].value
      ) || undefined
      const skuPartNumbers = (
        productAttributes['skupartnumbers'] &&
        productAttributes['skupartnumbers'].value &&
        productAttributes['skupartnumbers'].value[0].split('|')
      ) || undefined

      // price attributes
      const skuPriceData = (
        productAttributes[`sku_price_data_${countryCode}`] &&
        productAttributes[`sku_price_data_${countryCode}`].value &&
        productAttributes[`sku_price_data_${countryCode}`].value[0].split(';')
      ) || undefined
      const priceData = (
        productAttributes[`price_data_${countryCode}`] &&
        productAttributes[`price_data_${countryCode}`].value &&
        productAttributes[`price_data_${countryCode}`].value[0]
      ) || undefined
      const price = (
        productAttributes[`price_${countryCode}`] &&
        productAttributes[`price_${countryCode}`].value &&
        productAttributes[`price_${countryCode}`].value[0]
      ) || undefined

      // color attributes
      const colorValue = (
        productAttributes['product_attr_colour'] &&
        productAttributes['product_attr_colour'].value[0]
      ) || undefined
      const colorId = (
        productAttributes['product_attr_colour'] &&
        productAttributes['product_attr_colour']['non-ml'][0]
      ) || undefined

      // promos/campaigns
      const promoName = `product_attr_promo_${countryCode}`
      const categoriesContainsBlackFriday = (
        ((productAttributes['search_categories'] && productAttributes['search_categories'].value) || [])
          .some((c) => /.*black.*friday.*/ig.test(c))
      )
      const productAttrPromos = productAttributes[promoName] || (
        categoriesContainsBlackFriday
          ? {
            'non-ml': ['product_attr_promo_black20friday'],
            value: ['BLACK FRIDAY'],
          }
          : {}
      )
      let campaignLabel = await parseProductPromos(tenant, countryCode, productAttrPromos)

      // lookups
      let lengthLookup = {}
      if (lengthVariants) {
        for (let i = 0; i < lengthVariants.length; i += 1) {
          const variant = lengthVariants[i]
          let [id,, length] = variant.split(':')
          lengthLookup[id] = length
        }
      }

      let widthLookup = {}
      if (widthVariants) {
        for (let i = 0; i < widthVariants.length; i += 1) {
          const variant = widthVariants[i]
          let [id,, width] = variant.split(':')
          widthLookup[id] = width
        }
      }

      let skuPartNumbersLookup = {}
      if (skuPartNumbers) {
        for (let i = 0; i < skuPartNumbers.length; i += 1) {
          const variant = skuPartNumbers[i]
          let [id, skuPartNumber] = variant.split(',')
          skuPartNumbersLookup[id] = skuPartNumber
        }
      }

      let skuPriceDataLookup = {}
      const currentPrice = Number.parseFloat(price)
      let wasPrice
      const wpRegRes = /w(\d+\.\d+)/ig.exec(priceData)
      if (wpRegRes) {
        wasPrice = Number.parseFloat(wpRegRes[1])
      }
      if (skuPriceData) {
        for (let i = 0; i < skuPriceData.length; i += 1) {
          const variant = skuPriceData[i]
          const cpRegRes = /cp_(\d+):(\d+\.\d+)/g.exec(variant) || []
          const wpRegRes = /wp_\d+:(\d+\.\d+)/g.exec(variant) || []
          const id = cpRegRes[1]
          const skuPrices = {
            id,
            currentPrice: Number.parseFloat(cpRegRes[2]),
            wasPrice: Number.parseFloat(wpRegRes[1]) || undefined,
          }
          skuPriceDataLookup[id] = skuPrices
        }
      }

      for (let i = 0; i < sizeVariants.length; i += 1) {
        const variant = sizeVariants[i]
        const stock = (sizeStocks && sizeStocks[i]) || 0
        let [id,, localizedSize] = variant.split(':')
        if (/^\d{4}$/.test(localizedSize)) {
          localizedSize =
            localizedSize.substring(0, 2) + '/' +
            localizedSize.substring(2)
        }
        const numId = id.substring(1)
        const skuPrice = skuPriceDataLookup[numId]
        let _currentPrice = currentPrice
        let _wasPrice = wasPrice
        if (skuPrice) {
          _currentPrice = skuPrice.currentPrice
          _wasPrice = skuPrice.wasPrice
        }
        const newSize = {
          id: numId,
          skuId: skuPartNumbersLookup[numId],
          buyable: 'true',
          price: _currentPrice,
          wasPrice: _wasPrice,
          currency: currency,
          inventory: {
            onlineInventory: stock,
            storeInventory: 0,
          },
          colour: {
            id: colorId.toUpperCase(),
            value: colorValue,
          },
          size: {
            id: `Size_${localizedSize}`,
            value: localizedSize,
            length: lengthLookup[id],
            width: widthLookup[id],
          },
          campaignLabel,
        }
        sizes.push(newSize)
      }

      return sortSizes(sizes)
    }

  const generateColorsForProduct =
    async (tenant, countryCode, items, productUrl) => {
      const colors = []

      for (let item of items) {
        const {
          attribute,
        } = item

        const convertedAttributes = parseAttributes(attribute)

        const promoName = `product_attr_promo_${countryCode}`
        const attributeSchema = joi.object().keys({
          value: joi.any().required(),
          'non-ml': joi.any(),
        }).unknown()
        const attributesSchema = joi.object().keys({
          name: attributeSchema,
          label: attributeSchema,
          price: attributeSchema,
          seo_url: attributeSchema,
          partnumber: attributeSchema,
          [promoName]: attributeSchema.optional(true),
        }).unknown()

        try {
          validateBody(convertedAttributes, attributesSchema)
          const partNumber = convertedAttributes.partnumber.value[0]
          // const colorPartNumberSuffix = partNumber.substring(partNumber.length - 3)
          const partNumberSplit = partNumber.slice(0, partNumber.length - 3) +
            '_' + partNumber.slice(partNumber.length - 3)
          const id = convertedAttributes['product_attr_colour']['non-ml'][0]
          const name = convertedAttributes['product_attr_colour'].value[0]
          const colorSizes = await generateSizesForProductColor(
            tenant, countryCode, convertedAttributes
          )
          const colorUrl = `${productUrl}`
          const urls = generateImageURLsForProduct(
            tenant, countryCode, partNumberSplit
          )

          const categoriesContainsBlackFriday = (
            ((convertedAttributes['search_categories'] && convertedAttributes['search_categories'].value) || [])
              .some((c) => /.*black.*friday.*/ig.test(c))
          )
          const productAttrPromos = convertedAttributes[promoName] || (
            categoriesContainsBlackFriday
              ? {
                'non-ml': ['product_attr_promo_black20friday'],
                value: ['BLACK FRIDAY'],
              }
              : {}
          )
          const campaignLabel = await parseProductPromos(
            tenant, countryCode, productAttrPromos
          )
          const newColor = {
            id: id.toUpperCase(),
            referenceId: partNumber,
            value: name,
            url: colorUrl,
            images: urls.slice(0, 3),
            pattern: urls[3],
            sizes: colorSizes,
            campaignLabel,
          }

          colors.push(newColor)
        } catch (error) {
          logger.error(error.message)
        }
      }

      return colors
    }

  const parseProductFromItemsSectionAndThemes =
    async (tenant, countryCode, itemsSection, themes) => {
      if (!itemsSection || !itemsSection.items || !itemsSection.items.item) {
        return {}
      }

      const {
        currency,
      } = getCountryProperties(tenant, countryCode, ['currency'])

      const product = itemsSection.items.item[0]
      const {
        attribute,
        id,
      } = product

      const convertedAttributes = parseAttributes(attribute)
      const promoName = `product_attr_promo_${countryCode}`
      const attributeSchema = joi.object().keys({
        value: joi.any().required(),
        'non-ml': joi.any(),
      }).unknown()
      const attributesSchema = joi.object().keys({
        name: attributeSchema,
        description: attributeSchema,
        label: attributeSchema,
        price: attributeSchema,
        seo_url: attributeSchema,
        partnumber: attributeSchema,
        [promoName]: attributeSchema.optional(true),
      }).unknown()

      try {
        validateBody(convertedAttributes, attributesSchema)
        const seoToken = convertedAttributes.seo_url.value[0].split('|')[1]
        const parameters = {
          SEO_TOKEN: seoToken,
        }
        const productUrl = buildURL(config.URLS.WCS_SEO_TOKEN_LINK, parameters, tenant, countryCode)
        const filteredItems = themes[0].theme.filter(
          (t) => t.name === 'PDP - Other Style Colours'
        )[0]
        const otherItems = (filteredItems && filteredItems.items.item) || []
        const allItems = [product, ...otherItems]
        const productColors = await generateColorsForProduct(
          tenant, countryCode, allItems, productUrl
        )
        const price = (
          convertedAttributes[`price_${countryCode}`] ||
          convertedAttributes['price_uk']
        ).value[0]
        const onSale = (
          convertedAttributes[`onsale_${countryCode}`] &&
          convertedAttributes[`onsale_${countryCode}`].value
        )[0]
        let wasPrice = (
          convertedAttributes[`price_data_${countryCode}`] &&
          convertedAttributes[`price_data_${countryCode}`].value[0]
        ) || undefined
        if (wasPrice) {
          wasPrice = parseFloat(wasPrice.substring(1))
        }
        const categoriesContainsBlackFriday = (
          ((convertedAttributes['search_categories'] && convertedAttributes['search_categories'].value) || [])
            .some((c) => /.*black.*friday.*/ig.test(c))
        )
        const productAttrPromos = convertedAttributes[promoName] || (
          categoriesContainsBlackFriday
            ? {
              'non-ml': ['product_attr_promo_black20friday'],
              value: ['BLACK FRIDAY'],
            }
            : {}
        )
        const campaignLabel = await parseProductPromos(tenant, countryCode, productAttrPromos)
        let description = ''
        if (convertedAttributes.description && convertedAttributes.description.value) {
          description = convertedAttributes.description.value[0] || ''
        }
        const newProduct = {
          id: id.substring(1),
          referenceId: convertedAttributes.stylepartnumber.value[0],
          name: convertedAttributes.name.value[0],
          description: description,
          price: price,
          wasPrice: (onSale && wasPrice) || undefined,
          currency: currency,
          label: convertedAttributes.label.value[0],
          colors: productColors,
          url: productUrl,
          campaignLabel,
        }
        return newProduct
      } catch (error) {
        logger.error(error.message)
        return {}
      }
    }

  const parseQueryFacets = (facets) => {
    const toReturn = []
    const fhFacets = {}
    const facetList = facets.split(',')

    for (let facet of facetList) {
      const [name, value] = facet.split(':')
      if (!fhFacets[name]) {
        fhFacets[name] = `${value}`
      } else {
        fhFacets[name] += `;${value}`
      }
    }

    for (let fname of Object.keys(fhFacets)) {
      toReturn.push({
        name: fname,
        value: fhFacets[fname],
      })
    }

    return toReturn
  }

  const getSuggestionsByTerm = async (_term) => {
    const {
      tenant: _tenant,
      countryCode: _countryCode,
      locale: _locale,
    } = globals
    if (!_tenant || !_countryCode || !_term) {
      throw new CustomError({
        message: 'Tenant, countryCode, and term are required',
        code: 400,
      })
    }

    const tenant = _tenant.toLowerCase()
    const countryCode = _countryCode.toLowerCase()
    const term = _term.toLowerCase()
    const encodedTerm = encodeSearchTerm(term)

    const longTenant = `pvh_${tenant}`
    let locale = _locale
    if (locale === undefined) {
      locale = getCountryProperties(tenant, countryCode, ['locale']).locale
    }

    const scope = `//${tenant}/${locale}/countries_pvh>{${countryCode}}` +
      `&search=${encodedTerm}`

    const urlParameters = {
      LONG_TENANT: longTenant,
      SCOPE: scope,
    }

    const url = buildURL(
      config.URLS.FH_GET_SUGGESTIONS_BY_TERM, urlParameters, tenant, countryCode
    )
    const response = await fetch(url)

    if (!response.ok) {
      throw new CustomError({
        message: `Error fetching ${url} from FredHopper.`,
        code: response.status,
      })
    }

    const json = await response.json()

    // validate response from FH is the expected response
    const suggestion = joi.object().keys({
      searchterm: joi.string(),
      nrResults: joi.number(),
    }).unknown()
    const suggestionGroup = joi.object().keys({
      indexName: joi.string().required(),
      indexTitle: joi.string().required(),
      suggestions: joi.array().items(suggestion).min(0),
    })
    const schema = joi.object().keys({
      suggestionGroups: joi.array().items(suggestionGroup).min(1),
    })
    validateBody(json, schema)

    // get keyword suggestions
    let suggestions = []
    for (let sg of json.suggestionGroups) {
      if (sg.indexName === '1keywords') {
        suggestions = sg.suggestions
      }
    }

    // map to old values
    const toReturn = []
    for (let s of suggestions) {
      const {
        searchterm,
        nrResults,
      } = s
      toReturn.push({
        term: searchterm,
        frequency: nrResults,
      })
    }

    return toReturn
  }

  const getProductsByTerm = async (_term, _page = 1, _facets, _categoryId) => {
    const {
      tenant: _tenant,
      countryCode: _countryCode,
      locale: _locale,
    } = globals
    if (!_tenant || !_countryCode || !_term) {
      throw new CustomError({
        message: 'Tenant, countryCode, and term are required',
        code: 400,
      })
    }

    const tenant = _tenant.toLowerCase()
    const countryCode = _countryCode.toLowerCase()
    const term = _term.toLowerCase()
    const page = _page < 1 ? 1 : _page
    const pageSize = 36

    const encodedTerm = encodeSearchTerm(term)
    let locale = _locale
    if (locale === undefined) {
      locale = getCountryProperties(tenant, countryCode, ['locale']).locale
    }

    let location = `//${tenant}/${locale}/$s=${encodedTerm}/` +
      `countries_pvh>{${countryCode}}`

    if (_categoryId) {
      const categories = _categoryId.split(';')
      for (let cat /* meow */ of categories) {
        location += `/categories<{${cat}}`
      }
    }

    if (_facets) {
      const facets = parseQueryFacets(_facets)
      for (let {
        name,
        value,
      } of facets) {
        location += `/${name}>{${value}}`
      }
    }

    const params = {
      LOCATION: location,
      VIEW_SIZE: `${pageSize}`,
      START_INDEX: `${pageSize * (page - 1)}`,
    }

    const url = buildURL(config.URLS.FH_GET_PRODUCTS_BY_TERM, params, tenant, countryCode)
    const options = {
      headers: {
        Accept: 'application/json',
        Authorization: 'Basic ' +
          Buffer.from(
            config.FH_CREDENTIALS[tenant].username + ':' +
            config.FH_CREDENTIALS[tenant].password
          ).toString('base64'),
      },
    }
    const response = await fetch(url, options)

    if (!response.ok) {
      throw new CustomError({
        message: `Error fetching ${url} from FredHopper.`,
        code: response.status,
      })
    }

    const json = await response.json()

    const schema = joi.object().keys({
      universes: joi.object().keys({
        universe: joi.array(),
      }),
    }).unknown()

    validateBody(json, schema)

    const {
      universes,
    } = json
    const {
      universe,
    } = universes
    let itemsSection, facetMap
    for (let u of universe) {
      if (u.name === tenant) {
        itemsSection = u['items-section']
        facetMap = u['facetmap']
      }
    }

    if (!itemsSection || !facetMap) {
      return {
        filters: {},
        products: [],
        totalProductsAmount: 0,
      }
    }

    const filters = versionCheck(globals.ctx.state.acceptVersion, ['>=4'])
      ? parseFacetMap(facetMap, tenant, countryCode)
      : parseFacetMapDEPRECATED(facetMap, tenant, countryCode)
    const { products, totalProductsAmount } = await parseListFromItemsSection(
      itemsSection, tenant, countryCode
    )

    return {
      filters,
      products,
      totalProductsAmount,
    }
  }

  const getProductsByCategoryId = async (categoryId, _page = 1, _facets) => {
    const {
      tenant: _tenant,
      countryCode: _countryCode,
      locale: _locale,
    } = globals
    if (!_tenant || !_countryCode || !categoryId) {
      throw new CustomError({
        message: 'Tenant, countryCode, and categoryId are required',
        code: 400,
      })
    }

    const tenant = _tenant.toLowerCase()
    const countryCode = _countryCode.toLowerCase()
    const page = _page < 1 ? 1 : _page
    const pageSize = 36

    let locale = _locale
    if (locale === undefined) {
      locale = getCountryProperties(tenant, countryCode, ['locale']).locale
    }
    let location = `//${tenant}/${locale}/countries_pvh>{${countryCode}}`

    const categories = categoryId.split(';')
    for (let cat /* meow */ of categories) {
      location += `/categories<{${cat}}`
    }

    if (_facets) {
      const facets = parseQueryFacets(_facets)
      for (let {
        name,
        value,
      } of facets) {
        location += `/${name}>{${value}}`
      }
    }

    const params = {
      LOCATION: location,
      VIEW_SIZE: `${pageSize}`,
      START_INDEX: `${pageSize * (page - 1)}`,
    }

    const url = buildURL(
      config.URLS.FH_GET_PRODUCTS_BY_CATEGORY_ID, params, tenant, countryCode
    )
    const options = {
      headers: {
        Accept: 'application/json',
        Authorization: 'Basic ' +
          Buffer.from(
            config.FH_CREDENTIALS[tenant].username +
            ':' + config.FH_CREDENTIALS[tenant].password
          ).toString('base64'),
      },
    }
    const response = await fetch(url, options)

    if (!response.ok) {
      throw new CustomError({
        message: `Error fetching ${url} from FredHopper.`,
        code: response.status,
      })
    }

    const json = await response.json()

    const schema = joi.object().keys({
      universes: joi.object().keys({
        universe: joi.array(),
      }),
    }).unknown()

    validateBody(json, schema)

    const {
      universes,
    } = json
    const {
      universe,
    } = universes
    let itemsSection, facetMap
    for (let u of universe) {
      if (u.name === tenant) {
        itemsSection = u['items-section']
        facetMap = u['facetmap']
      }
    }

    if (!itemsSection || !facetMap) {
      return {
        filters: {},
        products: [],
        totalProductsAmount: 0,
      }
    }

    const filters = versionCheck(globals.ctx.state.acceptVersion, ['>=4'])
      ? parseFacetMap(facetMap, tenant, countryCode)
      : parseFacetMapDEPRECATED(facetMap, tenant, countryCode)
    const { products, totalProductsAmount } = await parseListFromItemsSection(
      itemsSection, tenant, countryCode
    )

    return {
      filters,
      products,
      totalProductsAmount,
    }
  }

  const getProductById = async (_productId) => {
    const {
      tenant: _tenant,
      countryCode: _countryCode,
    } = globals
    if (!_tenant || !_countryCode || !_productId) {
      throw new CustomError({
        message: 'Tenant, countryCode, and productId are required.',
        status: 400,
      })
    }

    const tenant = _tenant.toLowerCase()
    const countryCode = _countryCode.toLowerCase()

    let locale = globals.locale
    if (locale === undefined) {
      locale = getCountryProperties(tenant, countryCode, ['locale']).locale
    }
    const location = `//${tenant}/${locale}/countries_pvh>{${countryCode}}`

    const urlParameters = {
      LOCATION: location,
      SECOND_ID: `p${_productId}`,
    }
    const url = buildURL(
      config.URLS.FH_GET_PRODUCT_DETAILS_BY_ID, urlParameters, tenant, countryCode,
    )
    const options = {
      headers: {
        Accept: 'application/json',
        Authorization: 'Basic ' +
          Buffer.from(
            config.FH_CREDENTIALS[tenant].username + ':' +
            config.FH_CREDENTIALS[tenant].password
          ).toString('base64'),
      },
    }

    const response = await fetch(url, options)

    if (!response.ok) {
      throw new CustomError({
        message: `Error fetching ${url} from FredHopper.`,
        code: response.status,
      })
    }

    const json = await response.json()

    const schema = joi.object().keys({
      universes: joi.object().keys({
        universe: joi.array(),
      }),
    }).unknown()

    validateBody(json, schema)

    const {
      universes,
    } = json
    const {
      universe,
    } = universes
    let itemsSection, themes
    for (let u of universe) {
      if (u.name === tenant) {
        itemsSection = u['items-section']
        themes = u['themes']
      }
    }

    if (!itemsSection || !themes) {
      return {}
    }

    const product = await parseProductFromItemsSectionAndThemes(
      tenant, countryCode, itemsSection, themes
    )

    return product
  }

  const getProductByPartNumber = async (_partNumber) => {
    const {
      tenant: _tenant,
      countryCode: _countryCode,
      storeId,
      langId,
    } = globals
    if (!_tenant || !_countryCode || !_partNumber) {
      throw new CustomError({
        message: 'Tenant, countryCode, and partNumber are required.',
        status: 400,
      })
    }

    const tenant = _tenant.toLowerCase()
    const countryCode = _countryCode.toLowerCase()

    let locale = globals.locale
    const res = getCountryProperties(tenant, countryCode, ['locale', 'catalogId'])
    const catalogId = res.catalogId
    if (locale === undefined) {
      locale = res.locale
    }

    let params = {
      STORE_ID: storeId,
      PART_NUMBER: _partNumber,
      LANG_ID: langId,
      CATALOG_ID: catalogId,
    }
    let url = buildURL(
      config.URLS.WCS_GET_PRODUCT_DETAILS_BY_PART_NUMBER,
      params,
      tenant,
      countryCode,
    )
    let options = {
      headers: {
        Accept: 'application/json',
      },
    }
    let response = await fetch(url, options)

    if (!response.ok) {
      throw new CustomError({
        message: `Error fetching ${url} from WCS.`,
        code: response.status,
      })
    }

    let json = await response.json()

    const parentProductId = json.catalogEntryView[0].parentCatalogEntryID[1]

    // get PDP by stylepartnumber from FH
    const location = `//${tenant}/${locale}/countries_pvh>{${countryCode}}`
    params = {
      LOCATION: location,
      SECOND_ID: `p${parentProductId}`,
    }
    url = buildURL(
      config.URLS.FH_GET_PRODUCT_DETAILS_BY_ID,
      params,
      tenant,
      countryCode,
    )
    options = {
      headers: {
        Accept: 'application/json',
        Authorization: 'Basic ' +
          Buffer.from(
            config.FH_CREDENTIALS[tenant].username + ':' +
            config.FH_CREDENTIALS[tenant].password
          ).toString('base64'),
      },
    }
    response = await fetch(url, options)

    if (!response.ok) {
      throw new CustomError({
        message: `Error fetching ${url} from FH.`,
        code: response.status,
      })
    }

    json = await response.json()

    const schema = joi.object().keys({
      universes: joi.object().keys({
        universe: joi.array(),
      }),
    }).unknown()

    validateBody(json, schema)

    const {
      universes,
    } = json
    const {
      universe,
    } = universes
    let itemsSection, themes
    for (let u of universe) {
      if (u.name === tenant) {
        itemsSection = u['items-section']
        themes = u['themes']
      }
    }

    if (!itemsSection || !themes) {
      return {}
    }

    const product = parseProductFromItemsSectionAndThemes(
      tenant, countryCode, itemsSection, themes
    )

    return product
  }

  return {
    parseFilter,
    parseFacetMap,
    parseAttributes,
    parseRelatedCombis,
    parseListFromItemsSection,
    generateImageURLsForProduct,
    generateSizesForProductColor,
    generateColorsForProduct,
    parseProductFromItemsSectionAndThemes,
    getSuggestionsByTerm,
    getProductsByTerm,
    getProductsByCategoryId,
    getProductById,
    getProductByPartNumber,
  }
})(globals)

module.exports = newProductsController
