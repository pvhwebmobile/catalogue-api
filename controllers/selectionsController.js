const config = require('config')
const fetch = require('node-fetch')
const joi = require('@hapi/joi')
const https = require('https')

const { CustomError } = require('../libs/errors')
const Validation = require('../libs/validation')
const logger = require('../libs/logger')
const { buildURL } = require('../libs/url')
const { getCountryProperties } = require('../libs/countryConfig')
const { validateBody } = require('../libs/bodyValidator')
const SelectionModel = require('../models/selection')

const validation = new Validation()

// temp: this ignores the self-signed certs that the devel wcs uses
const customProxy = new https.Agent({
  rejectUnauthorized: false,
})
class Selections {
  async createSelection (ctx, next, version) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let products = ctx.request.body.products

    validation.isStoreIdValid(tenant, countryCode, storeId)

    // TODO: selection validation required
    const WCSBasket = await this.WCSCreateBasketWithReservedStock(ctx, version)
    let data

    if (WCSBasket.orderId) {
      const { hasPromotion, promotions } = Selections.getPromotionsFromWCSBasket(WCSBasket)
      const selectionProducts = this.matchOrderPriceAndInventory(products, WCSBasket.orderItem, version)

      let selection = {
        storeId,
        countryCode,
        tenant,
        name: ctx.request.body.name,
        siteId: ctx.request.body.siteId,
        status: config.STATUSES.created,
        orderId: WCSBasket.orderId,
        WCTrustedToken: WCSBasket.WCTrustedToken,
        WCToken: WCSBasket.WCToken,
        totalPrice: WCSBasket.grandTotal * 1,
        currency: WCSBasket.grandTotalCurrency,
        products: selectionProducts,
        hasPromotion,
        promotions,
      }

      const selectionModelInstance = new SelectionModel(selection)

      data = await selectionModelInstance.save().catch((error) => {
        throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
      })

      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, data: data }
    } else {
      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: false, errorkey: WCSBasket.errorkey, message: WCSBasket.errorMessage }
    }
    next()
  }

  async getSelectionsListByStatusAndSiteId (ctx) {
    // From the POS system it's only possible to fetch data by the siteId (physical store ID), no access to country or tenant
    const countryCode = (ctx.params.countryCode && ctx.params.countryCode.toUpperCase()) || 'DE'
    const storeId = ctx.params.storeId
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'

    if (countryCode && storeId && tenant) {
      validation.isStoreIdValid(tenant, countryCode, storeId)
    }

    const currentDate = new Date()
    const status = ctx.query.status
    const siteId = ctx.params.siteId
    const query = {
      siteId,
      status: config.STATUSES[status] || config.STATUSES.sentToTill,
      sentToTillDate: { '$gt': currentDate.setMinutes(currentDate.getMinutes() - 25) },
    }

    if (status && !config.STATUSES[status]) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.STATUS_UNDEFINED, status: config.ERRORS.SELECTIONS.STATUS })
    }

    const selections = await SelectionModel.find(query).sort({ sentToTillDate: 1 }).catch(function (error) {
      throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
    }).map((model) => model.toObject())

    // get order ids and place request to WCS
    const orderIds = selections.map(obj => obj.orderId)
    let orderPersonalInfo
    if (orderIds.length > 0) {
      orderPersonalInfo = await this.getPersonalDetailsForOrders(tenant, countryCode, orderIds)
    }

    // insert first and last name from orders from WCS
    if (orderPersonalInfo) {
      orderPersonalInfo.map((item) => {
        const { ordersId, personalInfo } = item
        const selectionIndex = orderIds.findIndex((oid) => oid === ordersId)
        selections[selectionIndex].personalDetails = {
          shippingFirstName: personalInfo.firstName,
          shippingLastName: personalInfo.lastName,
        }
      })
    }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = {
      success: true,
      data: selections,
    }
  }

  async getSelectionByOrderId (ctx, next) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'

    validation.isStoreIdValid(tenant, countryCode, storeId)

    let orderId = ctx.params.orderId

    if (!orderId) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.ORDER_ID_UNDEFINED, status: config.ERRORS.SELECTIONS.STATUS })
    }

    const selection = await SelectionModel.findOne({ orderId: orderId, storeId: storeId }).catch(function (error) {
      throw new CustomError({ message: error.message, code: error.name })
    })

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: selection }
  }

  async getConnectivityWcs (ctx, next) {
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const { langId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      STORE_ID: storeId,
      LANG_ID: ctx.state.langId || langId,
    }
    const fetchValidationRulesURL = buildURL(
      config.URLS.WCS_GET_VALIDATION_RULES, urlParameters, tenant, countryCode
    )
    const fetchValidationRules = await fetch(fetchValidationRulesURL, {})

    if (!fetchValidationRules.ok) {
      throw new CustomError({ message: `Error fetching ${fetchValidationRulesURL}.`, status: fetchValidationRules.status })
    }

    ctx.status = fetchValidationRules.status
    ctx.type = 'JSON'
    ctx.body = { success: fetchValidationRules.status === 200 }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: false, data: [] }
  }

  async deleteSelectionById (ctx) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const selectionId = ctx.params.selectionId

    if (!selectionId) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.SELECTION_ID_UNDEFINED, status: config.ERRORS.SELECTIONS.STATUS })
    }

    let selection = await SelectionModel.findOneAndRemove({ _id: selectionId, storeId }).catch(function (error) {
      throw new CustomError({ message: error.message, code: error.name })
    })

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: selection }
  }

  async updateSelectionPersonalDetails (ctx) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const selectionId = ctx.params.selectionId

    let personalDetails = ctx.request.body
    let billAddressSameAsShipping = false
    if (
      (
        !personalDetails.billingZipCode &&
        !personalDetails.billingPhone &&
        !personalDetails.billingPersonTitle &&
        !personalDetails.billingLastName &&
        !personalDetails.billingFirstName &&
        !personalDetails.billingCity &&
        !personalDetails.billingAddress &&
        !personalDetails.billingAddress2
      ) ||
      (
        personalDetails.shippingZipCode === personalDetails.billingZipCode &&
        personalDetails.shippingPhone === personalDetails.billingPhone &&
        personalDetails.shippingPersonTitle === personalDetails.billingPersonTitle &&
        personalDetails.shippingLastName === personalDetails.billingLastName &&
        personalDetails.shippingFirstName === personalDetails.billingFirstName &&
        personalDetails.shippingCity === personalDetails.billingCity &&
        personalDetails.shippingAddress === personalDetails.billingAddress &&
        personalDetails.shippingAddress2 === personalDetails.billingAddress2
      )
    ) {
      billAddressSameAsShipping = true
    }

    validation.isStoreIdValid(tenant, countryCode, storeId)

    if (!selectionId) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.SELECTION_ID_UNDEFINED, status: config.ERRORS.SELECTIONS.STATUS })
    }

    let selection = await SelectionModel.findOne({ _id: selectionId }).catch(function (error) {
      throw new CustomError({ message: error.message, code: error.name })
    })

    if (!selection) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.SELECTION_NOT_FOUND, status: config.ERRORS.SELECTIONS.STATUS })
    }

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const _countryCode = countryCode === 'UK' ? 'GB' : countryCode
    let body = {
      storeId,
      langId: ctx.state.langId || langId,
      catalogId: catalogId,
      orderId: selection.orderId,
      shipModeId: personalDetails.shippingModeId,
      userEmail1: personalDetails.email,
      deliveryStLocId: !personalDetails.shipStoreId ? selection.siteId : personalDetails.shipStoreId,
      billAddressSameAsShip: billAddressSameAsShipping,
      prepareOrder: '1',
      bill_userType: 'G',
      bill_calledFromCheckout: 'Y',
      bill_addressType: 'B',
      bill_actionType: 'add',
      ship_zipCode: personalDetails.shippingZipCode,
      ship_phone1: personalDetails.shippingPhone,
      ship_personTitle: personalDetails.shippingPersonTitle,
      ship_lastName: personalDetails.shippingLastName,
      ship_firstName: personalDetails.shippingFirstName,
      ship_country: _countryCode,
      ship_city: personalDetails.shippingCity,
      ship_address1: personalDetails.shippingAddress,
      ship_address2: personalDetails.shippingAddress2,
      ship_actionType: 'add',
      ship_calledFromCheckout: 'Y',
      ship_addressType: 'SB',
      ship_userType: 'G',
    }

    if (!billAddressSameAsShipping) {
      body.bill_zipCode = personalDetails.billingZipCode
      body.bill_phone1 = personalDetails.billingPhone
      body.bill_personTitle = personalDetails.billingPersonTitle
      body.bill_lastName = personalDetails.billingLastName
      body.bill_firstName = personalDetails.billingFirstName
      body.bill_city = personalDetails.billingCity
      body.bill_address1 = personalDetails.billingAddress
      body.bill_address2 = personalDetails.billingAddress2
      body.bill_country = personalDetails.billingCountry === 'UK'
        ? 'GB'
        : personalDetails.billingCountry
    } else {
      body.bill_zipCode = personalDetails.shippingZipCode
      body.bill_phone1 = personalDetails.shippingPhone
      body.bill_personTitle = personalDetails.shippingPersonTitle
      body.bill_lastName = personalDetails.shippingLastName
      body.bill_firstName = personalDetails.shippingFirstName
      body.bill_city = personalDetails.shippingCity
      body.bill_address1 = personalDetails.shippingAddress
      body.bill_address2 = personalDetails.shippingAddress2
      body.bill_country = _countryCode
    }

    let options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        WCTrustedToken: selection.WCTrustedToken,
        WCToken: selection.WCToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    }

    const fetchCreateAddressURL = buildURL(config.URLS.WCS_CREATE_ADDRESS_PREPARE_ORDER, {}, tenant, countryCode)
    const fetchCreateAddress = await fetch(fetchCreateAddressURL, options)

    if (!fetchCreateAddress.ok) {
      if (fetchCreateAddress.status === 400) {
        const _basket = await fetchCreateAddress.json()
        throw new CustomError({ message: _basket, status: config.ERRORS.SELECTIONS.STATUS })
      }
      throw new CustomError({ message: `Error fetching ${fetchCreateAddressURL}.`, status: fetchCreateAddress.status })
    }

    let basket = await fetchCreateAddress.json()

    const schema = joi.object().keys({
      totalShippingCharge: joi.string().required(),
      grandTotal: joi.string().required(),
    }).unknown()
    validateBody(basket, schema)

    body = {
      storeId,
      langId: ctx.state.langId || langId,
      catalogId: catalogId,
      orderId: selection.orderId,
    }

    options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        WCTrustedToken: selection.WCTrustedToken,
        WCToken: selection.WCToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    }

    const fetchPlaceOrderPendingPaymentURL = buildURL(config.URLS.WCS_PLACE_ORDER_PENDING_PAYMENT, {}, tenant, countryCode)
    const fetchPlaceOrderPendingPayment = await fetch(fetchPlaceOrderPendingPaymentURL, options)

    if (!fetchPlaceOrderPendingPayment.ok) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.CANT_PLACE_ORDER_PENDING_PAYMENT, status: fetchPlaceOrderPendingPayment.status })
    }

    let shippingPrice = parseFloat(basket.totalShippingCharge)
    if (basket.adjustment && basket.adjustment.length > 0) {
      for (let adj of basket.adjustment) {
        if (adj.xadju_calUsageId === '-7') {
          const amount = parseFloat(adj.amount)
          shippingPrice += amount
        }
      }
    }

    selection = await Selections.updateSelectionCollection(ctx, {
      status: config.STATUSES.sentToTill,
      sentToTillDate: new Date(),
      shippingModeId: personalDetails.shippingModeId,
      shippingPrice: shippingPrice,
      totalPrice: basket.grandTotal,
    })

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: selection }
  }

  async updateSelectionStatusByOrderId (ctx) {
    const {
      tid,
      pspReference,
      merchantReference,
    } = ctx.request.body

    const { action, orderId } = ctx.params

    let update

    switch (action) {
      case 'addedToTransaction':
        update = { status: config.STATUSES.addedToTransaction }
        break

      case 'paid': {
        logger.info(
          `GK payment call: ${tid} / ${pspReference} / ${merchantReference}`
        )
        update = {
          status: config.STATUSES.paid,
          terminalId: tid,
          pspReference,
          merchantReference,
        }
        await this.WCSConfirmOrderPayment(ctx)
        break
      }
      case 'cancelled':
        update = {
          status: config.STATUSES.sentToTill,
        }
        break

      case 'rejected':
        update = {
          status: config.STATUSES.rejected,
        }
        await this.WCSCancelBasket(orderId)
        break

      default: throw new CustomError({ message: config.ERRORS.SELECTIONS.STATUS_NOT_FOUND, status: config.ERRORS.SELECTIONS.STATUS })
    }

    const selection = await Selections.updateSelectionCollectionByOrderId(orderId, update)

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: selection }
  }

  async getDeliveryMethodsBySelectionId (ctx, next) {
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    let storeId = ctx.params.storeId
    let selectionId = ctx.params.selectionId || 0

    validation.isStoreIdValid(tenant, countryCode, storeId)

    if (!selectionId) {
      throw new CustomError({ message: config.ERRORS.STORES.NO_SELECTION_ID, code: config.ERRORS.STORES.CODE })
    }

    const selection = await SelectionModel.findOne({ _id: selectionId }).catch(function (error) {
      throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
    })

    if (!selection) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.ORDER_NOT_FOUND, status: config.ERRORS.SELECTIONS.STATUS })
    }

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])

    const options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        WCTrustedToken: selection.WCTrustedToken,
        WCToken: selection.WCToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        storeId,
        orderId: selection.orderId,
        catalogId: catalogId,
        langId: ctx.state.langId || langId,
      }),
    }

    const fetchDeliveryMethodsURL = buildURL(config.URLS.WCS_GET_DELIVERY_METHODS_LIST, {}, tenant, countryCode)
    const fetchDeliveryMethods = await fetch(fetchDeliveryMethodsURL, options)

    if (!fetchDeliveryMethods.ok) {
      throw new CustomError({ message: config.ERRORS.COUNTRIES.CANT_GET_DELIVERY_METHODS, status: fetchDeliveryMethods.status })
    }

    let deliveryMethods = await fetchDeliveryMethods.json()

    const shippingInfoSchema = joi.object().keys({
      shipCharge: joi.number(),
      shipModeCode: joi.string(),
      shipModeId: joi.string(),
      shipModeTitle: joi.string(),
    }).unknown()
    const schema = {
      shippingInfo: joi.array().items(shippingInfoSchema.required()),
      orderId: joi.string().required(),
    }
    validateBody(deliveryMethods, schema)

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: deliveryMethods }
  }

  async getValidationRules (ctx, next) {
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const options = {
      agent: customProxy,
      method: 'GET',
    }

    const { langId } = getCountryProperties(tenant, countryCode, ['langId'])
    const parameters = {
      LANG_ID: ctx.state.langId || langId,
      STORE_ID: storeId,
    }
    const fetchValidationRulesURL = buildURL(
      config.URLS.WCS_GET_VALIDATION_RULES, parameters, tenant, countryCode
    )
    const fetchValidationRules = await fetch(fetchValidationRulesURL, options)

    if (!fetchValidationRules.ok) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.CANT_GET_VALIDATION_RULES, status: fetchValidationRules.status })
    }

    let validationRules = await fetchValidationRules.json()

    const schema = joi.object().keys({
      LV: joi.object().required(),
      LU: joi.object().required(),
      LT: joi.object().required(),
      GB: joi.object().required(),
      HR: joi.object().required(),
      RO: joi.object().required(),
      HU: joi.object().required(),
      FR: joi.object().required(),
      BG: joi.object().required(),
      BE: joi.object().required(),
      DE: joi.object().required(),
      DK: joi.object().required(),
      FI: joi.object().required(),
      IE: joi.object().required(),
      CZ: joi.object().required(),
      AT: joi.object().required(),
      SE: joi.object().required(),
      SI: joi.object().required(),
      SK: joi.object().required(),
      IT: joi.object().required(),
      PL: joi.object().required(),
      PT: joi.object().required(),
      COMMON: joi.object().required(),
      RU: joi.object().required(),
      CH: joi.object().required(),
      ES: joi.object().required(),
      NL: joi.object().required(),
      EE: joi.object().required(),
    })
    validateBody(validationRules, schema)

    // adjust all regex
    const userEmail1ReplacementRegex = '^\\w(?:(?:\\.*\\-*|\\+)\\w)*@\\w(?:(?:\\.*\\-*\\w)*)\\.(?:[a-z]+(?:\\.[a-z]{2})?)$'
    for (let rules in validationRules) {
      for (let rule in validationRules[rules]) {
        if (validationRules[rules][rule].regex) {
          validationRules[rules][rule].wcsRegex = validationRules[rules][rule].regex
          validationRules[rules][rule].regex = rule === 'userEmail1' ? userEmail1ReplacementRegex : validationRules[rules][rule].regex.slice(1, -1)
        }
      }
    }

    // adjust zipcode regex for UK
    validationRules.GB.zipCode.regex = validationRules.GB.zipCode.regex.slice(1, -1)

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: validationRules }
  }

  async getValidationZipCode (ctx, next) {
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const zipcode = ctx.params.zipcode
    const orderId = ctx.params.orderId
    const city = ctx.params.city
    const address1 = ctx.params.address1

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const options = {
      agent: customProxy,
      method: 'POST',
    }

    const params = {
      ADDRESS1: address1,
      CITY: city,
      STORE_ID: storeId,
      COUNTRY_CODE: countryCode === 'UK' ? 'GB' : countryCode,
      ORDER_ID: orderId,
      ZIP_CODE: zipcode,
    }

    const fetchValidationZipCodeURL = buildURL(config.URLS.WCS_GET_VALIDATION_ZIPCODE, params, tenant, countryCode)
    const fetchValidationZipCode = await fetch(fetchValidationZipCodeURL, options)

    if (!fetchValidationZipCode.ok) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.CANT_GET_VALIDATION_ZIPCODE, status: fetchValidationZipCode.status })
    }

    let validationZipCode = await fetchValidationZipCode.json()

    // TODO: get schema back from wcs
    const schema = joi.object()
    validateBody(validationZipCode, schema)

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: validationZipCode }
  }

  async updateSelection (ctx, next, version) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const siteId = ctx.request.body.siteId
    const name = ctx.request.body.name
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const selectionId = ctx.params.selectionId
    let products = ctx.request.body.products

    validation.isStoreIdValid(tenant, countryCode, storeId)

    let selection = await SelectionModel.findOne({ _id: selectionId }).catch(function (error) {
      throw new CustomError({ message: error.message, code: error.name })
    })

    if (!selection) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.SELECTION_NOT_FOUND, status: config.ERRORS.SELECTIONS.STATUS })
    }

    // TODO: selection validation required
    const WCSBasket = await this.WCSUpdateBasketWithReservedStock(ctx, version)

    let data

    if (WCSBasket.orderId) {
      const { promotions, hasPromotion } = Selections.getPromotionsFromWCSBasket(WCSBasket)
      const selectionProducts = this.matchOrderPriceAndInventory(products, WCSBasket.orderItem, version)
      let shippingModeId
      if (WCSBasket.orderItem && WCSBasket.orderItem.length > 0) {
        shippingModeId = WCSBasket.orderItem[0].shipModeId
      }

      let shippingPrice = parseFloat(WCSBasket.totalShippingCharge)
      if (WCSBasket.adjustment && WCSBasket.adjustment.length > 0) {
        for (let adj of WCSBasket.adjustment) {
          if (adj.xadju_calUsageId === '-7') {
            const amount = parseFloat(adj.amount)
            shippingPrice += amount
          }
        }
      }
      let update = {
        name: name,
        storeId: storeId,
        siteId: siteId,
        countryCode: countryCode,
        tenant: tenant,
        orderId: WCSBasket.orderId,
        WCTrustedToken: selection.WCTrustedToken,
        WCToken: selection.WCToken,
        totalPrice: WCSBasket.grandTotal * 1,
        currency: WCSBasket.grandTotalCurrency,
        products: selectionProducts,
        promotions,
        hasPromotion,
        shippingModeId,
        shippingPrice,
        userPromotionCode: selection.userPromotionCode,
      }

      data = await Selections.updateSelectionCollection(ctx, update)

      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, data: data }
    } else {
      ctx.status = 200
      ctx.type = 'JSON'
      ctx.body = { success: true, errorkey: WCSBasket.errorkey }
    }
    next()
  }

  // WCS related calls and helpers
  matchOrderPriceAndInventory (
    selectionProducts, orderProducts, version = '2'
  ) {
    if (version === '2') {
      return this.matchOrderPriceAndInventoryBySKU(
        selectionProducts, orderProducts
      )
    } else {
      const useSku = selectionProducts.every(({ skuId }) => skuId !== undefined)
      if (useSku) {
        return this.matchOrderPriceAndInventoryBySKU(
          selectionProducts, orderProducts
        )
      }
      return this.matchOrderPriceAndInventoryByCatEntryId(
        selectionProducts, orderProducts
      )
    }
  }

  matchOrderPriceAndInventoryBySKU (selectionProducts, orderProducts) {
    let products = []
    if (orderProducts && selectionProducts) {
      orderProducts.forEach((orderItem) => {
        let orderItemQuantity = Number.parseInt(orderItem.quantity)
        selectionProducts.forEach((selectionItem) => {
          if (orderItem.partNumber === `${selectionItem.skuId}` && orderItemQuantity > 0) {
            let { adjustment, quantity, unitPrice, orderItemPrice } = orderItem

            quantity = Number.parseInt(quantity)
            unitPrice = Number.parseFloat(unitPrice)
            orderItemPrice = Number.parseFloat(orderItemPrice)

            let totalDiscount = 0
            if (adjustment) {
              totalDiscount = adjustment.filter((adj) => adj.usage === 'Discount')
                .map((disc) => Number.parseFloat(disc.amount))
                .reduce((a, c) => a + c, 0)
            }

            const itemDiscount = totalDiscount / quantity

            selectionItem.originalUnitPrice = unitPrice
            selectionItem.promoUnitPrice = unitPrice + itemDiscount
            selectionItem.originalTotalPrice = orderItemPrice
            selectionItem.promoTotalPrice = orderItemPrice + totalDiscount

            products.push(selectionItem)
            orderItemQuantity--
          }
        })
      })
    }

    return products
  }

  matchOrderPriceAndInventoryByCatEntryId (selectionProducts, orderProducts) {
    let products = []
    if (orderProducts && selectionProducts) {
      orderProducts.forEach((orderItem) => {
        let orderItemQuantity = Number.parseInt(orderItem.quantity)
        selectionProducts.forEach((selectionItem) => {
          if (orderItem.productId === `${selectionItem.id}` && orderItemQuantity > 0) {
            let { adjustment, quantity, unitPrice, orderItemPrice } = orderItem

            quantity = Number.parseInt(quantity)
            unitPrice = Number.parseFloat(unitPrice)
            orderItemPrice = Number.parseFloat(orderItemPrice)

            let totalDiscount = 0
            if (adjustment) {
              totalDiscount = adjustment.filter((adj) => adj.usage === 'Discount')
                .map((disc) => Number.parseFloat(disc.amount))
                .reduce((a, c) => a + c, 0)
            }

            const itemDiscount = totalDiscount / quantity

            selectionItem.originalUnitPrice = unitPrice
            selectionItem.promoUnitPrice = unitPrice + itemDiscount
            selectionItem.originalTotalPrice = orderItemPrice
            selectionItem.promoTotalPrice = orderItemPrice + totalDiscount

            products.push(selectionItem)
            orderItemQuantity--
          }
        })
      })
    }

    return products
  }

  async getPersonalDetailsForOrders (tenant, countryCode, orderIds) {
    const orderIdsString = orderIds.join(',')
    const auth = `Basic ${Buffer.from(config.WCS_CREDENTIALS.username + ':' + config.WCS_CREDENTIALS.password).toString('base64')}`
    const options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        'Authorization': auth,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ orderIds: orderIdsString }),
    }

    const getPersonalDetailsURL = buildURL(config.URLS.WCS_ORDERS_GET_PERSONAL_DETAILS, {}, tenant, countryCode)
    const wcsResponse = await fetch(getPersonalDetailsURL, options)

    if (!wcsResponse.ok) {
      logger.error({ message: `Error fetching ${getPersonalDetailsURL}.`, status: wcsResponse.status })
      return null
    }

    const wcsResponseData = await wcsResponse.json()

    const orderPersonalInfoSchema = joi.object().keys({
      ordersId: joi.string().required(),
      personalInfo: joi.object().keys({
        firstName: joi.string().required(),
        lastName: joi.string().required(),
      }).required(),
    })
    const schema = joi.object().keys({
      orderPersonalInfo: joi.array().has(orderPersonalInfoSchema).required(),
    })
    validateBody(wcsResponseData, schema)

    const orderPersonalInfo = wcsResponseData.orderPersonalInfo
    return orderPersonalInfo
  }

  async WCSGuestLogin (ctx) {
    const tenant = ctx.params.tenant || 'th'
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId

    const urlParameters = { 'STORE_ID': storeId }
    const fetchGuestLoginURL = buildURL(config.URLS.WCS_ORDERS_GUEST_LOGIN, urlParameters, tenant, countryCode)
    const fetchGuestLogin = await fetch(fetchGuestLoginURL, {
      agent: customProxy,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    })

    if (!fetchGuestLogin.ok) {
      throw new CustomError({ message: `Error fetching ${fetchGuestLoginURL}.`, status: fetchGuestLogin.status })
    }

    const guestLogin = await fetchGuestLogin.json()

    const schema = joi.object().keys({
      WCToken: joi.string().required(),
      WCTrustedToken: joi.string().required(),
    }).unknown()
    validateBody(guestLogin, schema)

    validation.isWCSResponseValid(guestLogin)

    // store wcs tokens in session
    // ctx.fileSession.set('WCToken', guestLogin.WCToken);
    // ctx.fileSession.set('WCTrustedToken', guestLogin.WCTrustedToken);

    return guestLogin
  }

  async WCSCreateBasketWithReservedStock (ctx, version) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const selection = ctx.request.body
    const selectionProducts = selection.products
    logger.info('Attempting to login as guest to WCS')
    const guestLogin = await this.WCSGuestLogin(ctx, storeId)
    logger.info('Logged in as guest')
    const WCToken = guestLogin.WCToken
    const WCTrustedToken = guestLogin.WCTrustedToken
    let basket = null

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const basketProducts = Selections.combineSelectionProductsToWCSBasketProducts(selectionProducts, version)

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const body = Object.assign({
      storeId: storeId,
      langId: ctx.state.langId || langId,
      catalogId: catalogId,
      siteId: selection.siteId,
    }, basketProducts)
    const options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        WCTrustedToken: WCTrustedToken,
        WCToken: WCToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    }

    const fetchCreateBasketURL = buildURL(config.URLS.WCS_CREATE_BASKET_WITH_RESERVED_STOCK, {}, tenant, countryCode)
    logger.info('Attempting to create basket')
    const fetchCreateBasket = await fetch(fetchCreateBasketURL, options)

    if (!fetchCreateBasket.ok) {
      try {
        basket = await fetchCreateBasket.json()
      } catch (e) {
        throw new CustomError({ message: `Error fetching ${fetchCreateBasketURL}.`, status: fetchCreateBasket.status })
      }
      return basket
    }

    logger.info('Created basket')
    basket = await fetchCreateBasket.json()

    const orderItemSchema = joi.object().keys({
      partNumber: joi.string().required(),
      adjustment: joi.any().optional(),
      quantity: joi.string().required(),
      unitPrice: joi.string().required(),
      orderItemPrice: joi.string().required(),
    }).unknown()
    const schema = joi.object().keys({
      orderItem: joi.array().items(orderItemSchema).min(1),
      orderId: joi.string().required(),
      grandTotal: joi.string().required(),
      grandTotalCurrency: joi.string().required(),
    }).unknown()
    validateBody(basket, schema)

    return Object.assign({ WCToken, WCTrustedToken }, basket)
  }

  async WCSCancelBasket (orderId) {
    const selection = await SelectionModel.findOne({ orderId: orderId }).catch(function (error) {
      throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
    })

    if (!selection) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.SELECTION_NOT_FOUND, status: config.ERRORS.SELECTIONS.STATUS })
    }

    const options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        WCTrustedToken: selection.WCTrustedToken,
        WCToken: selection.WCToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        orderId: orderId,
      }),
    }

    const fetchCancelBasketURL = buildURL(config.URLS.WCS_CANCEL_BASKET, {}, selection.tenant, selection.countryCode)
    const fetchCancelBasket = await fetch(fetchCancelBasketURL, options)

    if (!fetchCancelBasket.ok) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.CANT_CANCEL_BASKET, status: fetchCancelBasket.status })
    }

    return true
  }

  async WCSConfirmOrderPayment (ctx) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const terminalId = ctx.request.body.tid
    const pspReference = ctx.request.body.pspReference
    const merchantReference = ctx.request.body.merchantReference
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const orderId = ctx.params.orderId

    validation.isStoreIdValid(tenant, countryCode, storeId)

    logger.info('Retrieving selection details from db')
    const selection = await SelectionModel.findOne({ orderId: orderId }).catch(function (error) {
      throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
    })

    if (!selection) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.SELECTION_NOT_FOUND, status: config.ERRORS.SELECTIONS.STATUS })
    }

    const body = {
      orderId: orderId,
      terminalId: terminalId,
      pspReference: pspReference,
      merchantReference: merchantReference,
    }

    const auth = `Basic ${Buffer.from(config.WCS_CREDENTIALS.username + ':' + config.WCS_CREDENTIALS.password).toString('base64')}`
    const options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': auth,
      },
      body: JSON.stringify(body),
    }

    const fetchConfirmOrderPaymentURL = buildURL(config.URLS.WCS_CONFIRM_ORDER_PAYMENT, {}, tenant, countryCode)
    logger.info('Confirming order with WCS')
    const fetchConfirmOrderPayment = await fetch(fetchConfirmOrderPaymentURL, options)

    if (!fetchConfirmOrderPayment.ok) {
      logger.crit('Order confirmation failed')
      throw new CustomError({ message: config.ERRORS.SELECTIONS.CANT_CONFIRM_ORDER_PAYMENT_WCS, status: fetchConfirmOrderPayment.status })
    }

    logger.info('Order confirmed')
    return true
  }

  async WCSUpdateBasketWithReservedStock (ctx, version) {
    const countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    const selectionId = ctx.params.selectionId
    const siteId = ctx.request.body.siteId
    const selectionProducts = ctx.request.body.products

    const selection = await SelectionModel.findOne({ _id: selectionId }).catch(function (error) {
      throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
    })

    if (!selection) {
      throw new CustomError({ message: config.ERRORS.SELECTIONS.SELECTION_NOT_FOUND, status: config.ERRORS.SELECTIONS.STATUS })
    }

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const basketProducts = Selections.combineSelectionProductsToWCSBasketProducts(selectionProducts, version)

    const body = Object.assign({
      storeId: storeId,
      langId: ctx.state.langId ||
        config.WCS_COUNTRIES[tenant][countryCode].langId,
      catalogId: config.WCS_COUNTRIES[tenant][countryCode].catalogId,
      orderId: selection.orderId,
      siteId: siteId,
    }, basketProducts)
    const options = {
      agent: customProxy,
      method: 'POST',
      headers: {
        WCTrustedToken: selection.WCTrustedToken,
        WCToken: selection.WCToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    }

    const fetchUpdateBasketURL = buildURL(config.URLS.WCS_UPDATE_BASKET_WITH_RESERVED_STOCK, {}, tenant, countryCode)
    const fetchUpdateBasket = await fetch(fetchUpdateBasketURL, options)

    if (!fetchUpdateBasket.ok) {
      let basket
      try {
        basket = await fetchUpdateBasket.json()
      } catch (e) {
        throw new CustomError({ message: `Error fetching ${fetchUpdateBasketURL}.`, status: fetchUpdateBasket.status })
      }
      return basket
    }

    const basket = await fetchUpdateBasket.json()

    const orderItemSchema = joi.object().keys({
      partNumber: joi.string().required(),
      adjustment: joi.any().optional(),
      quantity: joi.string().required(),
      unitPrice: joi.string().required(),
      orderItemPrice: joi.string().required(),
    }).unknown()
    const schema = joi.object().keys({
      orderItem: joi.array().items(orderItemSchema).min(1),
      orderId: joi.string().required(),
      grandTotal: joi.string().required(),
      grandTotalCurrency: joi.string().required(),
    }).unknown()
    validateBody(basket, schema)

    return basket
  }

  static getPromotionsFromWCSBasket (wcsBasket) {
    let hasPromotion = false
    let promotions

    const { adjustment } = wcsBasket

    if (!adjustment || adjustment.length === 0) {
      return { hasPromotion: false, promotions: null }
    }

    promotions = adjustment.filter(({ xadju_calUsageId: uid }) => uid === '-1')
      .map(({ code, description, amount, currency, language, descriptionLanguage }) =>
        ({ code, description, amount, currency, language, descriptionLanguage }))

    if (promotions.length > 0) {
      hasPromotion = true
    }

    return { hasPromotion, promotions }
  }

  static combineSelectionProductsToWCSBasketProducts (
    selectionProducts, version = '2'
  ) {
    const basketProducts = {}
    const combinedProducts = {}

    if (version === '3') {
      const useSku = selectionProducts.every(({ skuId }) => skuId !== undefined)

      if (useSku) {
        let index = 0
        selectionProducts.forEach(({ skuId }) => {
          if (combinedProducts[skuId]) {
            combinedProducts[skuId].quantity += 1
          } else {
            combinedProducts[skuId] = {
              index,
              id: skuId,
              quantity: 1,
            }
            index += 1
          }
        })
      } else {
        let index = 0
        selectionProducts.forEach(({ id }) => {
          if (combinedProducts[id]) {
            combinedProducts[id].quantity += 1
          } else {
            combinedProducts[id] = {
              index,
              id,
              quantity: 1,
            }
            index += 1
          }
        })
      }

      for (let key in combinedProducts) {
        const { index, id, quantity } = combinedProducts[key]
        basketProducts[`${useSku ? 'partNumber' : 'catEntryId'}_${index}`] = id
        basketProducts[`quantity_${index}`] = quantity.toString()
      }
    } else {
      let index = 0
      selectionProducts.forEach(({ skuId: partNumber }) => {
        if (combinedProducts[partNumber]) {
          combinedProducts[partNumber].quantity += 1
        } else {
          combinedProducts[partNumber] = {
            index,
            partNumber,
            quantity: 1,
          }
          index += 1
        }
      })

      for (let key in combinedProducts) {
        const { index, partNumber, quantity } = combinedProducts[key]
        basketProducts[`partNumber_${index}`] = partNumber
        basketProducts[`quantity_${index}`] = quantity.toString()
      }
    }

    return basketProducts
  }

  static async updateSelectionCollection (ctx, selection = ctx.request.body) {
    const {
      storeId,
      selectionId: _id,
    } = ctx.params
    const $set = Object.assign({}, selection)

    let hasUserPromoCode = false
    if (selection.promotions && selection.promotions.length > 0) {
      hasUserPromoCode = selection.promotions
        .findIndex(({ userPromotionCode }) => userPromotionCode !== undefined) > -1
    }

    if (!hasUserPromoCode && $set.userPromotionCode) {
      $set.userPromotionCode = undefined
    }

    return SelectionModel.findOneAndUpdate({ _id, storeId }, { $set }, { new: true }).catch(error => {
      throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
    })
  }

  static async updateSelectionCollectionByOrderId (orderId, selection) {
    const $set = Object.assign({}, selection)

    let hasUserPromoCode = false
    if (selection.promotions && selection.promotions.length > 0) {
      hasUserPromoCode = selection.promotions
        .findIndex(({ userPromotionCode }) => userPromotionCode !== undefined) > -1
    }

    if (!hasUserPromoCode && $set.userPromotionCode) {
      $set.userPromotionCode = undefined
    }

    return SelectionModel.findOneAndUpdate({ orderId: orderId }, { $set }, { new: true }).catch(error => {
      throw new CustomError({ message: error.message, status: config.ERRORS.SELECTIONS.STATUS })
    })
  }
}

module.exports = Selections
