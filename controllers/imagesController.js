const config = require('config')
const AWS = require('aws-sdk')

const Validation = require('../libs/validation')
const logger = require('../libs/logger')

let validation = new Validation()

AWS.config.update(config.AWS.S3, true)
const s3 = new AWS.S3({ region: config.AWS.S3.region })

class Images {
  async getImageById (ctx, next) {
    let countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    let storeId = ctx.params.storeId
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let folder = ctx.params.folder || 'categories'

    validation.isStoreIdValid(tenant, countryCode, storeId)

    let image = await this.downloadObject(
      tenant,
      `${tenant}/${folder}/${ctx.params.imageId}` +
      `${config.IMAGES_STORAGE.extension}`
    )
    image = image.Body

    ctx.status = 200
    ctx.type = 'image/jpg'
    ctx.body = image
  }

  async getSplashScreenImagesList (ctx, next) {
    let countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    let storeId = ctx.params.storeId
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let folder = 'splashScreen'

    validation.isStoreIdValid(tenant, countryCode, storeId)

    let images = await this.getListObjects(tenant, folder)

    let data = images.Contents.map(image => {
      return image.Key.slice(0, -4).substring(3)
    })
    data.shift()

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: data }
  }

  getListObjects (tenant, folder) {
    return new Promise(function (resolve, reject) {
      s3.listObjects({
        Bucket: config.AWS.S3.bucketName[tenant],
        Prefix: tenant + '/' + folder + '/',
      }, function (err, data) {
        if (err) {
          logger.error(err, err.stack)
          return reject(err)
        } else {
          resolve(data)
        }
      })
    })
  }

  downloadObject (tenant, identifier) {
    return new Promise(function (resolve, reject) {
      s3.getObject({
        Bucket: config.AWS.S3.bucketName[tenant],
        Key: identifier,
      }, function (err, data) {
        if (err) {
          logger.error(err)
          return reject(err)
        }
        resolve(data)
      })
    })
  }
}

module.exports = Images
