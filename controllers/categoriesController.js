const config = require('config')
const fetch = require('node-fetch')
const joi = require('@hapi/joi')

const { CustomError } = require('../libs/errors')
const Validation = require('../libs/validation')
const { buildURL } = require('../libs/url')
const { getCountryProperties } = require('../libs/countryConfig')
const { validateBody } = require('../libs/bodyValidator')

let validation = new Validation()

const imageVersion = '1.17'

class Categories {
  // isGrouping - flag to highlight do we need to do the custom categories
  // grouping
  async getTopCategoriesByStoreId (ctx, next) {
    let countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let storeId = ctx.params.storeId
    let isGrouping = ctx.query.isGrouping * 1 || false

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const {
      langId,
      catalogId,
    } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      'STORE_ID': storeId,
      'LANG_ID': ctx.state.langId || langId,
      'CATALOG_ID': catalogId,
    }
    const fetchCategoriesURL = buildURL(
      config.URLS.WCS_LIST_OF_TOP_CATEGORIES,
      urlParameters,
      tenant,
      countryCode,
    )
    const fetchCategories = await fetch(fetchCategoriesURL, {})

    if (!fetchCategories.ok) {
      throw new CustomError({
        message: `Error fetching ${fetchCategoriesURL}.`,
        status: fetchCategories.status,
      })
    }

    let rawCategories = await fetchCategories.json()

    const catalogSchema = joi.object().keys({
      uniqueID: joi.string().required(),
      identifier: joi.string().required(),
      name: joi.string().required(),
      shortDescription: joi.string().required(),
    }).unknown()
    const schema = joi.object().keys({
      catalogGroupView: joi.array().has(catalogSchema).required(),
    }).unknown()
    validateBody(rawCategories, schema)

    let categories = this.parseCategory(
      ctx,
      tenant,
      rawCategories.identifier,
      rawCategories.catalogGroupView,
      isGrouping,
    )

    let i = categories.length
    while (i--) {
      if (config.FEATURE_CATEGORY[tenant][categories[i].identifier]) {
        for (let j = 0; j < categories.length; j++) {
          if (
            categories[j].identifier ===
            config.FEATURE_CATEGORY[tenant][categories[i].identifier].parent
          ) {
            categories[j].categories.unshift(categories[i])
            categories.splice(i, 1)
            break
          }
        }
      }
    }

    if (isGrouping) {
      let j = categories.length
      while (j--) {
        // if main category is listed in sales_category, eg. TH_SALE_DE
        const parent = categories[j]
        if (config.SALES_CATEGORY[tenant][parent.identifier]) {
          // loop through all children and find where to move them to
          // eg. TH_SALE_DE with child TH_SALE_DE_MEN is moved to be
          // child of TH_MEN
          for (let k = 0; k < parent.categories.length; k++) {
            const child = parent.categories[k]
            if (
              config.SALES_CATEGORY[tenant][parent.identifier][child.identifier]
            ) {
              const mapTo =
                config.SALES_CATEGORY[tenant][parent.identifier][child.identifier]
              for (let l = 0; l < categories.length; l++) {
                if (categories[l].identifier === mapTo) {
                  child.name = 'SALE'
                  child.description = 'SALE'
                  categories[l].categories.push(child)
                }
              }
            }
          }
          // afterwards remove the parent category, eg. TH_SALE_DE
          categories.splice(j, 1)
        }
      }
    }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: categories }
  }

  async getCategoriesForPOC (ctx) {
    const countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) ||
      'TH'
    const storeId = ctx.params.storeId

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const {
      langId,
      catalogId,
    } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      'STORE_ID': storeId,
      'LANG_ID': ctx.state.langId || langId,
      'CATALOG_ID': catalogId,
    }
    const fetchCategoriesURL = buildURL(
      config.URLS.WCS_LIST_OF_TOP_CATEGORIES, urlParameters, tenant, countryCode,
    )
    const fetchCategories = await fetch(fetchCategoriesURL, {})

    if (!fetchCategories.ok) {
      throw new CustomError({
        message: `Error fetching ${fetchCategoriesURL}.`,
        status: fetchCategories.status,
      })
    }

    const categories = await fetchCategories.json()

    const catalogSchema = joi.object().keys({
      uniqueID: joi.string().required(),
      identifier: joi.string().required(),
      name: joi.string().required(),
      shortDescription: joi.string().required(),
    }).unknown()
    const schema = joi.object().keys({
      catalogGroupView: joi.array().items(catalogSchema).required(),
    }).unknown()
    validateBody(categories, schema)

    const { identifier, catalogGroupView } = categories

    const parsedCategories = this.parseCategoryWithoutGrouping(
      ctx, tenant, identifier, catalogGroupView,
    )

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: parsedCategories }
  }

  parseCategoryWithoutGrouping (ctx, tenant, _, catalogGroupView) {
    let categories = []

    for (let i = 0; i < catalogGroupView.length; i++) {
      let subcategories = []
      let identifier = catalogGroupView[i].identifier.split('_')

      identifier.pop()
      identifier = identifier.join('_')

      let imageId = `categories/${identifier}?v=${imageVersion}`
      if (/^TH_SALE.*WOMEN$/.test(identifier)) {
        imageId = `categories/TH_WOMEN_SALE?v=${imageVersion}`
      } else if (/^TH_SALE.*MEN$/.test(identifier)) {
        imageId = `categories/TH_MEN_SALE?v=${imageVersion}`
      } else if (/^TH_SALE.*KIDS$/.test(identifier)) {
        imageId = `categories/TH_KIDS_SALE?v=${imageVersion}`
      }

      categories.push({
        id: catalogGroupView[i].uniqueID,
        identifier: identifier,
        imageId: imageId,
        largeImageId: imageId,
        name: catalogGroupView[i].name,
        description: catalogGroupView[i].shortDescription,
        isFeature: !!config.FEATURE_CATEGORY[tenant][identifier],
        categories: [],
      })

      if (catalogGroupView[i].catalogGroupView) {
        subcategories = this.parseCategoryWithoutGrouping(
          ctx, tenant, identifier, catalogGroupView[i].catalogGroupView,
        )
      }

      if (subcategories && subcategories.length > 1) {
        subcategories.unshift(
          this.buildAllCategory(ctx, categories[categories.length - 1].id),
        )
      }

      categories[categories.length - 1].categories = subcategories
    }
    return categories
  }

  parseCategory (
    ctx, tenant, parentIdentifier,
    catalogGroupView, isGrouping, parentFHIdentifier,
  ) {
    let categories = []
    let result = []
    let categoryGroups = {}

    // loop though all the categories is the group (heren/clothes/etc.)
    for (let i = 0; i < catalogGroupView.length; i++) {
      let subcategories = []
      let identifier = catalogGroupView[i].identifier.split('_')
      let thisFHIdentifier = catalogGroupView[i].x_fhIdentifier ||
        catalogGroupView[i].fhidentifier

      let fhIdentifier
      if (parentFHIdentifier) {
        fhIdentifier = parentFHIdentifier + ';' + thisFHIdentifier
      } else {
        fhIdentifier = thisFHIdentifier
      }

      if (!fhIdentifier) {
        continue
      }
      fhIdentifier = fhIdentifier.replace(/_/g, '137')

      // build category identifier
      identifier.pop()
      identifier = identifier.join('_')

      // translate name and description if the translations exist
      const name = this.translateCategoryName(ctx.i18n, identifier)
      const desc = this.translateCategoryDescription(ctx.i18n, identifier)

      // categories to be blocked with all subcategories. fe SALES
      if (
        config.BLOCKED_CATEGORY[tenant][identifier] ||
        catalogGroupView[i].x_catalogSelectable === 'false'
      ) {
        continue
      }

      // not show main or sales categories that are not on a list (some
      // features can be one of main)
      if (
        !parentIdentifier &&
        (!config.MAIN_CATEGORY[tenant][identifier] &&
          !config.SALES_CATEGORY[tenant][identifier])
      ) {
        continue
      }

      // disable features that are not on a list
      // don't proceed with the feature categories which are not on the list,
      // and which parent don't have subcategories enabled
      if (
        config.FEATURE_CATEGORY[tenant][parentIdentifier] &&
        !config.FEATURE_CATEGORY[tenant][parentIdentifier].subcategories &&
        !config.FEATURE_CATEGORY[tenant][identifier]
      ) {
        continue
      }

      // avoid adding the disabled categories into the grouping (f.e. clothes)
      if (!isGrouping || !config.DISABLE_CATEGORY[tenant][identifier]) {
        let imageId = `categories/${identifier}?v=${imageVersion}`
        if (/^TH_SALE.*WOMEN$/.test(identifier)) {
          imageId = `categories/TH_WOMEN_SALE?v=${imageVersion}`
        } else if (/^TH_SALE.*MEN$/.test(identifier)) {
          imageId = `categories/TH_MEN_SALE?v=${imageVersion}`
        } else if (/^TH_SALE.*KIDS$/.test(identifier)) {
          imageId = `categories/TH_KIDS_SALE?v=${imageVersion}`
        }
        categories.push({
          id: catalogGroupView[i].uniqueID,
          identifier: identifier,
          fhIdentifier: fhIdentifier,
          imageId: imageId,
          largeImageId: imageId,
          name: name || catalogGroupView[i].name,
          description: desc || catalogGroupView[i].shortDescription,
          isFeature: !!config.FEATURE_CATEGORY[tenant][identifier],
          categories: [],
        })
      }

      if (catalogGroupView[i].catalogGroupView) {
        subcategories = this.parseCategory(
          ctx, tenant, identifier,
          catalogGroupView[i].catalogGroupView, isGrouping, fhIdentifier,
        )
      }

      // put the subcategories of disabled category or feature category on the
      // same level and subcategories are not specified
      if (
        isGrouping &&
        (config.DISABLE_CATEGORY[tenant][identifier] ||
          (config.FEATURE_CATEGORY[tenant][identifier] &&
            !config.FEATURE_CATEGORY[tenant][identifier].subcategories)
        )
      ) {
        categories = categories.concat(subcategories)
      } else {
        // add all subcategories
        if (!config.MAIN_CATEGORY[tenant][identifier] &&
          subcategories && subcategories.length > 1) {
          const allCategory = this.buildAllCategory(
            ctx, categories[categories.length - 1].id,
          )
          allCategory.fhIdentifier = fhIdentifier
          subcategories.unshift(allCategory)
        }

        categories[categories.length - 1].categories = subcategories
      }
    }

    if (!isGrouping) {
      return categories
    }

    // custom category grouping tops/bottom/etc.
    for (let i = 0; i < categories.length; i++) {
      let group = config.GROUP_CATEGORY[tenant][categories[i].identifier]
      if (group) {
        if (!categoryGroups[group]) {
          categoryGroups[group] = []
        }
        categoryGroups[group].push(categories[i])
      } else {
        result.push(categories[i])
      }
    }

    for (let i in categoryGroups) {
      let allCategory
      if (categoryGroups[i].length > 1) {
        allCategory = this.buildAllCategory(ctx, categoryGroups[i])
        categoryGroups[i].unshift(allCategory)
      }

      let imageId = `categories/${parentIdentifier}_${i}?v=${imageVersion}`
      if (/^TH_SALE.*WOMEN$/.test(parentIdentifier)) {
        imageId = `categories/TH_WOMEN_SALE_${i}?v=${imageVersion}`
      } else if (/^TH_SALE.*MEN$/.test(parentIdentifier)) {
        imageId = `categories/TH_MEN_SALE_${i}?v=${imageVersion}`
      } else if (/^TH_SALE.*KIDS$/.test(parentIdentifier)) {
        imageId = `categories/TH_KIDS_SALE_${i}?v=${imageVersion}`
      }

      result.push({
        id: i,
        fhIdentifier: (allCategory && allCategory.fhIdentifier).replace(/_/g, '137'),
        name: ctx.i18n.__(`locales.${parentIdentifier.toLowerCase()}_${i}`),
        description: i,
        imageId: imageId,
        largeImageId: imageId,
        isGrouped: true,
        categories: categoryGroups[i],
      })
    }

    return result
  }

  buildAllCategory (ctx, categories) {
    let ids = ''; let fhIdentifiers = ''

    if (Array.isArray(categories)) {
      for (let i = 0; i < categories.length; i++) {
        ids += categories[i].id + ','
        let fhID = ''
        if (i === 0) {
          fhID = categories[i].fhIdentifier
        } else {
          const splitFHIds = categories[i].fhIdentifier.split(';')
          fhID = splitFHIds[splitFHIds.length - 1]
        }
        fhIdentifiers += fhID + ','
      }
      ids = ids.slice(0, -1)
      fhIdentifiers = fhIdentifiers.slice(0, -1)
    } else {
      ids = categories
    }

    return {
      id: ids,
      fhIdentifier: fhIdentifiers.replace(/_/g, '137'),
      name: ctx.i18n.__('locales.all'),
      description: ctx.i18n.__('locales.all'),
      imageId: '',
      largeImageId: '',
      categories: [],
      isFeature: !!config.FEATURE_CATEGORY[ids],
    }
  }

  translateCategoryName (i18n, identifier) {
    const localeLookup = `locales.${identifier.toLowerCase()}_name`
    const translatedCategory = i18n.__(localeLookup)
    if (translatedCategory !== localeLookup) {
      return translatedCategory
    }
    return null
  }

  translateCategoryDescription (i18n, identifier) {
    const localeLookup = `locales.${identifier.toLowerCase()}_description`
    const translatedCategory = i18n.__(localeLookup)
    if (translatedCategory !== localeLookup) {
      return translatedCategory
    }
    return null
  }
}

module.exports = Categories
