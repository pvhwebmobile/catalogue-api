/* global jest */

const successfulCallback = (ctx, next) => {
  ctx.status = 200
  ctx.type = 'JSON'
  ctx.body = { success: true, data: {} }
  if (next) next()
}

const mockGetCountriesList = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

const mockGetLanguagesByCountry = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

const mockPingWCSConnection = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

module.exports = {
  mockGetCountriesList,
  mockGetLanguagesByCountry,
  mockPingWCSConnection,
}
