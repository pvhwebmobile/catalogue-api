/**
 * This is a simple mock for productsController that
 * allows spying on functions for products API
 */

/* global jest */

const {
  mockGetProductById,
  mockGetSuggestionsByTerm,
  mockGetProductsByTerm,
  mockGetProductsByCategoryId,
  mockGetInventoryBySkus,
} = require('productsControllerFunctions')

const mock = jest.fn().mockImplementation(() => {
  return {
    getProductById: mockGetProductById,
    getSuggestionsByTerm: mockGetSuggestionsByTerm,
    getProductsByTerm: mockGetProductsByTerm,
    getProductsByCategoryId: mockGetProductsByCategoryId,
    getInventoryBySkus: mockGetInventoryBySkus,
  }
})

module.exports = mock
