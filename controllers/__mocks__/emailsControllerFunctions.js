/* global jest */

const successfulCallback = (ctx, next) => {
  ctx.status = 200
  ctx.type = 'JSON'
  ctx.body = { success: true, data: {} }
  if (next) next()
}

const mockSendWishlistEmail = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

module.exports = {
  mockSendWishlistEmail,
}
