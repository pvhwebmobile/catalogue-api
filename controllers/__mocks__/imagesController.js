/**
 * This is a simple mock for imagesController that
 * allows spying on functions for images API
 */

/* global jest */

const {
  mockGetSplashScreenImagesList,
  mockGetImageById,
} = require('./imagesControllerFunctions')

const mock = jest.fn().mockImplementation(() => {
  return {
    getSplashScreenImagesList: mockGetSplashScreenImagesList,
    getImageById: mockGetImageById,
  }
})

module.exports = mock
