/**
 * This is a simple mock for countriesController that
 * allows spying on functions for countries API
 */

/* global jest */

const {
  mockGetCountriesList,
  mockGetLanguagesByCountry,
  mockPingWCSConnection,
} = require('./countriesControllerFunctions')

const mock = jest.fn().mockImplementation(() => ({
  getCountriesList: mockGetCountriesList,
  getLanguagesByCountry: mockGetLanguagesByCountry,
  pingWCSConnection: mockPingWCSConnection,
}))

module.exports = mock
