/**
 * This is a simple mock for emailsController that
 * allows spying on functions for email API
 */

/* global jest */

const {
  mockSendWishlistEmail,
} = require('./emailsControllerFunctions')

const mock = jest.fn().mockImplementation(() => ({
  sendWishlistEmail: mockSendWishlistEmail,
}))

module.exports = mock
