/* global jest */

const mockGetStoreList = jest.fn((location, isFile) => {
  return {
    DE: [
      {
        storeId: 'A233',
        city: 'DUESSELDORF',
        name: 'TH DUESSELDORF',
      },
      {
        storeId: 'A234',
        city: 'BERLIN',
        name: 'TH BERLIN',
      },
    ],
  }
})

const mockGetStores = jest.fn((tenant, countryCode) => {
  if (countryCode.toLowerCase() === 'de') {
    return [
      {
        storeId: 'A233',
        city: 'DUESSELDORF',
        name: 'TH DUESSELDORF',
        payment: 'gk',
      },
      {
        storeId: 'A234',
        city: 'BERLIN',
        name: 'TH BERLIN',
      },
    ]
  }
  return undefined
})

const mockGetStore = jest.fn((tenant, countryCode, storeId) => {
  if (storeId === 'A233') {
    return {
      storeId: 'A233',
      city: 'DUESSELDORF',
      name: 'TH DUESSELDORF',
      payment: 'gk',
    }
  }
  return undefined
})

const mockCreateOrUpdateStore = jest.fn((store) => {
  return store
})

const mockPatchStore = jest.fn((tenant, countryCode, storeId, store) => {
  if (storeId === 'A233') {
    return {
      storeId: 'A233',
      tenant: 'th',
      countryCode: 'de',
      payment: 'gk',
      ...store,
    }
  }
  return undefined
})

const mockDeleteStores = jest.fn((conditions) => {
  return 1
})

module.exports = {
  mockGetStoreList,
  mockGetStores,
  mockGetStore,
  mockCreateOrUpdateStore,
  mockPatchStore,
  mockDeleteStores,
}
