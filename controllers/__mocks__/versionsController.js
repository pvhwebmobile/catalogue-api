const {
  mockGetVersion,
  mockCheckForUpdate,
} = require('./versionsControllerFunctions')

const mock = {
  getVersion: mockGetVersion,
  checkForUpdate: mockCheckForUpdate,
}

module.exports = mock
