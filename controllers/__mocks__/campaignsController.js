/**
 * This is a simple mock for campaignsController
 */


const {
  mockGetCampaignLabel,
} = require('./campaignsControllerFunctions')

module.exports = {
  getCampaignLabel: mockGetCampaignLabel,
}
