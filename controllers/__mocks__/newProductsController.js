/**
 * This is a simple mock for newProductsController that
 * allows spying on functions for stores API
 */

const {
  mockGetSuggestionsByTerm,
  mockGetProductsByTerm,
  mockGetProductsByCategoryId,
  mockGetProductById,
} = require('./newProductsControllerFunctions')

const mock = (_) => ({
  getSuggestionsByTerm: mockGetSuggestionsByTerm,
  getProductsByTerm: mockGetProductsByTerm,
  getProductsByCategoryId: mockGetProductsByCategoryId,
  getProductById: mockGetProductById,
})

module.exports = (globals) => mock(globals)
