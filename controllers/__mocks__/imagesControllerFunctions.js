/* global jest */

const successfulCallback = (ctx, next) => {
  ctx.status = 200
  ctx.type = 'JSON'
  ctx.body = { success: true, data: {} }
  next()
}

const mockGetSplashScreenImagesList = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

const mockGetImageById = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

module.exports = {
  mockGetSplashScreenImagesList,
  mockGetImageById,
}
