/**
 * This is a simple mock for storesController that
 * allows spying on functions for stores API
 */

const {
  mockUploadStoreLocatorFile,
  mockUploadStoreLocatorJSON,
} = require('./uploadsControllerFunctions')

const mock = {
  uploadStoreLocatorFile: mockUploadStoreLocatorFile,
  uploadStoreLocatorJSON: mockUploadStoreLocatorJSON,
}

module.exports = mock
