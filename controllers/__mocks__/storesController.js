/**
 * This is a simple mock for storesController that
 * allows spying on functions for stores API
 */

const {
  mockGetStoreList,
  mockGetStores,
  mockGetStore,
  mockUpdateStore,
  mockCreateOrUpdateStore,
  mockPatchStore,
  mockDeleteStores,
} = require('./storesControllerFunctions')

const mock = {
  getStoreList: mockGetStoreList,
  getStores: mockGetStores,
  getStore: mockGetStore,
  updateStore: mockUpdateStore,
  createOrUpdateStore: mockCreateOrUpdateStore,
  patchStore: mockPatchStore,
  deleteStores: mockDeleteStores,
}

module.exports = mock
