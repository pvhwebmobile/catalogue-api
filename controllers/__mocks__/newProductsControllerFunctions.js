/* global jest */

const mockGetSuggestionsByTerm = jest.fn((term) => {
  return [
    { term: 'ABC', frequency: 123 },
  ]
})

const mockGetProductsByTerm = jest.fn(
  (term, page, facets, categoryId) => {
    return [
      {
        id: '369370',
        partNumber: 'AM0AM03974901',
        isStyleColor: false,
        styleId: undefined,
        name: 'Beanie mit Rugby-Streifen',
        price: 39.9,
        currency: 'EUR',
        mainImage: 'http://tommy-europe.scene7.com/is/image/TommyEurope/AM0AM03974_901_main',
        colorsAmount: 2,
        label: 'TOMMY JEANS',
        colorId: 'PRODUCT_ATTR_COLOUR_CORPORATE',
      },
    ]
  })

const mockGetProductsByCategoryId = jest.fn(
  (categoryId, page, facets) => {
    return [
      {
        id: '369370',
        partNumber: 'AM0AM03974901',
        isStyleColor: false,
        styleId: undefined,
        name: 'Beanie mit Rugby-Streifen',
        price: 39.9,
        currency: 'EUR',
        mainImage: 'http://tommy-europe.scene7.com/is/image/TommyEurope/AM0AM03974_901_main',
        colorsAmount: 2,
        label: 'TOMMY JEANS',
        colorId: 'PRODUCT_ATTR_COLOUR_CORPORATE',
      },
    ]
  })

const mockGetProductById = jest.fn((productId) => {
  if (!productId) {
    return
  }
  return {
    'id': '267264',
    'name': 'T-Shirt mit V-Ausschnitt',
    'description': 'Der V-Ausschnitt dieses leichten Jersey-T-Shirts verleiht einem zeitlosen Design selbstbewussten, urbanen Flair.<BR><BR>Produktdetails<BR><BR>• Reine Baumwolle<BR>• V-Ausschnitt<BR>• Ausschnitt mit aufgesetzten Tommy-Streifen an der Innenseite<BR>• aufgestickte Tommy Hilfiger Flag auf der Brust<BR><BR>Passform und Schnitt<BR><BR>• Regular Fit<BR>• Das Model ist 1,86 m groß und trägt Größe M.<BR><BR>Material und Pflege<BR><BR>• 100 % Baumwolle',
    'price': 24.9,
    'currency': 'EUR',
    'colors': [
      {
        'images': [
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_002_main?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_002_alternate1?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_002_alternate2?$main$',
        ],
        'pattern': 'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_002_main?$minithumb$',
        'sizes': [
          {
            'id': '267266',
            'skuId': '8719701100484',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 28,
              'storeInventory': 0,
            },
            'PRODUCT_ATTR_COLOUR': 'BLACK IRIS',
            'Size': 'L',
          },
          {
            'id': '267267',
            'skuId': '8719701100613',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 43,
              'storeInventory': 5,
            },
            'PRODUCT_ATTR_COLOUR': 'BLACK IRIS',
            'Size': 'M',
          },
          {
            'id': '267268',
            'skuId': '8719701100651',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 11,
              'storeInventory': 3,
            },
            'PRODUCT_ATTR_COLOUR': 'BLACK IRIS',
            'Size': 'S',
          },
          {
            'id': '267269',
            'skuId': '8719701100668',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 26,
              'storeInventory': 1,
            },
            'PRODUCT_ATTR_COLOUR': 'BLACK IRIS',
            'Size': 'XL',
          },
          {
            'id': '267270',
            'skuId': '8719701100521',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 4,
              'storeInventory': 2,
            },
            'PRODUCT_ATTR_COLOUR': 'BLACK IRIS',
            'Size': 'XS',
          },
          {
            'id': '267271',
            'skuId': '8719701100491',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 13,
              'storeInventory': 0,
            },
            'PRODUCT_ATTR_COLOUR': 'BLACK IRIS',
            'Size': 'XXL',
          },
        ],
        'url': 'https://de.b2ceuup.tommy.com/t-shirt-mit-v-ausschnitt-dm0dm04410002',
        'name': 'BLACK IRIS',
      },
      {
        'images': [
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_038_main?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_038_alternate1?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_038_alternate2?$main$',
        ],
        'pattern': 'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_038_main?$minithumb$',
        'sizes': [
          {
            'id': '267535',
            'skuId': '8719701100453',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 15,
              'storeInventory': 4,
            },
            'PRODUCT_ATTR_COLOUR': 'LT GREY HTR',
            'Size': 'S',
          },
          {
            'id': '267536',
            'skuId': '8719701100583',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 26,
              'storeInventory': 1,
            },
            'PRODUCT_ATTR_COLOUR': 'LT GREY HTR',
            'Size': 'M',
          },
          {
            'id': '267537',
            'skuId': '8719701100620',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 24,
              'storeInventory': 0,
            },
            'PRODUCT_ATTR_COLOUR': 'LT GREY HTR',
            'Size': 'L',
          },
          {
            'id': '267538',
            'skuId': '8719701100460',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 12,
              'storeInventory': 1,
            },
            'PRODUCT_ATTR_COLOUR': 'LT GREY HTR',
            'Size': 'XXL',
          },
          {
            'id': '267539',
            'skuId': '8719701100538',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 7,
              'storeInventory': 4,
            },
            'PRODUCT_ATTR_COLOUR': 'LT GREY HTR',
            'Size': 'XS',
          },
          {
            'id': '267540',
            'skuId': '8719701100644',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 14,
              'storeInventory': 1,
            },
            'Size': 'XL',
            'PRODUCT_ATTR_COLOUR': 'LT GREY HTR',
          },
        ],
        'url': 'https://de.b2ceuup.tommy.com/t-shirt-mit-v-ausschnitt-dm0dm04410038',
        'name': 'LT GREY HTR',
      },
      {
        'images': [
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_078_main?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_078_alternate1?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_078_alternate2?$main$',
        ],
        'pattern': 'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_078_main?$minithumb$',
        'sizes': [
          {
            'id': '267888',
            'skuId': '8719701100637',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 20,
              'storeInventory': 5,
            },
            'PRODUCT_ATTR_COLOUR': 'TOMMY BLACK',
            'Size': 'L',
          },
          {
            'id': '267889',
            'skuId': '8719701100545',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 11,
              'storeInventory': 1,
            },
            'Size': 'M',
            'PRODUCT_ATTR_COLOUR': 'TOMMY BLACK',
          },
          {
            'id': '267890',
            'skuId': '8719701100590',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 12,
              'storeInventory': 0,
            },
            'PRODUCT_ATTR_COLOUR': 'TOMMY BLACK',
            'Size': 'S',
          },
          {
            'id': '267891',
            'skuId': '8719701100552',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 41,
              'storeInventory': 4,
            },
            'PRODUCT_ATTR_COLOUR': 'TOMMY BLACK',
            'Size': 'XL',
          },
          {
            'id': '267892',
            'skuId': '8719701100569',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 11,
              'storeInventory': 0,
            },
            'PRODUCT_ATTR_COLOUR': 'TOMMY BLACK',
            'Size': 'XS',
          },
          {
            'id': '267893',
            'skuId': '8719701100675',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 18,
              'storeInventory': 4,
            },
            'PRODUCT_ATTR_COLOUR': 'TOMMY BLACK',
            'Size': 'XXL',
          },
        ],
        'url': 'https://de.b2ceuup.tommy.com/t-shirt-mit-v-ausschnitt-dm0dm04410078',
        'name': 'TOMMY BLACK',
      },
      {
        'images': [
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_100_main?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_100_alternate1?$main$',
          'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_100_alternate2?$main$',
        ],
        'pattern': 'http://tommy-europe.scene7.com/is/image/TommyEurope/DM0DM04410_100_main?$minithumb$',
        'sizes': [
          {
            'id': '268145',
            'skuId': '8719701100507',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 26,
              'storeInventory': 1,
            },
            'PRODUCT_ATTR_COLOUR': 'CLASSIC WHITE',
            'Size': 'M',
          },
          {
            'id': '268146',
            'skuId': '8719701100477',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 27,
              'storeInventory': 3,
            },
            'Size': 'S',
            'PRODUCT_ATTR_COLOUR': 'CLASSIC WHITE',
          },
          {
            'id': '268147',
            'skuId': '8719701100682',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 62,
              'storeInventory': 0,
            },
            'PRODUCT_ATTR_COLOUR': 'CLASSIC WHITE',
            'Size': 'XL',
          },
          {
            'id': '268148',
            'skuId': '8719701100514',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 9,
              'storeInventory': 5,
            },
            'PRODUCT_ATTR_COLOUR': 'CLASSIC WHITE',
            'Size': 'XS',
          },
          {
            'id': '268149',
            'skuId': '8719701100606',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 29,
              'storeInventory': 4,
            },
            'PRODUCT_ATTR_COLOUR': 'CLASSIC WHITE',
            'Size': 'XXL',
          },
          {
            'id': '268150',
            'skuId': '8719701100576',
            'buyable': 'true',
            'price': 24.9,
            'currency': 'EUR',
            'inventory': {
              'onlineInventory': 35,
              'storeInventory': 5,
            },
            'PRODUCT_ATTR_COLOUR': 'CLASSIC WHITE',
            'Size': 'L',
          },
        ],
        'url': 'https://de.b2ceuup.tommy.com/t-shirt-mit-v-ausschnitt-dm0dm04410100',
        'name': 'CLASSIC WHITE',
      },
    ],
    'label': 'TOMMY JEANS',
    'url': 'https://de.b2ceuup.tommy.com/t-shirt-mit-v-ausschnitt-dm0dm04410',
  }
})

module.exports = {
  mockGetSuggestionsByTerm,
  mockGetProductsByTerm,
  mockGetProductsByCategoryId,
  mockGetProductById,
}
