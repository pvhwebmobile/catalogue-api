/* globals jest */
const fs = jest.genMockFromModule('fs')

let mockFiles = Object.create(null)

const __setMockFiles = (newMockFiles) => {
  mockFiles = Object.create(null)
  for (let file in newMockFiles) {
    if (!mockFiles[file]) {
      mockFiles[file] = newMockFiles[file]
    }
  }
}

const readFileSync = async (filePath) => {
  return mockFiles[filePath] || undefined
}

fs.__setMockFiles = __setMockFiles
fs.readFileSync = readFileSync

module.exports = fs
