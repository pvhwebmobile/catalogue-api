/* globals jest */

const mockUploadStoreLocatorFile = jest.fn(async (file) => {
  if (!file) return false
  return { upserted: 5, deleted: 1, invalid: 5 }
})

const mockUploadStoreLocatorJSON = jest.fn(async (json) => {
  if (!json || !json.eCom_StoreFeed_EIS) return false
  return { upserted: 5, deleted: 1, invalid: 5 }
})

module.exports = {
  mockUploadStoreLocatorFile,
  mockUploadStoreLocatorJSON,
}
