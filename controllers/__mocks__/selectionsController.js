/**
 * This is a simple mock for selectionsController that
 * allows spying on functions for selections API
 */

/* global jest */

const {
  mockCreateSelection,
  mockUpdateSelection,
  mockUpdateSelectionPersonalDetails,
  mockGetDeliveryMethodsBySelectionId,
  mockGetValidationRules,
  mockGetValidationZipCode,
  mockGetSelectionsListByStatusAndSiteId,
  mockGetSelectionByOrderId,
  mockUpdateSelectionStatusByOrderId,
  mockGetConnectivityWcs,
} = require('selectionsControllerFunctions')

const mock = jest.fn().mockImplementation(() => {
  return {
    createSelection: mockCreateSelection,
    updateSelection: mockUpdateSelection,
    updateSelectionPersonalDetails: mockUpdateSelectionPersonalDetails,
    getDeliveryMethodsBySelectionId: mockGetDeliveryMethodsBySelectionId,
    getValidationRules: mockGetValidationRules,
    getValidationZipCode: mockGetValidationZipCode,
    getSelectionsListByStatusAndSiteId: mockGetSelectionsListByStatusAndSiteId,
    getSelectionByOrderId: mockGetSelectionByOrderId,
    updateSelectionStatusByOrderId: mockUpdateSelectionStatusByOrderId,
    getConnectivityWcs: mockGetConnectivityWcs,
  }
})

module.exports = mock
