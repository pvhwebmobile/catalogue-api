/* global jest */

const mockGetVersion = jest.fn((appName) => {
  return {
    version: '1.2.3',
    updateForced: true,
    updateLink: `apps.pvh.com/${appName}/1.2.3`,
  }
})

const mockCheckForUpdate = jest.fn((appName, _) => {
  return {
    version: '1.2.3',
    updateAvailable: true,
    updateForced: true,
    updateLink: `apps.pvh.com/${appName}/1.2.3`,
  }
})

module.exports = {
  mockGetVersion,
  mockCheckForUpdate,
}
