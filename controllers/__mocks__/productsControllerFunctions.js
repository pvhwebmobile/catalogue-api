/* global jest */

const successfulCallback = (ctx, next) => {
  ctx.status = 200
  ctx.type = 'JSON'
  ctx.body = { success: true, data: {} }
  next()
}

const mockGetProductById = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

const mockGetSuggestionsByTerm = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

const mockGetProductsByTerm = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

const mockGetProductsByCategoryId = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

const mockGetInventoryBySkus = jest.fn((ctx, next) => {
  successfulCallback(ctx, next)
})

module.exports = {
  mockGetProductById,
  mockGetSuggestionsByTerm,
  mockGetProductsByTerm,
  mockGetProductsByCategoryId,
  mockGetInventoryBySkus,
}
