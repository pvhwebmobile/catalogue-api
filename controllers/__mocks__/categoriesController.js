/**
 * This is a simple mock for categoriesController that
 * allows spying on functions for categories API
 */

/* global jest */

const {
  mockGetTopCategoriesByStoreId,
  mockGetCategoriesForPOC,
} = require('./categoriesControllerFunctions')

const mock = jest.fn().mockImplementation(() => ({
  getTopCategoriesByStoreId: mockGetTopCategoriesByStoreId,
  getCategoriesForPOC: mockGetCategoriesForPOC,
}))

module.exports = mock
