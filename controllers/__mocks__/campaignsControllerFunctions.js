/* global jest */

const mockGetCampaignLabel = jest.fn((tenant, countryCode, identifier) => {
  if (identifier === 'black20friday') {
    return {
      _id: '5dceafb72d04450013cd03ba',
      tenant,
      countryCode,
      priority: 1,
      name: 'Black Friday',
      identifier: 'black20friday',
      label: {
        name: 'BLACK FRIDAY',
        backgroundColor: '#000A22',
        fontColor: '#FFFFFF',
        description: 'This product is part of the Black Friday sales! Use promocode <b>BF20</b> in the checkout.',
      },
      banner: {
        title: "It's Black Friday!",
        description: 'Get -20% off of selected items using <b>BF20</b> promocode.',
        fontColor: '#FFFFFF',
        backgroundColor: '#000A22',
      },
      active: true,
    }
  }
  return undefined
})

module.exports = {
  mockGetCampaignLabel,
}
