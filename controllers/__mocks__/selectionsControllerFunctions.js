/**
 * This file is used because we are not supporting ES6 mocks at
 * the moment and I want to spy on these functions using jest
 * in our API unit tests :) - Collin
 */

/* global jest */

const successfulCall = (ctx, next) => {
  ctx.status = 200
  ctx.type = 'JSON'
  ctx.body = { success: true, data: {} }
  next()
}

const mockCreateSelection = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockUpdateSelection = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockUpdateSelectionPersonalDetails = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockGetDeliveryMethodsBySelectionId = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockGetValidationRules = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockGetValidationZipCode = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockGetSelectionsListByStatusAndSiteId = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockGetSelectionByOrderId = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockUpdateSelectionStatusByOrderId = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

const mockGetConnectivityWcs = jest.fn((ctx, next) => {
  successfulCall(ctx, next)
})

module.exports = {
  mockCreateSelection,
  mockUpdateSelection,
  mockUpdateSelectionPersonalDetails,
  mockGetDeliveryMethodsBySelectionId,
  mockGetValidationRules,
  mockGetValidationZipCode,
  mockGetSelectionsListByStatusAndSiteId,
  mockGetSelectionByOrderId,
  mockUpdateSelectionStatusByOrderId,
  mockGetConnectivityWcs,
}
