const fs = require('fs')

const joi = require('@hapi/joi')

const { createOrUpdateStore, deleteStores } = require('./storesController')
const { validateBody } = require('../libs/bodyValidator')
const { countryCodeFromName } = require('../libs/countryUtils')
const { CustomError } = require('../libs/errors')
const { formatOpeningHours } = require('../libs/sharepoint')

const parseJSONFile = async (filePath) => {
  const buffer = await fs.readFileSync(
    filePath,
    { encoding: 'utf8', autoClose: true },
  )
  const json = await JSON.parse(buffer.toString())
  return json
}

const parseTenantFromStoreName = (storeName) => {
  const tommyRegex = /^tommy.*$/i
  const calvinRegex = /^calvin.*$/i

  if (tommyRegex.test(storeName)) {
    return 'th'
  } else if (calvinRegex.test(storeName)) {
    return 'ck'
  }

  return undefined
}

const uploadStoreLocatorFile = async (file) => {
  if (!file.path) return undefined
  const json = await parseJSONFile(file.path)
  return uploadStoreLocatorJSON(json)
}

const uploadStoreLocatorJSON = async (json) => {
  const storeSchema = joi.object().keys({
    ecomName: joi.string().allow('').required(),
    address1: joi.string().allow('').required(),
    address2: joi.string().allow('').optional(),
    address3: joi.string().allow('').optional(),
    zipCode: joi.string().allow('').required(),
    city: joi.string().allow('').required(),
    subRegion: joi.string().allow('').optional(),
    country: joi.string().allow('').required(),
    phoneNumber: joi.string().allow('').required(),
    openingHours: joi.string().allow('').required(),
    division: joi.string().allow('').optional(),
    flagship: joi.boolean().default(false),
    HilfigerClub: joi.boolean().default(false),
    image: joi.string().allow('').optional(),
    retailID: joi.string().allow('').required(),
    salesType: joi.string().allow('').required(),
    countryCode: joi.string().allow('').required(),
    region: joi.string().allow('').required(),
    longitude: joi.string().allow('').required(),
    latitude: joi.string().allow('').required(),
    sapFMS: joi.string().allow('').required(),
    active: joi.boolean().default(false),
    isEligibleClickCollectEIS: joi.boolean().default(false),
    isEligibleReturnInStoreEIS: joi.boolean().default(false),
    isEligibleReserveInStoreOnline: joi.boolean().default(false),
    isEligibleClickCollectOnline: joi.boolean().default(false),
    isEligibleReturnInStoreOnline: joi.boolean().default(false),
    posCheckout: joi.boolean().default(false),
  })
  const messageSchema = joi.object().keys({
    stores: joi.array().items(storeSchema),
    timestamp: joi.date().iso().required(),
  })

  const schema = joi.object().keys({
    eCom_StoreFeed_EIS: messageSchema,
  })

  if (!validateBody(json, schema)) {
    // invalid schema
    throw new CustomError({ message: 'File failed schema validation.', status: 406 })
  }

  let upserted = 0
  let invalid = 0

  const { stores, timestamp } = json.eCom_StoreFeed_EIS

  for (let store of stores) {
    const {
      city,
      zipCode,
      address1,
      address2,
      address3,
      sapFMS: storeId,
      posCheckout,
      longitude,
      latitude,
      region,
      subRegion,
      country,
      phoneNumber,
      openingHours: openingHoursRaw,
      division,
      flagship,
      HilfigerClub: hilfigerClub,
      retailID: retailId,
      salesType,
      active,
      isEligibleClickCollectEIS,
      isEligibleReturnInStoreEIS,
      isEligibleReserveInStoreOnline,
      isEligibleClickCollectOnline,
      isEligibleReturnInStoreOnline,
    } = store
    const tenant = parseTenantFromStoreName(store.ecomName)
    const payment = posCheckout ? 'gk' : 'ecom'
    const name = `${tenant.toUpperCase()} ${city} ${storeId}`
    const location = {
      type: 'Point',
      coordinates: [ longitude, latitude ],
    }
    const countryCode = countryCodeFromName(country)
    const openingHours = formatOpeningHours(openingHoursRaw)
    const divisions = division.indexOf(', ') ? division.split(', ') : division
    const availableStore = {
      tenant,
      region,
      subRegion,
      country,
      countryCode,
      city,
      zipCode,
      address1,
      address2,
      address3,
      location,
      name,
      storeId,
      phoneNumber,
      openingHours,
      divisions,
      flagship,
      hilfigerClub,
      retailId,
      salesType,
      active,
      payment,
      isEligibleClickCollectEIS,
      isEligibleReturnInStoreEIS,
      isEligibleReserveInStoreOnline,
      isEligibleClickCollectOnline,
      isEligibleReturnInStoreOnline,
      uploadedAt: timestamp,
    }
    try {
      await createOrUpdateStore(storeId, availableStore)
      upserted += 1
    } catch (error) {
      invalid += 1
    }
  }

  // find all stores with undefined uploadedAt or uploadedAt <= timestamp
  let deleted = await deleteStores({
    $or: [
      { uploadedAt: { $lt: new Date(timestamp) } },
      { uploadedAt: { $eq: null } },
    ],
  })

  return { upserted, deleted, invalid }
}

module.exports = {
  parseJSONFile,
  parseTenantFromStoreName,
  uploadStoreLocatorFile,
  uploadStoreLocatorJSON,
}
