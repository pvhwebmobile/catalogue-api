const campaignModel = require('../models/campaigns')
const { CustomError } = require('../libs/errors')

const getCampaigns = (tenant, countryCode, query, sort = { priority: 'asc' }) => {
  return campaignModel.find({ tenant, countryCode, ...query }).sort(sort)
    .catch((err) => {
      throw new CustomError({ message: err.message, status: 500 })
    })
}

const getActiveCampaigns = async (tenant, countryCode) => {
  const activeCampaigns = await getCampaigns(
    tenant, countryCode, { active: true },
  )
  return activeCampaigns
}

const getPrioritizedCampaignBanner = (activeCampaigns) => {
  const prioritizedActiveCampaigns = activeCampaigns.sort(
    (a, b) => a.priority - b.priority
  )
  let banner
  let index = 0
  while (!banner && index < prioritizedActiveCampaigns.length) {
    banner = prioritizedActiveCampaigns[index++].banner
  }

  return banner
}

const getCampaignLabel = async (tenant, countryCode, identifier) => {
  const campaigns = await getCampaigns(tenant, countryCode, { identifier, active: true })
  if (campaigns && campaigns[0]) {
    return campaigns[0].label
  }
}

module.exports = {
  getCampaigns,
  getActiveCampaigns,
  getPrioritizedCampaignBanner,
  getCampaignLabel,
}
