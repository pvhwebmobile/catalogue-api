const fetch = require('node-fetch')
const { parse } = require('plist')

const { CustomError } = require('../libs/errors')
const { versionCheck } = require('../libs/versioning')

const iTommyPListURL = 'https://pvhmobileapps.s3.eu-central-1.amazonaws.com/RELEASE/iTommy-RELEASE/iTommy-RELEASE.ipa.plist'
const iCalvinPListURL = 'https://pvhmobileapps.s3.eu-central-1.amazonaws.com/RELEASE/iCalvin-RELEASE/iCalvin-RELEASE.ipa.plist'

const getRemotePList = async (url) => {
  const xml = await fetch(url)
  const text = await xml.text()
  const plist = await parse(text)
  return plist
}

const getiTommyPList = async () => {
  const plist = await getRemotePList(iTommyPListURL)
  return plist
}

const getiCalvinPList = async () => {
  const plist = await getRemotePList(iCalvinPListURL)
  return plist
}

const getEISPlist = async (tenant) => {
  let plist
  if (tenant === 'th') {
    plist = await getiTommyPList()
  } else if (tenant === 'ck') {
    plist = await getiCalvinPList()
  } else {
    throw new CustomError({ message: 'Tenant not supported', status: 400 })
  }

  const { metadata } = plist.items[0]
  const {
    'update-link': updateLink,
    'update-forced': updateForced,
    'update-at': updateAt,
    'bundle-version': bundleVersion,
  } = metadata

  return {
    version: bundleVersion,
    updateForced,
    updateLink,
    updateAt,
  }
}

const getVersion = async (appName) => {
  switch (appName.toLowerCase()) {
    case 'itommy':
      return getEISPlist('th')
    case 'icalvin':
      return getEISPlist('ck')
    default:
      throw new CustomError({ message: 'No app found.', status: 400 })
  }
}

const checkForEISUpdate = async (tenant, clientVersion) => {
  let plist
  if (tenant === 'th') {
    plist = await getiTommyPList()
  } else if (tenant === 'ck') {
    plist = await getiCalvinPList()
  } else {
    throw new CustomError({ message: 'Tenant not supported', status: 400 })
  }

  const { metadata } = plist.items[0]
  const {
    'update-link': updateLink,
    'update-forced': updateForced,
    'update-at': updateAt,
    'bundle-version': bundleVersion,
  } = metadata

  let updateAvailable = false
  if (Date.now() >= new Date(updateAt).getTime()) {
    updateAvailable = !versionCheck(clientVersion, [`>=${bundleVersion}`])
  }
  return {
    version: bundleVersion,
    updateAvailable,
    updateForced,
    updateLink,
  }
}

const checkForUpdate = async (appName, clientVersion) => {
  switch (appName.toLowerCase()) {
    case 'itommy':
      return checkForEISUpdate('th', clientVersion)
    case 'icalvin':
      return checkForEISUpdate('ck', clientVersion)
    default:
      throw new CustomError({ message: 'No app found.', status: 400 })
  }
}

module.exports = {
  getRemotePList,
  getiTommyPList,
  getiCalvinPList,
  getEISPlist,
  getVersion,
  checkForEISUpdate,
  checkForUpdate,
}
