const fetch = require('node-fetch')
const https = require('https')
const config = require('config')

const { getCountryProperties } = require('../libs/countryConfig')
const { CustomError } = require('../libs/errors')
const { buildURL } = require('../libs/url')
const SelectionModel = require('../models/selection')

const customProxy = new https.Agent({
  rejectUnauthorized: false,
})

const applyPromotionCode =
  async (selectionId, promotionCode, tenant, countryCode, langId) => {
    return modifyPromotionCode(selectionId, tenant, countryCode, langId, promotionCode)
  }

const removePromotionCode =
  async (selectionId, tenant, countryCode, langId) => {
    return modifyPromotionCode(selectionId, tenant, countryCode, langId)
  }

/**
 * Applies/removes a promo code based on the presence of promotionCode
 * If promotionCode is not present; it is removed
 * if promotionCode is present; it is applied
 * @param {String} selectionId ObjectID of selection in mongodb
 * @param {String} tenant 2-letter tenant code; ck or th
 * @param {String} countryCode 2-letter country code; DE
 * @param {String} langId Represents language client requests
 * @param {String} promotionCode Promo code supplied by user
 */
const modifyPromotionCode =
  async (selectionId, tenant, countryCode, langId, promotionCode) => {
    const selection = await getSelection({ _id: selectionId })
    const { orderId, promotions, userPromotionCode, WCToken, WCTrustedToken } = selection

    let applying = false
    if (promotionCode !== undefined) {
      applying = true
    }

    if (applying && userPromotionCode) {
      throw new CustomError({
        message: 'Order already has promotion',
        status: 200,
        errorCode: '-2',
      })
    }

    const {
      storeId,
      langId: defaultLangId,
      catalogId,
    } = getCountryProperties(
      tenant,
      countryCode,
      ['storeId', 'langId', 'catalogId'],
    )

    const url = buildURL(
      config.URLS.WCS_POST_PROMOTION_CODE, {}, tenant, countryCode,
    )
    const body = {
      storeId,
      langId: langId || defaultLangId,
      catalogId,
      orderId,
      promotionCode: applying ? promotionCode : userPromotionCode,
      taskType: applying ? 'A' : 'R',
    }

    const options = {
      method: 'POST',
      agent: customProxy,
      headers: {
        WCToken,
        WCTrustedToken,
      },
      body: JSON.stringify(body),
    }

    const response = await fetch(url, options)

    if (!response.ok) {
      throw new CustomError({
        message: 'Failed to update promotion code in WCS.',
        status: 200,
        errorCode: '-1',
      })
    }

    const cartDetails = await response.json()

    const { hasPromotion, promotions: wcsPromotions } = getPromotionsFromWCSBasket(cartDetails)
    if (applying) {
      if (!hasPromotion) {
        throw new CustomError({
          message: 'Failed to update promotion code in WCS.',
          status: 200,
          errorCode: '-1',
        })
      }
      if (
        wcsPromotions &&
        wcsPromotions.length > ((promotions && promotions.length) || 0)
      ) {
        const oldPromotionCodes = (promotions && promotions.map(({ code }) => code)) || []
        for (let i = 0; i < wcsPromotions.length; i += 1) {
          const { code } = wcsPromotions[i]
          if (oldPromotionCodes.indexOf(code) === -1) {
            wcsPromotions[i].userPromotionCode = promotionCode
          }
        }
      }
    }

    const updatedSelection = await wcsCartDetailsToSelection(
      cartDetails,
      hasPromotion,
      wcsPromotions,
      promotionCode,
    )

    return updatedSelection
  }

const getSelection = async (query) => {
  const selection = await SelectionModel.findOne(query)
    .catch((err) => {
      throw new CustomError({ message: err.message, code: err.name })
    })

  return selection
}

const updateSelection = async (query, update) => {
  const selection = await SelectionModel.findOneAndUpdate(
    query, update, { new: true }
  ).catch((err) => {
    throw new CustomError({ message: err.message, code: err.name })
  })

  return selection
}

const getWCSCartDetails =
  async (tenant, countryCode, WCToken, WCTrustedToken) => {
    const url = buildURL(config.URLS.WCS_GET_BASKET_DETAILS, {}, tenant, countryCode)
    const options = {
      headers: {
        WCToken,
        WCTrustedToken,
      },
      agent: customProxy,
    }

    const response = await fetch(url, options)

    if (!response.ok) {
      throw new CustomError({ message: response.body, code: response.status })
    }

    const cartDetails = await response.json()
    return cartDetails
  }

const wcsCartDetailsToSelection = async (
  cartDetails,
  hasPromotion,
  promotions,
  userPromotionCode
) => {
  const selection = await SelectionModel.findOne({
    orderId: cartDetails.orderId,
  })

  const { orderItem: orderItems } = cartDetails
  const selectionProducts = matchOrderPriceAndInventory(
    selection.products, orderItems,
  )

  let shippingModeId
  if (orderItems && orderItems.length > 0) {
    shippingModeId = orderItems[0].shipModeId
  }

  let shippingPrice = parseFloat(cartDetails.totalShippingCharge)
  if (cartDetails.adjustment && cartDetails.adjustment.length > 0) {
    for (let adj of cartDetails.adjustment) {
      if (adj.xadju_calUsageId === '-7') {
        const amount = parseFloat(adj.amount)
        shippingPrice += amount
      }
    }
  }
  const update = {
    shippingPrice: shippingPrice.toFixed(2),
    totalPrice: cartDetails.grandTotal * 1,
    currency: cartDetails.grandTotalCurrency,
    products: selectionProducts,
    hasPromotion,
    promotions,
    userPromotionCode,
    shippingModeId,
  }

  const newSelection = await updateSelection(
    { orderId: cartDetails.orderId }, update
  )

  return newSelection
}

const matchOrderPriceAndInventory = (
  selectionProducts, orderProducts
) => {
  const useSku = selectionProducts.every(({ skuId }) => skuId !== undefined)
  if (useSku) {
    return matchOrderPriceAndInventoryBySKU(
      selectionProducts, orderProducts
    )
  }
  return matchOrderPriceAndInventoryByCatEntryId(
    selectionProducts, orderProducts
  )
}

const matchOrderPriceAndInventoryBySKU =
  (selectionProducts, orderProducts) => {
    let products = []
    if (orderProducts && selectionProducts) {
      orderProducts.forEach((orderItem) => {
        let orderItemQuantity = Number.parseInt(orderItem.quantity)
        selectionProducts.forEach((selectionItem) => {
          if (orderItem.partNumber === `${selectionItem.skuId}` && orderItemQuantity > 0) {
            let { adjustment, quantity, unitPrice, orderItemPrice } = orderItem

            quantity = Number.parseInt(quantity)
            unitPrice = Number.parseFloat(unitPrice)
            orderItemPrice = Number.parseFloat(orderItemPrice)

            let totalDiscount = 0
            if (adjustment) {
              totalDiscount = adjustment.filter((adj) => adj.usage === 'Discount')
                .map((disc) => Number.parseFloat(disc.amount))
                .reduce((a, c) => a + c, 0)
            }

            const itemDiscount = totalDiscount / quantity

            selectionItem.originalUnitPrice = unitPrice
            selectionItem.promoUnitPrice = unitPrice + itemDiscount
            selectionItem.originalTotalPrice = orderItemPrice
            selectionItem.promoTotalPrice = orderItemPrice + totalDiscount

            products.push(selectionItem)
            orderItemQuantity--
          }
        })
      })
    }

    return products
  }

const matchOrderPriceAndInventoryByCatEntryId =
  (selectionProducts, orderProducts) => {
    let products = []
    if (orderProducts && selectionProducts) {
      orderProducts.forEach((orderItem) => {
        let orderItemQuantity = Number.parseInt(orderItem.quantity)
        selectionProducts.forEach((selectionItem) => {
          if (orderItem.productId === `${selectionItem.id}` && orderItemQuantity > 0) {
            let { adjustment, quantity, unitPrice, orderItemPrice } = orderItem

            quantity = Number.parseInt(quantity)
            unitPrice = Number.parseFloat(unitPrice)
            orderItemPrice = Number.parseFloat(orderItemPrice)

            let totalDiscount = 0
            if (adjustment) {
              totalDiscount = adjustment.filter((adj) => adj.usage === 'Discount')
                .map((disc) => Number.parseFloat(disc.amount))
                .reduce((a, c) => a + c, 0)
            }

            const itemDiscount = totalDiscount / quantity

            selectionItem.originalUnitPrice = unitPrice
            selectionItem.promoUnitPrice = unitPrice + itemDiscount
            selectionItem.originalTotalPrice = orderItemPrice
            selectionItem.promoTotalPrice = orderItemPrice + totalDiscount

            products.push(selectionItem)
            orderItemQuantity--
          }
        })
      })
    }

    return products
  }

const getPromotionsFromWCSBasket = (wcsBasket) => {
  let hasPromotion = false
  let promotions

  const { adjustment } = wcsBasket

  if (!adjustment || adjustment.length === 0) {
    return { hasPromotion: false, promotions: null }
  }

  promotions = adjustment.filter(({ xadju_calUsageId: uid }) => uid === '-1')
    .map(({ code, description, amount, currency, language, descriptionLanguage }) =>
      ({ code, description, amount, currency, language, descriptionLanguage }))

  if (promotions.length > 0) {
    hasPromotion = true
  }

  return { hasPromotion, promotions }
}

module.exports = {
  applyPromotionCode,
  removePromotionCode,
  getSelection,
  updateSelection,
  getWCSCartDetails,
  wcsCartDetailsToSelection,
  matchOrderPriceAndInventory,
  matchOrderPriceAndInventoryBySKU,
  matchOrderPriceAndInventoryByCatEntryId,
  getPromotionsFromWCSBasket,
}
