const storeModel = require('../models/store')
const { CustomError } = require('../libs/errors')
const storeJSONLocation = '../config/stores'

const getStoreList = async (location = storeJSONLocation, isFile = true) => {
  if (isFile) {
    return require(location)
  }
  return {}
}

const getStoreModels = async (query) => {
  const storeMap = {}

  await storeModel.find(query)
    .sort({ name: -1 })
    .catch((err) => {
      throw new CustomError({ message: err.message, status: 500 })
    })
    .map((model) => {
      const s = model.toObject()
      storeMap[s.storeId] = s
    })

  return storeMap
}

const mergeStores = (storeList, storeModels) => {
  if (storeList === undefined) return []
  const toReturn = storeList.slice(0)
  for (let i = 0; i < toReturn.length; i += 1) {
    const store = toReturn[i]
    const storeModel = storeModels[store.storeId]
    if (storeModel && storeModel.payment) {
      toReturn[i] = {
        ...storeModel,
        ...toReturn[i],
      }
    }
  }

  return toReturn
}

const getStores = async (tenant, countryCode, merge = false) => {
  const storeList = await getStoreList()
  const countryStores = storeList[countryCode.toUpperCase()]

  let storeModels = {}
  if (tenant && countryCode) {
    storeModels = await getStoreModels({
      tenant: tenant.toLowerCase(),
      countryCode: countryCode.toUpperCase(),
    })
  }

  const listOfStoreModels = Object.keys(storeModels)
    .map((key) => storeModels[key])

  return merge ? mergeStores(countryStores, storeModels) : listOfStoreModels
}

const getStore = async (tenant, countryCode, storeId, merge = false) => {
  const storeList = await getStoreList()
  const countryStores = storeList[countryCode.toUpperCase()] || []

  const filteredStores = countryStores.filter(
    ({ storeId: _storeId }) => _storeId === storeId
  )

  const storeModels = await getStoreModels({
    tenant: tenant.toLowerCase(),
    countryCode: countryCode.toUpperCase(),
    storeId,
  })

  if (merge) {
    if (filteredStores.length === 0) return undefined
    const store = filteredStores[0]
    return mergeStores([store], storeModels)[0]
  } else {
    const listOfStoreModels = Object.keys(storeModels)
      .map((key) => storeModels[key])
    return listOfStoreModels[0]
  }
}

const createOrUpdateStore = (storeId, store) => {
  let { tenant, countryCode } = store
  return storeModel.findOneAndUpdate({
    tenant: tenant.toLowerCase(),
    countryCode: countryCode.toUpperCase(),
    storeId,
  }, store, { new: true, upsert: true, runValidators: true })
    .catch((err) => {
      throw new CustomError({ message: err.message, status: 500 })
    })
}

const patchStore = async (tenant, countryCode, storeId, toPatch) => {
  const _toPatch = { ...toPatch }
  if (_toPatch.countryCode) {
    _toPatch.countryCode = _toPatch.countryCode.toLowerCase()
  }
  return storeModel.findOneAndUpdate({
    tenant: tenant.toLowerCase(),
    countryCode: countryCode.toUpperCase(),
    storeId,
  }, { $set: _toPatch }, { new: true })
    .catch((err) => {
      throw new CustomError({ message: err.message, status: 500 })
    })
}

const deleteStores = async (conditions) => {
  const result = await storeModel.deleteMany(conditions)
    .catch((err) => {
      throw new CustomError({ message: err.message, status: 500 })
    })
  return result.deletedCount
}

module.exports = {
  getStoreList,
  getStores,
  getStore,
  createOrUpdateStore,
  patchStore,
  deleteStores,
}
