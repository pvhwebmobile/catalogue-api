const https = require('https')
const fetch = require('node-fetch')
const config = require('config')
const joi = require('@hapi/joi')
const Validation = require('../libs/validation')
const Utils = require('../libs/utils')
const CustomError = require('../libs/errors').CustomError
const { buildURL } = require('../libs/url')
const { getCountryProperties } = require('../libs/countryConfig')
const { validateBody } = require('../libs/bodyValidator')
const { versionCheck } = require('../libs/versioning')

let validation = new Validation()

// temp: this ignores the self-signed certs that the devel wcs uses
const customProxy = new https.Agent({
  rejectUnauthorized: false,
})

class Products {
  async getProductById (ctx, next) {
    const countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) ||
      'TH'
    const storeId = ctx.params.storeId
    const productId = ctx.params.productId
    // TODO: remove v1 compatibility when it's depricated
    const acceptVersion = ctx.state.acceptVersion
    const isOldVersion = acceptVersion === '1.0.0' || ctx.query.version === 1
    const userAgent = ctx.state.userAgent
    const isIpad = /(itommy|icalvin)/ig.test(userAgent)

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      'STORE_ID': storeId,
      'LANG_ID': ctx.state.langId || langId,
      'CATALOG_ID': catalogId,
      'PRODUCT_ID': productId,
    }
    const fetchProductURL = buildURL(config.URLS.WCS_PRODUCT_DETAILS_BY_ID, urlParameters, tenant, countryCode)
    const fetchProduct = await fetch(fetchProductURL, {})

    if (!fetchProduct.ok) {
      throw new CustomError({ message: `Error fetching ${fetchProductURL}.`, status: fetchProduct.status })
    }

    let rawProduct = await fetchProduct.json()

    const { currency, priceField } = getCountryProperties(tenant, countryCode, ['currency', 'priceField'])
    const attributeValuesSchema = joi.object().keys({
      identifier: joi.string().required(),
      value: joi.string().required(),
    }).unknown()
    const attributeSchema = joi.object().keys({
      identifier: joi.string().required(),
      values: joi.array().items(attributeValuesSchema).min(1).required(),
    }).unknown()
    const sKUSchema = joi.object().keys({
      uniqueID: joi.string().required(),
      partNumber: joi.string().required(),
      buyable: joi.string().required(),
      [`price_${currency}_${priceField}`]: joi.string().required(),
      [`wasprice_${currency}_${priceField}`]: joi.string().optional(),
      currency,
    }).unknown()
    const entrySchema = joi.object().keys({
      attributes: joi.array().items(attributeSchema).min(1).required(),
      uniqueID: joi.string().required(),
      name: joi.string().required(),
      longDescription: joi.string().optional(),
      currentMinPrice: joi.string().optional(),
      [`price_${currency}_${priceField}`]: joi.string().optional(),
      [`wasprice_${currency}_${priceField}`]: joi.string().optional(),
      price: joi.array().items(
        joi.object().keys({ currency: joi.string().required() }).unknown()
      ).min(2).required(),
      partNumber: joi.string().required(),
      styleColours: joi.array(),
      sKUs: joi.array().items(sKUSchema).min(1).required(),
    }).unknown()
    const schema = joi.object().keys({
      errors: joi.any().optional(),
      catalogEntryView: joi.array().items(entrySchema).min(0).required(),
    }).unknown()

    if (!validateBody(rawProduct, schema)) {
      ctx.status = 404
      ctx.type = 'JSON'
      ctx.body = {
        success: false,
        data: {},
      }
      return
    }

    this.validateRemoteError(rawProduct.errors)

    // TODO: validate an error message if product is not exist
    rawProduct = rawProduct.catalogEntryView[0]

    if (!rawProduct) {
      throw new CustomError({
        message: config.ERRORS.PRODUCTS.PRODUCT_BY_ID_NOT_FOUND,
        code: config.ERRORS.PRODUCTS.CODE,
      })
    }

    let productAttrs = config.PRODUCT_ATTRIBUTES
    let productLabel = this.getProductDetailsByAttribute(rawProduct.attributes, productAttrs.Label.key)
    let styleProductURL = rawProduct.seo_token_ntk.split('|')[1].split('-')
    styleProductURL = styleProductURL.slice(0, styleProductURL.length - 1).join('-')
    styleProductURL = `${config.BASE_URLS.WCS.replace('CC', countryCode.toLowerCase())}/${styleProductURL}`
    let productURL = `${config.BASE_URLS.WCS.replace('CC', countryCode.toLowerCase())}/${rawProduct.seo_token_ntk.split('|')[1]}`
    const wasPrice = rawProduct[`wasprice_${currency}_${priceField}`] * 1 ||
      undefined
    let product = {
      id: rawProduct.uniqueID,
      name: rawProduct.name,
      description: rawProduct.longDescription,
      price: rawProduct.currentMinPrice * 1 || rawProduct[`price_${currency}_${priceField}`] * 1 || 0,
      wasPrice: wasPrice,
      currency: rawProduct.price[1].currency,
      colors: {},
      label: isOldVersion ? productLabel.value : productLabel,
      url: productURL,
    }
    let images = {}
    const urls = {}
    let skuIdsArr = []
    let colors = rawProduct.styleColours

    if (colors && colors.length > 0) {
      for (let i = 0; i < colors.length; i++) {
        urls[colors[i][productAttrs.PRODUCT_ATTR_COLOUR.key]] =
          `${styleProductURL}-${colors[i].partnumber.toLowerCase()}`

        images[colors[i][productAttrs.PRODUCT_ATTR_COLOUR.key]] =
          this.buildProductImagesByColor(
            colors[i].partnumber, tenant, acceptVersion, isIpad
          )
      }
    } else {
      images = this.buildProductImagesByColor(
        rawProduct.partNumber, tenant, acceptVersion, isIpad
      )
    }

    if (rawProduct.sKUs && rawProduct.sKUs.length > 0) {
      for (let i = 0; i < rawProduct.sKUs.length; i++) {
        skuIdsArr.push(rawProduct.sKUs[i].uniqueID)
      }
    } else {
      throw new CustomError({ message: config.ERRORS.PRODUCTS.STATUS, code: config.ERRORS.PRODUCTS.PRODUCT_MALFORMED })
    }

    let inventoryAvailability = await this.getProductsAvailability(ctx, tenant, storeId, countryCode, skuIdsArr, ctx.query.byPartNumber)

    for (let i = 0; i < rawProduct.sKUs.length; i++) {
      const wasPrice =
        rawProduct.sKUs[i][`wasprice_${currency}_${priceField}`] * 1 ||
          undefined
      let size = {
        id: rawProduct.sKUs[i].uniqueID,
        skuId: rawProduct.sKUs[i].partNumber,
        buyable: rawProduct.sKUs[i].buyable,
        price: rawProduct.sKUs[i][`price_${currency}_${priceField}`] * 1,
        wasPrice,
        currency: rawProduct.price[1].currency,
        inventory: inventoryAvailability[rawProduct.sKUs[i].uniqueID],
      }
      for (let j = 0; j < rawProduct.sKUs[i].attributes.length; j++) {
        if (productAttrs[rawProduct.sKUs[i].attributes[j].identifier].value) {
          const currentAttr = productAttrs[rawProduct.sKUs[i].attributes[j].identifier].value
          const skuAttrs = rawProduct.sKUs[i].attributes[j]
          // TODO: remove v1 compatibility when it's depricated
          if (isOldVersion) {
            size[rawProduct.sKUs[i].attributes[j].identifier] = skuAttrs.values[0].value
          } else {
            size[currentAttr] = {
              id: skuAttrs.values[0].identifier,
              value: skuAttrs.values[0].value,
            }
          }
        }
      }
      // init product color group if it's not exist
      // TODO: remove v1 compatibility when it's depricated
      const sizeColorKey = isOldVersion ? size[productAttrs.PRODUCT_ATTR_COLOUR.key] : size[productAttrs.PRODUCT_ATTR_COLOUR.value].value
      if (!product.colors[sizeColorKey]) {
        product.colors[sizeColorKey] = Object.assign({
          label: size.label,
          images: images[sizeColorKey] ? images[sizeColorKey].product : images.product,
          pattern: images[sizeColorKey] ? images[sizeColorKey].pattern : images.pattern,
          sizes: [],
          url: urls[sizeColorKey],
        }, isOldVersion ? { name: size[productAttrs.PRODUCT_ATTR_COLOUR.key] } : size[productAttrs.PRODUCT_ATTR_COLOUR.value])
      }
      product.colors[sizeColorKey].sizes.push(size)
      skuIdsArr.push(size.id)
    }

    product.colors = Object.keys(product.colors).map(key => product.colors[key])

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: product }
  }

  async getProductsAvailability (ctx, tenant, storeId, countryCode, skuIdsArr, byPartNumber) {
    validation.isStoreIdValid(tenant, countryCode, storeId)

    let onlineInventory = await this.getOnlineProductsAvailability(ctx, tenant, storeId, countryCode, skuIdsArr, byPartNumber)
    let storeInventory = await this.getStoreProductsAvailability(tenant, storeId, countryCode, skuIdsArr, byPartNumber)
    let inventory = {}

    for (let i in onlineInventory) {
      inventory[i] = {
        onlineInventory: onlineInventory[i].availableQuantity,
        storeInventory: 0,
      }
    }
    for (let i in storeInventory) {
      if (!inventory[i]) {
        inventory[i] = {
          onlineInventory: 0,
          storeInventory: storeInventory[i].availableQuantity,
        }
      } else {
        inventory[i].storeInventory = storeInventory[i].availableQuantity
      }
    }

    return inventory
  }

  async getOnlineProductsAvailability (ctx, tenant, storeId, countryCode, idsArr, byPartNumber) {
    let inventory = {}
    byPartNumber = byPartNumber * 1 || 0

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      'STORE_ID': storeId,
      'LANG_ID': ctx.state.langId || langId,
      'CATALOG_ID': catalogId,
      'IDS': idsArr.join(','),
    }
    const startURL = byPartNumber ? config.URLS.WCS_PRODUCT_AVAILABILITY_BY_PARTNUMBER : config.URLS.WCS_PRODUCT_AVAILABILITY_BY_ID
    const fetchProductsURL = buildURL(startURL, urlParameters, tenant, countryCode)
    let fetchProduct = await fetch(fetchProductsURL, { agent: customProxy, rejectUnauthorized: false })

    if (!fetchProduct.ok) {
      throw new CustomError({ message: `Error fetching ${fetchProductsURL}.`, code: fetchProduct.status })
    }

    let rawProduct = await fetchProduct.json()

    const inventorySchema = joi.object().keys({
      x_partnumber: joi.string(),
      productId: joi.string(),
      inventoryStatus: joi.string().optional(),
      unitOfMeasure: joi.string().optional(),
      availableQuantity: joi.string().required(),
    }).or('x_partnumber', 'productId').unknown()
    const schema = joi.object().keys({
      errors: joi.any(),
      InventoryAvailability: joi.array().items(inventorySchema).min(1),
    }).unknown()

    if (!validateBody(rawProduct, schema)) {
      return inventory
    }

    this.validateRemoteError(rawProduct.errors)

    for (let i = 0; i < rawProduct.InventoryAvailability.length; i++) {
      let id = byPartNumber ? rawProduct.InventoryAvailability[i].x_partnumber : rawProduct.InventoryAvailability[i].productId

      inventory[id] = {
        status: rawProduct.InventoryAvailability[i].inventoryStatus,
        unitOfMeasure: rawProduct.InventoryAvailability[i].unitOfMeasure,
        availableQuantity: rawProduct.InventoryAvailability[i].availableQuantity * 1,
      }
    }

    // mock the inventory
    // if (skuIdsArr && skuIdsArr.length > 0) {
    //     for (let i = 0; i < skuIdsArr.length; i++) {
    //         inventory[skuIdsArr[i]] = {
    //             status: "Available",
    //             unitOfMeasure: "C62",
    //             availableQuantity: Utils.getRandomInt(0, 5)
    //         }
    //     }
    // }

    return inventory
  }

  async getStoreProductsAvailability (tenant, storeId, countryCode, skuIdsArr, byPartNumber) {
    let inventory = {}
    byPartNumber = byPartNumber * 1 || 0
    // let baseUrl = byPartNumber ? config.SAP_URLS.SAP_STORE_STOCK_BY_SKU : config.SAP_URLS.SAP_STORE_STOCK_BY_SKU
    // let skuStr = ''

    // skuIdsArr.forEach(function(item) {
    //     skuStr += "EAN11 eq '" + item + "' or ";
    // });
    // skuStr = skuStr.slice(0, -4);
    //
    // let url = Utils.buildWCSUrl(baseUrl, storeId, countryCode)
    //     .replace('STORE_ID', storeId)
    //     .replace('SKU_IDS', skuStr);
    //
    // let fetchProduct = await fetch(url, {headers: {
    //     'Authorization': 'Basic ' + new Buffer(config.SAP_CREDENTIALS.username + ':' + config.SAP_CREDENTIALS.password).toString('base64')
    // }});
    // let rawProduct = await fetchProduct.json();
    //
    // if (rawProduct.error) {
    //     throw new CustomError({message: rawProduct.error.message.value, code: ''});
    // }
    //
    // rawProduct = rawProduct.d.results;
    //
    // for (let i = 0; i < rawProduct.length; i++) {
    //     inventory[rawProduct[i].EAN11] = {
    //         availableQuantity: rawProduct[i].Qty*1,
    //     }
    // }

    // TODO: mock the inventory until the Stores stock user/network would be ready
    if (skuIdsArr && skuIdsArr.length > 0) {
      for (let i = 0; i < skuIdsArr.length; i++) {
        inventory[skuIdsArr[i]] = {
          availableQuantity: Utils.getRandomInt(0, 5),
        }
      }
    }

    return inventory
  }

  async getInventoryBySkus (ctx, next) {
    let countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let storeId = ctx.params.storeId

    validation.isStoreIdValid(tenant, countryCode, storeId)

    let inventoryAvailability = await this.getProductsAvailability(ctx, tenant, storeId, countryCode, ctx.params.skus.split(','), ctx.query.byPartNumber)

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: inventoryAvailability }
  }

  async getSuggestionsByTerm (ctx, next) {
    let countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let storeId = ctx.params.storeId
    const term = ctx.params.term

    validation.isStoreIdValid(tenant, countryCode, storeId)

    // check if the term existing
    if (!term) {
      throw new CustomError({ message: config.ERRORS.PRODUCTS.NO_SEARCH_TERM, code: config.ERRORS.PRODUCTS.CODE })
    }

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      'STORE_ID': storeId,
      'LANG_ID': ctx.state.langId || langId,
      'CATALOG_ID': catalogId,
      'TERM': term,
    }
    const fetchSuggestionsURL = buildURL(config.URLS.WCS_SUGGESTIONS_BY_TERM, urlParameters, tenant, countryCode)
    const fetchSuggestions = await fetch(fetchSuggestionsURL, {})

    if (!fetchSuggestions.ok) {
      throw new CustomError({ message: `Error fetching ${fetchSuggestionsURL}.`, status: fetchSuggestions.status })
    }

    let rawSuggestions = await fetchSuggestions.json()

    const suggestionSchema = joi.object().keys({
      entry: joi.any(),
    }).unknown()
    const schema = joi.object().keys({
      suggestionView: joi.array().items(suggestionSchema).min(1),
      errors: joi.any().optional(),
    }).unknown()

    if (!validateBody(rawSuggestions, schema)) {
      ctx.status = 404
      ctx.type = 'JSON'
      ctx.body = { success: false, data: [] }
      return
    }

    let suggestions = rawSuggestions.suggestionView[0].entry

    this.validateRemoteError(rawSuggestions.errors)

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: suggestions }
  }

  async getProductsByTerm (ctx, next) {
    let countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let storeId = ctx.params.storeId
    let pageNumber = ctx.query.page || 1
    let facet = ctx.query.facet || ''
    const term = ctx.params.term
    const acceptVersion = ctx.state.acceptVersion
    const userAgent = ctx.state.userAgent
    const isIpad = /(itommy|icalvin)/ig.test(userAgent)

    validation.isStoreIdValid(tenant, countryCode, storeId)

    if (!term) {
      throw new CustomError({ message: config.ERRORS.PRODUCTS.NO_SEARCH_TERM, code: config.ERRORS.PRODUCTS.CODE })
    }

    // format the multiple values for the filters
    if (facet) {
      facet = this.formatFiltersValue(facet)
    }

    let categories = []
    if (ctx.query.categoryId) {
      categories = ctx.query.categoryId.split(',').map(function (category) {
        return 'categoryId=' + category
      })
    }

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      'STORE_ID': storeId,
      'LANG_ID': ctx.state.langId || langId,
      'CATALOG_ID': catalogId,
      'TERM': term,
      'PAGE_NUMBER': pageNumber,
      'FACET': facet,
      'CATEGORY_IDS': categories.length > 0 ? categories.join('&') : '',
    }
    const fetchSearchURL = buildURL(config.URLS.WCS_PRODUCTS_SEARCH_BY_TERM, urlParameters, tenant, countryCode)
    const fetchSearch = await fetch(fetchSearchURL, {})

    if (!fetchSearch.ok) {
      throw new CustomError({ message: `Error fetching ${fetchSearchURL}.`, status: fetchSearch.status })
    }

    let rawSearch = await fetchSearch.json()

    const schema = joi.object().keys({
      catalogEntryView: joi.array().min(0),
      facetView: joi.any().optional(),
      errors: joi.any().optional(),
    }).unknown()

    if (!validateBody(rawSearch, schema)) {
      ctx.status = 404
      ctx.type = 'JSON'
      ctx.body = { success: false, data: {}, error: { message: 'Products not found for term.' } }
      return
    }

    this.validateRemoteError(rawSearch.errors)

    let productsData = {
      products: this.formatProductList(rawSearch.catalogEntryView, tenant, countryCode, acceptVersion, isIpad),
      filters: this.formatFiltersList(rawSearch.facetView),
      totalProductsAmount: rawSearch.recordSetTotal,
    }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: productsData }
  }

  async getProductsByCategoryId (ctx, next) {
    let countryCode = ctx.params.countryCode && ctx.params.countryCode.toUpperCase()
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let storeId = ctx.params.storeId
    let pageNumber = ctx.query.page || 1
    let facet = ctx.query.facet || ''
    const acceptVersion = ctx.state.acceptVersion
    const userAgent = ctx.state.userAgent
    const isIpad = /(itommy|icalvin)/ig.test(userAgent)

    validation.isStoreIdValid(tenant, countryCode, storeId)

    if (!ctx.params.categoryId) {
      throw new CustomError({ message: config.ERRORS.PRODUCTS.NO_CATEGORY_ID, code: config.ERRORS.PRODUCTS.CODE })
    }

    // format the multiple values for the filters
    if (facet) {
      facet = this.formatFiltersValue(facet)
    }

    let categoryId = ctx.params.categoryId
    let categories = categoryId.split(',')
    let url

    const { langId, catalogId } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])

    // PLP for list of categories or single category
    if (categories.length > 1) {
      categories = categories.map(function (category) {
        return 'categoryId=' + category
      })
      const urlParameters = {
        'STORE_ID': storeId,
        'LANG_ID': ctx.state.langId || langId,
        'CATALOG_ID': catalogId,
        'TERM': '%20',
        'PAGE_NUMBER': pageNumber,
        'FACET': facet,
        'CATEGORY_IDS': categories.length > 0 ? categories.join('&') : '',
      }
      url = buildURL(config.URLS.WCS_PRODUCTS_SEARCH_BY_TERM, urlParameters, tenant, countryCode)
    } else {
      const urlParameters = {
        'STORE_ID': storeId,
        'LANG_ID': ctx.state.langId || langId,
        'CATALOG_ID': catalogId,
        'PAGE_NUMBER': pageNumber,
        'FACET': facet,
        'CATEGORY_ID': categoryId,
      }
      url = buildURL(config.URLS.WCS_PRODUCTS_BY_CATEGORY_ID, urlParameters, tenant, countryCode)
    }

    let fetchSearch = await fetch(url, {})

    if (!fetchSearch.ok) {
      throw new CustomError({ message: `Error fetching ${url}.`, status: fetchSearch.status })
    }

    let rawSearch = await fetchSearch.json()

    const schema = joi.object().keys({
      errors: joi.any().optional(),
      catalogEntryView: joi.array().min(0).required(),
      facetView: joi.any().optional(),
      recordSetTotal: joi.any().optional(),
    }).unknown()
    validateBody(rawSearch, schema)

    this.validateRemoteError(rawSearch.errors)

    let productsData = {
      products: this.formatProductList(rawSearch.catalogEntryView, tenant, countryCode, acceptVersion, isIpad),
      filters: rawSearch.facetView ? this.formatFiltersList(rawSearch.facetView) : {},
      totalProductsAmount: rawSearch.recordSetTotal,
    }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: productsData }
  }

  // helpers

  formatFiltersValue (filters) {
    let facet = {}
    let facetStr = ''

    filters = filters.split(',')

    for (let i = 0; i < filters.length; i++) {
      let filter = filters[i].split(':')

      if (!facet[filter[0]]) {
        facet[filter[0]] = filter[1]
      } else {
        facet[filter[0]] += ',' + filter[1]
      }
    }

    for (let i in facet) {
      facetStr += 'facet=' + facet[i] + '&'
    }

    return facetStr
  }

  formatFiltersList (entries) {
    let filters = {}
    let filtersLength = Object.keys(config.FILTERS).length
    let filtersCount = 0

    if (filtersLength === 0) {
      return filters
    }

    if (!entries || entries.length === 0) {
      return filters
    }

    for (let i = 0; i < entries.length; i++) {
      // end the search when the all filters are found
      if (filtersCount === filtersLength) {
        break
      }
      // check if the curent filter is on the list
      if (!config.FILTERS[entries[i].extendedData.attridentifier]) {
        continue
      }
      // add the filter instance
      let filterType = entries[i].extendedData.attridentifier
      filters[filterType] = {
        name: entries[i].name,
        items: [],
      }
      // fill in the filter values
      for (let j = 0; j < entries[i].entry.length; j++) {
        filters[filterType].items.push({
          count: entries[i].entry[j].count,
          value: decodeURIComponent(entries[i].entry[j].value),
          label: entries[i].entry[j].label,
        })
      }
      filtersCount++
    }

    return filters
  }

  formatProductList (entries, tenant, countryCode, acceptVersion, isIpad) {
    let search = []

    if (!entries || entries.length === 0) {
      return search
    }

    const { currency, priceField } = getCountryProperties(tenant, countryCode, ['currency', 'priceField'])

    const attributeValuesSchema = joi.object().keys({
      identifier: joi.string().required(),
      value: joi.string().required(),
    }).unknown()
    const attributeSchema = joi.object().keys({
      identifier: joi.string().required(),
      values: joi.array().items(attributeValuesSchema).min(1).required(),
    }).unknown()
    const entrySchema = joi.object().keys({
      associatedStyleColours: joi.array().optional(),
      uniqueID: joi.string().required(),
      partNumber: joi.string().required(),
      x_catalogEntryTypeCode: joi.string(),
      parentStyle: joi.object().keys({
        catentryId: joi.string().required(),
      }).unknown().optional(),
      name: joi.string().required(),
      currentMinPrice: joi.string().optional(),
      [`price_${currency}_${priceField}`]: joi.string().optional(),
      [`wasprice_${currency}_${priceField}`]: joi.string().optional(),
      price: joi.array().items(joi.object().keys({
        currency: joi.string().required(),
      }).unknown()).min(1).required(),
      attributes: joi.array().items(attributeSchema).min(1).required(),
    }).unknown()

    for (let i = 0; i < entries.length; i++) {
      const entry = entries[i]

      if (!validateBody(entry, entrySchema)) continue

      let associatedStyleColoursLength = entries[i].associatedStyleColours ? entries[i].associatedStyleColours.length + 1 : 1
      const mainImageSuffix = versionCheck(acceptVersion, ['^2.4'])
        ? (isIpad ? '?$eisPLP$' : '?$<SIZE>$')
        : ''
      const mainImage = entries[i].styleColours &&
        entries[i].styleColours.length > 0
        ? config.BASE_URLS[tenant].S7 + this.getColorByPartNumber(entries[i].styleColours[0].partnumber) + '_main' + mainImageSuffix
        : config.BASE_URLS[tenant].S7 + this.getColorByPartNumber(entries[i].partNumber) + '_main' + mainImageSuffix
      const price = entries[i].currentMinPrice * 1 ||
        entries[i][`price_${currency}_${priceField}`] * 1 ||
        0
      const wasPrice = entries[i][`wasprice_${currency}_${priceField}`] * 1 ||
        undefined
      let item = {
        id: entries[i].uniqueID,
        partNumber: entries[i].partNumber,
        isStyleColor: `${entries[i].x_catalogEntryTypeCode}` === '3',
        styleId: entries[i].parentStyle && entries[i].parentStyle.catentryId,
        name: entries[i].name,
        price: price,
        wasPrice: wasPrice,
        currency: entries[i].price[1].currency,
        mainImage: mainImage,
        colorsAmount: (entries[i].styleColours && entries[i].styleColours.length) || associatedStyleColoursLength,
        label: this.getProductDetailsByAttribute(entries[i].attributes, config.PRODUCT_ATTRIBUTES.PRODUCT_ATTR_BRAND.key).value,
        colorId: this.getProductDetailsByAttribute(entries[i].attributes, config.PRODUCT_ATTRIBUTES.PRODUCT_ATTR_COLOUR.key).id,
      }

      search.push(item)
    }

    return search
  }

  validateRemoteError (error) {
    error = error && (error[0])

    if (error) {
      throw new CustomError({ message: error.errorMessage, code: error.errorCode })
    }
  }

  getColorByPartNumber (partnumber) {
    let partNumberArr = partnumber.split('')
    partNumberArr.splice(partNumberArr.length - 3, 0, '_')

    return partNumberArr.join('')
  }

  buildProductImagesByColor (partnumber, tenant, acceptVersion, isIpad = false) {
    let colorByPartNumber = this.getColorByPartNumber(partnumber)
    const urlPrefix = config.BASE_URLS[tenant].S7 + colorByPartNumber
    const urlSuffix = versionCheck(acceptVersion, ['^2.4'])
      ? (isIpad ? '$eisPDP$' : '$<SIZE>$')
      : '$main$'
    const patternSuffix = versionCheck(acceptVersion, ['^2.4'])
      ? (isIpad ? '' : '?$<SIZE>$')
      : '?$minithumb$'
    return {
      product: [
        urlPrefix + '_main?' + urlSuffix,
        urlPrefix + '_alternate1?' + urlSuffix,
        urlPrefix + '_alternate2?' + urlSuffix,
      ],
      colorCode: partnumber.slice(-3),
      pattern: urlPrefix + '_main' + patternSuffix,
    }
  }

  getProductDetailsByAttribute (entryAttrs, attributeKey) {
    if (!entryAttrs || entryAttrs.length === 0) {
      return ''
    }

    for (let i = 0; i < entryAttrs.length; i++) {
      if (entryAttrs[i].identifier === attributeKey) {
        const value = entryAttrs[i].values[0]
        return {
          id: value.identifier,
          value: value.value,
        }
      }
    }

    return {
      id: '',
      value: '',
    }
  }
}

module.exports = Products
