const https = require('https')
const fetch = require('node-fetch')
const config = require('config')
const CustomError = require('../libs/errors').CustomError
const Validation = require('../libs/validation')

const validation = new Validation()

const { buildURL } = require('../libs/url')
const { getCountryProperties } = require('../libs/countryConfig')
const { versionCheck } = require('../libs/versioning')
const {
  langIdFromLanguageId,
  languageFromLanguageId,
} = require('../libs/countryUtils')

const customProxy = new https.Agent({
  rejectUnauthorized: false,
})

class Countries {
  async getCountriesList (ctx, next) {
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'

    const languageMap = await this.getCountryLanguages()

    const countries = config.WCS_COUNTRIES[tenant]
    if (languageMap) {
      for (let countryCode in countries) {
        countries[countryCode].language = languageMap[countryCode]
      }
    }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: countries }
  }

  async getStoresListByCountry (ctx, next) {
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'
    let countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    let storeId = ctx.params.storeId

    validation.isStoreIdValid(tenant, countryCode, storeId)

    // TODO: mocked stores list until SAP user/network would be ready
    const stores = require('../config/stores')[countryCode]
    if (versionCheck(config.SERVER_VERSION, ['<2'])) {
      for (let store of stores) {
        if (store.payment) {
          delete store.payment
        }
      }
    }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: stores || [] }
  }

  // based on the WCS Validation Rules ping
  async pingWCSConnection (ctx, next) {
    const tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) ||
      'TH'
    const countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    const storeId = ctx.params.storeId

    validation.isStoreIdValid(tenant, countryCode, storeId)

    const {
      langId,
    } = getCountryProperties(tenant, countryCode, ['langId', 'catalogId'])
    const urlParameters = {
      STORE_ID: storeId,
      LANG_ID: langId,
    }
    const fetchValidationRulesURL = buildURL(
      config.URLS.WCS_GET_VALIDATION_RULES, urlParameters, tenant, countryCode
    )
    const fetchValidationRules = await fetch(fetchValidationRulesURL, { agent: customProxy })

    if (!fetchValidationRules.ok) {
      throw new CustomError({
        message: `Error fetching ${fetchValidationRulesURL}.`,
        status: fetchValidationRules.status,
      })
    }

    ctx.status = fetchValidationRules.status
    ctx.type = 'JSON'
    ctx.body = { success: fetchValidationRules.status === 200 }
  }

  parseEsitesToLanguageMap (esites) {
    const languageMap = {}
    for (let esite of esites) {
      const { countryCode, language } = esite
      languageMap[countryCode] = []
      let def = true
      for (let lang of language) {
        const { id, Label, target } = lang
        const newLanguage = {
          id,
          label: Label,
          target,
          lang: languageFromLanguageId(id),
          langId: langIdFromLanguageId(id),
          default: def,
        }
        languageMap[countryCode].push(newLanguage)
        def = false
      }

      if (languageMap[countryCode].length <= 1) {
        delete languageMap[countryCode]
      }
    }
    return languageMap
  }

  async getCountryLanguages (country = 'ALL') {
    const url = buildURL(config.URLS.WCS_GET_ALL_LANGUAGE_OPTIONS, {}, 'th', 'uk')
    const response = await fetch(url, { agent: customProxy, rejectUnauthorized: false })

    if (!response.ok) {
      return
    }

    const json = await response.json()

    const esites = json.responseDataMap && json.responseDataMap.esites
    const languageMap = this.parseEsitesToLanguageMap(esites)

    if (country === 'ALL') {
      return languageMap
    } else {
      return languageMap[country]
    }
  }

  async getLanguagesByCountry (ctx) {
    let countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()

    const languageMap = await this.getCountryLanguages(countryCode)
    const data = {
      languages: languageMap || [],
    }

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data }
  }
}

module.exports = Countries
