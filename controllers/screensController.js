const { getCategories } = require('./newCategoriesController')
const { getActiveCampaigns, getPrioritizedCampaignBanner } = require('./campaignsController')

const getGenderScreen = async (
  tenant, countryCode, storeId, catalogId, langId
) => {
  const categories = await getCategories(tenant, countryCode, storeId, catalogId, langId, true)
  const activeCampaigns = await getActiveCampaigns(tenant, countryCode)
  const banner = getPrioritizedCampaignBanner(activeCampaigns)

  return {
    categories,
    banner,
  }
}

module.exports = {
  getGenderScreen,
}
