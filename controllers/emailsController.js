const config = require('config')
const Mustache = require('mustache')
const sgMail = require('@sendgrid/mail')
const fs = require('fs')

const { CustomError } = require('../libs/errors')
const Validation = require('../libs/validation')
const logger = require('../libs/logger')

let templates = {
}
let images = {
  wishlist: {
    TH: {},
    CK: {},
  },
}

let validation = new Validation()

class Emails {
  constructor () {
    sgMail.setApiKey(config.SENDGRID_API_KEY)
    this._readTemplates()
    this._readImages()
  }

  async sendWishlistEmail (ctx, next) {
    let countryCode = ctx.params.countryCode &&
      ctx.params.countryCode.toUpperCase()
    let storeId = ctx.params.storeId
    let reqBody = ctx.request.body
    let tenant = (ctx.params.tenant && ctx.params.tenant.toUpperCase()) || 'TH'

    validation.isStoreIdValid(tenant, countryCode, storeId)

    if (!reqBody.email) {
      throw new CustomError({
        message: config.ERRORS.EMAILS.EMAIL_ADDRESS_REQUIRED,
        status: config.ERRORS.EMAILS.STATUS,
      })
    }
    if (!reqBody.items || reqBody.items.length < 1) {
      throw new CustomError({
        message: config.ERRORS.EMAILS.ITEMS_LIST_REUIRED_OR_EMPTY,
        status: config.ERRORS.EMAILS.STATUS,
      })
    }

    if (!config.EMAILS[tenant].WISHLIST.from[countryCode]) {
      throw new CustomError({
        message: config.ERRORS.EMAILS.FROM_EMAIL_IS_UNDEFINED_FOR_COUNTRY,
        status: config.ERRORS.EMAILS.STATUS,
      })
    }

    const template = tenant === 'TH' ? templates.th_wishlist : templates.ck_wishlist
    let output = Mustache.render(template, {
      items: reqBody.items,
      total: reqBody.total,
      ecom_url: config.BASE_URLS[tenant].WCS.replace('CC', countryCode.toLowerCase()),
      images: images.wishlist[tenant],
      locales: {
        'free_delivery_on_orders_above':
          ctx.i18n.__('locales.tmpl.free_delivery_on_orders_above'),
        'women': ctx.i18n.__('locales.tmpl.women'),
        'women_link': ctx.i18n.__('locales.tmpl.women_link'),
        'men': ctx.i18n.__('locales.tmpl.men'),
        'men_link': ctx.i18n.__('locales.tmpl.men_link'),
        'kids': ctx.i18n.__('locales.tmpl.kids'),
        'kids_link': ctx.i18n.__('locales.tmpl.kids_link'),
        'shopping_bag': ctx.i18n.__('locales.tmpl.shopping_bag'),
        'thanks_for_browsing': ctx.i18n.__('locales.tmpl.thanks_for_browsing'),
        'go_to_checkout': ctx.i18n.__('locales.tmpl.go_to_checkout'),
        'total': ctx.i18n.__('locales.tmpl.total'),
        'incl_vat': ctx.i18n.__('locales.tmpl.incl_vat'),
        'looking_for': ctx.i18n.__('locales.tmpl.looking_for'),
        'something_else': ctx.i18n.__('locales.tmpl.something_else'),
        'visit_us_on': ctx.i18n.__('locales.tmpl.visit_us_on'),
        'to_see_more_styles': ctx.i18n.__('locales.tmpl.to_see_more_styles'),
        'shop_women': ctx.i18n.__('locales.tmpl.shop_women'),
        'shop_men': ctx.i18n.__('locales.tmpl.shop_men'),
        'find_a_store': ctx.i18n.__('locales.tmpl.find_a_store'),
        'near_you': ctx.i18n.__('locales.tmpl.near_you'),
        'free_delivery': ctx.i18n.__('locales.tmpl.free_delivery'),
        'for_orders_over_100': ctx.i18n.__('locales.tmpl.for_orders_over_100'),
        'returns': ctx.i18n.__('locales.tmpl.returns'),
        'free_and_easy_returns':
          ctx.i18n.__('locales.tmpl.free_and_easy_returns'),
        'tracking': ctx.i18n.__('locales.tmpl.tracking'),
        'track_your_package_online':
          ctx.i18n.__('locales.tmpl.track_your_package_online'),
        'help': ctx.i18n.__('locales.tmpl.help'),
        'personalized_customer_care':
          ctx.i18n.__('locales.tmpl.personalized_customer_care'),
        'follow_us': ctx.i18n.__('locales.tmpl.follow_us'),
        'terms_and_conditions':
          ctx.i18n.__('locales.tmpl.terms_and_conditions'),
        'privacy_and_policy': ctx.i18n.__('locales.tmpl.privacy_and_policy'),
        'company_information': ctx.i18n.__('locales.tmpl.company_information'),
        'we_will_not_share_your_information':
          ctx.i18n.__('locales.tmpl.we_will_not_share_your_information'),
        'th_eu_bv_chamber_of_commerce':
          ctx.i18n.__('locales.tmpl.th_eu_bv_chamber_of_commerce'),
        'ck_eu_bv_chamber_of_commerce':
          ctx.i18n.__('locales.tmpl.ck_eu_bv_chamber_of_commerce'),
        'hilfiger_stores_bv': ctx.i18n.__('locales.tmpl.hilfiger_stores_bv'),
        'th_licensing': ctx.i18n.__('locales.tmpl.th_licensing'),
        'ck_licensing': ctx.i18n.__('locales.tmpl.ck_licensing'),
        'view_item': ctx.i18n.__('locales.tmpl.view_item'),
        'size': ctx.i18n.__('locales.tmpl.size'),
        'shipping_info_link': ctx.i18n.__('locales.tmpl.shipping_info_link'),
        'store_locator_link': ctx.i18n.__('locales.tmpl.store_locator_link'),
        'returns_and_refunds_link':
          ctx.i18n.__('locales.tmpl.returns_and_refunds_link'),
        'customer_service_contact_link':
          ctx.i18n.__('locales.tmpl.customer_service_contact_link'),
        'customer_service_link':
          ctx.i18n.__('locales.tmpl.customer_service_link'),
        'terms_and_conditions_link':
          ctx.i18n.__('locales.tmpl.terms_and_conditions_link'),
        'privacy_policy_link': ctx.i18n.__('locales.tmpl.privacy_policy_link'),
        'company_info_link': ctx.i18n.__('locales.tmpl.company_info_link'),
      },
    })

    await sgMail.send({
      to: reqBody.email,
      from: config.EMAILS[tenant].WISHLIST.from[countryCode],
      fromname: config.EMAILS[tenant].WISHLIST.fromname,
      subject: ctx.i18n.__(`locales.${tenant.toLowerCase()}_wishlist_subject`),
      html: output, // <%body%> tag for text
    })

    ctx.status = 200
    ctx.type = 'JSON'
    ctx.body = { success: true, data: '' }
  }

  _readTemplates (tenant) {
    // wishlist templates
    fs.readFile(config.EMAILS_TEMPLATES.TH.WISHLIST, 'utf8', (err, data) => {
      if (err) {
        return logger.error(err.message)
      }
      templates.th_wishlist = data
    })

    fs.readFile(config.EMAILS_TEMPLATES.CK.WISHLIST, 'utf8', (err, data) => {
      if (err) {
        return logger.error(err.message)
      }
      templates.ck_wishlist = data
    })
  }

  _readImages () {
    for (let tKey in config.EMAILS_IMAGES) {
      for (let key in config.EMAILS_IMAGES[tKey]) {
        images.wishlist[tKey][key] = config.EMAILS_IMAGES[tKey][key]
      }
    }
  }
}

module.exports = Emails
