const mongoose = require('mongoose')
const Schema = mongoose.Schema

const storeSchema = new Schema({
  tenant: {
    type: String,
    enum: ['th', 'ck'],
    default: 'th',
    required: true,
  },
  region: {
    type: String,
  },
  subRegion: {
    type: String,
  },
  country: {
    type: String,
  },
  countryCode: {
    type: String,
    required: true,
    validate: {
      validator: (v) => /^[a-zA-Z]{2}$/.test(v),
      message: 'Country code is not 2 letters.',
    },
    set: (value) => value.toUpperCase(),
  },
  city: {
    type: String,
    required: true,
  },
  zipCode: {
    type: String,
    required: true,
  },
  address1: {
    type: String,
    required: true,
  },
  address2: {
    type: String,
  },
  address3: {
    type: String,
  },
  location: {
    type: {
      type: String,
      enum: ['Point'],
    },
    coordinates: {
      type: [Number],
    },
  },
  name: {
    type: String,
    required: true,
  },
  storeId: {
    type: String,
    unique: true,
    required: true,
    validate: {
      validator: (v) => /^[A-Z0-9]{4}$/.test(v),
      message: 'Not a valid store id.',
    },
  },
  phoneNumber: {
    type: String,
  },
  openingHours: {
    raw: {
      type: String,
    },
    monday: {
      opensAt: {
        type: String,
        default: null,
      },
      closesAt: {
        type: String,
        default: null,
      },
      isOpen: {
        type: Boolean,
        default: false,
      },
    },
    tuesday: {
      opensAt: {
        type: String,
        default: null,
      },
      closesAt: {
        type: String,
        default: null,
      },
      isOpen: {
        type: Boolean,
        default: false,
      },
    },
    wednesday: {
      opensAt: {
        type: String,
        default: null,
      },
      closesAt: {
        type: String,
        default: null,
      },
      isOpen: {
        type: Boolean,
        default: false,
      },
    },
    thursday: {
      opensAt: {
        type: String,
        default: null,
      },
      closesAt: {
        type: String,
        default: null,
      },
      isOpen: {
        type: Boolean,
        default: false,
      },
    },
    friday: {
      opensAt: {
        type: String,
        default: null,
      },
      closesAt: {
        type: String,
        default: null,
      },
      isOpen: {
        type: Boolean,
        default: false,
      },
    },
    saturday: {
      opensAt: {
        type: String,
        default: null,
      },
      closesAt: {
        type: String,
        default: null,
      },
      isOpen: {
        type: Boolean,
        default: false,
      },
    },
    sunday: {
      opensAt: {
        type: String,
        default: null,
      },
      closesAt: {
        type: String,
        default: null,
      },
      isOpen: {
        type: Boolean,
        default: false,
      },
    },
  },
  divisions: {
    type: [String],
  },
  flagship: {
    type: Boolean,
    default: false,
  },
  hilfigerClub: {
    type: Boolean,
    default: false,
  },
  retailId: {
    type: String,
  },
  salesType: {
    type: String,
  },
  active: {
    type: Boolean,
    default: false,
    required: true,
  },
  payment: {
    type: String,
    enum: ['ecom', 'gk'],
    default: 'ecom',
    required: true,
  },
  isEligibleClickCollectEIS: {
    type: Boolean,
    required: true,
    default: false,
  },
  isEligibleReturnInStoreEIS: {
    type: Boolean,
    required: true,
    default: false,
  },
  isEligibleReserveInStoreOnline: {
    type: Boolean,
    required: true,
    default: false,
  },
  isEligibleClickCollectOnline: {
    type: Boolean,
    required: true,
    default: false,
  },
  isEligibleReturnInStoreOnline: {
    type: Boolean,
    required: true,
    default: false,
  },
  uploadedAt: {
    type: Date,
    default: Date.now(),
  },
}, {
  versionKey: false,
  timestamps: true,
})

storeSchema.set('toObject', { getters: true })

module.exports = mongoose.model('Store', storeSchema)
