const mongoose = require('mongoose')
const Schema = mongoose.Schema

const campaignLabel = new Schema({
  name: {
    type: String,
    required: true,
  },
  backgroundColor: {
    type: String,
    required: true,
  },
  fontColor: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
}, { _id: false })

const campaignBanner = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  fontColor: {
    type: String,
    required: true,
  },
  backgroundColor: {
    type: String,
    required: true,
  },
}, { _id: false })

const campaignSchema = new Schema({
  tenant: {
    type: String,
    required: true,
  },
  countryCode: {
    type: String,
    required: true,
  },
  priority: {
    type: Number,
    default: 999,
  },
  name: {
    type: String,
    required: true,
  },
  identifier: {
    type: String,
    required: true,
  },
  label: {
    type: campaignLabel,
  },
  banner: {
    type: campaignBanner,
  },
  active: {
    type: Boolean,
    required: true,
  },
})

campaignSchema.set('toObject', { getters: true })

module.exports = mongoose.model('Campaign', campaignSchema)
