const mongoose = require('mongoose')
const uid = require('uid')
const idValidator = require('mongoose-id-validator')
const config = require('config')
const Schema = mongoose.Schema

const clientSchema = new Schema({
  name: {
    type: String,
    unique: false,
    required: 'Selection name is required',
  },
  status: {
    type: Number,
    enum: Object.values(config.STATUSES),
    default: 1,
  },
  orderId: {
    type: String,
    unique: true,
  },
  totalPrice: {
    type: Number,
    required: 'Total Price is required',
  },
  shippingPrice: {
    type: Number,
    default: null,
  },
  currency: {
    type: String,
    required: 'Currency is required',
    length: 3,
  },
  WCTrustedToken: {
    type: String,
    unique: true,
  },
  WCToken: {
    type: String,
    unique: true,
  },
  storeId: {
    type: Number,
    required: 'Store ID is required',
  },
  tenant: {
    type: String,
    required: 'Tenant is required',
    length: 2,
  },
  countryCode: {
    type: String,
    required: 'Country Code is required',
    length: 2,
  },
  siteId: {
    type: String,
    required: 'Physical Site ID is required',
  },
  sentToTillDate: {
    type: Date,
    default: null,
  },
  terminalId: {
    type: String,
    max: 20,
  },
  pspReference: {
    type: String,
    max: 100,
  },
  merchantReference: {
    type: String,
    max: 100,
  },
  cancelReason: {
    type: String,
  },
  shippingModeId: {
    type: String,
    required: false,
  },
  hasPromotion: {
    type: Boolean,
    default: false,
  },
  userPromotionCode: {
    type: String,
    required: false,
  },
  promotions: [{
    code: String,
    description: String,
    amount: Number,
    currency: String,
    language: String,
    descriptionLanguage: String,
    userPromotionCode: String,
  }],
  products: [{
    id: {
      type: String,
    },
    skuId: {
      type: String,
    },
    styleId: {
      type: String,
    },
    name: {
      type: String,
      required: 'Product Name is required',
    },
    image: {
      type: String,
      required: 'Product Image is required',
    },
    amount: {
      type: Number,
      default: 1,
    },
    originalUnitPrice: {
      type: Number,
      required: 'originalUnitPrice is required',
    },
    promoUnitPrice: {
      type: Number,
    },
    originalTotalPrice: {
      type: Number,
      required: 'originalTotalPrice (originalUnitPrice * quantity) is required',
    },
    promoTotalPrice: {
      type: Number,
    },
    size: {
      type: String,
      required: 'Product Size is required',
    },
    label: {
      type: String,
      required: 'Label is required',
    },
    colorName: {
      type: String,
      required: 'colorName is required',
    },
    colorPattern: {
      type: String,
      required: 'colorPattern is required',
    },
    fullfillment: {
      type: String,
      enum: Object.keys(config.FULLFILLMENTS).map(key => key),
      default: 'online',
    },
  }],
}, {
  versionKey: false,
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
  },
  toJSON: {
    transform (doc, ret) {
      delete ret.hashed_secret
    },
  },
})

clientSchema.plugin(idValidator)

clientSchema.pre('validate', function preSave (next) {
  if (this.isNew) {
    if (!this.id) this.id = uid(16)
    if (!this.id) this.secret = uid(32)
  }
  next()
})

module.exports = mongoose.model('Selection', clientSchema)
