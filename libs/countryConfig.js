const config = require('config')

const getCountry = (_tenant = 'TH', _countryCode = 'NL') => {
  const tenant = _tenant.toUpperCase()
  const countryCode = _countryCode.toUpperCase()

  const tenantCountries = config.WCS_COUNTRIES[tenant]
  if (!tenantCountries) {
    throw new Error(`Tenant ${tenant} not supported.`)
  }

  const country = tenantCountries[countryCode]
  if (!country) {
    throw new Error(`Country ${countryCode} not supported.`)
  }

  return country
}

const getCountryProperties = (_tenant = 'TH', _countryCode = 'NL', properties) => {
  const toReturn = {}
  const country = getCountry(_tenant, _countryCode)

  if (!properties) {
    throw new Error("Properties not defined. Call 'getCountry' instead.")
  }

  for (let prop of properties) {
    if (!country[prop]) throw new Error(`Property ${prop} not found.`)
    toReturn[prop] = country[prop]
  }

  return toReturn
}

module.exports = {
  getCountry,
  getCountryProperties,
}
