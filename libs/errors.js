var http = require('http')
var util = require('util')

// custom error handler
function CustomError (error) {
  Error.apply(this, arguments)
  Error.captureStackTrace(this, CustomError)

  this.status = error.status
  this.message = error.message || http.STATUS_CODES[error.status] || 'Error'

  if (error.errorCode) {
    this.errorCode = error.errorCode
  }
}

util.inherits(CustomError, Error)

CustomError.prototype.name = 'CustomError'

exports.CustomError = CustomError
