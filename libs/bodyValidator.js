const joi = require('@hapi/joi')

const validateBody = (body, schema) => {
  if (!body || !schema) {
    throw new Error('Body and schema are required.')
  }
  const { error, value } = joi.validate(body, schema)
  if (error) {
    return false
  }
  return value
}

module.exports = {
  validateBody,
}
