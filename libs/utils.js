const config = require('config')
const fs = require('fs')

class Utils {
  buildWCSUrl (url, storeId, countryCode, tenant) {
    tenant = tenant || 'TH'

    return url
      .replace('STORE_ID', storeId)
      .replace('LANG_ID', config.WCS_COUNTRIES[tenant][countryCode].langId)
      .replace('CATALOG_ID', config.WCS_COUNTRIES[tenant][countryCode].catalogId)
  }

  readTemplate (path) {
    return fs.readFileSync(path, 'utf8')
  }

  getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }
}

module.exports = new Utils()
