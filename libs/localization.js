const i18n = require('i18n')

class LocalizationService {
  constructor (opts) {
    if (LocalizationService.instance) {
      return LocalizationService.instance
    }

    // configure i18n
    this.i18n = {}
    i18n.configure({
      directory: './config/locales',
      extension: '.json',
      defaultLocale: 'en_GB',
      updateFiles: false,
      locales: [
        'en_GB',
        'de_DE',
        'nl_NL',
        'it_IT',
        'ru_RU',
        'fr_FR',
      ],
      modes: [
        'query',
        'url',
      ],
      api: {
        '__': 'translate',
      },
      register: this.i18n,
    })

    LocalizationService.instance = this
    return this
  }

  setLocale (locale) {
    if (locale) this.i18n.setLocale(locale)
  }

  translate (phrase, locale) {
    if (locale) {
      return this.i18n.translate({ phrase, locale })
    }

    return this.i18n.translate(phrase)
  }
}

module.exports = new LocalizationService()
