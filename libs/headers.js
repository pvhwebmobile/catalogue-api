const parseAuthorizationBasicHeader = (header) => {
  if (!header) {
    return
  }

  const p = header.split(' ')
  if (p.length !== 2) {
    return
  }

  const [type, token] = p

  if (type !== 'Basic') {
    return
  }

  const buffer = Buffer.from(token, 'base64')
  return buffer.toString('ascii')
}

const parseAuthorizationBearerHeader = (header) => {
  if (!header) {
    return
  }

  const p = header.split(' ')
  if (p.length !== 2) {
    return
  }

  const [type, token] = p

  if (type !== 'Bearer') {
    return
  }

  return token
}

const parseAuthorizationHeader = (ctx, type = 'bearer') => {
  const header = parseHeader(ctx, 'authorization')

  if (!header) return

  let token
  switch (type) {
    case 'basic': token = parseAuthorizationBasicHeader(header); break
    case 'bearer': token = parseAuthorizationBearerHeader(header); break
    default: return
  }

  return token
}

const parseUserAgentHeader = (ctx) => parseHeader(ctx, 'user-agent')

const parseHeader = (ctx, key) => {
  if (!ctx.header || !ctx.header[key]) {
    return
  }

  return ctx.header[key]
}

module.exports = {
  parseAuthorizationBasicHeader,
  parseAuthorizationBearerHeader,
  parseAuthorizationHeader,
  parseUserAgentHeader,
  parseHeader,
}
