const { parseUserAgentHeader } = require('./headers')

const userAgentMiddleware = (ctx, next) => {
  const userAgent = parseUserAgentHeader(ctx) || 'anonymouse <:3)~~'

  const sotfClient = /^sotf.*/i
  const itommyClient = /^itommy.*/i
  const icalvinClient = /^icalvin.*/i

  if (itommyClient.test(userAgent)) {
    ctx.state.client = 'itommy'
  } else if (icalvinClient.test(userAgent)) {
    ctx.state.client = 'icalvin'
  } else if (sotfClient.test(userAgent)) {
    ctx.state.client = 'sotf'
  }

  ctx.state.userAgent = userAgent
  ctx.set('X-User-Agent', userAgent)
  return next()
}

module.exports = {
  userAgentMiddleware,
}
