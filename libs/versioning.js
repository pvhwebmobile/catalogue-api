const semver = require('semver')
const config = require('config')

const clientVersionRequestMiddleware = async (ctx, next) => {
  const { headers } = ctx.request
  let acceptVersion = semver.coerce(headers['accept-version'] || config.MINIMUM_VERSION)
  if (!semver.satisfies(acceptVersion, config.SUPPORTED_VERSIONS.join(' || '))) {
    acceptVersion = config.MINIMUM_VERSION
  }
  ctx.state.acceptVersion = acceptVersion.toString()
  await next()
}

const serverVersionRequestMiddleware = async (ctx, next) => {
  const serverVersion = semver.coerce(config.SERVER_VERSION)
  ctx.state.serverVersion = serverVersion.toString()
  ctx.set('Content-Version', serverVersion.toString())
  await next()
}

const versionCheck = (requestedVersion, allowedVersions) => {
  const allowedSemvers = allowedVersions.join(' || ')
  const requestedSemver = semver.coerce(requestedVersion)
  return semver.satisfies(requestedSemver, allowedSemvers)
}

module.exports = {
  clientVersionRequestMiddleware,
  serverVersionRequestMiddleware,
  versionCheck,
}
