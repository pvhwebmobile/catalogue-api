const jwt = require('jsonwebtoken')
const config = require('config')

const {
  parseAuthorizationHeader,
} = require('./headers')

const jwtSecret = config.SECRET
const { username, password } = config.SERVER_CREDENTIALS

const basicAuth = (userPass, su, sp) => userPass === `${su}:${sp}`

const bearerAuth = (token, secret) => {
  try {
    jwt.verify(token, secret)
    return true
  } catch (err) {
    return false
  }
}

const headerBasicAuthMiddleware = (ctx, next) => {
  const token = parseAuthorizationHeader(ctx, 'basic')
  if (!token) {
    ctx.throw(401, 'Authorization Error: is missing or formatted incorrectly. Expected \'Authorization: Basic <token>\'')
    return
  }

  const authd = basicAuth(token, username, password)

  if (!authd) {
    ctx.throw(401, 'Authorization Error')
    return
  }

  return next()
}

const headerBearerAuthMiddleware = (ctx, next) => {
  const bearerToken = parseAuthorizationHeader(ctx, 'bearer')
  if (!bearerToken) {
    ctx.throw(401, 'Authorization Error: header is missing or formatted incorrectly. Expected \'Authorization: Bearer <token>\'')
    return
  }

  const authd = bearerAuth(bearerToken, jwtSecret)

  if (!authd) {
    ctx.throw(401, 'Authorization Error')
    return
  }

  return next()
}

module.exports = {
  basicAuth,
  bearerAuth,
  headerBasicAuthMiddleware,
  headerBearerAuthMiddleware,
}
