const config = require('config')
const { CustomError } = require('./errors')

/**
 * Builds a URL based on environment and parameters provided
 *
 * @param _urlObject <URLObject> Expects an object formatted like:
 * URLObject: { type: String, url: String, parameters: Array<Parameter> }
 * Parameter: { name: String, required: Boolean }
 * @param _parameters <Object> Expects an object with keys and values
 * to replace in the url provided in the urlObject
 * @param _countryCode <String> 2-digit country code, defaults to nl.
 * @return url <String> Final URL with all parameters replaced with
 * provided parameters
 */
const buildURL = (_urlObject, _parameters, _tenant = 'th', _countryCode = 'nl') => {
  let tenant = _tenant.toUpperCase()
  let countryCode = _countryCode.toLowerCase()

  let { type, url, parameters } = _urlObject
  if (!type || !url || !parameters) throw new Error('Invalid URL object.')

  let baseURL
  switch (type) {
    case 'WCS': baseURL = config.BASE_URLS[tenant].WCS; break
    case 'SOLR': baseURL = config.BASE_URLS[tenant].SOLR; break
    case 'FH': baseURL = config.BASE_URLS[tenant].FH; break
    case 'FH_SEARCH': baseURL = config.BASE_URLS[tenant].FH_SEARCH; break
    case 'S7': baseURL = config.BASE_URLS[tenant].S7; break
    default: throw new Error(`Unsupported URL type ${type} provided.`)
  }

  // this is a shitty fix because WCS uses calvinklein.uk for lower environments
  // and calvinklein.co.uk for production... sorry
  if (
    tenant === 'CK' &&
    countryCode === 'uk' &&
    process.env.NODE_ENV === 'production'
  ) {
    url = url.replace('BASE_URL', baseURL).replace('CC', 'co.uk')
  } else {
    url = url.replace('BASE_URL', baseURL).replace('CC', countryCode)
  }

  for (let param of parameters) {
    const { name, required } = param
    if (required && !_parameters[name]) {
      throw new Error(`Missing required parameter ${name}.`)
    }
    url = url.replace(name, _parameters[name])
  }
  return encodeURI(url)
}

const validateURL = (_url, _expectedUrlParameters, _expectedQueryParameters) => {
  const url = _url
  if (!url) throw new CustomError({ status: 400, message: 'URL is malformed.' })

  const [ urlWithParameters, urlWithQueryParameters ] = url.split('?')
  let urlParameters = []
  let queryParameters = []

  if (urlWithParameters) {
    urlParameters = urlWithParameters.split('/').filter(x => x.length > 0)
  }

  if (urlWithQueryParameters) {
    queryParameters = urlWithQueryParameters.split('&').map((qp) => qp.split('='))
  }

  if (urlParameters.length !== _expectedUrlParameters.length) {
    throw new CustomError({ status: 400, message: 'URL has unexpected URL parameters.' })
  }

  if (queryParameters.length > Object.keys(_expectedQueryParameters).length) {
    throw new CustomError({ status: 400, message: 'URL has unexpected query parameters.' })
  }

  // expected URL parameter is a regex of what the URL parameter should be
  // order matters
  for (let u = 0; u < urlParameters.length; u += 1) {
    const provided = urlParameters[u]
    const expected = _expectedUrlParameters[u]
    const result = expected.test(provided)
    if (!result) throw new CustomError({ status: 400, message: `URL parameter ${provided} does not match ${expected}.` })
  }

  // expected query parameter is a { pattern: RegExp, required: Boolean }
  // provided query parameter is a [ key, value ]
  // order doesnt matter
  const eqpKeys = Object.keys(_expectedQueryParameters)
  for (let q = 0; q < eqpKeys.length; q += 1) {
    let found = false
    const eqpKey = eqpKeys[q]
    let { pattern, required } = _expectedQueryParameters[eqpKey]
    for (let _q = 0; _q < queryParameters.length && !found; _q += 1) {
      if (queryParameters[_q][0] === eqpKey) {
        found = queryParameters[_q]
      }
    }
    if (!found && required) {
      throw new CustomError({ status: 400, message: `Required query parameter ${eqpKey} was not found.` })
    }

    if (found) {
      const [providedKey, providedValue] = found
      const result = pattern.test(providedValue)
      if (!result) throw new CustomError({ status: 400, message: `Query parameter ${providedKey} does not match ${pattern}.` })
    }
  }

  return { urlParameters, queryParameters }
}

const validateURLMiddleware = (expectedUrlParameters, expectedQueryParameters) =>
  (ctx, next) => {
    validateURL(ctx.url, expectedUrlParameters, expectedQueryParameters)
    return next()
  }

module.exports = {
  buildURL,
  validateURL,
  validateURLMiddleware,
}
