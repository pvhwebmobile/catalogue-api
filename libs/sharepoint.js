const formatOpeningHours = (openingHoursRaw) => {
  const openingHours = {
    raw: openingHoursRaw,
    monday: {
      isOpen: false,
      opensAt: null,
      closesAt: null,
    },
    tuesday: {
      isOpen: false,
      opensAt: null,
      closesAt: null,
    },
    wednesday: {
      isOpen: false,
      opensAt: null,
      closesAt: null,
    },
    thursday: {
      isOpen: false,
      opensAt: null,
      closesAt: null,
    },
    friday: {
      isOpen: false,
      opensAt: null,
      closesAt: null,
    },
    saturday: {
      isOpen: false,
      opensAt: null,
      closesAt: null,
    },
    sunday: {
      isOpen: false,
      opensAt: null,
      closesAt: null,
    },
  }

  const openingHoursRegex = /^((MON|TUE|WED|THU|FRI|SAT|SUN) (\d{1,2}:\d{1,2}-\d{1,2}:\d{1,2}|CLOSED)(~~)){0,6}((MON|TUE|WED|THU|FRI|SAT|SUN) (\d{1,2}:\d{1,2}-\d{1,2}:\d{1,2}|CLOSED))$/i

  if (
    !openingHoursRegex.test(openingHoursRaw) ||
    !openingHoursRaw ||
    openingHoursRaw.length < 1
  ) {
    return openingHours
  }

  const days = openingHoursRaw.split('~~')
  const dayNames = {
    MON: 'monday',
    TUE: 'tuesday',
    WED: 'wednesday',
    THU: 'thursday',
    FRI: 'friday',
    SAT: 'saturday',
    SUN: 'sunday',
  }
  for (let day of days) {
    const [name, time] = day.split(' ')
    if (time.toUpperCase() !== 'CLOSED') {
      const [opensAt, closesAt] = time.split('-')
      openingHours[dayNames[name]] = {
        isOpen: true,
        opensAt,
        closesAt,
      }
    }
  }

  return openingHours
}

module.exports = {
  formatOpeningHours,
}
