const mongoose = require('mongoose')
const config = require('config/index')
const logger = require('./logger')

class Database {
  constructor () {
    mongoose.Promise = require('bluebird')
    mongoose.set('useFindAndModify', false)
  }

  connect () {
    return new Promise(async (resolve, reject) => {
      let options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        connectTimeoutMS: 5000,
        autoIndex: false, // Don't build indexes
        poolSize: 10, // Maintain up to 10 socket connections
        // If not connected, return errors immediately rather than waiting for reconnect
        bufferMaxEntries: 0,
      }

      mongoose.connect(config.DB.uri, options)
      mongoose.connection
        .on('error', error => { logger.error(error); reject(error) })
        .on('open', () => { logger.info('database connected'); resolve(mongoose.connections[0]) })
        .on('close', () => logger.info('database closed'))
    })
  }
}

module.exports = new Database()
