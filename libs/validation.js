const config = require('config')
const CustomError = require('../libs/errors').CustomError

class Validations {
  isStoreIdValid (tenant, countryCode, storeId) {
    countryCode = countryCode.toUpperCase()
    if (!config.WCS_COUNTRIES[tenant]) {
      throw new CustomError({ message: config.ERRORS.STORES.WRONG_TENANT, status: config.ERRORS.STORES.STATUS })
    }
    if (!config.WCS_COUNTRIES[tenant][countryCode]) {
      throw new CustomError({ message: config.ERRORS.STORES.WRONG_COUNTRY_CODE, status: config.ERRORS.STORES.STATUS })
    }
    if (config.WCS_COUNTRIES[tenant][countryCode].storeId !== storeId) {
      throw new CustomError({ message: config.ERRORS.STORES.WRONG_STORE_ID, status: config.ERRORS.STORES.STATUS })
    }

    return true
  }

  isOrderIdValid (orderId, WCTrustedToken, WCToken) {
    // TODO: use sessions to store WCTrustedToken adn WCToken in the API; you don't want to send them to the client
    // todo: trow error
    if (!(orderId && WCTrustedToken && WCToken)) {
      throw new CustomError({ message: 'orderId && WCTrustedToken && WCToken = false', status: config.ERRORS.STORES.STATUS })
    }
    return true
  }

  isItemValid (productId, quantity) {
    // TODO: what to check?
    // todo: throw error
    if (!(productId && quantity)) {
      throw new CustomError({ message: 'orderId && WCTrustedToken && WCToken = false', status: config.ERRORS.STORES.STATUS })
    }
    return true
  }

  isAddressValid (address) {
    // required fields
    if (!(address.firstName &&
            address.lastName &&
            address.addressType &&
            address.zipCode &&
            address.addressLine && Array.isArray(address.addressLine) && address.addressLine.length &&
            address.city &&
            address.country &&
            address.nickName &&
            address.email1)) {
      throw new CustomError({ message: 'Address not complete', status: config.ERRORS.STORES.STATUS })
    }
    return true
  }

  isTransactionValid (transaction) {
    // required fields
    // TODO: what else to check?
    if (!(transaction.ewcResponse &&
              transaction.ewcMaskedNumber &&
              transaction.errorViewName &&
              transaction.totalBalance &&
              transaction.authToken &&
              transaction.payMethodId &&
              transaction.paymentMethodName &&
              // transaction.idealFinancialInstitution &&
              transaction.billing_address_id &&
              // transaction.javascriptUrl &&
              transaction.piAmount &&
              transaction.ewcPaymentName &&
              transaction.catalogId &&
              // transaction.payType &&
              transaction.langId &&
              transaction.billingAddressId &&
              // transaction.rdReturnCode &&
              transaction.URL &&
              transaction.storeId &&
              transaction.orderId &&
              transaction.appName)) {
      throw new CustomError({ message: 'Transaction not complete', status: config.ERRORS.STORES.STATUS })
    }
    return true
  }

  isWCSResponseValid (responseData) {
    if (responseData.errors) {
      let message = (Array.isArray(responseData.errors) && responseData.errors.length && responseData.errors[0] && (responseData.errors[0].errorMessage || responseData.errors[0])) || 'WCS call failed'
      throw new CustomError({ message: message, status: config.ERRORS.STORES.STATUS })
    }
    if (responseData.code) {
      let message = responseData.description || 'Add transaction failed'
      message = responseData.reasonCode + ' ' + message
      throw new CustomError({ message: message, status: config.ERRORS.STORES.STATUS })
    }
    return true
  }
}

module.exports = Validations
