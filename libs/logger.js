const { createLogger, format, transports } = require('winston')

let availableTransports = [
  new transports.File({ filename: 'eis-error.log', level: 'error' }),
  new transports.File({ filename: 'eis-all.log' }),
]

if (!process.env.JEST) availableTransports.push(new transports.Console())

const logger = createLogger({
  level: 'info',
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    format.errors({ stack: true }),
    format.splat(),
    format.simple(),
  ),
  defaultMeta: { service: 'eis-app-be' },
  transports: availableTransports,
})

module.exports = logger
