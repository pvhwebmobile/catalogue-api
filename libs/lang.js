const {
  langIdFromCountryCode,
  localeFromLangId,
} = require('./countryUtils')

const localizationService = require('./localization')

const languageMiddleware = (ctx, next) => {
  let requestedLanguage = ctx.getLocaleFromQuery()
  if (!requestedLanguage) {
    requestedLanguage = ctx.getLocaleFromUrl({ offset: 1 })
  }
  if (requestedLanguage && requestedLanguage.length !== 2) {
    requestedLanguage = ctx.getLocaleFromUrl({ offset: 0 })
  }

  if (requestedLanguage) {
    requestedLanguage = requestedLanguage.toLowerCase()
  }

  const langId = langIdFromCountryCode(requestedLanguage)
  const locale = localeFromLangId(langId)

  ctx.state.langId = langId
  ctx.state.locale = locale
  ctx.i18n.locale = locale
  if (locale) localizationService.setLocale(locale)

  ctx.set('Content-Language', locale)

  return next()
}

module.exports = {
  languageMiddleware,
}
