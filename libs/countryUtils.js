const countryLookup = {
  'Afghanistan': {
    name: 'Afghanistan',
    countryCode: 'AF',
  },
  'Åland Islands': {
    name: 'Åland Islands',
    countryCode: 'AX',
  },
  'Albania': {
    name: 'Albania',
    countryCode: 'AL',
  },
  'Algeria': {
    name: 'Algeria',
    countryCode: 'DZ',
  },
  'American Samoa': {
    name: 'American Samoa',
    countryCode: 'AS',
  },
  'Andorra': {
    name: 'Andorra',
    countryCode: 'AD',
  },
  'Angola': {
    name: 'Angola',
    countryCode: 'AO',
  },
  'Anguilla': {
    name: 'Anguilla',
    countryCode: 'AI',
  },
  'Antarctica': {
    name: 'Antarctica',
    countryCode: 'AQ',
  },
  'Antigua and Barbuda': {
    name: 'Antigua and Barbuda',
    countryCode: 'AG',
  },
  'Argentina': {
    name: 'Argentina',
    countryCode: 'AR',
  },
  'Armenia': {
    name: 'Armenia',
    countryCode: 'AM',
  },
  'Aruba': {
    name: 'Aruba',
    countryCode: 'AW',
  },
  'Australia': {
    name: 'Australia',
    countryCode: 'AU',
  },
  'Austria': {
    name: 'Austria',
    countryCode: 'AT',
  },
  'Azerbaijan': {
    name: 'Azerbaijan',
    countryCode: 'AZ',
  },
  'Bahamas': {
    name: 'Bahamas',
    countryCode: 'BS',
  },
  'Bahrain': {
    name: 'Bahrain',
    countryCode: 'BH',
  },
  'Bangladesh': {
    name: 'Bangladesh',
    countryCode: 'BD',
  },
  'Barbados': {
    name: 'Barbados',
    countryCode: 'BB',
  },
  'Belarus': {
    name: 'Belarus',
    countryCode: 'BY',
  },
  'Belgium': {
    name: 'Belgium',
    countryCode: 'BE',
  },
  'Belize': {
    name: 'Belize',
    countryCode: 'BZ',
  },
  'Benin': {
    name: 'Benin',
    countryCode: 'BJ',
  },
  'Bermuda': {
    name: 'Bermuda',
    countryCode: 'BM',
  },
  'Bhutan': {
    name: 'Bhutan',
    countryCode: 'BT',
  },
  'Bolivia (Plurinational State of)': {
    name: 'Bolivia (Plurinational State of)',
    countryCode: 'BO',
  },
  'Bonaire, Sint Eustatius and Saba': {
    name: 'Bonaire, Sint Eustatius and Saba',
    countryCode: 'BQ',
  },
  'Bosnia and Herzegovina': {
    name: 'Bosnia and Herzegovina',
    countryCode: 'BA',
  },
  'Botswana': {
    name: 'Botswana',
    countryCode: 'BW',
  },
  'Bouvet Island': {
    name: 'Bouvet Island',
    countryCode: 'BV',
  },
  'Brazil': {
    name: 'Brazil',
    countryCode: 'BR',
  },
  'British Indian Ocean Territory': {
    name: 'British Indian Ocean Territory',
    countryCode: 'IO',
  },
  'Brunei Darussalam': {
    name: 'Brunei Darussalam',
    countryCode: 'BN',
  },
  'Bulgaria': {
    name: 'Bulgaria',
    countryCode: 'BG',
  },
  'Burkina Faso': {
    name: 'Burkina Faso',
    countryCode: 'BF',
  },
  'Burundi': {
    name: 'Burundi',
    countryCode: 'BI',
  },
  'Cabo Verde': {
    name: 'Cabo Verde',
    countryCode: 'CV',
  },
  'Cambodia': {
    name: 'Cambodia',
    countryCode: 'KH',
  },
  'Cameroon': {
    name: 'Cameroon',
    countryCode: 'CM',
  },
  'Canada': {
    name: 'Canada',
    countryCode: 'CA',
  },
  'Cayman Islands': {
    name: 'Cayman Islands',
    countryCode: 'KY',
  },
  'Central African Republic': {
    name: 'Central African Republic',
    countryCode: 'CF',
  },
  'Chad': {
    name: 'Chad',
    countryCode: 'TD',
  },
  'Chile': {
    name: 'Chile',
    countryCode: 'CL',
  },
  'China': {
    name: 'China',
    countryCode: 'CN',
  },
  'Christmas Island': {
    name: 'Christmas Island',
    countryCode: 'CX',
  },
  'Cocos (Keeling) Islands': {
    name: 'Cocos (Keeling) Islands',
    countryCode: 'CC',
  },
  'Colombia': {
    name: 'Colombia',
    countryCode: 'CO',
  },
  'Comoros': {
    name: 'Comoros',
    countryCode: 'KM',
  },
  'Congo': {
    name: 'Congo',
    countryCode: 'CG',
  },
  'Congo, Democratic Republic of the': {
    name: 'Congo, Democratic Republic of the',
    countryCode: 'CD',
  },
  'Cook Islands': {
    name: 'Cook Islands',
    countryCode: 'CK',
  },
  'Costa Rica': {
    name: 'Costa Rica',
    countryCode: 'CR',
  },
  "Côte d'Ivoire": {
    name: "Côte d'Ivoire",
    countryCode: 'CI',
  },
  'Croatia': {
    name: 'Croatia',
    countryCode: 'HR',
  },
  'Cuba': {
    name: 'Cuba',
    countryCode: 'CU',
  },
  'Curaçao': {
    name: 'Curaçao',
    countryCode: 'CW',
  },
  'Cyprus': {
    name: 'Cyprus',
    countryCode: 'CY',
  },
  'Czech Republic': {
    name: 'Czech Republic',
    countryCode: 'CZ',
  },
  'Denmark': {
    name: 'Denmark',
    countryCode: 'DK',
  },
  'Djibouti': {
    name: 'Djibouti',
    countryCode: 'DJ',
  },
  'Dominica': {
    name: 'Dominica',
    countryCode: 'DM',
  },
  'Dominican Republic': {
    name: 'Dominican Republic',
    countryCode: 'DO',
  },
  'Ecuador': {
    name: 'Ecuador',
    countryCode: 'EC',
  },
  'Egypt': {
    name: 'Egypt',
    countryCode: 'EG',
  },
  'El Salvador': {
    name: 'El Salvador',
    countryCode: 'SV',
  },
  'Equatorial Guinea': {
    name: 'Equatorial Guinea',
    countryCode: 'GQ',
  },
  'Eritrea': {
    name: 'Eritrea',
    countryCode: 'ER',
  },
  'Estonia': {
    name: 'Estonia',
    countryCode: 'EE',
  },
  'Eswatini': {
    name: 'Eswatini',
    countryCode: 'SZ',
  },
  'Ethiopia': {
    name: 'Ethiopia',
    countryCode: 'ET',
  },
  'Falkland Islands (Malvinas)': {
    name: 'Falkland Islands (Malvinas)',
    countryCode: 'FK',
  },
  'Faroe Islands': {
    name: 'Faroe Islands',
    countryCode: 'FO',
  },
  'Fiji': {
    name: 'Fiji',
    countryCode: 'FJ',
  },
  'Finland': {
    name: 'Finland',
    countryCode: 'FI',
  },
  'France': {
    name: 'France',
    countryCode: 'FR',
  },
  'French Guiana': {
    name: 'French Guiana',
    countryCode: 'GF',
  },
  'French Polynesia': {
    name: 'French Polynesia',
    countryCode: 'PF',
  },
  'French Southern Territories': {
    name: 'French Southern Territories',
    countryCode: 'TF',
  },
  'Gabon': {
    name: 'Gabon',
    countryCode: 'GA',
  },
  'Gambia': {
    name: 'Gambia',
    countryCode: 'GM',
  },
  'Georgia': {
    name: 'Georgia',
    countryCode: 'GE',
  },
  'Germany': {
    name: 'Germany',
    countryCode: 'DE',
  },
  'Ghana': {
    name: 'Ghana',
    countryCode: 'GH',
  },
  'Gibraltar': {
    name: 'Gibraltar',
    countryCode: 'GI',
  },
  'Greece': {
    name: 'Greece',
    countryCode: 'GR',
  },
  'Greenland': {
    name: 'Greenland',
    countryCode: 'GL',
  },
  'Grenada': {
    name: 'Grenada',
    countryCode: 'GD',
  },
  'Guadeloupe': {
    name: 'Guadeloupe',
    countryCode: 'GP',
  },
  'Guam': {
    name: 'Guam',
    countryCode: 'GU',
  },
  'Guatemala': {
    name: 'Guatemala',
    countryCode: 'GT',
  },
  'Guernsey': {
    name: 'Guernsey',
    countryCode: 'GG',
  },
  'Guinea': {
    name: 'Guinea',
    countryCode: 'GN',
  },
  'Guinea-Bissau': {
    name: 'Guinea-Bissau',
    countryCode: 'GW',
  },
  'Guyana': {
    name: 'Guyana',
    countryCode: 'GY',
  },
  'Haiti': {
    name: 'Haiti',
    countryCode: 'HT',
  },
  'Heard Island and McDonald Islands': {
    name: 'Heard Island and McDonald Islands',
    countryCode: 'HM',
  },
  'Holy See': {
    name: 'Holy See',
    countryCode: 'VA',
  },
  'Honduras': {
    name: 'Honduras',
    countryCode: 'HN',
  },
  'Hong Kong': {
    name: 'Hong Kong',
    countryCode: 'HK',
  },
  'Hungary': {
    name: 'Hungary',
    countryCode: 'HU',
  },
  'Iceland': {
    name: 'Iceland',
    countryCode: 'IS',
  },
  'India': {
    name: 'India',
    countryCode: 'IN',
  },
  'Indonesia': {
    name: 'Indonesia',
    countryCode: 'ID',
  },
  'Iran (Islamic Republic of)': {
    name: 'Iran (Islamic Republic of)',
    countryCode: 'IR',
  },
  'Iraq': {
    name: 'Iraq',
    countryCode: 'IQ',
  },
  'Ireland': {
    name: 'Ireland',
    countryCode: 'IE',
  },
  'Isle of Man': {
    name: 'Isle of Man',
    countryCode: 'IM',
  },
  'Israel': {
    name: 'Israel',
    countryCode: 'IL',
  },
  'Italy': {
    name: 'Italy',
    countryCode: 'IT',
  },
  'Jamaica': {
    name: 'Jamaica',
    countryCode: 'JM',
  },
  'Japan': {
    name: 'Japan',
    countryCode: 'JP',
  },
  'Jersey': {
    name: 'Jersey',
    countryCode: 'JE',
  },
  'Jordan': {
    name: 'Jordan',
    countryCode: 'JO',
  },
  'Kazakhstan': {
    name: 'Kazakhstan',
    countryCode: 'KZ',
  },
  'Kenya': {
    name: 'Kenya',
    countryCode: 'KE',
  },
  'Kiribati': {
    name: 'Kiribati',
    countryCode: 'KI',
  },
  'Kosovo': {
    name: 'Kosovo',
    countryCode: 'XK',
  },
  "Korea (Democratic People's Republic of)": {
    name: "Korea (Democratic People's Republic of)",
    countryCode: 'KP',
  },
  'Korea, Republic of': {
    name: 'Korea, Republic of',
    countryCode: 'KR',
  },
  'Kuwait': {
    name: 'Kuwait',
    countryCode: 'KW',
  },
  'Kyrgyzstan': {
    name: 'Kyrgyzstan',
    countryCode: 'KG',
  },
  "Lao People's Democratic Republic": {
    name: "Lao People's Democratic Republic",
    countryCode: 'LA',
  },
  'Latvia': {
    name: 'Latvia',
    countryCode: 'LV',
  },
  'Lebanon': {
    name: 'Lebanon',
    countryCode: 'LB',
  },
  'Lesotho': {
    name: 'Lesotho',
    countryCode: 'LS',
  },
  'Liberia': {
    name: 'Liberia',
    countryCode: 'LR',
  },
  'Libya': {
    name: 'Libya',
    countryCode: 'LY',
  },
  'Liechtenstein': {
    name: 'Liechtenstein',
    countryCode: 'LI',
  },
  'Lithuania': {
    name: 'Lithuania',
    countryCode: 'LT',
  },
  'Luxembourg': {
    name: 'Luxembourg',
    countryCode: 'LU',
  },
  'Macao': {
    name: 'Macao',
    countryCode: 'MO',
  },
  'Madagascar': {
    name: 'Madagascar',
    countryCode: 'MG',
  },
  'Malawi': {
    name: 'Malawi',
    countryCode: 'MW',
  },
  'Malaysia': {
    name: 'Malaysia',
    countryCode: 'MY',
  },
  'Maldives': {
    name: 'Maldives',
    countryCode: 'MV',
  },
  'Mali': {
    name: 'Mali',
    countryCode: 'ML',
  },
  'Malta': {
    name: 'Malta',
    countryCode: 'MT',
  },
  'Marshall Islands': {
    name: 'Marshall Islands',
    countryCode: 'MH',
  },
  'Martinique': {
    name: 'Martinique',
    countryCode: 'MQ',
  },
  'Mauritania': {
    name: 'Mauritania',
    countryCode: 'MR',
  },
  'Mauritius': {
    name: 'Mauritius',
    countryCode: 'MU',
  },
  'Mayotte': {
    name: 'Mayotte',
    countryCode: 'YT',
  },
  'Mexico': {
    name: 'Mexico',
    countryCode: 'MX',
  },
  'Micronesia (Federated States of)': {
    name: 'Micronesia (Federated States of)',
    countryCode: 'FM',
  },
  'Moldova, Republic of': {
    name: 'Moldova, Republic of',
    countryCode: 'MD',
  },
  'Monaco': {
    name: 'Monaco',
    countryCode: 'MC',
  },
  'Mongolia': {
    name: 'Mongolia',
    countryCode: 'MN',
  },
  'Montenegro': {
    name: 'Montenegro',
    countryCode: 'ME',
  },
  'Montserrat': {
    name: 'Montserrat',
    countryCode: 'MS',
  },
  'Morocco': {
    name: 'Morocco',
    countryCode: 'MA',
  },
  'Mozambique': {
    name: 'Mozambique',
    countryCode: 'MZ',
  },
  'Myanmar': {
    name: 'Myanmar',
    countryCode: 'MM',
  },
  'Namibia': {
    name: 'Namibia',
    countryCode: 'NA',
  },
  'Nauru': {
    name: 'Nauru',
    countryCode: 'NR',
  },
  'Nepal': {
    name: 'Nepal',
    countryCode: 'NP',
  },
  'Netherlands': {
    name: 'Netherlands',
    countryCode: 'NL',
  },
  'New Caledonia': {
    name: 'New Caledonia',
    countryCode: 'NC',
  },
  'New Zealand': {
    name: 'New Zealand',
    countryCode: 'NZ',
  },
  'Nicaragua': {
    name: 'Nicaragua',
    countryCode: 'NI',
  },
  'Niger': {
    name: 'Niger',
    countryCode: 'NE',
  },
  'Nigeria': {
    name: 'Nigeria',
    countryCode: 'NG',
  },
  'Niue': {
    name: 'Niue',
    countryCode: 'NU',
  },
  'Norfolk Island': {
    name: 'Norfolk Island',
    countryCode: 'NF',
  },
  'North Macedonia': {
    name: 'North Macedonia',
    countryCode: 'MK',
  },
  'Northern Mariana Islands': {
    name: 'Northern Mariana Islands',
    countryCode: 'MP',
  },
  'Norway': {
    name: 'Norway',
    countryCode: 'NO',
  },
  'Oman': {
    name: 'Oman',
    countryCode: 'OM',
  },
  'Pakistan': {
    name: 'Pakistan',
    countryCode: 'PK',
  },
  'Palau': {
    name: 'Palau',
    countryCode: 'PW',
  },
  'Palestine, State of': {
    name: 'Palestine, State of',
    countryCode: 'PS',
  },
  'Panama': {
    name: 'Panama',
    countryCode: 'PA',
  },
  'Papua New Guinea': {
    name: 'Papua New Guinea',
    countryCode: 'PG',
  },
  'Paraguay': {
    name: 'Paraguay',
    countryCode: 'PY',
  },
  'Peru': {
    name: 'Peru',
    countryCode: 'PE',
  },
  'Philippines': {
    name: 'Philippines',
    countryCode: 'PH',
  },
  'Pitcairn': {
    name: 'Pitcairn',
    countryCode: 'PN',
  },
  'Poland': {
    name: 'Poland',
    countryCode: 'PL',
  },
  'Portugal': {
    name: 'Portugal',
    countryCode: 'PT',
  },
  'Puerto Rico': {
    name: 'Puerto Rico',
    countryCode: 'PR',
  },
  'Qatar': {
    name: 'Qatar',
    countryCode: 'QA',
  },
  'Réunion': {
    name: 'Réunion',
    countryCode: 'RE',
  },
  'Romania': {
    name: 'Romania',
    countryCode: 'RO',
  },
  'Russia': {
    name: 'Russia',
    countryCode: 'RU',
  },
  'Rwanda': {
    name: 'Rwanda',
    countryCode: 'RW',
  },
  'Saint Barthélemy': {
    name: 'Saint Barthélemy',
    countryCode: 'BL',
  },
  'Saint Helena, Ascension and Tristan da Cunha': {
    name: 'Saint Helena, Ascension and Tristan da Cunha',
    countryCode: 'SH',
  },
  'Saint Kitts and Nevis': {
    name: 'Saint Kitts and Nevis',
    countryCode: 'KN',
  },
  'Saint Lucia': {
    name: 'Saint Lucia',
    countryCode: 'LC',
  },
  'Saint Martin (French part)': {
    name: 'Saint Martin (French part)',
    countryCode: 'MF',
  },
  'Saint Pierre and Miquelon': {
    name: 'Saint Pierre and Miquelon',
    countryCode: 'PM',
  },
  'Saint Vincent and the Grenadines': {
    name: 'Saint Vincent and the Grenadines',
    countryCode: 'VC',
  },
  'Samoa': {
    name: 'Samoa',
    countryCode: 'WS',
  },
  'San Marino': {
    name: 'San Marino',
    countryCode: 'SM',
  },
  'Sao Tome and Principe': {
    name: 'Sao Tome and Principe',
    countryCode: 'ST',
  },
  'Saudi Arabia': {
    name: 'Saudi Arabia',
    countryCode: 'SA',
  },
  'Senegal': {
    name: 'Senegal',
    countryCode: 'SN',
  },
  'Serbia': {
    name: 'Serbia',
    countryCode: 'RS',
  },
  'Seychelles': {
    name: 'Seychelles',
    countryCode: 'SC',
  },
  'Sierra Leone': {
    name: 'Sierra Leone',
    countryCode: 'SL',
  },
  'Singapore': {
    name: 'Singapore',
    countryCode: 'SG',
  },
  'Sint Maarten (Dutch part)': {
    name: 'Sint Maarten (Dutch part)',
    countryCode: 'SX',
  },
  'Slovakia': {
    name: 'Slovakia',
    countryCode: 'SK',
  },
  'Slovenia': {
    name: 'Slovenia',
    countryCode: 'SI',
  },
  'Solomon Islands': {
    name: 'Solomon Islands',
    countryCode: 'SB',
  },
  'Somalia': {
    name: 'Somalia',
    countryCode: 'SO',
  },
  'South Africa': {
    name: 'South Africa',
    countryCode: 'ZA',
  },
  'South Georgia and the South Sandwich Islands': {
    name: 'South Georgia and the South Sandwich Islands',
    countryCode: 'GS',
  },
  'South Sudan': {
    name: 'South Sudan',
    countryCode: 'SS',
  },
  'Spain': {
    name: 'Spain',
    countryCode: 'ES',
  },
  'Sri Lanka': {
    name: 'Sri Lanka',
    countryCode: 'LK',
  },
  'Sudan': {
    name: 'Sudan',
    countryCode: 'SD',
  },
  'Suriname': {
    name: 'Suriname',
    countryCode: 'SR',
  },
  'Svalbard and Jan Mayen': {
    name: 'Svalbard and Jan Mayen',
    countryCode: 'SJ',
  },
  'Sweden': {
    name: 'Sweden',
    countryCode: 'SE',
  },
  'Switzerland': {
    name: 'Switzerland',
    countryCode: 'CH',
  },
  'Syrian Arab Republic': {
    name: 'Syrian Arab Republic',
    countryCode: 'SY',
  },
  'Taiwan, Province of China': {
    name: 'Taiwan, Province of China',
    countryCode: 'TW',
  },
  'Tajikistan': {
    name: 'Tajikistan',
    countryCode: 'TJ',
  },
  'Tanzania, United Republic of': {
    name: 'Tanzania, United Republic of',
    countryCode: 'TZ',
  },
  'Thailand': {
    name: 'Thailand',
    countryCode: 'TH',
  },
  'Timor-Leste': {
    name: 'Timor-Leste',
    countryCode: 'TL',
  },
  'Togo': {
    name: 'Togo',
    countryCode: 'TG',
  },
  'Tokelau': {
    name: 'Tokelau',
    countryCode: 'TK',
  },
  'Tonga': {
    name: 'Tonga',
    countryCode: 'TO',
  },
  'Trinidad and Tobago': {
    name: 'Trinidad and Tobago',
    countryCode: 'TT',
  },
  'Tunisia': {
    name: 'Tunisia',
    countryCode: 'TN',
  },
  'Turkey': {
    name: 'Turkey',
    countryCode: 'TR',
  },
  'Turkmenistan': {
    name: 'Turkmenistan',
    countryCode: 'TM',
  },
  'Turks and Caicos Islands': {
    name: 'Turks and Caicos Islands',
    countryCode: 'TC',
  },
  'Tuvalu': {
    name: 'Tuvalu',
    countryCode: 'TV',
  },
  'Uganda': {
    name: 'Uganda',
    countryCode: 'UG',
  },
  'Ukraine': {
    name: 'Ukraine',
    countryCode: 'UA',
  },
  'United Arab Emirates': {
    name: 'United Arab Emirates',
    countryCode: 'AE',
  },
  'United Kingdom': {
    name: 'United Kingdom',
    countryCode: 'UK',
  },
  'United States of America': {
    name: 'United States of America',
    countryCode: 'US',
  },
  'United States Minor Outlying Islands': {
    name: 'United States Minor Outlying Islands',
    countryCode: 'UM',
  },
  'Uruguay': {
    name: 'Uruguay',
    countryCode: 'UY',
  },
  'Uzbekistan': {
    name: 'Uzbekistan',
    countryCode: 'UZ',
  },
  'Vanuatu': {
    name: 'Vanuatu',
    countryCode: 'VU',
  },
  'Venezuela (Bolivarian Republic of)': {
    name: 'Venezuela (Bolivarian Republic of)',
    countryCode: 'VE',
  },
  'Viet Nam': {
    name: 'Viet Nam',
    countryCode: 'VN',
  },
  'Virgin Islands (British)': {
    name: 'Virgin Islands (British)',
    countryCode: 'VG',
  },
  'Virgin Islands (U.S.)': {
    name: 'Virgin Islands (U.S.)',
    countryCode: 'VI',
  },
  'Wallis and Futuna': {
    name: 'Wallis and Futuna',
    countryCode: 'WF',
  },
  'Western Sahara': {
    name: 'Western Sahara',
    countryCode: 'EH',
  },
  'Yemen': {
    name: 'Yemen',
    countryCode: 'YE',
  },
  'Zambia': {
    name: 'Zambia',
    countryCode: 'ZM',
  },
  'Zimbabwe': {
    name: 'Zimbabwe',
    countryCode: 'ZW',
  },
}

const countryCodeLookup = {
  'AF': {
    name: 'Afghanistan',
    countryCode: 'AF',
  },
  'AX': {
    name: 'Åland Islands',
    countryCode: 'AX',
  },
  'AL': {
    name: 'Albania',
    countryCode: 'AL',
  },
  'DZ': {
    name: 'Algeria',
    countryCode: 'DZ',
  },
  'AS': {
    name: 'American Samoa',
    countryCode: 'AS',
  },
  'AD': {
    name: 'Andorra',
    countryCode: 'AD',
  },
  'AO': {
    name: 'Angola',
    countryCode: 'AO',
  },
  'AI': {
    name: 'Anguilla',
    countryCode: 'AI',
  },
  'AQ': {
    name: 'Antarctica',
    countryCode: 'AQ',
  },
  'AG': {
    name: 'Antigua and Barbuda',
    countryCode: 'AG',
  },
  'AR': {
    name: 'Argentina',
    countryCode: 'AR',
  },
  'AM': {
    name: 'Armenia',
    countryCode: 'AM',
  },
  'AW': {
    name: 'Aruba',
    countryCode: 'AW',
  },
  'AU': {
    name: 'Australia',
    countryCode: 'AU',
  },
  'AT': {
    name: 'Austria',
    countryCode: 'AT',
  },
  'AZ': {
    name: 'Azerbaijan',
    countryCode: 'AZ',
  },
  'BS': {
    name: 'Bahamas',
    countryCode: 'BS',
  },
  'BH': {
    name: 'Bahrain',
    countryCode: 'BH',
  },
  'BD': {
    name: 'Bangladesh',
    countryCode: 'BD',
  },
  'BB': {
    name: 'Barbados',
    countryCode: 'BB',
  },
  'BY': {
    name: 'Belarus',
    countryCode: 'BY',
  },
  'BE': {
    name: 'Belgium',
    countryCode: 'BE',
  },
  'BZ': {
    name: 'Belize',
    countryCode: 'BZ',
  },
  'BJ': {
    name: 'Benin',
    countryCode: 'BJ',
  },
  'BM': {
    name: 'Bermuda',
    countryCode: 'BM',
  },
  'BT': {
    name: 'Bhutan',
    countryCode: 'BT',
  },
  'BO': {
    name: 'Bolivia (Plurinational State of)',
    countryCode: 'BO',
  },
  'BQ': {
    name: 'Bonaire, Sint Eustatius and Saba',
    countryCode: 'BQ',
  },
  'BA': {
    name: 'Bosnia and Herzegovina',
    countryCode: 'BA',
  },
  'BW': {
    name: 'Botswana',
    countryCode: 'BW',
  },
  'BV': {
    name: 'Bouvet Island',
    countryCode: 'BV',
  },
  'BR': {
    name: 'Brazil',
    countryCode: 'BR',
  },
  'IO': {
    name: 'British Indian Ocean Territory',
    countryCode: 'IO',
  },
  'BN': {
    name: 'Brunei Darussalam',
    countryCode: 'BN',
  },
  'BG': {
    name: 'Bulgaria',
    countryCode: 'BG',
  },
  'BF': {
    name: 'Burkina Faso',
    countryCode: 'BF',
  },
  'BI': {
    name: 'Burundi',
    countryCode: 'BI',
  },
  'CV': {
    name: 'Cabo Verde',
    countryCode: 'CV',
  },
  'KH': {
    name: 'Cambodia',
    countryCode: 'KH',
  },
  'CM': {
    name: 'Cameroon',
    countryCode: 'CM',
  },
  'CA': {
    name: 'Canada',
    countryCode: 'CA',
  },
  'KY': {
    name: 'Cayman Islands',
    countryCode: 'KY',
  },
  'CF': {
    name: 'Central African Republic',
    countryCode: 'CF',
  },
  'TD': {
    name: 'Chad',
    countryCode: 'TD',
  },
  'CL': {
    name: 'Chile',
    countryCode: 'CL',
  },
  'CN': {
    name: 'China',
    countryCode: 'CN',
  },
  'CX': {
    name: 'Christmas Island',
    countryCode: 'CX',
  },
  'CC': {
    name: 'Cocos (Keeling) Islands',
    countryCode: 'CC',
  },
  'CO': {
    name: 'Colombia',
    countryCode: 'CO',
  },
  'KM': {
    name: 'Comoros',
    countryCode: 'KM',
  },
  'CG': {
    name: 'Congo',
    countryCode: 'CG',
  },
  'CD': {
    name: 'Congo, Democratic Republic of the',
    countryCode: 'CD',
  },
  'CK': {
    name: 'Cook Islands',
    countryCode: 'CK',
  },
  'CR': {
    name: 'Costa Rica',
    countryCode: 'CR',
  },
  'CI': {
    name: "Côte d'Ivoire",
    countryCode: 'CI',
  },
  'HR': {
    name: 'Croatia',
    countryCode: 'HR',
  },
  'CU': {
    name: 'Cuba',
    countryCode: 'CU',
  },
  'CW': {
    name: 'Curaçao',
    countryCode: 'CW',
  },
  'CY': {
    name: 'Cyprus',
    countryCode: 'CY',
  },
  'CZ': {
    name: 'Czech Republic',
    countryCode: 'CZ',
  },
  'DK': {
    name: 'Denmark',
    countryCode: 'DK',
  },
  'DJ': {
    name: 'Djibouti',
    countryCode: 'DJ',
  },
  'DM': {
    name: 'Dominica',
    countryCode: 'DM',
  },
  'DO': {
    name: 'Dominican Republic',
    countryCode: 'DO',
  },
  'EC': {
    name: 'Ecuador',
    countryCode: 'EC',
  },
  'EG': {
    name: 'Egypt',
    countryCode: 'EG',
  },
  'SV': {
    name: 'El Salvador',
    countryCode: 'SV',
  },
  'GQ': {
    name: 'Equatorial Guinea',
    countryCode: 'GQ',
  },
  'ER': {
    name: 'Eritrea',
    countryCode: 'ER',
  },
  'EE': {
    name: 'Estonia',
    countryCode: 'EE',
  },
  'SZ': {
    name: 'Eswatini',
    countryCode: 'SZ',
  },
  'ET': {
    name: 'Ethiopia',
    countryCode: 'ET',
  },
  'FK': {
    name: 'Falkland Islands (Malvinas)',
    countryCode: 'FK',
  },
  'FO': {
    name: 'Faroe Islands',
    countryCode: 'FO',
  },
  'FJ': {
    name: 'Fiji',
    countryCode: 'FJ',
  },
  'FI': {
    name: 'Finland',
    countryCode: 'FI',
  },
  'FR': {
    name: 'France',
    countryCode: 'FR',
  },
  'GF': {
    name: 'French Guiana',
    countryCode: 'GF',
  },
  'PF': {
    name: 'French Polynesia',
    countryCode: 'PF',
  },
  'TF': {
    name: 'French Southern Territories',
    countryCode: 'TF',
  },
  'GA': {
    name: 'Gabon',
    countryCode: 'GA',
  },
  'GM': {
    name: 'Gambia',
    countryCode: 'GM',
  },
  'GE': {
    name: 'Georgia',
    countryCode: 'GE',
  },
  'DE': {
    name: 'Germany',
    countryCode: 'DE',
  },
  'GH': {
    name: 'Ghana',
    countryCode: 'GH',
  },
  'GI': {
    name: 'Gibraltar',
    countryCode: 'GI',
  },
  'GR': {
    name: 'Greece',
    countryCode: 'GR',
  },
  'GL': {
    name: 'Greenland',
    countryCode: 'GL',
  },
  'GD': {
    name: 'Grenada',
    countryCode: 'GD',
  },
  'GP': {
    name: 'Guadeloupe',
    countryCode: 'GP',
  },
  'GU': {
    name: 'Guam',
    countryCode: 'GU',
  },
  'GT': {
    name: 'Guatemala',
    countryCode: 'GT',
  },
  'GG': {
    name: 'Guernsey',
    countryCode: 'GG',
  },
  'GN': {
    name: 'Guinea',
    countryCode: 'GN',
  },
  'GW': {
    name: 'Guinea-Bissau',
    countryCode: 'GW',
  },
  'GY': {
    name: 'Guyana',
    countryCode: 'GY',
  },
  'HT': {
    name: 'Haiti',
    countryCode: 'HT',
  },
  'HM': {
    name: 'Heard Island and McDonald Islands',
    countryCode: 'HM',
  },
  'VA': {
    name: 'Holy See',
    countryCode: 'VA',
  },
  'HN': {
    name: 'Honduras',
    countryCode: 'HN',
  },
  'HK': {
    name: 'Hong Kong',
    countryCode: 'HK',
  },
  'HU': {
    name: 'Hungary',
    countryCode: 'HU',
  },
  'IS': {
    name: 'Iceland',
    countryCode: 'IS',
  },
  'IN': {
    name: 'India',
    countryCode: 'IN',
  },
  'ID': {
    name: 'Indonesia',
    countryCode: 'ID',
  },
  'IR': {
    name: 'Iran (Islamic Republic of)',
    countryCode: 'IR',
  },
  'IQ': {
    name: 'Iraq',
    countryCode: 'IQ',
  },
  'IE': {
    name: 'Ireland',
    countryCode: 'IE',
  },
  'IM': {
    name: 'Isle of Man',
    countryCode: 'IM',
  },
  'IL': {
    name: 'Israel',
    countryCode: 'IL',
  },
  'IT': {
    name: 'Italy',
    countryCode: 'IT',
  },
  'JM': {
    name: 'Jamaica',
    countryCode: 'JM',
  },
  'JP': {
    name: 'Japan',
    countryCode: 'JP',
  },
  'JE': {
    name: 'Jersey',
    countryCode: 'JE',
  },
  'JO': {
    name: 'Jordan',
    countryCode: 'JO',
  },
  'KZ': {
    name: 'Kazakhstan',
    countryCode: 'KZ',
  },
  'KE': {
    name: 'Kenya',
    countryCode: 'KE',
  },
  'KI': {
    name: 'Kiribati',
    countryCode: 'KI',
  },
  'KP': {
    name: "Korea (Democratic People's Republic of)",
    countryCode: 'KP',
  },
  'KR': {
    name: 'Korea, Republic of',
    countryCode: 'KR',
  },
  'XK': {
    name: 'Kosovo',
    countryCode: 'XK',
  },
  'KW': {
    name: 'Kuwait',
    countryCode: 'KW',
  },
  'KG': {
    name: 'Kyrgyzstan',
    countryCode: 'KG',
  },
  'LA': {
    name: "Lao People's Democratic Republic",
    countryCode: 'LA',
  },
  'LV': {
    name: 'Latvia',
    countryCode: 'LV',
  },
  'LB': {
    name: 'Lebanon',
    countryCode: 'LB',
  },
  'LS': {
    name: 'Lesotho',
    countryCode: 'LS',
  },
  'LR': {
    name: 'Liberia',
    countryCode: 'LR',
  },
  'LY': {
    name: 'Libya',
    countryCode: 'LY',
  },
  'LI': {
    name: 'Liechtenstein',
    countryCode: 'LI',
  },
  'LT': {
    name: 'Lithuania',
    countryCode: 'LT',
  },
  'LU': {
    name: 'Luxembourg',
    countryCode: 'LU',
  },
  'MO': {
    name: 'Macao',
    countryCode: 'MO',
  },
  'MG': {
    name: 'Madagascar',
    countryCode: 'MG',
  },
  'MW': {
    name: 'Malawi',
    countryCode: 'MW',
  },
  'MY': {
    name: 'Malaysia',
    countryCode: 'MY',
  },
  'MV': {
    name: 'Maldives',
    countryCode: 'MV',
  },
  'ML': {
    name: 'Mali',
    countryCode: 'ML',
  },
  'MT': {
    name: 'Malta',
    countryCode: 'MT',
  },
  'MH': {
    name: 'Marshall Islands',
    countryCode: 'MH',
  },
  'MQ': {
    name: 'Martinique',
    countryCode: 'MQ',
  },
  'MR': {
    name: 'Mauritania',
    countryCode: 'MR',
  },
  'MU': {
    name: 'Mauritius',
    countryCode: 'MU',
  },
  'YT': {
    name: 'Mayotte',
    countryCode: 'YT',
  },
  'MX': {
    name: 'Mexico',
    countryCode: 'MX',
  },
  'FM': {
    name: 'Micronesia (Federated States of)',
    countryCode: 'FM',
  },
  'MD': {
    name: 'Moldova, Republic of',
    countryCode: 'MD',
  },
  'MC': {
    name: 'Monaco',
    countryCode: 'MC',
  },
  'MN': {
    name: 'Mongolia',
    countryCode: 'MN',
  },
  'ME': {
    name: 'Montenegro',
    countryCode: 'ME',
  },
  'MS': {
    name: 'Montserrat',
    countryCode: 'MS',
  },
  'MA': {
    name: 'Morocco',
    countryCode: 'MA',
  },
  'MZ': {
    name: 'Mozambique',
    countryCode: 'MZ',
  },
  'MM': {
    name: 'Myanmar',
    countryCode: 'MM',
  },
  'NA': {
    name: 'Namibia',
    countryCode: 'NA',
  },
  'NR': {
    name: 'Nauru',
    countryCode: 'NR',
  },
  'NP': {
    name: 'Nepal',
    countryCode: 'NP',
  },
  'NL': {
    name: 'Netherlands',
    countryCode: 'NL',
  },
  'NC': {
    name: 'New Caledonia',
    countryCode: 'NC',
  },
  'NZ': {
    name: 'New Zealand',
    countryCode: 'NZ',
  },
  'NI': {
    name: 'Nicaragua',
    countryCode: 'NI',
  },
  'NE': {
    name: 'Niger',
    countryCode: 'NE',
  },
  'NG': {
    name: 'Nigeria',
    countryCode: 'NG',
  },
  'NU': {
    name: 'Niue',
    countryCode: 'NU',
  },
  'NF': {
    name: 'Norfolk Island',
    countryCode: 'NF',
  },
  'MK': {
    name: 'North Macedonia',
    countryCode: 'MK',
  },
  'MP': {
    name: 'Northern Mariana Islands',
    countryCode: 'MP',
  },
  'NO': {
    name: 'Norway',
    countryCode: 'NO',
  },
  'OM': {
    name: 'Oman',
    countryCode: 'OM',
  },
  'PK': {
    name: 'Pakistan',
    countryCode: 'PK',
  },
  'PW': {
    name: 'Palau',
    countryCode: 'PW',
  },
  'PS': {
    name: 'Palestine, State of',
    countryCode: 'PS',
  },
  'PA': {
    name: 'Panama',
    countryCode: 'PA',
  },
  'PG': {
    name: 'Papua New Guinea',
    countryCode: 'PG',
  },
  'PY': {
    name: 'Paraguay',
    countryCode: 'PY',
  },
  'PE': {
    name: 'Peru',
    countryCode: 'PE',
  },
  'PH': {
    name: 'Philippines',
    countryCode: 'PH',
  },
  'PN': {
    name: 'Pitcairn',
    countryCode: 'PN',
  },
  'PL': {
    name: 'Poland',
    countryCode: 'PL',
  },
  'PT': {
    name: 'Portugal',
    countryCode: 'PT',
  },
  'PR': {
    name: 'Puerto Rico',
    countryCode: 'PR',
  },
  'QA': {
    name: 'Qatar',
    countryCode: 'QA',
  },
  'RE': {
    name: 'Réunion',
    countryCode: 'RE',
  },
  'RO': {
    name: 'Romania',
    countryCode: 'RO',
  },
  'RU': {
    name: 'Russia',
    countryCode: 'RU',
  },
  'RW': {
    name: 'Rwanda',
    countryCode: 'RW',
  },
  'BL': {
    name: 'Saint Barthélemy',
    countryCode: 'BL',
  },
  'SH': {
    name: 'Saint Helena, Ascension and Tristan da Cunha',
    countryCode: 'SH',
  },
  'KN': {
    name: 'Saint Kitts and Nevis',
    countryCode: 'KN',
  },
  'LC': {
    name: 'Saint Lucia',
    countryCode: 'LC',
  },
  'MF': {
    name: 'Saint Martin (French part)',
    countryCode: 'MF',
  },
  'PM': {
    name: 'Saint Pierre and Miquelon',
    countryCode: 'PM',
  },
  'VC': {
    name: 'Saint Vincent and the Grenadines',
    countryCode: 'VC',
  },
  'WS': {
    name: 'Samoa',
    countryCode: 'WS',
  },
  'SM': {
    name: 'San Marino',
    countryCode: 'SM',
  },
  'ST': {
    name: 'Sao Tome and Principe',
    countryCode: 'ST',
  },
  'SA': {
    name: 'Saudi Arabia',
    countryCode: 'SA',
  },
  'SN': {
    name: 'Senegal',
    countryCode: 'SN',
  },
  'RS': {
    name: 'Serbia',
    countryCode: 'RS',
  },
  'SC': {
    name: 'Seychelles',
    countryCode: 'SC',
  },
  'SL': {
    name: 'Sierra Leone',
    countryCode: 'SL',
  },
  'SG': {
    name: 'Singapore',
    countryCode: 'SG',
  },
  'SX': {
    name: 'Sint Maarten (Dutch part)',
    countryCode: 'SX',
  },
  'SK': {
    name: 'Slovakia',
    countryCode: 'SK',
  },
  'SI': {
    name: 'Slovenia',
    countryCode: 'SI',
  },
  'SB': {
    name: 'Solomon Islands',
    countryCode: 'SB',
  },
  'SO': {
    name: 'Somalia',
    countryCode: 'SO',
  },
  'ZA': {
    name: 'South Africa',
    countryCode: 'ZA',
  },
  'GS': {
    name: 'South Georgia and the South Sandwich Islands',
    countryCode: 'GS',
  },
  'SS': {
    name: 'South Sudan',
    countryCode: 'SS',
  },
  'ES': {
    name: 'Spain',
    countryCode: 'ES',
  },
  'LK': {
    name: 'Sri Lanka',
    countryCode: 'LK',
  },
  'SD': {
    name: 'Sudan',
    countryCode: 'SD',
  },
  'SR': {
    name: 'Suriname',
    countryCode: 'SR',
  },
  'SJ': {
    name: 'Svalbard and Jan Mayen',
    countryCode: 'SJ',
  },
  'SE': {
    name: 'Sweden',
    countryCode: 'SE',
  },
  'CH': {
    name: 'Switzerland',
    countryCode: 'CH',
  },
  'SY': {
    name: 'Syrian Arab Republic',
    countryCode: 'SY',
  },
  'TW': {
    name: 'Taiwan, Province of China',
    countryCode: 'TW',
  },
  'TJ': {
    name: 'Tajikistan',
    countryCode: 'TJ',
  },
  'TZ': {
    name: 'Tanzania, United Republic of',
    countryCode: 'TZ',
  },
  'TH': {
    name: 'Thailand',
    countryCode: 'TH',
  },
  'TL': {
    name: 'Timor-Leste',
    countryCode: 'TL',
  },
  'TG': {
    name: 'Togo',
    countryCode: 'TG',
  },
  'TK': {
    name: 'Tokelau',
    countryCode: 'TK',
  },
  'TO': {
    name: 'Tonga',
    countryCode: 'TO',
  },
  'TT': {
    name: 'Trinidad and Tobago',
    countryCode: 'TT',
  },
  'TN': {
    name: 'Tunisia',
    countryCode: 'TN',
  },
  'TR': {
    name: 'Turkey',
    countryCode: 'TR',
  },
  'TM': {
    name: 'Turkmenistan',
    countryCode: 'TM',
  },
  'TC': {
    name: 'Turks and Caicos Islands',
    countryCode: 'TC',
  },
  'TV': {
    name: 'Tuvalu',
    countryCode: 'TV',
  },
  'UG': {
    name: 'Uganda',
    countryCode: 'UG',
  },
  'UA': {
    name: 'Ukraine',
    countryCode: 'UA',
  },
  'AE': {
    name: 'United Arab Emirates',
    countryCode: 'AE',
  },
  'UK': {
    name: 'United Kingdom',
    countryCode: 'UK',
  },
  'GB': {
    name: 'United Kingdom',
    countryCode: 'UK',
  },
  'US': {
    name: 'United States of America',
    countryCode: 'US',
  },
  'UM': {
    name: 'United States Minor Outlying Islands',
    countryCode: 'UM',
  },
  'UY': {
    name: 'Uruguay',
    countryCode: 'UY',
  },
  'UZ': {
    name: 'Uzbekistan',
    countryCode: 'UZ',
  },
  'VU': {
    name: 'Vanuatu',
    countryCode: 'VU',
  },
  'VE': {
    name: 'Venezuela (Bolivarian Republic of)',
    countryCode: 'VE',
  },
  'VN': {
    name: 'Viet Nam',
    countryCode: 'VN',
  },
  'VG': {
    name: 'Virgin Islands (British)',
    countryCode: 'VG',
  },
  'VI': {
    name: 'Virgin Islands (U.S.)',
    countryCode: 'VI',
  },
  'WF': {
    name: 'Wallis and Futuna',
    countryCode: 'WF',
  },
  'EH': {
    name: 'Western Sahara',
    countryCode: 'EH',
  },
  'YE': {
    name: 'Yemen',
    countryCode: 'YE',
  },
  'ZM': {
    name: 'Zambia',
    countryCode: 'ZM',
  },
  'ZW': {
    name: 'Zimbabwe',
    countryCode: 'ZW',
  },
}

const languageMap = {
  English: {
    langId: '44',
    lang: 'en',
  },
  Français: {
    langId: '-2',
    lang: 'fr',
  },
  Deutsch: {
    langId: '-3',
    lang: 'de',
  },
  Italiano: {
    langId: '-4',
    lang: 'it',
  },
  Español: {
    langId: '-5',
    lang: 'es',
  },
  'ру́сский язы́к': {
    langId: '-20',
    lang: 'ru',
  },
  Polski: {
    langId: '-22',
    lang: 'pl',
  },
  Nederlands: {
    langId: '31',
    lang: 'nl',
  },
  Vlaams: {
    langId: '31',
    lang: 'nl',
  },
}

const langIdMap = {
  ie: '44',
  uk: '44',
  en: '44',
  nl: '31',
  fr: '-2',
  de: '-3',
  ch: '-3',
  it: '-4',
  es: '-5',
  ru: '-20',
  pl: '-22',
}

const localeMap = {
  44: 'en_GB',
  31: 'nl_NL',
  '-2': 'fr_FR',
  '-3': 'de_DE',
  '-4': 'it_IT',
  '-5': 'es_ES',
  '-20': 'ru_RU',
  '-22': 'pl_PL',
}

const countryCodeFromName = (name) => {
  const country = countryLookup[name]
  if (country) return country.countryCode
  return undefined
}

const nameFromCountryCode = (countryCode) => {
  const country = countryCodeLookup[countryCode]
  if (country) return country.name
  return undefined
}

const langIdFromLanguageId = (languageId) => {
  const language = languageMap[languageId]
  if (language) return language.langId
  return undefined
}

const languageFromLanguageId = (languageId) => {
  const language = languageMap[languageId]
  if (language) return language.lang
  return undefined
}

const langIdFromCountryCode = (language) => {
  const langId = langIdMap[language]
  if (langId) return langId
  return undefined
}

const localeFromLangId = (langId) => {
  const locale = localeMap[langId]
  if (locale) return locale
  return undefined
}

module.exports = {
  countryCodeFromName,
  nameFromCountryCode,
  langIdFromLanguageId,
  languageFromLanguageId,
  langIdFromCountryCode,
  localeFromLangId,
}
