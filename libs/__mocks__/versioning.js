/* global jest */

module.exports = {
  clientVersionRequestMiddleware: jest.fn(async (ctx, next) => {
    if (ctx.request && ctx.request.header && ctx.request.header['accept-version']) {
      ctx.state.acceptVersion = ctx.request.header['accept-version']
    } else {
      ctx.state.acceptVersion = '2'
    }
    await next()
  }),
  versionCheck: jest.fn((r = '1', a = ['1']) => {
    return a.indexOf(r) !== -1
  }),
}
