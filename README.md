# Tommy X

This is NOT a complete documentation. It is a location to drop some documentation as it gets available.

## Orders API

### Create order and adding items

Adding the first item to an order will create the order as well. No extra call is neccesasy to create the order.

The call shouuld look like this:

```
POST :HOSTNAME/COUNTRY_ID/STORE_ID/orders/item
Content-Type: application/json
Authorization: Bearer :TOKEN

{
  "productId": "PRODUCT_ID",
	"quantity": "QUANTITY"
}

The response will look like this:

{
  "data": {
    "orderItem": [
      {
        "orderItemId": "1820046"
      }
    ],
    "orderId": "172030106026"
  },
  "success": true
}
```

Save the orderId, you will need it later.

The response will also contain two headers:
```
Set-Cookie: SESSION_ID=eyJTRVNTSU9OX0lEIjoiOGE1OGE2ODctNDAzZS00ODIxLTkxZGItMTc3MzRhOGFkZWY5IiwiX2V4cGlyZSI6MTUwNjUxMDQxNzc4MCwiX21heEFnZSI6ODY0MDAwMDB9; path=/; httponly
Set-Cookie: SESSION_ID.sig=ar2PcTIXJoDgojvF4y_hJRII2y0; path=/; httponly
```

In any subsequent request you should add these to headers, without the Set- at the beginning:
```
Cookie: SESSION_ID=eyJTRVNTSU9OX0lEIjoiOGE1OGE2ODctNDAzZS00ODIxLTkxZGItMTc3MzRhOGFkZWY5IiwiX2V4cGlyZSI6MTUwNjUxMDQxNzc4MCwiX21heEFnZSI6ODY0MDAwMDB9; path=/; httponly
Cookie: SESSION_ID.sig=ar2PcTIXJoDgojvF4y_hJRII2y0; path=/; httponly
```

To add an item to the cart you created you should add the orderId to the previous request. Adding an item will then look like this:

```
POST :HOSTNAME/COUNTRY_ID/STORE_ID/orders/item
Cookie: SESSION_ID=eyJTRVNTSU9OX0lEIjoiOGE1OGE2ODctNDAzZS00ODIxLTkxZGItMTc3MzRhOGFkZWY5IiwiX2V4cGlyZSI6MTUwNjUxMDQxNzc4MCwiX21heEFnZSI6ODY0MDAwMDB9; path=/; httponly
Cookie: SESSION_ID.sig=ar2PcTIXJoDgojvF4y_hJRII2y0; path=/; httponly
Content-Type: application/json
Authorization: Bearer :TOKEN

{
  "orderId": "ORDER_ID"
  "productId": "PRODUCT_ID",
	"quantity": "QUANTITY"
}
```

### Get order info

To get info about the order you can use a get reuqest:

```
GET :HOSTNAME/COUNTRY_ID/STORE_ID/orders/ORDER_ID
Cookie: SESSION_ID=eyJTRVNTSU9OX0lEIjoiOGE1OGE2ODctNDAzZS00ODIxLTkxZGItMTc3MzRhOGFkZWY5IiwiX2V4cGlyZSI6MTUwNjUxMDQxNzc4MCwiX21heEFnZSI6ODY0MDAwMDB9; path=/; httponly
Cookie: SESSION_ID.sig=ar2PcTIXJoDgojvF4y_hJRII2y0; path=/; httponly
Authorization: Bearer :TOKEN
```

The response looks like this:

```
{
  "data": {
    "items": [
      {
        "quantity": "1.0",
        "price": "29.9",
        "color": "BLU",
        "size": "OS",
        "name": "Pochet van zijde met print",
        "productId": "3074457345616726105"
      },
      {
        "quantity": "1.0",
        "price": "99.9",
        "color": "BLU",
        "size": "44",
        "name": "Overhemd van dobby katoen",
        "productId": "3074457345616726084"
      }
    ],
    "orderId": "1"
  },
  "success": true
}
```

### Finalize order

To add an address and/or transaction data to the order send a patch request:

```
PATCH :HOSTNAME/NL/STORE_ID/orders/ORDER_ID
Cookie: SESSION_ID=eyJTRVNTSU9OX0lEIjoiMTY5MTUzYjgtZWRjMy00ZTYyLThlZTYtMDZmM2Y0ZTJjNDk4IiwiX2V4cGlyZSI6MTUwNjUyMTQ5MTA2MiwiX21heEFnZSI6ODY0MDAwMDB9; path=/; httponly
Cookie: SESSION_ID.sig=741ClSavd0F9XrSQ2u1KSQrhHlM; path=/; httponly
content-type: application/json
Authorization: Bearer :TOKEN
```

The address payload should look like this:
```
{
  "address": {
    "nickName": "Mad developer",
    "firstName": "Niels",
    "lastName": "Vegter",
    "addressType": "ShippingAndBilling",
    "zipCode": "1011 HB",
    "addressLine": [
      "Sint Antoniesbreestraat 68"
    ],
    "city": "Amsterdam",
    "state": "Asmterdam",
    "country": "Netherlands",
    "email1": "niels@denes.nl",
    "phone1": "1234567890"
  },
  "transaction": "MOET NOG"
```

Note: nickName must be unique

The transaction payload is something we're still working on.

```
{
  "transaction": {
  }
}
```

Both address and transaction payloads can be combined.
